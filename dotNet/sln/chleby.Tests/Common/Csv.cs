﻿using System;
using chleby.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace chleby.Tests.Common
{
    [TestClass]
    public class Csv
    {
        [TestMethod]
        public void TestMethod1()
        {
            string content = @"Selfridges & Co Ltd,Selfridges,""400, Oxford Street"",,London,W1A 1AB,West End Daily,";
            string[] splited = content.SplitCsv();

            Assert.IsFalse(splited.Length < 0);

        }
    }
}
