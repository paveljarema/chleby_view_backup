﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;

namespace chleby.Tests.Common
{
    [TestClass]
    public class Dates
    {
        [TestMethod]
        public void TimeZoneInfo_Test()
        {
            ReadOnlyCollection<TimeZoneInfo> infos = TimeZoneInfo.GetSystemTimeZones();


            Assert.IsNotNull(infos);


            string ids = string.Join("\n", infos.Select(i => "Timezones.Add(\"" + i.Id + "\", \"" + i.DisplayName + "\");" ));


            Assert.IsNotNull(ids);
        }
    }
}
