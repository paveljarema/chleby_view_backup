﻿using chleby.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using chleby.Web.Other;

namespace chleby.Tests.Snapshots
{
    public class DummySnapshotParameterProvider : ISnapshotParameterProvider
    {
        public DateTime LastSnapshotDate { get; set; }
        public decimal DeliveryVat { get; set; }
        public SnapshoterMode Mode { get; set; }



        public DateTime CurrentDate
        {
            get;
            set;
        }
    }
}
