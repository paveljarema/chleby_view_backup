﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using chleby.Web.Models;
using chleby.Web;
using chleby.Web.Other;
using log4net;
using System.Linq;

namespace chleby.Tests.Snapshots
{
    [TestClass]
    public class SnapshotsTests
    {

        private ChlebyContext _db;
        private DummySnapshotParameterProvider _paramsProvider;
        private AppRegister _appRegister;
        private MainAppContext _chleby;

        [TestInitialize]
        public void Init()
        {
            _chleby = new MainAppContext();

            _appRegister = new AppRegister
            {
                Id = 102,
                Name = "osb",
                DBConnectionString = @"Server=BRO-PC\SQLEXPRESS;Database=chleby_osb;User ID=sa;Password=sa",
                DefaultPTId = 1,
                DefaultTTId = 1,
                DeliveryId = 1
            };

            _db = new ChlebyContext(_appRegister);

            _paramsProvider = new DummySnapshotParameterProvider();

            
        }

        [TestMethod]
        public void SnapshotTest()
        {
            _paramsProvider.Mode = Web.Other.SnapshoterMode.Production;
            _paramsProvider.DeliveryVat = 22;
            _paramsProvider.LastSnapshotDate = new DateTime(2014, 2, 10, 0, 0, 0);
            _paramsProvider.CurrentDate = new DateTime(2014, 2, 11, 0, 0, 0);

            var snapshoter = new Snapshoter(_db, _paramsProvider);
            SnapshotLogger snapshotLogger = new SnapshotLogger();
            ILog snpLog = LogManager.GetLogger("SnapshoterLogger");
            snapshotLogger.Logger = snpLog;
            snapshoter.SnapshotLogger = snapshotLogger;

            snapshoter.Snapshot(_paramsProvider.CurrentDate);
            Business currentClient = _db.Businesses.First();

            AppSnapshots lastSnapshot = _chleby.AppSnapshots.Where(s => s.AppRegisterId == _appRegister.Id).FirstOrDefault();
            if (lastSnapshot == null)
            {
                lastSnapshot = new AppSnapshots
                {
                    AppRegisterId = _appRegister.Id,
                    LastSnapshotDate = DateTime.MinValue,
                    Deadline = currentClient.CraftingDeadline
                };
                _chleby.AppSnapshots.Add(lastSnapshot);
            }
            lastSnapshot.LastSnapshotDate = _paramsProvider.CurrentDate;

           
            currentClient.LastSnapshotDate = _paramsProvider.CurrentDate;
            _db.SaveChanges();
            _chleby.SaveChanges();
        }
    }
}
