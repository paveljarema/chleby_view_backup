﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace chleby.Tests.Convert {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Convert.IConverterService")]
    public interface IConverterService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConverterService/ConvertHtmlToPdf", ReplyAction="http://tempuri.org/IConverterService/ConvertHtmlToPdfResponse")]
        byte[] ConvertHtmlToPdf(string html);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConverterService/ConvertHtmlToPdf", ReplyAction="http://tempuri.org/IConverterService/ConvertHtmlToPdfResponse")]
        System.Threading.Tasks.Task<byte[]> ConvertHtmlToPdfAsync(string html);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConverterService/ConvertHtmlToImage", ReplyAction="http://tempuri.org/IConverterService/ConvertHtmlToImageResponse")]
        byte[] ConvertHtmlToImage(string html);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IConverterService/ConvertHtmlToImage", ReplyAction="http://tempuri.org/IConverterService/ConvertHtmlToImageResponse")]
        System.Threading.Tasks.Task<byte[]> ConvertHtmlToImageAsync(string html);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IConverterServiceChannel : chleby.Tests.Convert.IConverterService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ConverterServiceClient : System.ServiceModel.ClientBase<chleby.Tests.Convert.IConverterService>, chleby.Tests.Convert.IConverterService {
        
        public ConverterServiceClient() {
        }
        
        public ConverterServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ConverterServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ConverterServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ConverterServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public byte[] ConvertHtmlToPdf(string html) {
            return base.Channel.ConvertHtmlToPdf(html);
        }
        
        public System.Threading.Tasks.Task<byte[]> ConvertHtmlToPdfAsync(string html) {
            return base.Channel.ConvertHtmlToPdfAsync(html);
        }
        
        public byte[] ConvertHtmlToImage(string html) {
            return base.Channel.ConvertHtmlToImage(html);
        }
        
        public System.Threading.Tasks.Task<byte[]> ConvertHtmlToImageAsync(string html) {
            return base.Channel.ConvertHtmlToImageAsync(html);
        }
    }
}
