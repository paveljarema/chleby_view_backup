﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using chleby.Web.Models;
using chleby.Web.ViewModel;

namespace chleby.Tests.ViewModels
{
    [TestClass]
    public class StandingOrderViewModelTests
    {
        private ChlebyContext _db;
        private ViewModelFactory _factory;
        private MainAppContext _chleby;
        private AppRegister _appRegister;

        [TestInitialize]
        public void Init()
        {
            _chleby = new MainAppContext();

            _appRegister = new AppRegister
            {
                Id = 102,
                Name = "osb",
                DBConnectionString = "Server=BRO-PC\\SQLEXPRESS;Database=chleby_bretzel;User ID=product_user;Password=MAKE50m3$$$",
                DefaultPTId = 1,
                DefaultTTId = 1,
                DeliveryId = 1
            };

            _db = new ChlebyContext(_appRegister);
            _factory = new ViewModelFactory(_db);

        }

        
        [TestMethod]
        public void CreateTest()
        {
            Trader trader = _db.Traders.First(t => t.Id == 7);
            int addressId = trader.Address.First().Id;

            var model = _factory.CreateStandingOrderViewModel(trader.Id, addressId);

            Assert.IsNotNull(model.Trader);
            Assert.AreEqual(trader.Id, model.TraderId);
            Assert.AreEqual(addressId, model.AddressId);
        }

        [TestMethod]
        public void DateCompareTest()
        {
            DateTime d1 = new DateTime(2014, 1, 27, 10, 30, 0);
            DateTime d2 = new DateTime(2014, 1, 27, 18, 30, 0);

            DateTime prod = new DateTime(2014, 1, 28, 17, 30, 0);

            TimeSpan ts1 = prod - d1;
            TimeSpan ts2 = prod - d2;

            int craftingTime = 0;

            Assert.IsTrue(ts1.TotalDays > 0);
            Assert.IsTrue(ts2.TotalDays < 1);
            Assert.IsTrue(ts1.TotalHours > 24);
            Assert.IsTrue(ts2.TotalHours < 24);
            Assert.IsTrue(ts1.TotalDays - (craftingTime + 1) > 0);
            Assert.IsTrue(ts2.TotalDays - (craftingTime + 1) < 0);
        }
    }
}
