﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using MS.Utils;

namespace chleby.Tests.Orders
{
    [TestClass]
    public class OrderTimezones
    {
        [TestMethod]
        public void OrderTimezonesTest()
        {
            int hour = 1;
            int minute = 30;

            var timezone = TimeZoneInfo.FindSystemTimeZoneById("US Mountain Standard Time");
            
            DateTime nowUtc = DateTime.UtcNow.Date;
            nowUtc = nowUtc.AddHours(hour);
            nowUtc = nowUtc.AddMinutes(minute);

            DateTime sundayDate = nowUtc.GetFirstDayOfWeek();
            IDictionary<DayOfWeek, KeyValuePair<DateTime, DateTime>> daysAndDates = new Dictionary<DayOfWeek, KeyValuePair<DateTime, DateTime>>();
            daysAndDates[DayOfWeek.Sunday] = new KeyValuePair<DateTime, DateTime>(sundayDate, TimeZoneInfo.ConvertTimeFromUtc(sundayDate, timezone));
            for (int i = 1; i < 7; i++)
            {
                DateTime nextDay = sundayDate.AddDays(i);
                daysAndDates[(DayOfWeek)i] = new KeyValuePair<DateTime, DateTime>(nextDay, TimeZoneInfo.ConvertTimeFromUtc(nextDay, timezone));
            }

            Assert.AreEqual(7, daysAndDates.Count);


            

        }

       
    }
}
