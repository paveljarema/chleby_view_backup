﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace chleby.Tests.Convert
{
    [TestClass]
    public class ConverterTests
    {
        [TestMethod]
        public void TestPdfCpnver()
        {
            Convert.ConverterServiceClient client = new ConverterServiceClient();

            string html = @"<html><h1>dasdada</h1></html>";

            byte[] pdf = client.ConvertHtmlToPdf(html);

            string pdfFile = Guid.NewGuid().ToString();

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream =
                   new System.IO.FileStream(pdfFile, System.IO.FileMode.Create,
                                            System.IO.FileAccess.Write);
                // Writes a block of bytes to this stream using data from
                // a byte array.
                _FileStream.Write(pdf, 0, pdf.Length);

                // close file stream
                _FileStream.Close();

            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}",
                                  _Exception.ToString());
            }

        }
    }
}
