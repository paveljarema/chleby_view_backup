﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization.Formatters;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace chleby.bgService
{
    public partial class MainService : ServiceBase
    {
        private readonly static ILog _log = LogManager.GetLogger(typeof(MainService));
        private Thread _mailQueueReceiverThread;
        private const string MSMQName = @".\private$\chleby.mail";
        private readonly ManualResetEventSlim _finishEvent = new ManualResetEventSlim(false);
        private const int retryDelaySeconds = 600;

        public MainService()
        {
            InitializeComponent();
        }

        internal void Start(string[] args)
        {
            OnStart(args);
            Console.WriteLine("onstart() called");
            Console.ReadKey();
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("service started");

            _mailQueueReceiverThread = new Thread(MailQueueReceiver);
            _mailQueueReceiverThread.Start();
        }

        private void MailQueueReceiver()
        {
            _log.Info("started mail receiver thread");
            var queue = OpenQueue(MSMQName);

            Message queueMsg = null;
            while (true)
            {
                try
                {
                    queueMsg = queue.Peek(new TimeSpan(0,0,3));
                }
                catch (MessageQueueException) { /*ignore timeouts */ }
                catch (ThreadAbortException) { return; }

                try
                {
                    if (queueMsg != null)
                    {
                        var id = GetScheduledMessageID(queue);
                        if (id != null)
                        {
                            queueMsg = queue.ReceiveById(id);
                            if (queueMsg != null)
                            {
                                var msgObj = queueMsg.Body;
                                if (!(msgObj is common.SerializeableMailMessage))
                                {
                                    _log.WarnFormat("receive message with unknown object, type={0}", msgObj.GetType());
                                    continue;
                                }

                                var mail = ((common.SerializeableMailMessage) msgObj).GetMailMessage();
                                var date = DateTime.FromBinary(BitConverter.ToInt64(queueMsg.Extension, 0));
                                _log.DebugFormat("got message={0}, for={1}", mail, date);

                                TrySendingMail(mail, queueMsg, queue);
                            }
                        }
                    }
                }
                catch (ThreadAbortException) { return; }
                catch (Exception e)
                {
                    _log.Error("exception while processing message", e);
                }

                if (_finishEvent.IsSet)
                {
                    _log.DebugFormat("exiting mail receive thread");
                    break;
                }
            }
        }

        private static void TrySendingMail(MailMessage mail, Message queueMsg, MessageQueue queue)
        {
            Exception cachedException = null;
            bool retry = false;
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Send(mail);
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                cachedException = ex;
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable ||
                        status == SmtpStatusCode.InsufficientStorage)
                    {
                        retry = true;
                    }
                }
            }
            catch (SmtpException ex)
            {
                cachedException = ex;
                if (ex.InnerException != null)
                {
                    if (ex.InnerException is WebException)
                    {
                        WebExceptionStatus status = (ex.InnerException as WebException).Status;
                        if (status == System.Net.WebExceptionStatus.NameResolutionFailure ||
                            status == System.Net.WebExceptionStatus.ConnectFailure)
                        {
                            retry = true;
                        }
                    }
                }
            }
            catch (ThreadAbortException) { return; }
            catch (Exception ex)
            {
                cachedException = ex;
            }
            if (cachedException != null)
            {
                if (retry)
                {
                    if (queueMsg.AppSpecific > 0)
                    {
                        queueMsg.Extension = System.BitConverter.GetBytes(DateTime.UtcNow.
                                                                                   ToUniversalTime()
                                                                                  .AddSeconds(retryDelaySeconds)
                                                                                  .ToBinary());
                        queueMsg.AppSpecific--;
                        queue.Send(queueMsg);
                        _log.InfoFormat("Failed to deliver, attempts left = {0}, exc = {1}", queueMsg.AppSpecific,
                                        cachedException.Message);
                    }
                    else
                    {
                        _log.Error("Failed to deliver, no more attempts.", cachedException);
                    }
                }
                else
                {
                    _log.Error("Failed to deliver, but no use to retry", cachedException);
                }
            }
        }

        protected override void OnStop()
        {
            _log.Info("stopping service, notifying other threads");
            _finishEvent.Set();

            if (!_mailQueueReceiverThread.Join(5000))
            {
                _log.Info("mail queue receiver didnt join, aborting id...");
                _mailQueueReceiverThread.Abort();
                _log.Info("mail queue receiver aborted");
            }
        }

        public MessageQueue OpenQueue(string queueName)
        {
            MessageQueue queue;
            if (!MessageQueue.Exists(queueName))
            {
                queue = MessageQueue.Create(queueName);
                _log.DebugFormat("created queue: {0}", queueName);
            }
            else
            {
                queue = new MessageQueue(queueName);
            }
            queue.Formatter = new BinaryMessageFormatter();
            queue.MessageReadPropertyFilter.SetAll();
            return queue;
        }

        public String GetScheduledMessageID(MessageQueue q)
        {
            DateTime OldestTimestamp = DateTime.MaxValue;
            String OldestMessageID = null;

            using (MessageEnumerator messageEnumerator = q.GetMessageEnumerator2())
            {
                while (messageEnumerator.MoveNext())
                {
                    DateTime ScheduledTime = DateTime.FromBinary(
                       BitConverter.ToInt64(messageEnumerator.Current.Extension, 0));
                    if (ScheduledTime < DateTime.UtcNow) // Take only the proper ones 
                    {
                        if (ScheduledTime < OldestTimestamp)
                        {
                            OldestTimestamp = ScheduledTime;
                            OldestMessageID = messageEnumerator.Current.Id;
                        }
                    }
                }
            }
            return OldestMessageID;
        }
    }
}
