﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using chleby.Web.Resources;


namespace chleby.Web.Models
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Items")]
    [ChlebyMenu("AdminItems")]
    public class ItemController : ChlebyController
    {
        //private chlebyWebContext db = new chlebyWebContext();

        //
        // GET: /Item/
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            var igs = db.ItemGroups.AsNoTracking().ToList();
            ViewBag.fszystkie = igs;
            ViewBag.Price = 0.00m;

            ViewBag.listagrup = igs.Where(i => !i.Items.Any()).ToList();
            ViewBag.AllUnits = db.Units.ToList();
            ViewBag.Attrs = db.AtributeMetas.OrderBy(x => x.Order).ToList();
            ViewBag.usedItems = db.InvoiceRows.Select(x => x.Item_Id).Distinct().ToList();
        }

        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
                {
                    Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                };
            var lista = (List<IGrouping<ItemGroup, Item>>)cvm.Index;
            lista.AddRange(db.ItemGroups.Where(i => !i.Items.Any()).AsEnumerable().
                Select(listag => new FakeGrouping<ItemGroup, Item>(listag)));
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();

            //get item attributes for export
            var exportVM = new ExportItemsViewModel();
            exportVM.Attributes = new List<ItemAttribute>();
            //  static ///////////////////////
            //Vat
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.Live,
                PropertyName = "Live"
            });
            //Vat
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.Vat,
                PropertyName = "Vat"
            });
            //AdditionalInfo
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.Description,
                PropertyName = "AdditionalInfo"
            });
            //BarcodeType
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.BarcodeType,
                PropertyName = "BarcodeType"
            });
            //BarcodeValue
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.BarcodeValue,
                PropertyName = "BarcodeValue"
            });
            //Tags
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.Tags,
                PropertyName = "Tags"
            });
            //Weight
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.Weight,
                PropertyName = "Weight"
            });
            //ProductionTime
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.ProductionTime,
                PropertyName = "CraftingTime"
            });
            //ItemPriceValue
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.ItemPrice,
                PropertyName = "ItemPriceValue"
            });
            //ItemPriceValue
            exportVM.Attributes.Add(new ItemAttribute
            {
                DisplayName = Strings.ItemPrice,
                PropertyName = "ItemPriceValue"
            });

            // custom ////////////////////////
            IList<AttributeMeta> customAttrs = db.AtributeMetas.OrderBy(x => x.Order).ToList();
            foreach (AttributeMeta customAttr in customAttrs)
            {
                exportVM.Attributes.Add(new ItemAttribute
                {
                    DisplayName = customAttr.Name,
                    IsCustom = true
                });
            }

            ViewBag.ExportVM = exportVM;

            return View(cvm);
        }

        public ActionResult AttrEdit(int id)
        {
            var item = new Item
            {
                Attributes = new Collection<AttributeValue>(),
                ItemGroup = db.ItemGroups.Find(id)
            };
            return PartialView("Attrs/AttrEdit", item);
        }

        //
        // POST: /Item/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult CreateGroup(ItemGroup itemgroup, Dictionary<string, string> attrs)
        {
            if (ModelState.IsValid)
            {
                itemgroup.Metas = new Collection<AttributeMeta>();
                if (attrs != null)
                {
                    foreach (var attr in attrs.Where(kvp => kvp.Value == "true"))
                        itemgroup.Metas.Add(db.AtributeMetas.Find(int.Parse(attr.Key)));
                }
                if (db.ItemGroups.Any())
                    itemgroup.Order = db.ItemGroups.Max(x => x.Order) + 1;
                else
                    itemgroup.Order = 0;
                db.ItemGroups.Add(itemgroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
            {
                Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                Create = null,
                Edit = null,
                Edit2 = null,
                Create2 = itemgroup
            };
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
            return View("Index", cvm);
        }

        [NeedWrite]
        [ActionName("Up")]
        public ActionResult Up(int id)
        {
            ItemGroup itemgr = db.ItemGroups.Find(id);
            if (itemgr.Order > 0)
            {
                var order = --itemgr.Order;
                db.ItemGroups.First(g => g.Order == order && g.Id != id).Order++;
                db.Entry(itemgr).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [NeedWrite]
        [ActionName("Down")]
        public ActionResult Down(int id)
        {
            ItemGroup itemgr = db.ItemGroups.Find(id);
            if (db.ItemGroups.Max(x => x.Order) != itemgr.Order)
            {
                itemgr.Order++;
                db.ItemGroups.First(g => g.Order == itemgr.Order && g.Id != id).Order--;
                db.Entry(itemgr).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult Create(Item item, int groupId, Dictionary<string, string> attrs, decimal price)
        {
            ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup> cvm;
            item.ItemGroup = db.ItemGroups.Find(groupId);
            item.Attributes = new Collection<AttributeValue>();

            if (ModelState.IsValid)
            {
                var fn = TrySaveImage(item.ImageFile, "item.ImageFile");
                if (fn != null)
                    item.Image = fn;

                if ((attrs != null && Request.Form["attrs"] == null) || SetAttributes(item, attrs))
                    db.SaveChanges();
                else
                {
                    cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
                    {
                        Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                        Create = item,
                        Create2 = null,
                        Edit = null
                    };
                    cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
                    ViewBag.Price = price;
                    ViewBag.GroupId = groupId;
                    return View("Index", cvm);
                }
                db.Items.Add(item);
                var newprice = new ItemPrice();
                newprice.Value = price;
                newprice.Item = item;
                db.Prices.Add(newprice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
            {
                Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                Create = item,
                Create2 = null,
                Edit = null,
                Edit2 = null
            };
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
            ViewBag.Price = price;
            ViewBag.GroupId = groupId;
            return View("Index", cvm);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult EditGroup(int id = 0)
        {
            ItemGroup itemgroup = db.ItemGroups.Find(id);
            if (itemgroup == null)
            {
                return HttpNotFound();
            }
            return PartialView(itemgroup);
        }

        //
        // POST: /ItemGroups/Edit/5

        [HttpPost]
        [NeedWrite]
        public ActionResult EditGroup(ItemGroup itemgroup, Dictionary<string, string> attrs)
        {
            if (ModelState.IsValid)
            {
                var currentIg = db.ItemGroups.Find(itemgroup.Id);
                db.Entry(currentIg).CurrentValues.SetValues(itemgroup);
                //db.Entry(itemgroup).State = EntityState.Modified;
                db.SaveChanges();

                itemgroup = db.ItemGroups.Include(x => x.Metas).Single(x => x.Id == itemgroup.Id);
                if (itemgroup.Metas != null) itemgroup.Metas.Clear();
                else itemgroup.Metas = new Collection<AttributeMeta>();
                db.SaveChanges();

                itemgroup = db.ItemGroups.Find(itemgroup.Id);
                if (attrs != null)
                {
                    foreach (var attr in attrs.Where(kvp => kvp.Value == "true"))
                        itemgroup.Metas.Add(db.AtributeMetas.Find(int.Parse(attr.Key)));
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
            {
                Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                Create = null,
                Create2 = null,
                Edit = null,
                Edit2 = itemgroup
            };
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
            return View("Index", cvm);
        }
        //
        // GET: /Item/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            ViewBag.Price = item.Prices.OfType<ItemPrice>().First().Value;
            return PartialView(item);
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ToggleLive(int id, bool live)
        {
            var item = db.Items.Find(id);
            if (item == null)
                return Json(new { msg = "wrong id" }, JsonRequestBehavior.AllowGet);
            var livechanged = item.Live != live;
            item.Live = live;

            try
            {
                db.SaveChanges();
                if (livechanged)
                {
                    var addrs =
                        db.OrderPositions.Where(x => x.Item_Id == id).Select(x => x.Order.Address).Distinct().ToList();
                    foreach (var addr in addrs)
                    {
                        var emp = addr.TraderEmployees.FirstOrDefault();
                        if (emp != null)
                        {
                            var mail = Membership.GetUser(emp.ExternalAccountData).Email;
                            new Mail(db, item.Live ? "itemLive" : "itemDead", mail, new { item }).Send();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new { msg = "db error - " + e.Message, value = item.Live }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { msg = "ok", value = live }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Item/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(Item item, Dictionary<string, string> attrs, int wybrany, decimal price = 0)
        {
            ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup> cvm;

            if (price < 0)
            {
                ModelState.AddModelError("price", chleby.Web.Resources.Strings.Controller_ItemController_GeaterThanZero);
            }

            if (ModelState.IsValid)
            {
                var oldlive = db.Items.AsNoTracking().Single(x => x.Id == item.Id).Live;
                var fn = TrySaveImage(item.ImageFile, "ImageFile");
                if (fn != null)
                    item.Image = fn;
                db.Entry(item).State = EntityState.Modified;
                item.ItemGroupId = wybrany;
                db.SaveChanges();
                var newitem = db.Items.Include(x => x.ItemGroup).Include(x => x.Attributes).Include(x => x.Prices).Single(i => i.Id == item.Id);
                newitem.Prices.OfType<ItemPrice>().First().Value = price;
                if (SetAttributes(newitem, attrs))
                {
                    item.Attributes = newitem.Attributes;
                    db.SaveChanges();
                    if (item.Live != oldlive)
                    {
                        var addrs =
                            db.OrderPositions.Where(x => x.Item_Id == item.Id)
                              .Select(x => x.Order.Address)
                              .Distinct()
                              .ToList();
                        foreach (var addr in addrs)
                        {
                            var emp = addr.TraderEmployees.FirstOrDefault();
                            if (emp != null)
                            {
                                var user = Membership.GetUser(emp.ExternalAccountData);
                                if (user != null)
                                {
                                    var mail = user.Email;
                                    new Mail(db, item.Live ? "itemLive" : "itemDead", mail, new { item }).Send();
                                }
                            }
                        }
                    }
                }
                else
                {
                    //item.Attributes = newitem.Attributes;
                    cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
                    {
                        Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                        Create = null,
                        Create2 = null,
                        Edit = newitem,
                        Edit2 = null
                    };
                    cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
                    ViewBag.Price = price;
                    return View("Index", cvm);
                }
                return RedirectToAction("Index");
            }
            item.ItemGroup = db.ItemGroups.Find(item.ItemGroupId);
            if (item.Attributes == null)
                item.Attributes = new Collection<AttributeValue>();
            if (Request.Form["attrs"] != null)
                SetAttributes(item, attrs);
            cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
            {
                Index = db.Items.Include(x => x.ItemGroup).GroupBy(ks => ks.ItemGroup).ToList(),
                Create = null,
                Create2 = null,
                Edit = item,
                Edit2 = null
            };
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
            ViewBag.Price = price;
            return View("Index", cvm);
        }

        private bool SetAttributes(Item item, Dictionary<string, string> attrs)
        {
            var ret = true;
            if (attrs == null) return true;
            foreach (var kvp in attrs)
            {
                var meta = item.ItemGroup.Metas.FirstOrDefault(m => m.Id == int.Parse(kvp.Key));
                if (meta == null) continue;
                if (item.Attributes == null)
                    item.Attributes = new Collection<AttributeValue>();
                var attr = item.Attributes.FirstOrDefault(a => a.MetadataId == meta.Id);
                string value = kvp.Value;
                if (meta.DataType == AttrDataType.SelectMany)
                {
                    value = Request.Form["attrs[" + meta.Id + "]"];
                    if (value == null)
                        value = Request.Form["zattrs[" + meta.Id + "]"];
                }
                else if (meta.DataType == AttrDataType.Int)
                {
                    if (string.IsNullOrEmpty(value))
                        value = "0";
                    else
                    {
                        decimal t = 0;
                        decimal.TryParse(value, out t);
                        value = t.ToString(CultureInfo.InvariantCulture);
                    }
                }
                if (attr == null)
                {
                    attr = new AttributeValue();
                    attr.Metadata = meta;
                    attr.MetadataId = meta.Id;
                    attr.Value = value;
                    ret = ValidateAttribute(attr, meta, ret);
                    item.Attributes.Add(attr);
                }
                else
                {
                    attr.Value = value;
                    ret = ValidateAttribute(attr, meta, ret);
                }
            }
            return ret;
        }

        private bool ValidateAttribute(AttributeValue attr, AttributeMeta meta, bool oldret)
        {
            bool ret = true;
            decimal Itemp;
            DateTime Dtemp;
            var field = "attrs[" + meta.Id + "]";
            switch (meta.DataType)
            {
                case AttrDataType.Int: //DECIMAL TO NOWY INT
                    if (!decimal.TryParse(attr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out Itemp))
                    {
                        ModelState.AddModelError(field, chleby.Web.Resources.Strings.Controller_ItemController_Req_NumericValue);
                        ret = false;
                    }
                    break;
                case AttrDataType.Bool:
                    if (!(attr.Value == "yes" || attr.Value == "no"))
                    {
                        ModelState.AddModelError(field, chleby.Web.Resources.Strings.Controller_ItemController_Attr_Req_YN);
                        ret = false;
                    }
                    break;
                case AttrDataType.Date:
                    if (!DateTime.TryParse(attr.Value, out Dtemp))
                    {
                        ModelState.AddModelError(field, chleby.Web.Resources.Strings.Controller_ItemController_Attr_Req_proper_date);
                        ret = false;
                    }
                    break;
                case AttrDataType.SelectOne:
                    if (!meta.PossibleValues.Split(';').Contains(attr.Value))
                    {
                        ModelState.AddModelError(field, chleby.Web.Resources.Strings.Controller_ItemController_Attr_Req_one);
                        ret = false;
                    }
                    break;
                case AttrDataType.SelectMany:
                    if (attr.Value == "$$-$$")
                    {
                        attr.Value = "(not set)";
                        break;
                    }
                    attr.Value = attr.Value.Replace(",$$-$$", "");
                    if (!attr.Value.Split(',').All(a => meta.PossibleValues.Split(';').Contains(a.Trim())))
                    {
                        ModelState.AddModelError(field, chleby.Web.Resources.Strings.Controller_ItemController_Attr_Req_many);
                        ret = false;
                    }
                    break;
                case AttrDataType.Expr:

                    break;
            }
            return oldret && ret;
        }

        //
        // POST: /Item/Delete/5
        [NeedWrite]
        [ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            Item item = db.Items.Find(id);
            if (db.InvoiceRows.Any(x => x.Item_Id == id))
            {
                throw new Exception("item used in invoices");
            }
            item.Attributes.ToList().ForEach(a => db.AttributeValues.Remove(a));
            item.Prices.ToList().ForEach(price => db.Prices.Remove(price));
            item.Visibility.ToList().ForEach(a => db.VisibleToes.Remove(a));
            db.OrderPositions.Where(i => i.Item.Id == id).ToList().ForEach(x => db.OrderPositions.Remove(x));
            db.SnapshotOrderPositions.Where(i => i.Item.Id == id).ToList().ForEach(x => db.SnapshotOrderPositions.Remove(x));
            db.InvoiceRows.Where(x => x.Item.Id == id).ToList().ForEach(x => x.Item = null);

            var addrs = db.OrderPositions.Where(x => x.Item_Id == id).Select(x => x.Order.Address).Distinct().ToList();
            foreach (var addr in addrs)
            {
                var emp = addr.TraderEmployees.FirstOrDefault();
                if (emp != null)
                {
                    try
                    {
                        var mail = Membership.GetUser(emp.ExternalAccountData).Email;
                        new Mail(db, "itemDead", mail, new {item}).Send();
                    }
                    catch {}
                }
            }

            db.Items.Remove(item);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult BatchEdit(Dictionary<string, string> prop, Dictionary<string, string> zattrs,
            Dictionary<string, bool> zpropset, Dictionary<string, bool> attrset,
            Dictionary<string, bool> batch)
        {
            var attrs = zattrs;
            var oldprop = new Dictionary<string, string>(prop);
            var oldattrs = new Dictionary<string, string>(attrs);
            foreach (var kvp in zpropset)
            {
                if (!kvp.Value)
                    prop.Remove(kvp.Key);
            }
            foreach (var kvp in attrset)
            {
                if (!kvp.Value)
                    attrs.Remove(kvp.Key);
            }

            foreach (var bkv in batch.Where(b => b.Value))
            {
                var iid = int.Parse(bkv.Key);
                var item = db.Items.Find(iid);
                if (!SetAttributes(item, attrs))
                {
                    var cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
                    {
                        Index = db.Items.GroupBy(ks => ks.ItemGroup).ToList(),
                    };
                    cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList();
                    ViewBag.prop = oldprop;
                    ViewBag.attr = oldattrs;
                    ViewBag.batch = batch;
                    ViewBag.attrset = attrset;
                    ViewBag.propset = zpropset;
                    return View("Index", cvm);
                }
                foreach (var pkv in prop)
                {
                    int intval = 0;
                    decimal decval = 0;
                    switch (pkv.Key)
                    {
                        case "LegacyCode":
                            item.LegacyCode = pkv.Value;
                            break;
                        case "BaseUnit":
                            if (!int.TryParse(pkv.Value, out intval))
                                intval = db.Units.First(x => x.Name == "---").Id;
                            item.BaseUnit = db.Units.Find(intval);
                            break;
                        case "Tags":
                            item.Tags = pkv.Value;
                            break;
                        case "BarcodeValue":
                            item.BarcodeValue = pkv.Value;
                            break;
                        case "BarcodeType":
                            item.BarcodeType = pkv.Value;
                            break;
                        case "CraftingTime":
                            int.TryParse(pkv.Value, out intval);
                            item.CraftingTime = intval;
                            break;
                        case "AdditionalInfo":
                            item.AdditionalInfo = pkv.Value;
                            break;
                        case "Vat":
                            decimal.TryParse(pkv.Value, out decval);
                            item.Vat = decval;
                            break;
                        case "MagicPrice":
                            decimal.TryParse(pkv.Value, out decval);
                            item.Prices.OfType<ItemPrice>().First().Value = decval;
                            break;
                    }
                }
            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult DeleteGroup(int id)
        {
            ItemGroup itemgroup = db.ItemGroups.Find(id);
            foreach (var item in itemgroup.Items.ToList())
            {
                Delete(item.Id);
            }

            foreach (var other in db.ItemGroups)
            {
                if (other.Order > itemgroup.Order)
                {
                    other.Order--;
                }
            }
            db.ItemGroups.Remove(itemgroup);
            db.SaveChanges();
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Image(int id)
        {
            if (id == 0)
                return null;
            var file = Server.MapPath("~/img/" + db.Items.Find(id).Image);
            if (System.IO.File.Exists(file))
                return File(file, "application/octet-stream");
            else
                return HttpNotFound();
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Import(int id)
        {
            var ig = db.ItemGroups.Find(id);
            if (ig == null)
                return HttpNotFound();

            return PartialView(ig);
        }

        [HttpPost]
        public ActionResult ImportFile(int id, HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            if (string.IsNullOrWhiteSpace(str))
            {
                _log.DebugFormat("got empty str; size={0} name={1}",file.ContentLength,file.FileName);
            }

            return Import(id, str);
        }

        [HttpPost]
        public ActionResult Import(int id, string csv, bool save = false)
        {
            var ig = db.ItemGroups.Find(id);
            if (ig == null)
                return HttpNotFound();

            ViewBag.csv = csv;

            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var items = new List<Item>();
            var dbitems = db.Items.Select(x => x.Name).ToList();
            foreach (var tline in lines)
            {
                var item = new Item();
                item.ItemGroup = ig;
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 9 + ig.Metas.Count)
                {
                    errors.Add(Tuple.Create(curline, -1, chleby.Web.Resources.Strings.Controller_ItemController_Wrong_Field_Count));
                    continue;
                }

                item.LegacyCode = fields[0];

                if (fields[1] != "yes" && fields[1] != "no")
                    errors.Add(Tuple.Create(curline, 2, "invalid live field (value must be 'yes' or 'no')"));
                item.Live = fields[1] == "yes";

                if (string.IsNullOrWhiteSpace(fields[2]))
                    errors.Add(Tuple.Create(curline, 3, chleby.Web.Resources.Strings.Controller_ItemController_EmptyName));

                item.Name = fields[2].Trim();
                if (items.Any(i => i.Name == item.Name) || dbitems.Contains(item.Name))
                {
                    //errors.Add(Tuple.Create(curline, 3, "name already used"));    
                    //omijamy uzytego po cichu
                    continue;
                }

                decimal price;
                if (!decimal.TryParse(fields[3], NumberStyles.Any, CultureInfo.InvariantCulture, out price))
                    errors.Add(Tuple.Create(curline, 4, "invalid price"));
                if (price < 0)
                    errors.Add(Tuple.Create(curline, 4, "negative price"));
                var itprice = new ItemPrice();
                itprice.Value = price;
                item.Prices = new Collection<Price>();
                item.Prices.Add(itprice);

                decimal vat;
                if (!decimal.TryParse(fields[4], out vat))
                    errors.Add(Tuple.Create(curline, 5, "invalid vat"));
                if (vat < 0)
                    errors.Add(Tuple.Create(curline, 5, "negative vat"));
                item.Vat = vat;

                decimal weight;
                if (decimal.TryParse(fields[5], out weight))
                {
                    item.Weight = weight;
                }

                item.AdditionalInfo = fields[6];

                int time;
                if (!int.TryParse(fields[7], out time))
                    errors.Add(Tuple.Create(curline, 8, "invalid production time"));
                if (time < 0)
                    errors.Add(Tuple.Create(curline, 8, "negative production time"));
                item.CraftingTime = time;

                item.Tags = fields[8];

                var unit = db.Units.First(x => x.Name == "---");
                item.BaseUnit = unit;

                var atcount = 0;
                item.Attributes = new Collection<AttributeValue>();
                foreach (var meta in ig.Metas)
                {
                    atcount++;
                    var attr = new AttributeValue
                        {
                            Metadata = meta,
                            MetadataId = meta.Id,
                            Value = fields[8 + atcount]
                        };
                    var good = ValidateAttribute(attr, meta, true);
                    if (!good)
                        errors.Add(Tuple.Create(curline, 8 + atcount + 1, "invalid " + meta.Name + " value: " + ModelState["attrs[" + meta.Id + "]"].Errors[0].ErrorMessage));
                    item.Attributes.Add(attr);
                }
                items.Add(item);
            }

            if (errors.Count == 0 && save)
            {
                items.ForEach(i => db.Items.Add(i));
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.errors = errors;
            return PartialView("Import", ig);
        }

        //[HttpPost]
        public ActionResult Export()
        {
            ExportItemsViewModel vm = new ExportItemsViewModel();
            return PartialView("Export", vm);
        }

        public ActionResult ExportExecute(ExportItemsViewModel vm)
        {
            switch (MyBusiness.AccSoftware)
            {
                case 0: //qb
                    return HttpNotFound();
                case 1: //xero
                    return XeroExport(vm);
            }
            return HttpNotFound();
        }

        public ActionResult XeroExport(ExportItemsViewModel vm)
        {
            var taxmap = db.XeroTaxMappings.ToDictionary(ks => ks.Tax, vs => vs.Name);
            var csv = new StringBuilder();
            //get selected properties to select
            IEnumerable<string> properties = vm.Attributes.Where(att => att.Selected).Select(att => att.PropertyName);
            csv.AppendLine(string.Join(",", properties));
            //csv.AppendLine(
            //    "Code,Description,PurchasesUnitPrice,PurchasesAccount,PurchasesTaxRate,SalesUnitPrice,SalesAccount,SalesTaxRate");
            foreach (var item in db.Items.ToList())
            {
                if (!taxmap.ContainsKey(item.Vat)) continue;
                //csv.AppendLine(string.Join(",", new string[]
                //    {
                //        "\"" + (string.IsNullOrWhiteSpace(item.LegacyCode) ? item.ItemGroup.Name.SafeLeft(14) + "-" + item.Name.SafeLeft(15) : item.LegacyCode.SafeLeft(30))+ "\"",
                //        "\""+ item.Name + "\"",
                //        "0",
                //        MyBusiness.XeroConfig.PurchaseAccount.ToString(CultureInfo.InvariantCulture),
                //        "\"" + taxmap[item.Vat] + "\"",
                //        item.ItemPriceValue.ToString(CultureInfo.InvariantCulture),
                //        MyBusiness.XeroConfig.SalesAccount.ToString(CultureInfo.InvariantCulture),
                //        "\"" + taxmap[item.Vat] + "\""
                //    }));

                IList<string> values = new List<string>();
                foreach (string propertyName in properties)
                {
                    var val = item.GetType().GetProperty(propertyName).GetValue(item, null);
                    values.Add(val != null ? val.ToString() : string.Empty);
                }
                csv.AppendLine(string.Join(",", values.ToArray()));
            }

            return File(Encoding.UTF8.GetBytes(csv.ToString()), "text/csv", "export-items.csv");
        }

        [NeedWrite]
        public ActionResult DwlTemplate(int id = 0)
        {
            var ig = db.ItemGroups.Find(id);
            if (ig == null) return HttpNotFound();

            return File(Encoding.UTF8.GetBytes(
@"Legacy code,Live*,Name*,Price*,VAT*,Weight,Description,Production Time*,Tags," +
string.Join(",", ig.Metas.Select(x => x.Name + "*"))), "text/csv; charset=utf-8", "Items.csv");
        }
    }
}
