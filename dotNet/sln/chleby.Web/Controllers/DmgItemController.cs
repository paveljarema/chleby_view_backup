﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using chleby.Web.Models;
using System.Web.Routing;
using chleby.Web.Resources;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("DmgItem")]
    public class DmgItemController : ChlebyController
    {
        //
        // GET: /DmgItem/
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_tid == -1)
            {
                RedirectToAction("Index", "Home").ExecuteResult(ControllerContext);
                requestContext.HttpContext.Response.End();
                return;
            }
        }

        [NeedRead]
        public ActionResult Index(string dt, int addrId = 0)
        {
            ViewBag.trader = _tid;

            var date = DateTime.UtcNow.Date;
            if (dt != null)
                date = DateTimeExtensions.FromStringUrl(dt);

            var addrs = db.Traders.Find(_tid).Address;
            if (addrId == 0)
            {
                var addr = addrs.FirstOrDefault();
                if (addr == null)
                    throw new Exception("no addresses");
                addrId = addr.Id;
            }
            else
            {
                var addr = addrs.FirstOrDefault(x => x.Id == addrId);
                if (addr == null)
                    addr = addrs.FirstOrDefault();
                if (addr == null)
                    throw new Exception("no addresses");
                addrId = addr.Id;
            }

            var cutoff = DateTime.UtcNow.Date.AddDays(-14);

            var modelSnapshot = (from pos in db.SnapshotOrderPositions
                                 where pos.EndDate > cutoff && pos.Address_Id == addrId && pos.MadeCount > 0
                                 orderby pos.EndDate descending
                                 select pos.EndDate).Distinct().ToList();
            var modelInvoice = (from ap in db.Invoices
                                from row in ap.Rows
                                from pos in row.DayData
                                where pos.Date > cutoff && ap.Address_Id == addrId && pos.Count > 0
                                orderby pos.Date descending
                                select pos.Date).Distinct().ToList();

            var curDaySnapshot = (from pos in db.SnapshotOrderPositions
                                  where pos.EndDate == date && pos.Address_Id == addrId && pos.Item is Item && pos.MadeCount > 0
                                  select new { pos.Item, pos.MadeCount, pos.Address, pos.Price }).ToList().
                                  Select(x =>
                                      new ComplaintData(date, (Item)x.Item, x.MadeCount, x.Address, x.Price, false)).ToList();

            var curDayInvoice = (from ap in db.Invoices
                                 from row in ap.Rows
                                 where row.Item is Item
                                 from dd in row.DayData
                                 where dd.Date == date && dd.Count > 0 && ap.Address_Id == addrId
                                 select new { dd.Count, row.Item, ap.Address, dd.Price }).ToList().
                                    Select(x =>
                                        new ComplaintData(date, (Item)x.Item, x.Count, x.Address, x.Price, true)
                                ).ToList();
            ViewBag.days = Tuple.Create(modelSnapshot, modelInvoice);
            ViewBag.dmgitems = db.DmgItems.Where(x => x.Address.TraderId == _tid).ToList();

            ViewBag.addrs = addrs;
            ViewBag.addrId = addrId;
            ViewBag.date = date;

            return View(Tuple.Create(curDaySnapshot, curDayInvoice));
        }

        public ActionResult Save(int addrId, DateTime date, Dictionary<int, decimal> di, Dictionary<int, decimal> item,
            Dictionary<int, string> diNotes, Dictionary<int, string> itemNotes)
        {
            var addr = db.Addresses.Find(addrId);
            if (addr == null || addr.TraderId != _tid)
                return HttpNotFound();
            Alerts.Clear();
            if (di != null)
                foreach (var kvp in di)
                {
                    var dmgitem = db.DmgItems.Find(kvp.Key);
                    if (dmgitem == null || dmgitem.AddressId != addrId || dmgitem.Ack) continue;
                    if (dmgitem.Quantity == kvp.Value && dmgitem.Note == diNotes[kvp.Key]) continue;

                    if (kvp.Value == 0)
                    {
                        new Mail(db, "delComplaint", MyBusiness.MailConfirmAddress, new
                        {
                            user = Membership.GetUser().UserName,
                            di = dmgitem
                        }).Send();
                        db.DmgItems.Remove(dmgitem);
                        continue;
                    }

                    var snapshotPosition = (from pos in db.SnapshotOrderPositions
                                where pos.Item_Id == dmgitem.ItemId && pos.Address_Id == addrId && pos.EndDate == date
                                select pos).FirstOrDefault();
                    var invoiceRow = (from row in db.InvoiceRows
                                where row.Text == dmgitem.Item.Name && row.Invoice.DeliveryAddress_Id == addrId
                                from dd in row.DayData
                                where dd.Date == date
                                select row).FirstOrDefault();

                    string editedItem = snapshotPosition != null ? snapshotPosition.Item.Name : (invoiceRow != null ? invoiceRow.Item.Name : null);
                    decimal count = snapshotPosition != null ? snapshotPosition.MadeCount : (invoiceRow != null ? invoiceRow.Count : 0);

                    if (count == 0) continue;

                    if (kvp.Value <= count)
                    {
                        var old = dmgitem.Quantity;
                        var oldnote = dmgitem.Note;
                        dmgitem.Quantity = kvp.Value;
                        dmgitem.Note = diNotes[kvp.Key];

                        new Mail(db, "setComplaint", MyBusiness.MailConfirmAddress, new
                        {
                            user = Membership.GetUser().UserName,
                            di = dmgitem,
                            old,
                            oldnote
                        }).Send();
                    }
                    else if(editedItem != null)
                    {
                        AddAlert(Severity.error, string.Format(Strings.DmgItem_Cant_Return_More_Than_Made, editedItem));
                    }
                }

            if (item != null)
                
                foreach (var kvp in item)
                {
                    if (kvp.Value == 0) continue;
                    var it = db.Items.Find(kvp.Key);
                    if (it == null) continue;

                    //check if reason is set
                    if (itemNotes == null)
                    {
                        AddAlert(Severity.error, string.Format(Strings.DmgItem_Reason_NotSet, it.Name));
                        //return RedirectToAction("Index", new { dt = date.ToStringUrl(), addrId });
                        continue;
                    }

                    var snapshotPosition = (from pos in db.SnapshotOrderPositions
                                where pos.Item_Id == kvp.Key && pos.Address_Id == addrId && pos.EndDate == date
                                select pos).FirstOrDefault();
                    var invoiceRow = (from row in db.InvoiceRows
                                where row.Text == it.Name && row.Invoice.DeliveryAddress_Id == addrId
                                from dd in row.DayData
                                where dd.Date == date
                                select row).FirstOrDefault();

                    string addedItem = snapshotPosition != null ? snapshotPosition.Item.Name : (invoiceRow != null ? invoiceRow.Item.Name : null);
                    decimal count = snapshotPosition != null ? snapshotPosition.MadeCount : (invoiceRow != null ? invoiceRow.Count : 0);

                    if (count == 0) continue;
                    if (count < 0 || kvp.Value > count)
                    {
                        if (addedItem != null)
                        {
                            AddAlert(Severity.error, string.Format(Strings.DmgItem_Cant_Return_More_Than_Made, addedItem));
                        }
                        continue;
                    }




                    var dmgitem = new DmgItem
                    {
                        AddressId = addrId,
                        DeliveryDate = date,
                        RaportDate = DateTime.UtcNow,
                        ItemId = kvp.Key,
                        Item = db.Items.Find(kvp.Key),
                        PricePerItem = it.GetItemPrice(addr.Trader).Value,
                        Quantity = kvp.Value,
                        Note = itemNotes[kvp.Key]
                    };

                    new Mail(db, "newComplaint", MyBusiness.MailConfirmAddress, new
                    {
                        user = Membership.GetUser().UserName,
                        di = dmgitem
                    }).Send();
                    db.DmgItems.Add(dmgitem);
                }
            if (Alerts.Count == 0)
            {
                db.SaveChanges();
            }
            return RedirectToAction("Index", new { dt = date.ToStringUrl(), addrId });
        }
    }
}