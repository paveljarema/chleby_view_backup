﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using chleby.Web.Models;
using System.Web.Routing;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("CHoliday")]
    [ChlebyMenu("TraderCalendar")]
    public class AddressHolidayController : ChlebyController
    {
        [NeedRead]
        public ActionResult Export()
        {
            var trader = db.Traders.Find(_tid);
            var listaadresow = new List<AddressHoliday>();
            foreach ( var adr in trader.Address)
            {
                listaadresow.AddRange(adr.AddressHolidays);
            }
            var list = db.BusinessHolidays.ToList();
            var name = string.Format("{0}_{1}_export_{2}.csv", trader.Name,
                                     "Holiday",
                                     DateTime.Now.ToString("yy-MM-dd-HH-mm"));
            string mergelist = CsvExporter.Export(listaadresow);
            mergelist += CsvExporter.Export(list, false);
            return File(System.Text.Encoding.UTF8.GetBytes(mergelist), "text/plain; charset=utf-8", name);
        }
        //
        // GET: /AddressHoliday/
        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModel<AddressHoliday>
            {
                Index = db.AddressHolidays.Where(x => x.Address.TraderId == _tid).ToList(),
                Create = null,
                Edit = null
            };
            ViewBag.GodHoliday = db.BusinessHolidays.ToList();
            ViewBag.address = db.Addresses.Where(elo => elo.Trader.Id == _tid).ToList();
            var invoices = db.Invoices.Where(x => x.Trader != null && x.Trader.Id == _tid).ToList();
            var invoicesFiltered = new List<Invoice>();
            foreach (Invoice inv in invoices)
            {
                if (!invoicesFiltered.Any(i => i.InvoiceNumber == inv.InvoiceNumber))
                {
                    //get invoice with latrest revision
                    int latestRevison = invoices.Where(i => i.Id == inv.Id).Select(i => i.Revision).Max();
                    if (latestRevison == inv.Revision)
                    {
                        //if latest invoice version is current
                        invoicesFiltered.Add(inv);
                    }
                    else
                    {
                        //find latest invoice version
                        invoicesFiltered.Add(invoices.Where(i => i.Id == inv.Id && i.Revision == latestRevison).First());
                    }
                }
            }
            ViewBag.invoice = invoicesFiltered;
            ViewBag.cartdays =
                db.Orders.OfType<AdhocOrder>().
                   Where(x => x.Address.TraderId == _tid).
                   Select(x => new {x.Address, x.Date}).
                   ToList().Select(x => Tuple.Create(x.Address, x.Date)).ToList();
                
            return View(cvm);
        }

        //
        // GET: /AddressHoliday/Create
        [NeedWrite]
        public ActionResult Create()
        {
            ViewBag.address = db.Addresses.Where(elo => elo.Trader.Id == _tid).ToList();
            return View();
        }

        //
        // POST: /AddressHoliday/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(AddressHoliday addressHoliday, string calendarView, string calendarMonth, string calendarYear, int? wybrany)
        {
            Session["CalendarView"] = calendarView;
            Session["CalendarMonth"] = calendarMonth;
            Session["CalendarYear"] = calendarYear;
            if (ModelState.IsValid)
            {
                if (!wybrany.HasValue)
                {
                    foreach (var temp in db.Addresses.Where(elo => elo.Trader.Id == _tid).ToList())
                    {
                        var adrhol = new AddressHoliday();
                        adrhol.Name = addressHoliday.Name;
                        adrhol.StartDate = addressHoliday.StartDate;
                        adrhol.EndDate = addressHoliday.EndDate;
                        adrhol.Address = temp;
                        db.AddressHolidays.Add(adrhol);
                    }
                }
                else
                {
                    addressHoliday.Address = db.Addresses.Find(wybrany.Value);
                    db.AddressHolidays.Add(addressHoliday);
                }
                Log("create", addressHoliday.Id);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<AddressHoliday>
            {
                Index = db.AddressHolidays.ToList(),
                Create = addressHoliday,
                Edit = null
            };
            ViewBag.GodHoliday = db.BusinessHolidays.ToList();
            ViewBag.address = db.Addresses.Where(elo => elo.Trader.Id == _tid).ToList();
            ViewBag.invoice = db.Invoices.Where(x => x.Trader != null && x.Trader.Id == _tid).ToList();
            ViewBag.cartdays =
                db.Orders.OfType<AdhocOrder>().
                   Where(x => x.Address.TraderId == _tid).
                   Select(x => new { x.Address, x.Date }).
                   ToList().Select(x => Tuple.Create(x.Address, x.Date)).ToList();
               
            return View("Index", cvm);
        }

        //
        // GET: /AddressHoliday/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            AddressHoliday addressholiday = db.AddressHolidays.Find(id);
            if (addressholiday == null)
            {
                return HttpNotFound();
            }
            ViewBag.Action = "Edit";
            ViewBag.address = db.Addresses.Where(elo => elo.Trader.Id == _tid).ToList();
            return PartialView(addressholiday);
        }

        //
        // POST: /AddressHoliday/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(AddressHoliday addressHoliday, string calendarView, string calendarMonth, string calendarYear, int wybrany)
        {
            Session["CalendarView"] = calendarView;
            Session["CalendarMonth"] = calendarMonth;
            Session["CalendarYear"] = calendarYear;
            if (ModelState.IsValid)
            {
                db.Entry(addressHoliday).State = EntityState.Modified;
                db.SaveChanges();
                var oldHoliday = db.AddressHolidays.Include(x=>x.Address).First(x=>x.Id == addressHoliday.Id);
                oldHoliday.Address.AddressHolidays.Remove(oldHoliday);
                var newHoliday = db.Addresses.Include(x => x.AddressHolidays).First(x => x.Id == wybrany);
                newHoliday.AddressHolidays.Add(oldHoliday);
                oldHoliday.Address = newHoliday;
                Log("edit", newHoliday.Id);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<AddressHoliday>
            {
                Index = db.AddressHolidays.ToList(),
                Create = null,
                Edit = addressHoliday
            };

            return View("Index", cvm);
        }

        //
        // GET: /AddressHoliday/Delete/5
        [NeedWrite]
        public ActionResult Delete(int id = 0)
        {
            AddressHoliday addressholiday = db.AddressHolidays.Find(id);
            db.AddressHolidays.Remove(addressholiday);
            Log("delete", addressholiday.Id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}