﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Prices")]
    [ChlebyMenu("Prices")]
    public class PricesController : ChlebyController
    {

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.Traders = db.Traders.ToList();
            ViewBag.TraderTypes = db.TraderTypes.Where(x => x.Id != db.AppConfig.DefaultPTId).ToList();
            ViewBag.TradersMod = db.Prices.OfType<TraderPrice>().Select(x => x.TraderId).Distinct().ToList();
            ViewBag.TraderTypesMod = db.Prices.OfType<TraderTypePrice>().Select(x => x.TraderTypeId).Where(x => x != db.AppConfig.DefaultPTId).Distinct().ToList();
        }

        [NeedRead]
        public ActionResult Index()
        {
            ViewBag.Action = "SetItemPrices";
            var itemlist = db.Items.GroupBy(ks => ks.ItemGroup).ToList().OrderBy(x => x.Key).ToList();
            return View("Index", Tuple.Create(
                db.Traders.ToList(),
                db.TraderTypes.ToList(),
                itemlist));
        }

        [NeedWrite]
        public ActionResult SetItemPrices(Dictionary<int, decimal> prices)
        {
            db.Items.ToList().ForEach(
                item => item.Prices.OfType<ItemPrice>().ToList().ForEach(
                    price => price.Value = prices[item.Id]
                )
            );
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SetTraderPrices(int id)
        {
            ViewBag.Action = "SetTraderPrices";
            ViewBag.Id = id;
            ViewBag.Trader = db.Traders.Find(id);
            var itemlist = db.Items.GroupBy(ks => ks.ItemGroup).ToList().OrderBy(x => x.Key).ToList();
            return View("ItemList", itemlist);
        }

        [HttpPost]
        public ActionResult SetTraderPrices(int id, Dictionary<int, decimal?> prices)
        {
            var tt = new List<Price>();
            foreach (var item in db.Items.ToList())
            {
                if (prices.ContainsKey(item.Id))
                {
                    if (prices[item.Id].HasValue)
                    {
                        var price = item.Prices.OfType<TraderPrice>().FirstOrDefault(x => x.TraderId == id);
                        if (price == null)
                        {
                            price = new TraderPrice();
                            db.Prices.Add(price);
                        }
                        price.Trader = db.Traders.Find(id);
                        price.Item = item;
                        price.Value = prices[item.Id].GetValueOrDefault();
                        tt.Add(price);
                    }
                    else
                    {
                        item.Prices.OfType<TraderPrice>().ToList().ForEach(p => db.Prices.Remove(p));
                    }
                }
            }

            db.SaveChanges();
            return RedirectToAction("SetTraderPrices", new { id });
        }

        public ActionResult SetTraderTypePrices(int id)
        {
            ViewBag.Action = "SetTraderTypePrices";
            ViewBag.Id = id;
            ViewBag.TraderType = db.TraderTypes.Find(id);
            var itemlist = db.Items.GroupBy(ks => ks.ItemGroup).ToList().OrderBy(x => x.Key).ToList();
            return View("ItemList", itemlist);
        }

        [HttpPost]
        public ActionResult SetTraderTypePrices(int id, Dictionary<int, decimal?> prices)
        {
            foreach (var item in db.Items.ToList())
            {
                if (prices[item.Id].HasValue)
                {
                    var price = item.Prices.OfType<TraderTypePrice>().FirstOrDefault(x => x.TraderTypeId == id);
                    if (price == null)
                    {
                        price = new TraderTypePrice();
                        db.Prices.Add(price);
                    }
                    price.TraderType = db.TraderTypes.Find(id);
                    price.Item = item;
                    price.Value = prices[item.Id].GetValueOrDefault();
                }
                else
                {
                    item.Prices.OfType<TraderTypePrice>().Where(x => x.TraderTypeId == id).ToList().ForEach(p => db.Prices.Remove(p));
                }
            }

            ViewBag.Id = id;
            db.SaveChanges();
            return RedirectToAction("SetTraderTypePrices", new { id });
        }

        [NeedWrite]
        public ActionResult SavePrices(int id, Dictionary<int, decimal?> prices)
        {
            SetTraderPrices(id, prices);
            return RedirectToAction("Index", "TraderItems");
        }
    }
}
