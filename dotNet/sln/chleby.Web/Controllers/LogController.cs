﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    [ChlebyModule("Log")]
    [ChlebyMenu("Log")]
    public class LogController : ChlebyController
    {
        [ChlebyMenu("AuditTrail")]
        public ActionResult Index()
        {
            var log = db.AuditLog.OrderByDescending(x => x.Date).ToList();
            return View(log);
        }


    }

}