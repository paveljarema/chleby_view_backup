﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MvcSiteMapProvider;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    public class HomeController : ChlebyController
    {
        public ActionResult Index()
        {
            if (Roles.IsUserInRole(Defines.Roles.Trader))
            {
                return RedirectToAction("Index", "Orders");
            }
            // return View();
            return RedirectToAction("Index", "TurboTraders");
        }

        public ActionResult HelpAndFeedback()
        {

            string mail = User.Identity.Name;

            ViewBag.WhoMail = mail;
            ViewBag.Date = DateTime.Now.Date.ToString("dd/MM/yyyy ddd");
            ViewBag.Time = DateTime.Now.ToString("HH:mm:ss");
            return View();
        }

        [HttpPost]
        public ActionResult HelpAndFeedback(string tresc, string typ)
        {

            string mail = User.Identity.Name;
            string czas = DateTime.Now.Date.ToString("dd/MM/yyyy ddd") + " at " + DateTime.Now.ToString("HH:mm:ss");
            /* Marek Spociński
             * 
             *  Hardcoded e-mail address for in-app help.
             * 
             */
            new Mail(db, "questionHelp", "help@mountainstream.ms", new
                {
                    user = mail,
                    typ = typ,
                    date = czas,
                    tresc = tresc,
                }).Send();
            Session["alerts"] = new List<UIAlert>();
            ((List<UIAlert>)Session["alerts"]).Add(new UIAlert { Status = Severity.success, Msg = chleby.Web.Resources.Strings.Controller_HomeController_Message_Sent });
            return View();
        }



        public ActionResult ApplicationForm()
        {
            return View();
        }
    }
}
