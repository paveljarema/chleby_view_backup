﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Extensibility;

namespace chleby.Web.Controllers
{
    //ModuleCategory: ADMIN
    [ChlebyAuthorize]
    [ChlebyModule(Name = "Employees", Category = "Admin")]
    public class EmployeesController : ChlebyController
    {
        //
        // GET: /Employees/
        public ActionResult Index()
        {

            return View();
        }

        //
        // GET: /Employees/Permissions
        // TODO: FORM action="" is not set
        // TODO: use form helpers in view
        public ActionResult Permissions()
        {
            return View();
        }

        //
        // POST: /Employees/Permissions
        // TODO: SAVE permissions
        [HttpPost]
        public ActionResult Permissions(string WontLetMeBuildWithSameParameters)
        {
            //redirect to Index + Sucess save info
            return null;
        }

        //
        // GET: /Employees/Add
        public ActionResult Add()
        {
            return View();
        }

        //
        // POST: /Employees/Add
        // TODO: ADD user to db
        // TODO: send mail with email inv
        [HttpPost]
        public ActionResult Add(string WontLetMeBuildWithSameParameters)
        {
            //redirect to Index + Sucess add info 
            return null;
        }

        public ActionResult Manage(int id = 0)
        {
            return View("Index");
        }
    }

    public class EmployeeDNP : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection()
        {
            return Enumerable.Range(1, 5).
                Select(x =>
                    new DynamicNode
                        {
                            Title = "Employee " + x,
                            RouteValues = new Dictionary<string, object>() {
                                { "id", x }
                            }
                        });
        }
    }

}
