﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using Newtonsoft.Json;
using chleby.Web.Models;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using chleby.Web.Resources;

namespace chleby.Web.Controllers
{
    public class InternalController : ChlebyController
    {
        //
        // GET: /Internal/

        /*public ActionResult Menu()
        {
            return PartialView(ModuleRegister.GetModuleCategories(AppName));
        }*/

        public ActionResult Switcher(string moduleCategory)
        {
            ViewBag.SwitcherModuleCategory = moduleCategory;

            return PartialView(db.Traders.Where(x => x.Live == 1).ToList());
        }


        public ActionResult SwitcherDone(int adminactor = -1)
        {
            Trader trader = db.Traders.Find(adminactor);

            //friendly redirects
            if (!trader.Employees.Any())
            {
                return RedirectToAction("Index", "TraderEmployees", new { tid = adminactor });
            }

            if (!trader.Address.Any())
            {
                return RedirectToAction("Index", "Address", new { tid = adminactor });
            }

            if (ViewBag.HandwalkStep == 0)
            {
                return RedirectToAction("Index", "Orders", new { tid = adminactor });
            }
            else
            {
                return RedirectToAction("Index", "Address", new { tid = adminactor });
            }

        }

        public ActionResult CreatePrices()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            var list = db.Items.ToList();
            foreach (var item in list)
            {
                ItemPrice price = new ItemPrice();
                price.Item = item;
                price.Value = 0;
                db.Prices.Add(price);
            }
            db.SaveChanges();
            return Content("prices created");

        }


        public ActionResult UpdateRoles()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            return Content(UpdateRolesForApp(AppName));
        }

        public static string UpdateRolesForApp(string app)
        {
            var ret = "";
            var mrs = MvcApplication.ModuleRegister;
            Membership.ApplicationName = "app_" + app;
            Roles.ApplicationName = "app_" + app;
            if (!Roles.RoleExists(Defines.Roles.Trader)) Roles.CreateRole(Defines.Roles.Trader);
            if (!Roles.RoleExists(Defines.Roles.Employee)) Roles.CreateRole(Defines.Roles.Employee);
            if (!Roles.RoleExists(Defines.Roles.SuperAdmin)) Roles.CreateRole(Defines.Roles.SuperAdmin);
            foreach (var mr in mrs)
            {
                try
                {
                    Roles.CreateRole(mr.Name + Defines.Roles.ReadSuffix);
                    ret += mr.Name + Defines.Roles.ReadSuffix + "\n";
                }
                catch
                {
                }
                try
                {
                    Roles.CreateRole(mr.Name + Defines.Roles.WriteSuffix);
                    ret += mr.Name + Defines.Roles.WriteSuffix + "\n";
                }
                catch
                {
                }
                foreach (var sub in mr.SubPerms)
                {
                    try
                    {
                        Roles.CreateRole(Defines.Roles.CustomPrefix + sub.Name);
                        ret += Defines.Roles.CustomPrefix + sub.Name + "\n";
                    }
                    catch
                    {
                    }
                }
            }
            return ret;
        }

        public ActionResult NextDeadline()
        {
            var b = db.Businesses.First();
            var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            dt = dt.AddHours(b.CraftingDeadline.Hour);
            dt = dt.AddMinutes(b.CraftingDeadline.Minute);


            var dtlocal = TimeZoneInfo.ConvertTimeFromUtc(b.CraftingDeadline, Defines.Timezones[b.Timezone]);

            return Content(string.Format("utc: {0}, local: {1}\nutcnow: {2}, localnow: {3}",
                dt, dtlocal, DateTime.UtcNow, DateTime.Now));
        }

        public ActionResult AutoSnapshot(string key)
        {
            if (key != "ywobelhc5zculk4ynzcigam3ynjat2repus1")
            {
                return DebugReport("invalid key");
            }
            var b = db.Businesses.First();
            var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            dt = dt.AddHours(b.CraftingDeadline.Hour);
            dt = dt.AddMinutes(b.CraftingDeadline.Minute);
            //var last = DateTime.ParseExact(db.Settings.First(x => x.Name == "Snapshot_LastDate").Value, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            //var last = DateTime.ParseExact(Settings["Snapshot"]["LastDate"], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            var last = b.LastSnapshotDate ?? DateTime.MinValue;
            var utc = DateTime.UtcNow;
            string ret = "";

            if (last.Date >= dt.Date)
            {
                ret += "snapshot already done at " + last;
            }
            else if (utc > dt)
            {
                ret += "doing snapshot at " + utc;
                new Snapshoter(db, new DefaultSnapshotParameterProvider(db, db.AppConfig.Name)).Snapshot(utc);
            }

            return DebugReport(ret);
        }

        public ActionResult Snapshot(int? day = null)
        {
#if !DEBUG
            return HttpNotFound();
#endif
            if (day.HasValue)
            {
                var dt = DateTime.Now;
                dt = new DateTime(dt.Year, dt.Month, day.GetValueOrDefault());
                db.SaveChanges();
                new Snapshoter(db, new DefaultSnapshotParameterProvider(db, db.AppConfig.Name)).Snapshot(dt);
            }
            return View(db.SnapshotOrderPositions.ToList());
        }

        public ActionResult SnapshotClear()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            db.SnapshotOrderPositions.ToList().ForEach(x => db.SnapshotOrderPositions.Remove(x));
            db.SaveChanges();
            return Content("done");
        }

        public ActionResult CSVTest()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            var list = db.Addresses.ToList();
            return Content(CsvExporter.Export(list), "text/plain");
        }

        public ActionResult CSVTestDwl()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            var list = db.Addresses.ToList();
            var name = string.Format("{0}_{1}_export_{2}.csv", "murzyn",
                                     db.Set(list.FirstOrDefault().GetType()).ElementType.Name,
                                     DateTime.Now.ToString("yy-MM-dd-HH-mm"));
            return File(Encoding.UTF8.GetBytes(CsvExporter.Export(list)), "text/plain; charset=utf-8", name);
        }

        public ActionResult ResetAllPwd()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            Response.ContentType = "text/plain";
            foreach (MembershipUser user in Membership.GetAllUsers())
            {
                if (user.UserName != "mbazaczek@gmail.com")
                {
                    var pass = user.ResetPassword();
                    var te = db.TraderEmployees.First(x => x.ExternalAccountData == (Guid)user.ProviderUserKey);
                    Response.Write(string.Format("{2}\t{3}\t{0}\t{1}\n",
                        user.UserName, pass,
                        te.FirstName + " " + te.LastName, te.Trader.Name));
                }
            }
            return null;
        }

        public ActionResult MegaCSVImport()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            Response.ContentType = "text/plain";
            Response.Write("<pre>");

            var LLdrs = GetCSV("deliveryrounds");
            var LLcts = GetCSV("customertype");
            var LLitems = GetCSV("item");
            var LLaddress = GetCSV("address");
            var LLaddh = GetCSV("addressholiday");
            var LLcontact = GetCSV("contact");
            var LLtrprics = GetCSV("traderprice");
            var LLctprics = GetCSV("typeprices");
            var LLusers = GetCSV("user");
            var LLgroup = GetCSV("dough");

            Response.Write("processing addressgroups\n");
            foreach (var dr in LLdrs)
            {
                var ag = new AddressGroup();
                ag.Name = dr[1];
                ag.Order = int.Parse(dr[2]);
                db.AddressGroups.Add(ag);
                Response.Write(dr[0] + "\n");
            }

            Response.Write("processing tradertypes\n");
            foreach (var t in LLcts)
            {
                var ct = new TraderType();
                ct.Name = t[1];
                db.TraderTypes.Add(ct);
                Response.Write(t[0] + "\n");
            }

            Response.Write("processing users\n");
            foreach (var user in LLusers)
            {
                if (user[2] != "customer") continue;
                var trader = new Trader();
                trader.PrimaryName = string.IsNullOrWhiteSpace(user[3]) ? "empty" + user[0] : user[3];
                trader.Live = user[6] == "1" ? 1 : 0;
                trader.PaymentTerms = db.PaymentTerms.First();
                if (string.IsNullOrWhiteSpace(user[7]) || user[7] == "NULL")
                    trader.Type = db.TraderTypes.Local.First();
                else
                    trader.Type = db.TraderTypes.Local.AsEnumerable().First(x => x.Name == LLcts.First(y => y[0] == user[7])[1]);

                var email = user[13];
                var pass = Membership.GeneratePassword(8, 0);

                var mainte = new TraderEmployee();
                if (string.IsNullOrWhiteSpace(email) || email == "NULL")
                    email = string.Format("user_{0}@legacy.standing-order.com", user[0]);
                Response.Write(string.Format("{0}: {1} {2}\n", user[0], email, pass));
                mainte.FirstName = trader.Name;
                mainte.LastName = "[company]";
                mainte.Trader = trader;

                if (!false)
                {
                    var mbuser = Membership.CreateUser(email, pass, email);
                    Roles.AddUserToRole(email, Defines.Roles.Trader);
                    Roles.AddUserToRole(email, "CEmployees" + Defines.Roles.ReadSuffix);
                    Roles.AddUserToRole(email, "CEmployees" + Defines.Roles.WriteSuffix);
                    Roles.AddUserToRole(email, "Employees" + Defines.Roles.ReadSuffix);
                    Roles.AddUserToRole(email, "Employees" + Defines.Roles.WriteSuffix);
                    mainte.ExternalAccountData = (Guid)mbuser.ProviderUserKey;
                }

                db.TraderEmployees.Add(mainte);
                Response.Write("  processing contacts\n");
                foreach (var contact in LLcontact.Where(x => x[4] == user[0]))
                {
                    var te = new TraderEmployee();
                    pass = Membership.GeneratePassword(8, 0);
                    email = string.Format("contact_{0}@legacy.standing-order.com", contact[0]);
                    Response.Write(string.Format("    {0}: {1} {2}\n", contact[0], email, pass));
                    var names = contact[1].Split(' ');
                    te.FirstName = names[0];
                    te.LastName = names.Length == 2 ? names[1] : "-";
                    te.Phone = contact[3];
                    te.BusinessRole = contact[2] + " - " + contact[5];
                    te.Trader = trader;
                    if (!false)
                    {
                        var mbuser = Membership.CreateUser(email, pass, email);
                        Roles.AddUserToRole(email, Defines.Roles.Trader);
                        te.ExternalAccountData = (Guid)mbuser.ProviderUserKey;
                    }
                    db.TraderEmployees.Add(te);
                }

                Response.Write("  processing addresses\n");
                foreach (var addr in LLaddress.Where(x => x[7] == user[0]))
                {
                    var address = new Address();
                    address.Trader = trader;
                    address.Address1 = string.IsNullOrWhiteSpace(addr[1]) ? "-" : addr[1];
                    address.Address2 = addr[2];
                    address.Address3 = addr[3];
                    address.Post = string.IsNullOrWhiteSpace(addr[4]) ? "-" : addr[4];
                    address.City = "London";
                    address.SpecialNote = addr[5];
                    address.DeliveryPrice = string.IsNullOrWhiteSpace(addr[6]) ? 0.00m : decimal.Parse(addr[6]);
                    //7 = userid
                    address.AddressGroup = addr[8] == "NULL" ? null :
                        db.AddressGroups.Local.AsEnumerable().First(x => x.Name == LLdrs.First(y => y[0] == addr[8])[1]);
                    address.RoundOrder = addr[9] == "NULL" ? 0 : int.Parse(addr[9]);
                    address.LegacyCode = addr[11];
                    db.Addresses.Add(address);
                    Response.Write(string.Format("    {0}\n", addr[1]));
                }
            }

            Response.Write("creating attrs\n");
            db.AtributeMetas.Add(new AttributeMeta
                {
                    DataType = AttrDataType.Int,
                    Name = "weight1"
                });
            db.AtributeMetas.Add(new AttributeMeta
            {
                DataType = AttrDataType.Int,
                Name = "weight2"
            });
            db.AtributeMetas.Add(new AttributeMeta
            {
                DataType = AttrDataType.SelectOne,
                Name = "trays",
                PossibleValues = "None;S;M;L"
            });
            db.AtributeMetas.Add(new AttributeMeta
            {
                DataType = AttrDataType.Int,
                Name = "starter_amount"
            });
            db.AtributeMetas.Add(new AttributeMeta
            {
                DataType = AttrDataType.SelectOne,
                Name = "pack_order",
                PossibleValues = "None;Sliced;Bag Up;Focaccia&Pizza"
            });

            Response.Write("processing groups\n");
            foreach (var group in LLgroup)
            {
                var gr = new ItemGroup();
                gr.Name = group[1];
                gr.Order = int.Parse(group[3]);
                gr.Metas = db.AtributeMetas.Local.ToList();
                db.ItemGroups.Add(gr);
            }

            Response.Write("processing items\n");
            foreach (var item in LLitems)
            {
                var it = new Item();
                it.Attributes = new Collection<AttributeValue>();
                it.Name = item[1];
                it.Attributes.Add(new AttributeValue
                    {
                        Metadata = db.AtributeMetas.Local.First(x => x.Name == "weight1"),
                        Value = item[2]
                    });
                it.Attributes.Add(new AttributeValue
                {
                    Metadata = db.AtributeMetas.Local.First(x => x.Name == "weight2"),
                    Value = item[3]
                });
                it.Vat = decimal.Parse(item[4]);
                if (string.IsNullOrWhiteSpace(item[5])) item[5] = "0";
                it.Attributes.Add(new AttributeValue
                {
                    Metadata = db.AtributeMetas.Local.First(x => x.Name == "pack_order"),
                    Value = db.AtributeMetas.Local.First(x => x.Name == "pack_order").PossibleValues.Split(';')[int.Parse(item[5])]
                });
                if (string.IsNullOrWhiteSpace(item[6])) item[6] = "0" + item[6];
                it.Attributes.Add(new AttributeValue
                {
                    Metadata = db.AtributeMetas.Local.First(x => x.Name == "starter_amount"),
                    Value = item[6]
                });
                it.Attributes.Add(new AttributeValue
                {
                    Metadata = db.AtributeMetas.Local.First(x => x.Name == "trays"),
                    Value = db.AtributeMetas.Local.First(x => x.Name == "trays").PossibleValues.Split(';')[int.Parse(item[7])]
                });
                it.CraftingTime = int.Parse(item[8]) + int.Parse(item[9]);
                it.ItemGroup = db.ItemGroups.Local.AsEnumerable().First(x => x.Name == LLgroup.First(y => y[0] == item[10])[1]);
                it.Live = item[11] == "1";
                it.LegacyCode = item[12];
                it.BaseUnit = db.Units.First(x => x.Name == "---");
                db.Items.Add(it);
            }

            Response.Write("processing trader prices\n");
            foreach (var trpr in LLtrprics)
            {
                if (trpr[2] == "NULL")
                {
                    Response.Write(string.Format("  {0}: item=null\n", trpr[0]));
                    continue;
                }
                if (trpr[3] == "NULL")
                {
                    Response.Write(string.Format("  {0}: user=null\n", trpr[0]));
                    continue;
                }

                var trprice = new TraderPrice();
                trprice.Trader = db.Traders.Local.ToList().First(x => x.Name == LLusers.First(y => y[0] == trpr[3])[3]);
                trprice.Item = db.Items.Local.AsEnumerable().First(x => x.Name == LLitems.First(y => y[0] == trpr[2])[1]);
                trprice.Value = decimal.Parse(trpr[1]);
                Response.Write(string.Format("  {0} {1} {2}\n", trprice.Item.Name, trprice.Trader.Name, trprice.Value));
                db.Prices.Add(trprice);
            }

            Response.Write("processing type prices\n");
            foreach (var ctpr in LLctprics)
            {
                if (ctpr[2] == "NULL")
                {
                    Response.Write(string.Format("  {0}: item=null\n", ctpr[0]));
                    continue;
                }
                if (ctpr[3] == "NULL")
                {
                    Response.Write(string.Format("  {0}: type=null\n", ctpr[0]));
                    continue;
                }
                if (ctpr[1] == "NULL")
                {
                    Response.Write(string.Format("  {0}: price=null\n", ctpr[0]));
                    continue;
                }

                var ttrprice = new TraderTypePrice();
                ttrprice.TraderType = db.TraderTypes.Local.AsEnumerable().First(x => x.Name == LLcts.First(y => y[0] == ctpr[3])[1]);
                ttrprice.Item = db.Items.Local.AsEnumerable().First(x => x.Name == LLitems.First(y => y[0] == ctpr[2])[1]);
                ttrprice.Value = decimal.Parse(ctpr[1]);
                Response.Write(string.Format("  {0} {1} {2}\n", ttrprice.Item.Name, ttrprice.TraderType.Name, ttrprice.Value));
                db.Prices.Add(ttrprice);
            }

            Response.Write("fixing prices\n");
            foreach (var item in db.Items.Local)
            {
                var price = new ItemPrice();
                price.Item = item;
                var anyprice = db.Prices.Local.FirstOrDefault(x => x.Item == item);
                if (anyprice == null)
                    Response.Write(string.Format("  item {0} without price\n", item.Name));
                price.Value = anyprice == null ? 12345.67m : anyprice.Value;
                db.Prices.Add(price);
            }

            Response.Write("YOLO!\n");
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var err in ex.EntityValidationErrors)
                {
                    foreach (var e in err.ValidationErrors)
                    {
                        Response.Write(string.Format("ERROR: {0} {1} {2}\n", err.Entry.Entity.GetType().Name, e.PropertyName, e.ErrorMessage));
                    }
                }
            }

            return null;
        }

        public ActionResult ImportFix()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            Response.ContentType = "text/plain";
            Response.Write("<pre>");

            Response.Write("fixing invoice address\n");
            foreach (var trader in db.Traders.ToList())
            {
                if (!trader.Address.Any(x => x.AddressIsPrimary))
                {
                    var addr = trader.Address.FirstOrDefault();
                    if (addr != null)
                    {
                        addr.AddressIsPrimary = true;
                        Response.Write(string.Format("  setting {0} for {1}\n", addr.Address1, trader.Name));
                    }
                    else
                    {
                        Response.Write(string.Format("  {0} zero addresses\n", trader.Name));
                    }
                }
            }

            Response.Write("fixing person responsible\n");
            foreach (var trader in db.Traders.ToList())
            {
                trader.Employees.First().Addresses = trader.Address;
            }
            db.SaveChanges();

            return null;
        }

        public ActionResult ImportOrders()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            Response.ContentType = "text/plain";
            Response.Write("<pre>");

            var LLsos = GetCSV("orders");

            Response.Write("processing orders\n");
            foreach (var addr in db.Addresses.ToList())
            {
                Response.Write(string.Format("  address {1}\t[{0}]:\n", addr.Address1, addr.LegacyCode));
                var foraddr = LLsos.Where(x => x[3].ToUpper() == addr.LegacyCode.ToUpper()).ToList();
                Response.Write(string.Format("    found {0} rows\n", foraddr.Count));
                if (foraddr.Count == 0) continue;

                for (int i = 0; i < 7; ++i)
                {
                    var order = new StandingOrder();
                    order.Address = addr;
                    order.Delivery = foraddr.Any(x => x[2].ToUpper() == "DE" && x[0] == (i + 1).ToString(CultureInfo.InvariantCulture));
                    order.Day = i;
                    db.Orders.Add(order);
                    Response.Write(string.Format("    order for {0} (de={1})\n", i + 1, order.Delivery));

                    foreach (var itemid in foraddr.Select(x => x[2]).Distinct().ToList())
                    {
                        Response.Write("      creating empty orderpos: ");
                        if (itemid.ToUpper() == "DE")
                        {
                            Response.Write("skipping delivery\n");
                            continue;
                        }
                        var pos = new OrderPosition();
                        var itemcode = itemid;
                        var item = db.Items.FirstOrDefault(x => x.LegacyCode.ToUpper() == itemcode.ToUpper());
                        if (item == null)
                        {
                            Response.Write(string.Format("      can't find {0}\n", itemcode));
                            continue;
                        }
                        pos.Item = item;
                        pos.Order = order;
                        pos.OrderedCount = 0;
                        Response.Write(string.Format("{0}[{1}]\n", pos.Item.LegacyCode, pos.Item.Name, pos.OrderedCount));
                        db.OrderPositions.Add(pos);
                    }

                    foreach (var row in foraddr)
                    {
                        if (row[0] == (i + 1).ToString(CultureInfo.InvariantCulture))
                        {
                            if (row[2].ToUpper() == "DE")
                            {
                                Response.Write("      skipping delivery\n");
                                continue;
                            }
                            var pos = db.OrderPositions.Local.FirstOrDefault(x =>
                                x.Item.LegacyCode.ToUpper() == row[2].ToUpper() &&
                                (x.Order as StandingOrder).Day == i &&
                                (x.Order as StandingOrder).Address == addr);
                            if (pos == null)
                            {
                                Response.Write(string.Format("      can't pos for {0}\n", row[2]));
                                continue;
                            }
                            pos.OrderedCount = int.Parse(row[1]);
                            Response.Write(string.Format("      {0}[{1}]x {2}\n", pos.Item.LegacyCode, pos.Item.Name, pos.OrderedCount));
                        }
                    }
                }
            }
            Response.Write("YOLO!\n");
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var err in ex.EntityValidationErrors)
                {
                    foreach (var e in err.ValidationErrors)
                    {
                        Response.Write(string.Format("ERROR: {0} {1} {2}\n", err.Entry.Entity.GetType().Name, e.PropertyName, e.ErrorMessage));
                    }
                }
            }

            return null;
        }

        private List<string[]> GetCSV(string name)
        {
            var list = new List<string[]>();
            using (var sr = new StreamReader(Server.MapPath("~/legacy/" + name + ".csv"), Encoding.UTF8))
            {
                sr.ReadLine();
                while (!sr.EndOfStream)
                    list.Add(Regex.Split(sr.ReadLine(), "@@"));
            }
            return list;
        }

        public ActionResult Crash()
        {
#if !DEBUG
            return HttpNotFound();
#endif
            throw new ApplicationException("A CHUJ KURWA!!!");
        }

        public ActionResult MailTest(string to)
        {
#if !DEBUG
            return HttpNotFound();
#endif
            new Mail(db, "testMail", to).Send();

            return Content("ok");
        }
        public ActionResult CreateInternalItems()
        {
            if (db.Entities.Any(x => x.Name == "Delivery"))
                return DebugReport("delivery exists");

            db.Entities.Add(new WorldEntity
                {
                    BaseUnit = db.Units.First(x => x.Name == "---"),
                    Name = "Delivery",
                    LegacyCode = "DE"
                });
            return DebugReport("added delivery");
        }

        public ActionResult FixDelivery()
        {
            new Snapshoter(db, new DefaultSnapshotParameterProvider(db, db.AppConfig.Name)).FixDelivery();
            return null;
        }

        public ActionResult Install()
        {
            Response.ContentType = "text/plain";
            Response.Write("<pre>");


            db.Businesses.Add(new Business
                {
                    CraftingDeadline = DateTime.Now,
                    Currency = "$",
                    DeliveryDayValue = 127,
                    LegalName = "name",
                    LegalAddress1 = "address",
                    LegalCity = "city",
                    LegalPost = "00-000",
                    LegalPhone = "000-000-000",
                    Timezone = "0",
                    UsingTradingProps = false
                });
            Response.Write("business\n");

            db.Units.Add(new UnitOfMeasure
            {
                Name = "---"
            });
            Response.Write("uom\n");

            db.SaveChanges();

            db.Entities.Add(new WorldEntity
            {
                BaseUnit = db.Units.First(x => x.Name == "---"),
                Name = "Delivery",
                LegacyCode = "DE"
            });
            Response.Write("delivery\n");

            db.Settings.Add(new Settings { Name = "ShowAttr_AdditionalInfo", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_LegacyCode", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_Vat", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_BarcodeType", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_BarcodeValue", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_Tags", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_CraftingTime", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttr_BaseUnit", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "ShowAttrOrder_value", Value = "LegacyCode,BaseUnit,Tags,BarcodeValue,BarcodeType,CraftingTime,AdditionalInfo,Vat,MagicPrice" });
            db.Settings.Add(new Settings { Name = "ShowAttr_MagicPrice", Value = "hidden" });
            db.Settings.Add(new Settings { Name = "PriceData_DeliveryVat", Value = "20" });
            db.Settings.Add(new Settings { Name = "Snapshot_LastDate", Value = "01/01/2000 13:00:00" });
            db.Settings.Add(new Settings { Name = "Invoice_LastNumber", Value = "101" });
            db.Settings.Add(new Settings { Name = "Default_PaymentTerm", Value = "5" });
            db.Settings.Add(new Settings { Name = "Default_TraderType", Value = "0" });
            Response.Write("settings\n");

            var user = Membership.CreateUser("admin@example.org", "qwerty");
            db.BusinessEmployees.Add(new BusinessEmployee
            {
                FirstName = "admin",
                LastName = "super",
                ExternalAccountData = (Guid)user.ProviderUserKey
            });
            db.SaveChanges();

            UpdateRoles();

            Roles.AddUserToRole("admin@example.org", "superadmin");

            return null;
        }

        public ActionResult NameTest()
        {
            Response.ContentType = "text/plain";
            var traders = db.Traders.ToList();
            foreach (var trader in traders)
            {
                var sb = new StringBuilder(trader.Name.ToLower());
                for (int i = 0; i < sb.Length; i++)
                {
                    if (!((int)sb[i] < (int)byte.MaxValue && char.IsLetterOrDigit(sb[i])))
                    {
                        sb[i] = ' ';
                    }
                }
                var sbs = trader.Id + "," + sb.ToString().Trim().Replace("  ", "").Replace(" ", "-");
                Response.Write(string.Format("{0}: {1}\n", trader.Name, sbs));
            }
            return null;
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult EmptyEditor(string typename, string fieldName)
        {
            var type = Type.GetType("chleby.Web.Models." + typename);
            if (type == null)
                return HttpNotFound();
            var model = Activator.CreateInstance(type);
            ViewBag.SLUI_FieldName = fieldName;
            return PartialView("Editors/ItemWrapper", model);
        }

        public ActionResult XmlMenu()
        {
            return PartialView("_Menu");
        }

        public ActionResult MenuTest()
        {
            var root = new List<MenuRoot>
                {
                    new MenuRoot
                        {
                            Name="trader",
                            Nodes=new List<MenuNode> {
                                new MenuNode("N1","A","C","Q"),
                                new MenuNode("N2","A","C","Q",new List<MenuNode>
                                    {
                                        new MenuNode("N21","A","C","Q"),
                                        new MenuNode("N22","A","C","Q", new List<MenuNode>
                                            {
                                                new MenuNode("N221","A","C","Q"),
                                                new MenuNode("N222","A","C","Q"),
                                            }),
                                        new MenuNode("N23","A","C","Q")
                                    }),
                                new MenuNode("N3","A","C","Q"),
                        }
                    },
                    new MenuRoot
                        {
                            Name="admin",
                            Nodes=new List<MenuNode> {
                                new MenuNode("M1","A","C","Q"),
                                new MenuNode("M2","A","C","Q",new List<MenuNode>
                                    {
                                        new MenuNode("M21","A","C","Q"),
                                    }),
                                new MenuNode("M3","A","C","Q"),
                        }
                    }
                };

            var xs = new XmlSerializer(typeof(List<MenuRoot>));
            Response.ContentType = "text/plain";
            xs.Serialize(Response.OutputStream, root);

            return null;
        }

        public ActionResult Dump()
        {
            return View();
        }

        public ActionResult SendBackup()
        {
            new Snapshoter(db, new DefaultSnapshotParameterProvider(db, db.AppConfig.Name)).SendOrdersMail();
            return Content("done");
        }

        public ActionResult TTTest()
        {
            var obj = new
            {
                s = "str",
                i = 123,
                d = DateTime.Now,
                o = new
                {
                    a = "aaa",
                    b = "bbb",
                    o = new { x = 444 }
                },
                t = new[] { new[] { 1, 2 }, new[] { 3, 4 }, new[] { 5, 6 }, new[] { 7, 8 } }
            };

            var tt =
@"
s='{s}'
i='{i}'
d='{d:ddd MM, HH:mm}'
o.a='{o.a}'
o.b='{o.b}'
o.o.x={o.o.x:0.0000}
t.*='{t.*}'
t='{t[0[
       {$[1[ {#}
           {$:0.00},
       ]1]};
   ]0]}'
";

            return Content(TextTemplate.FromString(tt).Format(obj));
        }

        public ActionResult HandwalkNext()
        {
            if (MyBusiness.HandwalkStep == 0 || MyBusiness.HandwalkStep >= Defines.HandwalkSteps.Keys.Max())
                return RedirectToAction("Index", "Home");

            var err = CheckHandwalkStep(MyBusiness.HandwalkStep + 1);
            if (err != null)
            {
                SetAlert(Severity.warning, err.Item1);
                return RedirectToAction(err.Item2, err.Item3, new { tid = err.Item4 });
            }
            MyBusiness.HandwalkStep++;

            db.Entry(MyBusiness).State = EntityState.Modified;
            db.SaveChanges();
            HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult HandwalkPrev()
        {
            if (MyBusiness.HandwalkStep <= 1)
                return RedirectToAction("Index", "Home");

            var err = CheckHandwalkStep(MyBusiness.HandwalkStep - 1);
            if (err != null)
            {
                SetAlert(Severity.warning, err.Item1);
                return RedirectToAction(err.Item2, err.Item3, new { tid = err.Item4 });
            }
            MyBusiness.HandwalkStep--;

            db.Entry(MyBusiness).State = EntityState.Modified;
            db.SaveChanges();
            HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult HandwalkMove(int id = 0)
        {
            if (MyBusiness.HandwalkStep == -1)
            {
                MyBusiness.HandwalkStep = 1;
                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            if (id < 1 || id > Defines.HandwalkSteps.Keys.Max())
                return RedirectToAction("Index", "Home");

            var err = CheckHandwalkStep(id);
            if (err != null)
            {
                SetAlert(Severity.warning, err.Item1);
                return RedirectToAction(err.Item2, err.Item3, new { tid = err.Item4 });
            }

            MyBusiness.HandwalkStep = id;
            db.Entry(MyBusiness).State = EntityState.Modified;
            db.SaveChanges();
            HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

            return RedirectToAction("Index", "Home");
        }

        private Tuple<string, string, string, int> CheckHandwalkStep(int step)
        {
            if (step > 1) //> business company
            {
                if (MyBusiness.LegalName == "!!!GUWNOFIX!!!") //nikt nie zmienil adresu
                {
                    return Tuple.Create("You have to set company details", "Index", "Home", -1);
                }
            }

            if (step == 4 && (string)Session[AppName + "_step4done"] != "true")
            {
                Session[AppName + "-hujinfo"] = Strings.BusinessEmployee_Handwalk_InfoP1 + "<a href='" + Url.Action("Import", "BusinessEmployee") + "' class='business-employees-import-modal'>" + Strings.BusinessEmployee_Handwalk_InfoP2 + "</a>" +
                Strings.BusinessEmployee_Handwalk_InfoP3 + "<a href='#createModal' data-toggle='modal'>" + Strings.BusinessEmployee_Handwalk_InfoP4 + "</a>" + Strings.Strings_BusinessEmployee_Handwalk_InfoP5;
                //Session[AppName + "_step4done"] = "true";
            }
            if (step == 10)
            {
                SetAlert(Severity.info,
                         Strings.AllHoliday_Handwalk_Info);
            }
            if (step > 6) //> klienci
            {
                if (!db.Traders.Any())
                {
                    return Tuple.Create("Please add at least one customer", "Index", "TurboTraders", -1);
                }
                if (!db.TraderEmployees.Any())
                {
                    return Tuple.Create("Please add at least one contact", "Index", "TraderEmployees", db.Traders.First().Id);
                }
                if (!db.Addresses.Any())
                {
                    return Tuple.Create("Please add at least one address before proceeding. A customer must have an address set up to be able to place orders.", "Index", "Address", db.Traders.First().Id);
                }
            }

            return null;
        }

        public ActionResult UpdateMailTemplates()
        {
            db.MailTemplates.ToList().ForEach(x => db.MailTemplates.Remove(x));
            foreach (var file in Directory.GetFiles(Server.MapPath("~/data/"), "*.t"))
            {
                var name = Path.GetFileNameWithoutExtension(file);
                db.MailTemplates.Add(new MailTemplate
                    {
                        Name = name,
                        Template = System.IO.File.ReadAllText(file),
                        Subject = "todo"
                    });
            }
            db.SaveChanges();
            return null;
        }

        public ActionResult FixInvoiceDates()
        {
            Response.ContentType = "text/plain";
            var zerodate = new DateTime(2000, 1, 1);
            var invs = db.Invoices.Where(x => x.StartDate == zerodate).ToList();
            Response.Write(string.Format("invs: {0}\n", invs.Count));
            foreach (var inv in invs)
            {
                var min = new DateTime(2099, 1, 1);
                var max = new DateTime(1999, 1, 1);

                foreach (var row in inv.Rows)
                {
                    foreach (var dd in row.DayData)
                    {
                        if (dd.Date < min)
                            min = dd.Date;
                        if (dd.Date > max)
                            max = dd.Date;
                    }

                }
                inv.StartDate = min;
                inv.EndDate = max;
                Response.Write(string.Format("{0}: {1} -> {2}\n", inv.Id, min, max));
            }
            db.SaveChanges();
            return null;
        }

        public ActionResult FormatTest()
        {
            var ret = string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} | ", 123.34m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123.34m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123.3m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123.30m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123.00m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123.0m);
            ret += string.Format("{0} {0:C} {0:0.00} {0:0.##} {0:#.##} |", 123m);
            return Content(ret);
        }

        public ActionResult GetMailTemplates()
        {
            Response.ContentType = "text/xml; charset=utf-8";
            Response.ContentEncoding = Encoding.UTF8;
            var mts = db.MailTemplates.ToList();
            var xs = new XmlSerializer(mts.GetType());
            xs.Serialize(Response.OutputStream, mts);
            return null;
        }

        /*public ActionResult RemoveAddressPart()
        {
            Response.ContentType = "text/plain; charset=utf-8";
            Response.ContentEncoding = Encoding.UTF8;

            foreach (var invoice in db.Invoices.ToList())
            {
                var ap = invoice.Addresses.First();
                invoice.DeliveryAddress_Id = ap.Address_Id;
                invoice.ItemsPrice = ap.ItemsPrice;
                invoice.Vat = ap.Vat;
                invoice.DeliveryCount = ap.DeliveryCount;
                invoice.DeliveryPrice = ap.DeliveryPrice;
                invoice.DeliveryVat = ap.DeliveryVat;
                invoice.TotalPrice = ap.TotalPrice;

                foreach (var row in ap.Rows)
                {
                    row.Invoice = invoice;
                }
            }
            db.SaveChanges();
            return null;
        }*/

        public ActionResult FixInvoiceSums()
        {
            Response.ContentType = "text/plain; charset=utf-8";
            Response.ContentEncoding = Encoding.UTF8;
            var delivId = db.AppConfig.DeliveryId;
            var deliv = db.Entities.Find(delivId);
            foreach (var invoice in db.Invoices.ToList())
            {
                Response.Write(string.Format("-- {0}\n", invoice.Id));
                var sumprice = invoice.Rows.Where(x => x.Item==null || x.Item.Id != delivId).Sum(x => x.Price);
                var sumvat = invoice.Rows.Where(x => x.Item == null || x.Item.Id != delivId).Sum(x => x.Vat);
                if (sumprice != invoice.ItemsPrice)
                {
                    Response.Write(string.Format("{0}\t{1}\n", invoice.ItemsPrice, sumprice));
                    invoice.ItemsPrice = sumprice;
                }
                if (sumvat != invoice.Vat)
                {
                    Response.Write(string.Format("{0}\t{1}\n", invoice.Vat, sumvat));
                    invoice.Vat = sumvat;
                }
            }

            db.SaveChanges();
            return null;
        }

        public ActionResult RefreshDBCache()
        {
            MvcApplication.UpdateCachedDB();
            HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();
            return Content("done");
        }

        public ActionResult AllHelp()
        {
            return View(MvcApplication.Help);
        }
    }
}

