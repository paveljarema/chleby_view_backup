using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("CTypes", Parent = typeof(TurboTradersController))]
    public class TraderTypesController : MultiController
    {
        public override dynamic InitData(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            return db.TraderTypes.ToList();
        }

        //
        // POST: /TraderTypes/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(TraderType tradertype)
        {
            if (ModelState.IsValid)
            {
                db.TraderTypes.Add(tradertype);
                db.SaveChanges();
                return RedirectToAction("Index", "TurboTraders");
            }

            ViewBag.TraderName = tradertype.Name;
            return MultiView();
        }

        //
        // GET: /TraderTypes/Edit/5
        [NeedWrite]
        public ActionResult Edit(int id)
        {
            TraderType tradertype = db.TraderTypes.Single(x => x.Id == id);
            return View(tradertype);
        }

        //
        // POST: /TraderTypes/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(TraderType tradertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tradertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "TurboTraders");
            }
            ViewBag.Name = tradertype.Name;
            ViewBag.Id = tradertype.Id;
            return MultiView();
        }

        //
        // POST: /TraderTypes/Delete/5
        [NeedWrite]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TraderType tradertype = db.TraderTypes.Single(x => x.Id == id);
            db.TraderTypes.Remove(tradertype);
            db.SaveChanges();
            return RedirectToAction("Index", "TurboTraders");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult Default(int id = 0)
        {
            Settings["Default"]["TraderType"] = id.ToString(CultureInfo.InvariantCulture);
            //db.Settings.First(x => x.Name == "Default_TraderType").Value = id.ToString();
            db.SaveChanges();
            return Content("ok");
        }
    }
}