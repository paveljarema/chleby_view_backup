﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using AutoMapper;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Attributes")]
    [ChlebyMenu("Attributes")]
    public class AttrMetaController : ChlebyController
    {
        //
        // POST: /AttrMeta/Create

        public ActionResult Index()
        {
            return View();
        }

        [ChlebyMenu("CustomAttributes")]
        public ActionResult Custom()
        {
            return View(new ComboViewModel<AttrMetaVM>
            {
                Index = Mapper.Map<AttrMetaVM[]>(db.AtributeMetas.OrderBy(x => x.Order).ToList())
            });
        }

        [HttpPost]
        public ActionResult Create(AttrMetaVM vm)
        {
            var attributemeta = Mapper.Map<AttributeMeta>(vm);
            if (attributemeta.DataType == AttrDataType.Expr)
            {
                string err = Parser.TryParse(attributemeta.PossibleValues);
                if (err != null)
                    ModelState.AddModelError("PossibleValues", err);
            }
            if (ModelState.IsValid)
            {
                db.AtributeMetas.Add(attributemeta);
                Log("create", attributemeta.Id);
                db.SaveChanges();
                return RedirectToAction("Custom");
            }

            return View("Custom", new ComboViewModel<AttrMetaVM>
            {
                Index = Mapper.Map<AttrMetaVM[]>(db.AtributeMetas.OrderBy(x => x.Order).ToList()),
                Create = vm
            });
        }

        //
        // GET: /AttrMeta/Edit/5

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            AttributeMeta attributemeta = db.AtributeMetas.Find(id);
            if (attributemeta == null)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<AttrMetaVM>(attributemeta));
        }

        //
        // POST: /AttrMeta/Edit/5

        [HttpPost]
        public ActionResult Edit(AttrMetaVM vm)
        {
            var attributemeta = Mapper.Map<AttributeMeta>(vm);
            if (attributemeta.DataType == AttrDataType.Expr)
            {
                string err = Parser.TryParse(attributemeta.PossibleValues);
                if (err != null)
                    ModelState.AddModelError("PossibleValues", err);
            }
            TryValidateModel(attributemeta);
            if (ModelState.IsValid)
            {
                db.Entry(attributemeta).State = EntityState.Modified;
                Log("edit", attributemeta.Id);
                db.SaveChanges();
                return RedirectToAction("Custom");
            }
            return View("Custom", new ComboViewModel<AttrMetaVM>
            {
                Index = Mapper.Map<AttrMetaVM[]>(db.AtributeMetas.OrderBy(x => x.Order).ToList()),
                Edit = vm
            });
        }

        //
        // GET: /AttrMeta/Delete/5

        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            AttributeMeta attributemeta = db.AtributeMetas.Find(id);
            db.AtributeMetas.Remove(attributemeta);
            Log("delete", attributemeta.Id);
            db.SaveChanges();
            return RedirectToAction("Custom");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Base(Dictionary<string, string> radios, string hwbeh)
        {
            var order = "";
            foreach (var kvp in radios)
            {
                Settings["ShowAttr"][kvp.Key] = kvp.Value;
                order += kvp.Key + ',';
            }
            Settings["ShowAttrOrder"]["value"] = order.Substring(0, order.Length - 1);
            Log("changebase");
            db.SaveChanges();
            if (MyBusiness.HandwalkStep > 0)
            {
                if (hwbeh == "stay")
                    return RedirectToAction("Index");
                else if (hwbeh == "prev")
                    return RedirectToAction("HandwalkPrev", "Internal");
                else
                    return RedirectToAction("HandwalkNext", "Internal");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SetOrder(Dictionary<string, string> radios, string hwbeh)
        {
            int i = 0;
            if (radios != null)
            {
                foreach (var kvp in radios)
                {
                    var meta = db.AtributeMetas.Find(int.Parse(kvp.Key));
                    meta.Order = i++;
                    meta.ShowInternal = kvp.Value == "internal" || kvp.Value == "all";
                    meta.ShowToTrader = kvp.Value == "all";
                }
                db.SaveChanges();
            }
            if (MyBusiness.HandwalkStep > 0)
            {
                if (hwbeh == "stay")
                    return RedirectToAction("Custom");
                else if (hwbeh == "prev")
                    return RedirectToAction("HandwalkPrev", "Internal");
                else
                    return RedirectToAction("HandwalkNext", "Internal");
            }
            return RedirectToAction("Custom");
        }
    }
}