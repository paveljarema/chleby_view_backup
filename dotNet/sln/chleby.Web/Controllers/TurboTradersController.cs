﻿using chleby.Web.Models;
using MigraDoc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using chleby.Web.Resources;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Customers")]
    [HasChild(typeof(TradersController))]
    [HasChild(typeof(TraderTypesController))]
    [ChlebyMenu("CustomerList")]
    public class TurboTradersController : ChlebyController
    {
        //
        // GET: /TurboTraders/

        [NeedRead]
        public ActionResult Index(int? live)
        {
            if (!live.HasValue)
                ViewBag.DefaultFilter = Strings.Global_All;
            else if (live.Value == 0)
                ViewBag.DefaultFilter = Strings.NotLive;
            else if (live.Value == 1)
                ViewBag.DefaultFilter = Strings.Live;
            else if (live.Value == 2)
                ViewBag.DefaultFilter = Strings.Traders_Sales_Lead;

            var model = MultiModel.Dispatch(GetType(), this.ControllerContext.RequestContext);
            ViewBag.live = live;

            var tm = (ComboViewModel<Trader>) model[typeof(TradersController)].Model;
            if (live.HasValue)
                tm.Index = tm.Index.Where(x => x.Live == live);
            return View(model);
        }

        [NeedRead]
        public ActionResult PDF(int live=1)
        {
            var b = db.Businesses.First();
            var traders = db.Traders.ToList();
            if (!traders.Any()) return HttpNotFound();
            var pdfs = new PDFs(b);
            var doc = pdfs.CreateCustomerList(traders, live);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            var ms = new MemoryStream();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

    }
}
