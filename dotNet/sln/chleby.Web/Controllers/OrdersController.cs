﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using chleby.Web.ViewModel;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("Orders")]
    [ChlebyMenu("TraderOrder")]
    public class OrdersController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_tid == -1)
            {
                RedirectToAction("Index", "Home").ExecuteResult(ControllerContext);
                requestContext.HttpContext.Response.End();
                return;
            }

            if (!db.Orders.Any(x => x.Address.TraderId == _tid) && Session["ordersredirect"] == null)
            {
                Session["ordersredirect"] = "1";
                RedirectToAction("Index", "OrderOverride", new { tid = _tid }).ExecuteResult(ControllerContext);
                requestContext.HttpContext.Response.End();
                return;
            }
        }

        //
        // GET: /Orders/
        //[NeedRead]
        //public ActionResult Index(int id = 0)
        //{
        //    var trader = db.Traders.Find(_tid);
        //    if (!trader.Address.Any())
        //        return RedirectToAction("Index", "Address");
        //    //stos danych uzywanych w indexie
        //    var items = db.Items.Where(x => x.Live).ToList().Where(it =>
        //    {
        //        var vi = it.Visibility.FirstOrDefault(x => x.TraderId == _tid);
        //        if (vi == null) return true;
        //        else return !vi.HiddenByAdmin;

        //    }).ToList();
            
        //    ViewBag.Trader = trader;
        //    ViewBag.Addresses = trader.Address.Where(x => x.Live).ToList();
        //    ViewBag.DeliveryDays = db.Businesses.First().DeliveryDays;
        //    ViewBag.GroupedItems = items.GroupBy(ks => ks.ItemGroup).
        //        OrderBy(x => x.Key).ToList();
        //    items.Sort(new ItemListComparator((Trader)ViewBag.Trader));
        //    ViewBag.AllItems = items;

        //    ViewBag.Lastsnap = ToClientTime(MyBusiness.LastSnapshotDate) ?? DateTime.MinValue;
        //    ViewBag.LocalNow = ToClientTime(DateTime.UtcNow);

        //    var businessHolidaysDays = new List<DateTime>();
        //    foreach (var holiDay in db.BusinessHolidays.ToList())
        //    {
        //        DateTime helperdate = holiDay.StartDate;
        //        while (helperdate != holiDay.EndDate.AddDays(1))
        //        {
        //            businessHolidaysDays.Add(helperdate);
        //            helperdate = helperdate.AddDays(1);
        //        }

        //    }
        //    ViewBag.BusinessHolidaysDays = businessHolidaysDays;
        //    ViewBag.HasEmployees = trader.Employees.Any();

        //    if (trader.Address.Where(add => add.Live).Any())
        //    {
        //        if (id == 0)
        //        {
        //            // ReSharper disable PossibleNullReferenceException
        //            var addressFirst = trader.Address.Where(add => add.Live).FirstOrDefault();
        //            if (addressFirst != null)
        //            {
        //                id = addressFirst.Id;
        //            }
        //            // ReSharper restore PossibleNullReferenceException
        //        }

        //        return View(trader.Address.Where(x => x.Id == id && x.Live).ToList());
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        [NeedRead]
        public ActionResult Index(int id = 0)
        {
            var trader = db.Traders.Find(_tid);
            if (!trader.Address.Any())
                return RedirectToAction("Index", "Address");

            if (id == 0)
            {
                id = trader.Address.First().Id;
            }

            var factory = new ViewModelFactory(this.db);
            var model = factory.CreateStandingOrderViewModel(_tid, id);

            return View(model);
        }

        [NeedWrite]
        public ActionResult SaveSO(int addressId, Dictionary<string, int> order,
            Dictionary<string, bool> delivery, Dictionary<string, Dictionary<string, int>> newOrder)
        {
            var ordermgr = new OrderManager(db, MyBusiness, _tid);

            //zapis starych zmian
            if (order != null && !order.ContainsKey("controller"))
            {
                ordermgr.ModifyStanding(ToDictInt(order), ToDictInt(delivery));
            }

            //tworzenie calego nowego
            if (newOrder != null && !newOrder.ContainsKey("controller"))
            {
                foreach (var byitem in newOrder)
                {
                    var itemId = int.Parse(byitem.Key);
                    var counts = ToDictInt(byitem.Value);
                    // if (counts.All(x => x.Value == 0)) continue;
                    ordermgr.AddStanding(addressId, itemId, counts);
                }
                var orders = (from sto in db.Orders.OfType<StandingOrder>()
                             where sto.Address.Id == addressId
                             select sto).ToList();
                foreach (var sto in orders)
                {
                    sto.Delivery = delivery[sto.Day.ToString(CultureInfo.InvariantCulture)];
                }
                db.SaveChanges();
            }

            //return Redirect(Url.Action("Index") + "#tab_addr-" + addressId);
            return RedirectToAction("Index", new { id = addressId });
        }

        [NeedWrite]
        public ActionResult SaveAdhoc(Dictionary<string, int> order, bool delivery, string note)
        {
            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.ModifyAdhoc(ToDictInt(order), delivery, note);

            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult CreateSO(int addressId, int itemId, Dictionary<string, int> days)
        {
            var address = db.Addresses.Find(addressId);
            if (address == null)
                return DebugReport("address=null");
            if (address.TraderId != _tid)
                return DebugReport("not your address");
            var item = db.Items.Find(itemId);
            if (item == null)
                return DebugReport("item=null");
            if (!address.Live)
                return DebugReport("address dead");

            if (days.All(kvp => kvp.Value == 0)) return RedirectToAction("Index");
            if (days.Any(kvp => kvp.Value < 0)) return RedirectToAction("Index");

            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.AddStanding(addressId, itemId, ToDictInt(days));

            return RedirectToAction("Index", new { id = addressId });
        }

        [NeedWrite]
        public ActionResult DeleteSO(int itemId, int addressId)
        {
            var address = db.Addresses.Find(addressId);
            if (address == null)
                return DebugReport("address=null");
            if (address.TraderId != _tid)
                return DebugReport("not your address");
            var item = db.Items.Find(itemId);
            if (item == null)
                return DebugReport("item=null");
            if (!address.Live)
                return DebugReport("address dead");

            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.DeleteStanding(addressId, itemId);

            return RedirectToAction("Index", new { id = addressId });
        }

        [NeedWrite]
        public ActionResult CreateAdhoc(int addressId, int? itemId, DateTime? date, bool delivery, string note, string action, int count = 0)
        {
            var address = db.Addresses.Find(addressId);
            if (address == null)
                return DebugReport("address=null");
            if (address.TraderId != _tid)
                return DebugReport("not your address");
            if (itemId == null)
                return DebugReport("itemId=null");
            var item = db.Items.Find(itemId);
            if (item == null)
                return DebugReport("item=null");
            if (!address.Live)
                return DebugReport("address dead");

            if (!date.HasValue)
            {
                return RedirectToAction("Index");
            }
            if (!item.CanOrder(MyBusiness, date.Value) && action != "override")
            {
                return DebugReport("past!");
            }
            int dow = (int)date.Value.DayOfWeek - 1;
            if (dow < 0) dow += 7;
            if (!MyBusiness.DeliveryDays.Contains(dow))
                return DebugReport("not in delivery days");

            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.AddAdhoc(addressId, itemId.Value, date.Value, count, delivery, note);

            if (action == "override")
            {
                var trader = db.Traders.Find(_tid);
                new Snapshoter(db, new DefaultSnapshotParameterProvider(db, db.AppConfig.Name)).CreateOverrideSnapshot(addressId, date, note, count, item, trader, delivery, MyBusiness);
                db.SaveChanges();
            }

            return RedirectToAction("Index", new { id = addressId });
        }

        [NeedWrite]
        public ActionResult DeleteAdhoc(int posId)
        {
            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.DeleteAdhocPos(posId);
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAllAdhoc(int orderId)
        {
            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            ordermgr.DeleteAdhoc(orderId);
            return RedirectToAction("Index");
        }

        public ActionResult Export()
        {
            var obj = from order in db.Orders.OfType<StandingOrder>()
                      from pos in order.Items
                      where pos.OrderedCount > 0 && pos.Order.Address.TraderId == _tid
                      select new
                              {
                                  day = order.Day + 1,
                                  qty = pos.OrderedCount,
                                  item = pos.Item.LegacyCode ?? pos.Item.Name,
                                  addr = order.Address.LegacyCode ?? pos.Order.Address.Address1
                              };
            var csv = CsvExporter.Export(obj);
            //var filedate = DateTime.UtcNow.AddHours(double.Parse(MyBusiness.Timezone));
            var filedate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[MyBusiness.Timezone]);
            var name = string.Format("orders_{0:yyyyMMddHHmmss}.csv", filedate);
            return File(Encoding.UTF8.GetBytes(csv), "text/csv; charset=utf-8", name);
        }

        private Dictionary<int, T> ToDictInt<T>(Dictionary<string, T> dict)
        {
            return dict.ToDictionary(ks => int.Parse(ks.Key), es => es.Value);
        }
    }
}
