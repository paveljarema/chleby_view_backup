﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using DotNetOpenAuth.AspNet;
using Microsoft.Ajax.Utilities;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using chleby.Web.Models;
using System.Data.SqlClient;

namespace chleby.Web.Controllers
{
    [Authorize]
    public class AccountController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            var u = Membership.GetUser();
            if (u != null)
            {
                ViewBag.email = u.Email;
            }
        }

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                //SetAlert(Severity.error, chleby.Web.Resources.Strings.Controller_AccountController_Permission);
                //ViewBag.relogin = true;
                return View("Relogin");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && Membership.ValidateUser(model.UserName, model.Password))
            {
                var log = new AuditLog
                    {
                        Action = User.Identity.IsAuthenticated ? "login" : "relogin",
                        Module = "Login",
                        User = model.UserName,
                    };
                db.AuditLog.Add(log);
                db.SaveChanges();
                Session.Clear();
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe, Url.Content("~/" + AppName));

                //obsluga bug@102
                Session.Add("__FILTERERROR_LOGIN", true);
                /////////////////

                /* WEIRD MAGIC HAPPENS HERE
                 * THATS WHY YOU DONT WANT TO PARSE URL YOURSELF
                 */
                if (returnUrl != null)
                {
                    if (!returnUrl.Contains("?") && !returnUrl.EndsWith("/"))
                        returnUrl += "/";
                    var urlparts = returnUrl.Split(new[] { "/" }, StringSplitOptions.None).ToList();
                    // 0/  1  /2 /3/    4    /  5  /6  !!! ZAMIENIONE LANG I TID !!!
                    //  /jeden/
                    //  /jeden/en/
                    //  /jeden/en/0/kontroler/
                    //  /jeden/en/0/kontroler/
                    //  /jeden/en/0/kontroler/akcja/
                    if (Roles.IsUserInRole(model.UserName, Defines.Roles.Trader))
                    {
                        var user = Membership.GetUser(model.UserName);
                        if (user != null)
                        {
                            var uid = (Guid)user.ProviderUserKey;
                            var tid =
                                db.TraderEmployees.Where(te => te.ExternalAccountData == uid).Select(x => x.TraderId).First();

                            if (urlparts[1] == AppName)
                            {
                                if (urlparts.Count <= 3)
                                {
                                    urlparts.Add(tid.ToString(CultureInfo.InvariantCulture));
                                }
                                else
                                {
                                    var maybetid = 0;
                                    if (!int.TryParse(urlparts[3], out maybetid))
                                    {
                                        urlparts.Insert(3, tid.ToString(CultureInfo.InvariantCulture));
                                    }
                                }
                            }
                            return RedirectToLocal(string.Join("/", urlparts));
                        }
                    }
                }
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            SetAlert(Severity.error, chleby.Web.Resources.Strings.Controler_Main_Password);
            return View(model);
        }

        //
        // POST: /Account/LogOff

        public ActionResult LogOff()
        {
            Session.Abandon();
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "novalu");
            cookie.HttpOnly = true;
            cookie.Path = Url.Content("~/" + AppName);// "/" + AppName;
            cookie.Expires = new System.DateTime(1999, 10, 12);
            cookie.Secure = FormsAuthentication.RequireSSL;
            if (FormsAuthentication.CookieDomain != null)
                cookie.Domain = FormsAuthentication.CookieDomain;
            HttpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            HttpContext.Response.Cookies.Add(cookie);

            var log = new AuditLog
            {
                Action = "logout",
                Module = "Login",
                User = User.Identity.Name,
            };
            db.AuditLog.Add(log);
            db.SaveChanges();

            return RedirectToAction("Login", "Account", new { returnUrl = Url.Action("Index", "Home") });
        }

        public ActionResult Manage()
        {
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                bool changePasswordSucceeded = false;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, userIsOnline: true);
                    if (currentUser == null)
                        _log.Warn("Account/Manage - currentUser = null, why?");
                    else
                        changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    Session["alerts"] = new List<UIAlert>();
                    ((List<UIAlert>)Session["alerts"]).Add(new UIAlert { Status = Severity.success, Msg = chleby.Web.Resources.Strings.Controller_AccountController_Password_Change });
                    return RedirectToAction("Manage");
                }
                else
                {
                    Session["alerts"] = new List<UIAlert>();
                    ((List<UIAlert>)Session["alerts"]).Add(new UIAlert { Status = Severity.error, Msg = chleby.Web.Resources.Strings.Controller_AccountController_Password_Incorrect });
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            return View("ResetPasswordFirst");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModelFirst model)
        {
            if (ModelState.IsValid)
            {
                var dbUsers = Membership.FindUsersByEmail(model.Email);

                if (dbUsers.Count > 0)
                {
                    var uname = Membership.GetUserNameByEmail(model.Email);
                    var user = Membership.GetUser(uname);
                    var info = db.UserPwdResetInfos.Create();
                    info.UserId = (Guid)user.ProviderUserKey;
                    info.ResetDate = DateTime.Now;
                    info.Token = Guid.NewGuid();
                    db.UserPwdResetInfos.Add(info);
                    db.SaveChanges();

                    new Mail(db, "pwdResetMail", model.Email, new
                        {
                            user = uname,
                            app = AppName,
                            code = info.Token.ToString(""),
                            host = HttpContext.Request.ServerVariables["HTTP_HOST"]
                        }).Send();
                }
            }
            return View("ResetPasswordFirstSuccess", model);
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordEmail(Guid id)
        {
            var infos = (from v in db.UserPwdResetInfos
                         where v.Token == id
                         select v).ToList();
            if (infos.Count == 1)
            {
                var info = infos[0];
                if (DateTime.Now - info.ResetDate > new TimeSpan(12, 0, 0))
                {
                    ViewBag.Result = "Kod wygasł (>12h)!";
                    return View();
                }
                var user = Membership.GetUser(info.UserId);
                if (user == null)
                    _log.Warn("Account/ResetPasswordEmail - user=null, corrupted db?");
                else
                {
                    var pass = user.ResetPassword();
                    ViewBag.Result += chleby.Web.Resources.Strings.Controller_AccountController_Password_New + pass;
                    db.UserPwdResetInfos.Remove(info);
                    db.SaveChanges();
                }
            }
            else
            {
                ViewBag.Result = chleby.Web.Resources.Strings.Controller_AccountController_Password_Wrong;
            }
            return View();
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        #endregion

        [HttpPost]
        public ActionResult ManageOther(string email)
        {
            var user = Membership.GetUser();
            if (user == null)
                return HttpNotFound();

            var byemail = Membership.GetUserNameByEmail(email);
            var byuser = Membership.GetUser(email);

            if ((byemail != user.Email && byemail != null) || byuser != null)
            {
                ModelState.AddModelError("email", chleby.Web.Resources.Strings.Controller_AccountController_Email_In_Use);
                ViewBag.email = email;
                return View("Manage");
            }

            var validator = new EmailAddressAttribute();
            if (!validator.IsValid(email))
            {
                ModelState.AddModelError("email", validator.ErrorMessage);
                ViewBag.email = email;
                return View("Manage");
            }

            var newuser = Membership.GetUser(email);
            if (newuser != null && newuser.UserName != user.UserName)
            {
                ModelState.AddModelError("email", chleby.Web.Resources.Strings.Controller_AccountController_Email_In_Use);
                ViewBag.email = email;
                return View("Manage");
            }

            user.Email = email;
            Membership.UpdateUser(user);

            ChangeUserName(user.UserName, email);

            return LogOff();
        }

        public static IEnumerable<object> ChangeUserName(string user, string newname)
        {
            IEnumerable<object> spresults;
            using (var maindb = new MainAppContext())
            {
                spresults = maindb.Database.SqlQuery<object>(
                   "usp_ChangeUsername @ApplicationName, @OldUserName, @NewUserName",
                   new SqlParameter("@ApplicationName", Membership.ApplicationName),
                   new SqlParameter("@OldUserName", user),
                   new SqlParameter("@NewUserName", newname)).ToList();
            }
            return spresults;
        }

        [AllowAnonymous]
        public ActionResult Invitation(Guid id)
        {
            var model = new SetPasswordModel();
            model.Expired = false;
            var info = db.UserPwdResetInfos.FirstOrDefault(x => x.Token == id);
            if (info == null)
            {
                model.Expired = true;
                return View(model);
            }

            if (DateTime.Now - info.ResetDate > new TimeSpan(48, 0, 0))
            {
                db.UserPwdResetInfos.Remove(info);
                db.SaveChanges();
                model.Expired = true;
                return View(model);
            }

            var user = Membership.GetUser(info.UserId);
            if (user == null)
            {
                model.Expired = true;
                return View(model);
            }

            model.Guid = (Guid)user.ProviderUserKey;
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Invitation(SetPasswordModel model)
        {
            var user = Membership.GetUser(model.Guid);
            if (user == null)
                return HttpNotFound();

            if (ModelState.IsValid)
            {
                var oldpass = user.ResetPassword();
                user.ChangePassword(oldpass, model.NewPassword);
                user.IsApproved = true;
                Membership.UpdateUser(user);

                var info = db.UserPwdResetInfos.FirstOrDefault(x => x.UserId == model.Guid);
                db.UserPwdResetInfos.Remove(info);
                db.SaveChanges();
                return Login(new LoginModel
                    {
                        UserName = user.UserName,
                        Password = model.NewPassword,
                        RememberMe = false
                    }, Url.Action("Index", "Home"));
            }

            return View(model);
        }

        public ActionResult PermReport()
        {
            var modules = (from um in MvcApplication.UsedModules
                           where um.App.Name == AppName
                           select um.Module).Union(
                           (from mr in MvcApplication.ModuleRegister
                            where mr.IsCore
                            select mr)).Where(p => p.Category != "-").Distinct().
                            GroupBy(x=>x.Admin).
                            ToList();
            return PartialView(modules);
        }
    }

}

