﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("UoMConv")]
    public class UoMConvController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.AllUnits = db.Units.OrderBy(x => x.Name).ToList();
        }

        public ActionResult Index()
        {
            return View(db.Conversions.ToList());
        }

        [NeedWrite]
        public ActionResult DeleteUoM(int id)
        {
            var uom = db.Units.Find(id);
            if (uom == null || uom.Name == "---")
                return HttpNotFound();

            var empty = db.Units.First(x => x.Name == "---");
            db.Conversions.Where(c => c.FromId == id || c.ToId == id).ToList().
                ForEach(c => db.Conversions.Remove(c));
            db.Items.Where(x => x.BaseUnitId == id).ToList().
                ForEach(x => x.BaseUnitId = empty.Id);
            db.Units.Remove(uom);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult SetUoM(Dictionary<string, string> uom)
        {
            foreach (var kvp in uom)
            {
                db.Units.Find(int.Parse(kvp.Key)).Name = kvp.Value;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult CreateUoM(UnitOfMeasure uom)
        {
            if (ModelState.IsValid)
            {
                db.Units.Add(uom);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult SetConv(IEnumerable<UoMConversion> conv)
        {
            var good = true;
            foreach (var key in ModelState.Keys)
            {
                if (key.EndsWith("Multiplier") && !ModelState.IsValidField(key))
                    good = false;
            }
            if (good)
            {
                foreach (var c in conv)
                {
                    if (c.FromId == c.ToId)
                    {
                        ModelState.AddModelError("ToId", "Error");

                        return View("Index", conv);
                    }
                    var dbc = db.Conversions.Find(c.Id);
                    dbc.FromId = c.FromId;
                    dbc.From = db.Units.Find(c.FromId);
                    dbc.ToId = c.ToId;
                    dbc.To = db.Units.Find(c.ToId);
                    dbc.FromMultiplier = c.FromMultiplier;
                    dbc.ToMultiplier = c.ToMultiplier;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Index", conv);
        }

        public ActionResult DeleteConv(int id)
        {
            var conv = db.Conversions.Find(id);
            if (conv == null)
                return HttpNotFound();

            db.Conversions.Remove(conv);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CreateConv(UoMConversion conv)
        {
            if (ModelState.IsValidField("FromMultiplier") && ModelState.IsValidField("ToMultiplier"))
            {
                if (db.Units.Find(conv.FromId) != null)
                {
                    if (db.Units.Find(conv.ToId) != null)
                    {
                        db.Conversions.Add(conv);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            ViewBag.OldObject = conv;
            return View("Index", db.Conversions.ToList());
        }
    }
}
