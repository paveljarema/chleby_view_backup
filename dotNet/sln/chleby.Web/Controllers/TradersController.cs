using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Customers", Parent = typeof(TurboTradersController))]
    [ChlebyMenu("CustomerList")]
    public class TradersController : MultiController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.PossibleTypes = db.TraderTypes.ToList();
            ViewBag.PaymentT = db.PaymentTerms.ToList();
            ViewBag.users = Membership.GetAllUsers().Cast<MembershipUser>().ToDictionary(user => (Guid)user.ProviderUserKey, user => user.UserName);
            ViewBag.usedTraders = db.Invoices.Select(x => x.Trader.Id).Distinct().ToList();
        }

        public override dynamic InitData(RequestContext requestContext)
        {
            Initialize(requestContext);
            ViewBag.MyBusiness = HttpContext.Cache[requestContext.RouteData.Values["app"].ToString() + "__Business"];
            return new ComboViewModel<Trader>
            {
                Index = db.Traders.ToList(),
            };
        }

        // POST: /Traders/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(Trader trader)
        {
            if (ModelState.IsValid)
            {
                db.Traders.Add(trader);
                db.SaveChanges();
                //return RedirectToAction("Index", "TurboTraders");
                return RedirectToAction("SwitcherDone", "Internal", new { adminactor = trader.Id });
            }

            return MultiView(callbackModel: model => model.Create = trader);
        }

        //
        // GET: /Traders/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id)
        {
            Trader trader = db.Traders.Single(x => x.Id == id);
            return PartialView(trader);
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ToggleLive(int id, int live)
        {
            var trader = db.Traders.Find(id);
            if (trader == null)
                return Json(new { msg = "wrong id" }, JsonRequestBehavior.AllowGet);

            foreach (var emp in trader.Employees)
            {
                var user = Membership.GetUser(emp.ExternalAccountData);
                if (user != null)
                {
                    user.IsApproved = live == 1;
                    Membership.UpdateUser(user);
                }
            }
            trader.Live = live;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new { msg = "db error - " + e.Message, value = trader.Live }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { msg = "ok", value = live }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Traders/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(Trader trader)
        {
            trader.PaymentTerms = db.PaymentTerms.Find(trader.PayId);
            ModelState.Clear();
            TryValidateModel(trader);
            if (ModelState.IsValid)
            {
                db.Entry(trader).State = EntityState.Modified;
                db.SaveChanges();
                trader = db.Traders.Find(trader.Id);
                trader.Employees = db.TraderEmployees.Where(x => x.Id == trader.Id).ToList();                  
                foreach (var emp in trader.Employees)
                {
                    var user = Membership.GetUser(emp.ExternalAccountData);
                    if (user != null)
                    {
                        user.IsApproved = trader.Live == 1;
                        Membership.UpdateUser(user);
                    }   
                }
                return RedirectToAction("Index", "TurboTraders");
            }

            return MultiView(callbackModel: model => model.Edit = trader);
        }
        //
        // POST: /Traders/Delete/5
        [NeedWrite]
        public ActionResult Delete(int id)
        {
            var trader = db.Traders.Find(id);
            if (db.Invoices.Any(x => x.Trader.Id == id))
            {
                trader.Live = 0;
                db.SaveChanges();
                return RedirectToAction("Index", "TurboTraders");
            }

            foreach (var emp in trader.Employees)
            {
                var user = Membership.GetUser(emp.ExternalAccountData);
                if (user != null)
                    Membership.DeleteUser(user.UserName);
            }
            trader.Employees.ToList().ForEach(x => db.TraderEmployees.Remove(x));
            trader.Address.ToList().ForEach(x => db.Addresses.Remove(x));
            db.SnapshotOrderPositions.Where(x => x.Address.TraderId == id).ToList().ForEach(x => db.SnapshotOrderPositions.Remove(x));
            db.Prices.OfType<TraderPrice>().Where(x => x.TraderId == id).ToList().ForEach(x => db.Prices.Remove(x));
            db.Traders.Remove(trader);
            db.SaveChanges();
            return RedirectToAction("Index", "TurboTraders");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Import()
        {
            ViewBag.What = "customers";
            return PartialView();
        }

        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return Import(str);
        }

        [HttpPost]
        public ActionResult Import(string csv, bool save = false)
        {
            ViewBag.csv = csv;
            ViewBag.What = "customers";

            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var traders = new List<Trader>();
            var dbtraders = db.Traders.ToList().Select(x => x.Name).ToList();
            var types = new List<TraderType>();
            var terms = new List<PaymentTerm>();
            foreach (var tline in lines)
            {
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                var trader = new Trader();
                trader.Employees = new Collection<TraderEmployee>();
                trader.Address = new Collection<Address>();
                trader.Live = 1;
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 4)
                {
                    errors.Add(Tuple.Create(curline, -1, "wrong field count"));
                    continue;
                }

                if (string.IsNullOrWhiteSpace(fields[0]))
                {
                    errors.Add(Tuple.Create(curline, 1, "empty name"));
                    continue;
                }
                trader.PrimaryName = fields[0].Trim();
                if (traders.Any(i => i.Name == trader.Name) || dbtraders.Contains(trader.Name))
                {
                    //errors.Add(Tuple.Create(curline, 1, "name already used"));
                    //omijamy po cichu uzytego
                    continue;
                }

                trader.TradingName = fields[1].Trim();

                if (string.IsNullOrWhiteSpace(fields[2]))
                {
                    errors.Add(Tuple.Create(curline, 3, "empty type"));
                    continue;
                }

                var ttname = fields[2].Trim().Normalize(NormalizationForm.FormKD);
                var tt = db.TraderTypes.FirstOrDefault(x => x.Name == ttname);
                if (tt == null)
                {
                    tt = types.FirstOrDefault(x => x.Name.Equals(ttname, StringComparison.InvariantCultureIgnoreCase));
                    if (tt == null)
                    {
                        tt = new TraderType { Name = ttname };
                        types.Add(tt);
                    }
                }
                trader.Type = tt;

                if (string.IsNullOrWhiteSpace(fields[3]))
                {
                    errors.Add(Tuple.Create(curline, 4, "empty term"));
                    continue;
                }

                var ptname = fields[3].Trim().Normalize(NormalizationForm.FormKD);
                var pt = db.PaymentTerms.FirstOrDefault(x => x.Name == ptname);
                if (pt == null)
                {
                    pt = terms.FirstOrDefault(x => x.Name.Equals(ptname, StringComparison.InvariantCultureIgnoreCase));
                    if (pt == null)
                    {
                        pt = new PaymentTerm() { Name = ptname };
                        terms.Add(pt);
                    }
                }
                trader.PaymentTerms = pt;

                traders.Add(trader);
            }
            ViewBag.errors = errors;

            if (errors.Count == 0 && save)
            {
                types.ForEach(i => db.TraderTypes.Add(i));
                terms.ForEach(i => db.PaymentTerms.Add(i));
                traders.ForEach(i => db.Traders.Add(i));
                db.SaveChanges();
                return MultiView();
            }

            return PartialView("Import");
        }

        [NeedRead]
        public ActionResult Export()
        {
            switch (MyBusiness.AccSoftware)
            {
                case 0: //qb
                    return HttpNotFound();
                case 1: //xero
                    return XeroExport();
            }
            return HttpNotFound();
        }

        public ActionResult XeroExport()
        {
            var csv = new StringBuilder();
            csv.AppendLine(
                "Name,EmailAddress,FirstName,LastName,POAttentionTo,POAddressLine1,POAddressLine2,POAddressLine3,POAddressLine4,POCity,PORegion,POPostalCode,POCountry,SAAttentionTo,SAAddressLine1,SAAddressLine2,SAAddressLine3,SAAddressLine4,SACity,SARegion,SAPostalCode,SACountry,PhoneNumber,FaxNumber,MobileNumber,DDINumber,SkypeName,BankAccountName,BankAccountNumber,BankAccountParticulars,TaxNumber,AccountsReceivableTaxCodeName,AccountsPayableTaxCodeName,Website,Discount,DueDateBillDay,DueDateBillTerm,DueDateSalesDay,DueDateSalesTerm");
            foreach (var trader in db.Traders.ToList())
            {
                var primaddr = trader.Address.First(x => x.AddressIsPrimary);

                foreach (var addr in trader.Address)
                {
                    var emp = addr.TraderEmployees.FirstOrDefault();
                    var email = Membership.GetUser(emp.ExternalAccountData).Email;

                    csv.AppendLine(string.Join(",", new string[]
                        {
                            "\"" + trader.Name + "-" + addr.Address1 + "\"",
                            "\"" + email + "\"",
                            "\"" + emp.FirstName + "\"",
                            "\"" + emp.LastName + "\"",
                            //postal addr
                            "", //attto
                            "\"" + primaddr.Address1 + "\"",
                            "\"" + primaddr.Address2 + "\"",
                            "\"" + primaddr.Address3 + "\"",
                            "", //addr4
                            "\"" + primaddr.City + "\"",
                            "", //region
                            "\"" + primaddr.Post + "\"",
                            "", //country
                            //street addr
                            "", //attto
                            "\"" + addr.Address1 + "\"",
                            "\"" + addr.Address2 + "\"",
                            "\"" + addr.Address3 + "\"",
                            "", //addr4
                            "\"" + addr.City + "\"",
                            "", //region
                            "\"" + addr.Post + "\"",
                            "", //country
                            "\"" + emp.Phone + "\"",
                            ",,,,,,,,,,,,,,," //16 nieuzywanych pol - 15 przecinkow
                        }));
                }
            }

            return File(Encoding.UTF8.GetBytes(csv.ToString()), "text/csv", "export-customers.csv");
        }

        [NeedWrite]
        public ActionResult DwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Legal Name*,Trading Name (if different),Price list*,Payment Terms*
"), "text/csv; charset=utf-8", "Customers.csv");
        }
    }
}