﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("CItems")]
    [ChlebyMenu("TraderItems")]
    public class TraderItemsController : ChlebyController
    {
        //
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_tid == -1)
            {
                RedirectToAction("Index","Home").ExecuteResult(ControllerContext);
                requestContext.HttpContext.Response.End();
                return;
            }

           

            ViewBag.Price = 0.00m;
            ViewBag.listagrup = db.ItemGroups.Where(i => !i.Items.Any() && i.Live);
            ViewBag.AllUnits = db.Units.ToList();
            ViewBag.Attrs = db.AtributeMetas.ToList();
            ViewBag.Trader = db.Traders.Find(_tid);          
        }

        [NeedRead]
        public ActionResult Index()
        {
            var itemlist = db.Items.Where(itemlive => itemlive.Live).GroupBy(ks => ks.ItemGroup).ToList().OrderBy(x => x.Key).ToList();
            var cvm = new ComboViewModelPenta<Item, IGrouping<ItemGroup, Item>, ItemGroup>
            {
                Index = itemlist,
                Create = null,
                Edit = null,
                Create2 = null,
                Edit2 = null
            };
            return View(cvm);

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ToggleAdminHide(int id, bool hidden)
        {
            if (!User.IsInRole(Defines.Roles.Employee) && !User.IsInRole(Defines.Roles.SuperAdmin))
                return HttpNotFound();

            var item = db.Items.Find(id);
            if (item == null)
                return Json(new { msg = "wrong id" }, JsonRequestBehavior.AllowGet);

            var vt = db.VisibleToes.FirstOrDefault(x => x.TraderId == _tid && x.ItemId == id);
            if (vt == null)
            {
                vt = new VisibleTo
                {
                    Item = db.Items.Find(id),
                    Trader = db.Traders.Find(_tid),
                };
                db.VisibleToes.Add(vt);
            }
            vt.HiddenByAdmin = hidden;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new { msg = "db error - " + e.Message, value = item.Live }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { msg = "ok", value = hidden }, JsonRequestBehavior.AllowGet);
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ToggleFav(int id, bool favorite)
        {
            var item = db.Items.Find(id);
            if (item == null)
                return Json(new { msg = "wrong id" }, JsonRequestBehavior.AllowGet);

            var vt = db.VisibleToes.FirstOrDefault(x => x.TraderId == _tid && x.ItemId == id);
            if (vt==null)
            {
                vt = new VisibleTo
                    {
                        Item = db.Items.Find(id),
                        Trader = db.Traders.Find(_tid),
                    };
                db.VisibleToes.Add(vt);
            }
            vt.Favourite = favorite;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new { msg = "db error - " + e.Message, value = item.Live }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { msg = "ok", value = favorite }, JsonRequestBehavior.AllowGet);
        }
    }
}