﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    [ChlebyModule("HelpBuble")]
    public class HelpBubleController : ChlebyController
    {


        //
        // GET: /HelpBuble/Edit/5
        [NeedWrite]
        public ActionResult Edit(string hbname)
        {
            ViewBag.hbname = hbname;
            HelpBuble helpbuble = db.HelpBubles.FirstOrDefault(x => x.name == hbname);
            if (helpbuble == null)
            {
                return HttpNotFound();
            }
            return View(helpbuble);
        }

        //
        // POST: /HelpBuble/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(HelpBuble helpbuble, string hbname)
        {
            if (ModelState.IsValid)
            {
                db.Entry(helpbuble).State = EntityState.Modified;
                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_HelpBubleController_Change_Saved);
                db.SaveChanges();
            }

            ViewBag.hbname = hbname;
            return View(helpbuble);
        }


    }
}