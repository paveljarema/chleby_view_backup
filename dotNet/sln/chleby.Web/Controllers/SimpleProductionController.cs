﻿using System;
using System.Collections.ObjectModel;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using MigraDoc;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using chleby.Web.Models;
using System.Data.Entity;
using chleby.Web.Resources;
using System.Data.Objects;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("SimpleProd")]
    [ChlebyMenu("AdminProduction")]
    public class SimpleProductionController : ChlebyController
    {
        //
        // GET: /SimpleProduction/

        [NeedRead]
        public ActionResult Index(string date = null)
        {

            var reportDate = DateTime.UtcNow.Date;

            var business = db.Businesses.First();

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(business.Timezone);
            
            if (date != null)
            {
                reportDate = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);

                //set hour and minutes from crafting deadline
                var deadlineLocal = TimeZoneInfo.ConvertTimeFromUtc(business.CraftingDeadline, timeZoneInfo);
                reportDate = reportDate.AddHours(deadlineLocal.Hour);
                reportDate = reportDate.AddMinutes(deadlineLocal.Minute);

                // convert to utc time
                reportDate = TimeZoneInfo.ConvertTimeToUtc(reportDate, timeZoneInfo);
            }
            else if (db.SnapshotOrderPositions.Any(x => x.MadeCount != 0 || x.OrderedCount != 0))
            {
                reportDate = db.SnapshotOrderPositions.Where(x => x.MadeCount != 0 || x.OrderedCount != 0).Select(x => x.EndDate).Distinct().OrderByDescending(x => x).FirstOrDefault();
            }

            var snapshots = (from pos in db.SnapshotOrderPositions
                    where (pos.EndDate.Year == reportDate.Date.Year && pos.EndDate.Month == reportDate.Date.Month && pos.EndDate.Day == reportDate.Date.Day) &&
                    (pos.OrderedCount > 0 || pos.MadeCount > 0) && pos.Item is Item
                    group pos by pos.Item
                        into g
                        select new
                            {
                                Item = g.Key,
                                Count = g.Sum(x => x.OrderedCount),
                                CountMade = g.Sum(x => x.MadeCount),
                                Addresses = g.GroupBy(y => y.Address)
                            }).ToList();

            ViewBag.date = TimeZoneInfo.ConvertTimeFromUtc(reportDate, timeZoneInfo);
            ViewBag.year = reportDate.Year;

            var dfi = DateTimeFormatInfo.CurrentInfo;
            var cal = dfi.Calendar;
            var myCWR = CultureInfo.InvariantCulture.DateTimeFormat.CalendarWeekRule;

            ViewBag.week = cal.GetWeekOfYear(reportDate, myCWR, System.DayOfWeek.Monday); //HARDCODED MONDAY AS FIRST DAY OF WEEK OMG
            //TODO: PROPER LINK TO SNAPSHOT

            ViewBag.alldates = db.SnapshotOrderPositions.Where(x => x.MadeCount != 0 || x.OrderedCount != 0).Select(x => x.StartDate).Distinct().OrderByDescending(x => x).ToList();
            IList<DateTime> endDates = db.SnapshotOrderPositions.Where(x => x.MadeCount != 0 || x.OrderedCount != 0)
                .Select(x => x.EndDate).Distinct().OrderByDescending(x => x).ToList();
            IList<DateTime> endDates2 = new List<DateTime>();
            // localize all end dates
            for (int i = 0; i < endDates.Count; i++)
            {
                endDates[i] = TimeZoneInfo.ConvertTime(endDates[i], timeZoneInfo);
                if (!endDates2.Contains(endDates[i].Date))
                {
                    endDates2.Add(endDates[i].Date);
                }
            }
            ViewBag.enddates = endDates2;
            ViewBag.items = db.Items.ToList();
            return View(snapshots.Select(x => new ProdIndexModel(x.Item, x.Count, x.Addresses, x.CountMade)));
        }


        [NeedRead]
        public ActionResult Zoom(int item, string date)
        {
            var dt = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);

            var q = from pos in db.SnapshotOrderPositions
                    where pos.EndDate == dt && pos.Item.Id == item && (pos.OrderedCount > 0 || pos.MadeCount > 0)
                    group pos by pos.Address
                        into g
                        select new { Address = g.Key, Count = g.Sum(x => x.OrderedCount) };

            ViewBag.item = db.Entities.Find(item);
            ViewBag.date = date;
            return View(q.ToList().Select(x => Tuple.Create(x.Address, x.Count)));
        }

        [NeedRead]
        public ActionResult Delivery(string date)
        {
            var dt = DateTime.UtcNow;
            if (date != null)
            {
                dt = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            var q = from pos in db.SnapshotOrderPositions
                    where pos.EndDate == dt.Date && (pos.OrderedCount > 0 || pos.MadeCount > 0)
                    group pos by pos.Address
                        into g
                        select new
                            {
                                Address = g.Key,
                                Items = from i in g
                                        select new { i.Item.Name, i.OrderedCount, i.MadeCount }
                            };

            ViewBag.date = dt;
            ViewBag.alldates = db.SnapshotOrderPositions.Select(x => x.EndDate).Distinct().OrderBy(x => x).ToList();
            return View(q.ToList().Select(x =>
                Tuple.Create(
                    x.Address,
                    x.Items.ToDictionary(
                        ks => ks.Name,
                        es => Tuple.Create(es.OrderedCount, es.MadeCount))
                    )
                ));
        }

        [NeedRead]
        public ActionResult QueueExport(string date)
        {
            var sb = new StringBuilder();
            var dels = new List<string>();
            var lista = db.SnapshotOrderPositions.Where(x => x.OrderedCount > 0 || x.MadeCount > 0);
            if (date != null)
            {
                var dt = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
                lista = lista.Where(x => x.EndDate == dt);
            }

            foreach (var pos in lista.ToList())
            {
                sb.AppendFormat("{0:dd/MM/yyyy};{1};{2};{3};{4}\n",
                    pos.EndDate, pos.OrderedCount, pos.MadeCount,
                    pos.Item.LegacyCode, pos.Address.LegacyCode);
            }
            foreach (var del in dels.Distinct())
            {
                sb.Append(del);
            }
            //var filedate = DateTime.UtcNow.AddHours(double.Parse(MyBusiness.Timezone));
            var filedate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[MyBusiness.Timezone]);
            var name = string.Format("prodqueue_{0:yyyyMMddHHmmss}.csv", filedate);
            return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv; charset=utf-8", name);
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int itemid, string date)
        {
            var dt = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            var model = db.SnapshotOrderPositions.Where(
                x => x.EndDate == dt.Date && x.Item_Id == itemid && (x.OrderedCount > 0 || x.MadeCount > 0));
            //ViewBag.addrs = db.Addresses.Select(x => new { x.Id, x.Address1 }).ToDictionary(ks => ks.Id, es => es.Address1);
            var addresses = db.Addresses.Include(inc => inc.Trader).ToDictionary(dic => dic.Id, dic => dic.Trader.Name + " " + dic.Address1);

            ViewBag.addrs = addresses.OrderBy(adr => adr.Value).ToDictionary(d => d.Key, d => d.Value);
            Item item = db.Items.Find(itemid);

            //check crafting time
            DateTime utcNow = DateTime.UtcNow;
            if (utcNow.AddDays(item.CraftingTime) > dt)
            {
                ViewBag.waring = string.Format(Strings.SimpleProduction_CraftingTime_Warning, item.Name, item.CraftingTime);
            }

            ViewBag.item = item;
            return PartialView(model.ToList());
        }

        [NeedWrite]
        public ActionResult EditSave(Dictionary<string, int> pos, string date, int itemId, int addrId = 0, int newCount = 0)
        {
            int sum = 0;
            var itemid = 0;
            var changes = new List<OrderDiff>();
            if (pos != null)
            {
                foreach (var kvp in pos)
                {
                    var val = Math.Max(kvp.Value, 0);
                    var p = db.SnapshotOrderPositions.Find(int.Parse(kvp.Key));
                    if (p.MadeCount != val)
                        changes.Add(new OrderDiff(p.Adhoc, p, val, p.MadeCount));
                    p.MadeCount = val;
                    if (itemid == 0)
                        itemid = p.Item_Id;

                    sum += val;
                }
            }

            if (addrId != 0)
            {
                var dt = DateTimeExtensions.FromStringUrl(date);
                var oldpos = db.SnapshotOrderPositions.
                    FirstOrDefault(x => x.EndDate == dt &&
                               x.Address_Id == addrId &&
                               x.Item_Id == itemId);
                if (oldpos != null)
                {
                    changes.Add(new OrderDiff(oldpos.Adhoc, oldpos, newCount, oldpos.MadeCount));
                    oldpos.MadeCount = newCount;
                }
                else
                {
                    var item = db.Items.Find(itemId);
                    ////check crafting time
                    //var today = DateTime.UtcNow;
                    //if (today.AddDays(item.CraftingTime) > dt && Alerts.Any())
                    //{
                    //    //warn about crafting time
                    //    SetAlert(Severity.warning, 
                    //        string.Format(Strings.SimpleProduction_CraftingTime_Warning, item.Name, item.CraftingTime, dt.ToShortDateString()));
                    //}
                    var addr = db.Addresses.Find(addrId);
                    var newpos = new SnapshotOrderPosition
                        {
                            Address_Id = addrId,
                            Address = addr,
                            Item_Id = itemId,
                            Item = item,
                            StartDate=dt,
                            Adhoc = true,
                            //EndDate = dt.AddDays(item.CraftingTime+1),
                            //add item should add on current day
                            EndDate = dt,
                            MadeCount = newCount,
                            Price = item.GetItemPrice(addr.Trader).Value,
                            Vat = item.Vat
                        };
                    changes.Add(new OrderDiff(newpos.Adhoc, newpos, newCount, 0));
                    db.SnapshotOrderPositions.Add(newpos);
                }
            }

            db.SaveChanges();
            NotifyChanges(db, changes, AppName, MyBusiness);
            return Json(new { sum, item = itemid });
        }

        [NeedWrite]
        public ActionResult Delete(int itemid, string date)
        {
            var changes = new List<OrderDiff>();
            var dt = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            db.SnapshotOrderPositions.Where(
                x => x.EndDate == dt.Date && x.Item_Id == itemid && (x.OrderedCount > 0 || x.MadeCount > 0)).
                ToList().ForEach(p =>
                {
                    changes.Add(new OrderDiff(p.Adhoc, p, 0, p.MadeCount));
                    p.MadeCount = 0;
                });
            NotifyChanges(db, changes, AppName, MyBusiness);
            db.SaveChanges();
            return RedirectToAction("Index", new { date });
        }

        [NonAction]
        public static void NotifyChanges(ChlebyContext db, List<OrderDiff> changes, string appName, Business myBusiness)
        {
            //var dtz = double.Parse(myBusiness.Timezone);
            var user = Membership.GetUser();
            var username = user == null ? "nobody" : user.UserName;
            var changedlist = changes.Where(x => x.OldCount != x.NewCount);
            if (!changedlist.Any()) return;

            foreach (var change in changes)
            {
                var cacheKey = "ordersSnapshotNotify_" + appName + "_" + change.Address + "_" + change.Trader;
                var cache = (List<OrderDiff>)System.Web.HttpContext.Current.Cache[cacheKey];
                if (cache == null)
                {
                    cache = new List<OrderDiff>();
                    System.Web.HttpContext.Current.Cache.Add(
                        cacheKey, cache,
                        null, DateTime.MaxValue, Defines.OrderNotifyPeriod,
                        CacheItemPriority.NotRemovable, (k, v, r) =>
                        {
                            if (!string.IsNullOrWhiteSpace(myBusiness.MailConfirmAddress))
                            {
                                new Mail(db, "prodChange", myBusiness.MailConfirmAddress, new
                                            {
                                                date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                                user = username,
                                                changes = (IEnumerable<OrderDiff>)v
                                            }).Send();
                            }

                            new Mail(db, "prodChange", changes.First().Email, new
                            {
                                date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                user = username,
                                changes = (IEnumerable<OrderDiff>)v
                            }).Send();
                        });
                }
                cache.Add(change);
            }
        }

        [NeedRead]
        [ChlebyMenu("TotalStanding")]
        public ActionResult WeekSummary()
        {
            var obj = from order in db.Orders.OfType<StandingOrder>()
                      from pos in order.Items
                      where pos.OrderedCount > 0
                      group pos by order.Day
                          into byday
                          select new
                          {
                              day = byday.Key,
                              items = byday.GroupBy(x => x.Item)
                          };

            var obj2 = from order in db.Orders.OfType<StandingOrder>()
                       from pos in order.Items
                       where pos.OrderedCount > 0
                       group pos by pos.Item into byitem
                       select new
                       {
                           day = byitem.Key,
                           addrs = byitem.GroupBy(x => x.Order.Address)
                       };
            ViewBag.byaddr = obj2.ToDictionary(ks => ks.day, es => es.addrs);
            return View(obj.ToDictionary(ks => ks.day, es => es.items));
        }

        [NeedRead]
        [ChlebyMenu("TotalCart")]
        public ActionResult CartSummary()
        {
            //var last = DateTime.ParseExact(
            //        MvcApplication.Settings[db.AppConfig.Name]["Snapshot"]["LastDate"],
            //        "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            
            var last = MyBusiness.LastSnapshotDate ?? DateTime.MinValue;
            ViewBag.Lastsnap = last;

            var future = from order in db.Orders.OfType<AdhocOrder>()
                         where order.Date > last
                         select order.Id;

            var snaps = from snappos in db.SnapshotOrderPositions
                        where snappos.Order is AdhocOrder
                        let order = snappos.Order as AdhocOrder
                        select order.Id;

            var ordersId = future.Union(snaps);

            var obj = from order in db.Orders.OfType<AdhocOrder>()
                      where ordersId.Contains(order.Id)
                      from pos in order.Items
                      //where pos.OrderedCount > 0
                      group pos by order.Date
                          into byday
                          select new
                          {
                              date = byday.Key,
                              byaddr = byday.GroupBy(x => x.Order.Address).
                              Select(x => new
                              {
                                  addr = x.Key,
                                  items = x.GroupBy(y => y.Item)
                              })
                          };
            
            // get standing orders
            var standingOrders = db.Orders.OfType<StandingOrder>().Include("Items").ToList();
            ViewBag.StandingOrders = standingOrders;
            var dict = new Dictionary<int, StandingOrder>();
            var adhocs = db.Orders.OfType<AdhocOrder>().Include("Items").Where(o => ordersId.Contains(o.Id)).ToList();
            IDictionary<DateTime, DateTime> localDates = new Dictionary<DateTime, DateTime>();
            foreach (AdhocOrder adhoc in adhocs)
            {
                // find standing order for this adhoc
                var so = db.Orders.OfType<StandingOrder>().Include("Items").Where(
                    o => o.Day == (int) adhoc.Date.DayOfWeek).FirstOrDefault();
                dict[adhoc.Id] = so;
                localDates[adhoc.Date] = ToClientTime(adhoc.Date);
            }
            ViewBag.LocalDates = localDates;
            ViewBag.StandingOrdersDict = dict;

            return View(obj.ToDictionary(
                ks => ks.date,
                es => es.byaddr.ToDictionary(
                    ks2 => ks2.addr,
                    es2 => es2.items)));
        }
        [NeedRead]
        [ChlebyMenu("TotalOrder")]
        public ActionResult TotalSummary(int week = 0, int year = 0)
        {
            if (year == 0) year = DateTime.Now.Year;
            if (week == 0) week = DateTime.Now.GetWeek();

            var start = WeekHelper.FirstDateOfWeek(year, week, DayOfWeek.Monday);
            var end = start.AddDays(7);
            var sos = from order in db.Orders.OfType<StandingOrder>()
                      from pos in order.Items
                      where pos.OrderedCount > 0
                      group pos by order.Day
                          into byday
                          select byday;

            var sosBA = from order in db.Orders.OfType<StandingOrder>()
                        from pos in order.Items
                        where pos.OrderedCount > 0
                        group pos by pos.Item
                            into g
                            select new
                            {
                                k = g.Key,
                                v = g.GroupBy(x => x.Order.Address)
                            };

            var aos = from order in db.Orders.OfType<AdhocOrder>()
                      where order.Date >= start && order.Date < end
                      from pos in order.Items
                      //where pos.OrderedCount > 0
                      group pos by order.Date
                          into bydate
                          select bydate;

            var aosBA = from order in db.Orders.OfType<AdhocOrder>()
                        where order.Date >= start && order.Date < end
                        from pos in order.Items
                       // where pos.OrderedCount > 0
                        group pos by pos.Item
                            into g
                            select new
                            {
                                k = g.Key,
                                v = g.GroupBy(x => x.Order.Address)
                            };

            DateTime firstAO = DateTime.Now.Date;
            var firstAOquery = (from order in db.Orders.OfType<AdhocOrder>()
                                where order.Date.Year == year
                                select order.Date);
            if (firstAOquery.Any())
                firstAO = firstAOquery.Min();

            ViewBag.sosBA = sosBA.ToDictionary(ks => ks.k, es => es.v);
            ViewBag.aosBA = aosBA.ToDictionary(ks => ks.k, es => es.v);
            ViewBag.week = week;
            ViewBag.year = year;
            ViewBag.date = start;
            ViewBag.firstWeek = firstAO.GetWeek();
            if (MyBusiness.HandwalkStep == 0)
                ViewBag.lastWeek = DateTime.Now.AddDays(Settings["Order"].Get<int>("FuturePeriod")).GetWeek();
            else
                ViewBag.lastWeek = DateTime.Now.GetWeek();
            return View(Tuple.Create(
                sos.ToDictionary(ks => ks.Key, es => es.ToList()),
                aos.ToDictionary(ks => ks.Key, es => es.ToList())
                ));
        }

        [NeedRead]
        public ActionResult Export()
        {
            var obj = from order in db.Orders.OfType<StandingOrder>()
                      from pos in order.Items
                      where pos.OrderedCount > 0
                      select new
                      {
                          day = order.Day + 1,
                          qty = pos.OrderedCount,
                          item = pos.Item.LegacyCode ?? pos.Item.Name,
                          addr = order.Address.LegacyCode ?? pos.Order.Address.Address1
                      };
            var csv = CsvExporter.Export(obj);
            var filedate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[MyBusiness.Timezone]);
            var name = string.Format("orders_{0:yyyyMMddHHmmss}.csv", filedate);
            return File(Encoding.UTF8.GetBytes(csv), "text/csv; charset=utf-8", name);
        }


        public ActionResult SOImport()
        {
            ViewBag.Action = "SOImport";
            ViewBag.DwlAction = "SODwlTemplate";
            ViewBag.What = "standing orders";
            return PartialView("Import");
        }

        [HttpPost]
        public ActionResult SOImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return SOImport(str);
        }

        [HttpPost]
        public ActionResult SOImport(string csv, bool save = false)
        {
            ViewBag.Action = "SOImport";
            ViewBag.DwlAction = "SODwlTemplate";
            ViewBag.What = "standing orders";
            ViewBag.csv = csv;

            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var sos = new List<Tuple<Address, Item, int, int, bool>>();
            var days = new Dictionary<string, int>
                {
                    {"MON", 0},
                    {"TUE", 1},
                    {"WED", 2},
                    {"THU", 3},
                    {"FRI", 4},
                    {"SAT", 5},
                    {"SUN", 6}
                };
            foreach (var tline in lines)
            {
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 6)
                {
                    errors.Add(Tuple.Create(curline, -1, "wrong field count"));
                    continue;
                }

                var itemname = fields[0].Trim().Normalize(NormalizationForm.FormKD);
                if (string.IsNullOrWhiteSpace(itemname))
                {
                    errors.Add(Tuple.Create(curline, 1, "empty item"));
                    continue;
                }

                var item = db.Items.FirstOrDefault(x => x.Name == itemname);
                if (item == null)
                {
                    errors.Add(Tuple.Create(curline, 1, "item not found"));
                    continue;
                }

                int qty;
                if (!int.TryParse(fields[1], out qty))
                {
                    errors.Add(Tuple.Create(curline, 2, "invalid qty"));
                    continue;
                }
                if (qty < 0)
                {
                    errors.Add(Tuple.Create(curline, 2, "negative qty"));
                    continue;
                }

                var custname = fields[2].Trim().Normalize(NormalizationForm.FormKD);
                var addrname = fields[3].Trim().Normalize(NormalizationForm.FormKD);

                var addr = db.Addresses.FirstOrDefault(x => x.Address1 == addrname && x.Trader.PrimaryName == custname);
                if (addr == null)
                {
                    errors.Add(Tuple.Create(curline, 3, "invalid address/customer"));
                    continue;
                }

                var day = fields[4].Trim().Normalize(NormalizationForm.FormKD);
                if (!days.ContainsKey(day))
                {
                    errors.Add(Tuple.Create(curline, 5, "invalid day"));
                    continue;
                }

                var deliv = fields[5].Trim().Normalize(NormalizationForm.FormKD);
                if (deliv != "Y" && deliv != "N")
                {
                    errors.Add(Tuple.Create(curline, 6, "invalid delivery"));
                    continue;
                }

                //addr, item, qty, day, deliv
                sos.Add(Tuple.Create(addr, item, qty, days[day], deliv == "Y"));
            }

            if (errors.Count == 0 && save)
            {
                var i = 0;
                var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)db).ObjectContext;
                objCtx.ExecuteStoreCommand("DELETE FROM [Orders] WHERE [Discriminator]='StandingOrder'");
                foreach (var byaddr in sos.GroupBy(x=>x.Item1))
                {
                    for (int day = 0; day < 7;++day)
                    {
                        var byday = byaddr.Where(x => x.Item4 == day);
                        var order = new StandingOrder();
                        order.Address_Id = byaddr.Key.Id;
                        order.Day = day;
                        order.Delivery = false;
                        if (byday.Any())
                            order.Delivery = byday.First().Item5;
                        order.Items = new Collection<OrderPosition>();
                        foreach (var item in byday)
                        {
                            order.Items.Add(new OrderPosition
                                {
                                    Adhoc = false,
                                    Item_Id = item.Item2.Id,
                                    OrderedCount = item.Item3
                                });
                        }
                        db.Orders.Add(order);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.errors = errors;
            return PartialView("Import");
        }

        public ActionResult AOImport()
        {
            ViewBag.Action = "AOImport";
            ViewBag.DwlAction = "AODwlTemplate";
            ViewBag.What = "cart orders";
            return PartialView("Import");
        }

        [HttpPost]
        public ActionResult AOImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return AOImport(str);
        }

        [HttpPost]
        public ActionResult AOImport(string csv, bool save = false)
        {
            ViewBag.Action = "AOImport";
            ViewBag.DwlAction = "AODwlTemplate";
            ViewBag.What = "cart orders";
            ViewBag.csv = csv;

            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var aos = new List<Tuple<Address, Item, int, DateTime, bool>>();
            foreach (var tline in lines)
            {
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length != 6)
                {
                    errors.Add(Tuple.Create(curline, -1, "wrong field count"));
                    continue;
                }

                var itemname = fields[0].Trim().Normalize(NormalizationForm.FormKD);
                if (string.IsNullOrWhiteSpace(itemname))
                {
                    errors.Add(Tuple.Create(curline, 1, "empty item"));
                    continue;
                }

                var item = db.Items.FirstOrDefault(x => x.Name == itemname);
                if (item == null)
                {
                    errors.Add(Tuple.Create(curline, 1, "item not found"));
                    continue;
                }

                int qty;
                if (!int.TryParse(fields[1], out qty))
                {
                    errors.Add(Tuple.Create(curline, 2, "invalid qty"));
                    continue;
                }
                if (qty < 0)
                {
                    errors.Add(Tuple.Create(curline, 2, "negative qty"));
                    continue;
                }

                var custname = fields[2].Trim().Normalize(NormalizationForm.FormKD);
                var addrname = fields[3].Trim().Normalize(NormalizationForm.FormKD);

                var addr = db.Addresses.FirstOrDefault(x => x.Address1 == addrname && x.Trader.PrimaryName == custname);
                if (addr == null)
                {
                    errors.Add(Tuple.Create(curline, 3, "invalid address/customer"));
                    continue;
                }

                var datestr = fields[4].Trim().Normalize(NormalizationForm.FormKD);
                DateTime date;
                if (!DateTime.TryParseExact(datestr, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out date))
                {
                    errors.Add(Tuple.Create(curline, 5, "invalid date"));
                    continue;
                }
                if (date.Date < DateTime.Now.Date)
                {
                    errors.Add(Tuple.Create(curline, 5, "date in past"));
                    continue;
                }

                var deliv = fields[5].Trim().Normalize(NormalizationForm.FormKD);
                if (deliv != "Y" && deliv != "N")
                {
                    errors.Add(Tuple.Create(curline, 6, "invalid delivery"));
                    continue;
                }

                aos.Add(Tuple.Create(addr, item, qty, date, deliv == "Y"));
            }

            if (errors.Count == 0 && save)
            {
                foreach (var ao in aos)
                {
                    var ordermgr = new OrderManager(db, MyBusiness, ao.Item1.TraderId);
                    ordermgr.AddAdhoc(ao.Item1.Id, ao.Item2.Id,
                                      ao.Item4, ao.Item3, ao.Item5, "");
                }
                return RedirectToAction("Index");
            }

            ViewBag.errors = errors;
            return PartialView("Import");
        }

        //duplikat z OO przez chujowosc permow
        public ActionResult DeleteCartOrder(string date, int addrId, int itemId)
        {
            var dt = DateTimeExtensions.FromStringUrl(date);
            var addr = db.Addresses.Find(addrId);

            var ordermgr = new OrderManager(db, MyBusiness, addr.TraderId);
            var pos = db.OrderPositions.Include(x => x.Order).FirstOrDefault(x => x.Item_Id == itemId &&
                                                   x.Order.Address_Id == addrId &&
                                                   (x.Order as AdhocOrder).Date == dt);

            if (pos != null && !db.SnapshotOrderPositions.Any(x => x.Order_Id == pos.Order.Id))
            {
                ordermgr.DeleteAdhocPos(pos.Id);
            }

            return RedirectToAction("CartSummary");
        }

        [NeedWrite]
        public ActionResult SODwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Item*,Qty*,Customer*,Address*,Day*,Delivery (Y/N)*"), "text/csv; charset=utf-8", "StandingOrders.csv");
        }

        [NeedWrite]
        public ActionResult AODwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Item*,Qty*,Customer*,Address*,Date (dd/MM/yyyy)*,Delivery (Y/N)*"), "text/csv; charset=utf-8", "CartOrders.csv");
        }
    }
}


