﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("UoM")]
    [ChlebyMenu("TraderItems")]
    public class UoMController : ChlebyController
    {
        //
        // GET: /UoM/
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.AllUnits = db.Units.OrderBy(x => x.Name).ToList();
        }

        public ActionResult Index()
        {
            var model = db.Units.Where(x => x.Name != "---").ToList();
            return View(model);
        }

        [NeedWrite]
        public ActionResult DeleteUoM(int id)
        {
            var uom = db.Units.Find(id);
            if (uom == null || uom.Name == "---")
                return HttpNotFound();

            var empty = db.Units.First(x => x.Name == "---");
            db.Conversions.Where(c => c.FromId == id || c.ToId == id).ToList().
                ForEach(c => db.Conversions.Remove(c));
            db.Items.Where(x => x.BaseUnitId == id).ToList().
                ForEach(x => x.BaseUnitId = empty.Id);
            db.Units.Remove(uom);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult SetUoM(Dictionary<string, string> uom)
        {
            foreach (var kvp in uom)
            {
                db.Units.Find(int.Parse(kvp.Key)).Name = kvp.Value;
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult CreateUoM(UnitOfMeasure uom)
        {
            if (string.IsNullOrWhiteSpace(uom.Name))
                ModelState.AddModelError("Name", chleby.Web.Resources.Strings.Controller_UOM_Name_Cannot_B_Empty);
            else if (db.Units.Count(x => x.Name == uom.Name) > 0)
                ModelState.AddModelError("Name", chleby.Web.Resources.Strings.Controller_UOM_Unit_Exists);

            if (ModelState.IsValid)
            {
                db.Units.Add(uom);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NewName = uom.Name;

            var model = db.Units.Where(x => x.Name != "---").ToList();

            return View("Index", model);
        }
    }
}
