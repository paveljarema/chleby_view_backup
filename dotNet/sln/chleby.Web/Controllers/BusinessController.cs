﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using AutoMapper;

namespace chleby.Web.Controllers
{

    //19-05-2013
    //  AKCJE z menusów: INDEX -> general settings
    // Company -> company settings
    // Invoices - > invoice/finance settings
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Settings")]
    [ChlebyMenu("AdminSettings")]
    public class BusinessController : ChlebyController
    {

        //
        // GET: /Business/
        [NeedRead]
        public ActionResult Index(FormCollection collection)
        {
            ViewBag.InvoicePeriod = Settings["Default"]["InvoicePeriod"];
            ViewBag.FirstDay = Settings["Invoice"].Get<int>("FirstDay");
            var model = AutoMapper.Mapper.Map<GeneralSettings>(GetBusinessWithTZ());
            return View(model);
        }

        [ChlebyMenu("CompanySettings")]
        public ActionResult Company()
        {
            var model = Mapper.Map<CompanySettings>(MyBusiness);
            return View(model);
        }

        [ChlebyMenu("InvoicingSettings")]
        public ActionResult Invoicing()
        {
            var cvm = new ComboViewModelQuad<PaymentTerm, PaymentTerm, InvoiceSettings>
            {
                Index = db.PaymentTerms.OrderBy(x => x.Order).ToList(),
                Create = null,
                Edit = null,
                Create2 = Mapper.Map<InvoiceSettings>(GetBusinessWithTZ())
            };
            cvm.Create2.InvoicePeriod = Settings["Default"]["InvoicePeriod"];
            cvm.Create2.FirstDay = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
            return View(cvm);
        }

        private Business GetBusinessWithTZ()
        {
            var business = MyBusiness;
            business.CraftingDeadlineLocal = TimeZoneInfo.ConvertTimeFromUtc(business.CraftingDeadline, Defines.Timezones[business.Timezone]);
            return business;
        }

        //
        // POST: /PaymentT/Create

        [HttpPost]
        public ActionResult CreatePayment(PaymentTerm paymentterm)
        {
            if (ModelState.IsValid)
            {
                db.PaymentTerms.Add(paymentterm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return PartialView(paymentterm);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult ManagePaymentTerms()
        {
            var terms = db.PaymentTerms.OrderBy(x => x.Order).ToList();
            ViewBag.ProtectedId = db.AppConfig.DefaultPTId;
            ViewBag.Action = "EditPaymentList";
            ViewBag.Controller = "Business";
            ViewBag.SLUI_Info = Resources.Strings.PaymentTerm_Header;
            return PartialView("_SimpleListUI", terms);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult ManageTT()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var terms = db.TraderTypes.OrderBy(x => x.Name).ToList();
            ViewBag.ProtectedId = db.AppConfig.DefaultTTId;
            ViewBag.Action = "EditTypes";
            ViewBag.Controller = "Business";
            ViewBag.SLUI_Info = Resources.Strings.SLUI_PriceList;
            return PartialView("_SimpleListUI", terms);
        }

        [AllowAnonymous]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Logo()
        {
            if (string.IsNullOrWhiteSpace(MyBusiness.Logo))
            {
                var bmp = Resources.Data.LogoPlaceholder;
                var ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Png);
                ms.Seek(0, SeekOrigin.Begin);
                return File(ms, "image/png");
            }
            else
            {
                var file = Server.MapPath("~/img/" + MyBusiness.Logo);
                return File(file, "application/octet-stream");
            }
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(GeneralSettings general, string hwbeh)
        {
            ModelState.Remove("Name");

            MapperExt.Map(general, MyBusiness);

            if (ModelState.IsValid)
            {
                _log.Debug("Model Valid. Saving settings...");
                //string dtz = MyBusiness.Timezone;
                //MyBusiness.CraftingDeadline = MyBusiness.CraftingDeadlineLocal.AddHours(-dtz);
                //convert to utc
                MyBusiness.CraftingDeadline = TimeZoneInfo.ConvertTimeToUtc(MyBusiness.CraftingDeadlineLocal, Defines.Timezones[MyBusiness.Timezone]);
                _log.Debug("Saving new deadline to cache...");
                HttpContext.Cache[AppName + "__BusinessCraftingDeadline"] = MyBusiness.CraftingDeadline;

                var fn = TrySaveImage(MyBusiness.LogoFile, "Create2.LogoFile");
                if (fn != null)
                    MyBusiness.Logo = fn;

                //masowy update smierdzi kupa
                var inttab = new List<int> { 0, 1, 2, 3, 4, 5, 6 };
                MyBusiness.DeliveryDays.ToList().ForEach(x => inttab.Remove(x));
                foreach (var pos in db.Orders.OfType<StandingOrder>().Where(x => inttab.Contains(x.Day)).SelectMany(order => order.Items))
                    pos.OrderedCount = 0;

                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                //cache crafting time
                var business = db.Businesses.First();
                HttpContext.Cache[AppName + "__Business"] = business;
                //HttpContext.Cache[AppName + "__BusinessCraftingDeadline"] = business.CraftingDeadline;
                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_BusinessController_Save_Alert);
                if (MyBusiness.HandwalkStep > 0)
                {
                    if (hwbeh == "stay")
                        return RedirectToAction("Index");
                    else if (hwbeh == "prev")
                        return RedirectToAction("HandwalkPrev", "Internal");
                    else
                        return RedirectToAction("HandwalkNext", "Internal");
                }
                return RedirectToAction("Index");
            }

            SetAlert(Severity.error, chleby.Web.Resources.Strings.Controller_BusinessController_error);
            return View("Index", general);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult EditInvoice(ComboViewModelQuad<PaymentTerm, PaymentTerm, InvoiceSettings> cvm, string hwbeh)
        {
            ModelState.Remove("Name");

            MapperExt.Map(cvm.Create2, MyBusiness);

            if (ModelState.IsValid)
            {
                Settings["Default"]["InvoicePeriod"] = cvm.Create2.InvoicePeriod;
                Settings["Invoice"]["FirstDay"] = ((int)cvm.Create2.FirstDay).ToString(CultureInfo.InvariantCulture);

                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_BusinessController_Save_Alert);
                if (MyBusiness.HandwalkStep > 0)
                {
                    if (hwbeh == "stay")
                        return RedirectToAction("Index");
                    else if (hwbeh == "prev")
                        return RedirectToAction("HandwalkPrev", "Internal");
                    else
                        return RedirectToAction("HandwalkNext", "Internal");
                }
                return RedirectToAction("Invoicing");
            }

            SetAlert(Severity.error, chleby.Web.Resources.Strings.Controller_BusinessController_error);
            return View("Invoicing", cvm);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult EditCompany(CompanySettings company, string hwbeh)
        {

            if (company.UsingTradingProps)
            {
                if (string.IsNullOrEmpty(company.TradingName))
                    ModelState.AddModelError("TradingName", "empty");
                if (string.IsNullOrEmpty(company.TradingCity))
                    ModelState.AddModelError("TradingCity", "empty");
                if (string.IsNullOrEmpty(company.TradingPost))
                    ModelState.AddModelError("TradingPost", "empty");
            }

            if (ModelState.IsValid)
            {
                MapperExt.Map(company, MyBusiness);
                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_BusinessController_Save_Alert);
                if (MyBusiness.HandwalkStep > 0)
                {
                    if (hwbeh == "stay")
                        return RedirectToAction("Index");
                    else if (hwbeh == "prev")
                        return RedirectToAction("HandwalkPrev", "Internal");
                    else
                        return RedirectToAction("HandwalkNext", "Internal");
                }
                return RedirectToAction("Company");
            }

            SetAlert(Severity.error, chleby.Web.Resources.Strings.Controller_BusinessController_error);
            return View("Company", company);
        }

        public ActionResult DeletePayment(int id = 0)
        {
            PaymentTerm paymentterm = db.PaymentTerms.Find(id);
            db.PaymentTerms.Remove(paymentterm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult DefaultPayment(int id = 0)
        {
            //db.Settings.First(x => x.Name == "Default_PaymentTerm").Value = id.ToString();
            Settings["Default"]["PaymentTerm"] = id.ToString(CultureInfo.InvariantCulture);
            db.SaveChanges();
            return Content("ok");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [NeedWrite]
        public ActionResult EditPaymentList(IEnumerable<PaymentTerm> list)
        {
            var order = 0;

            if (!ModelState.IsValid)
                return RedirectToAction("Index");

            var cod = db.PaymentTerms.Find(db.AppConfig.DefaultPTId);

            var listids = list.Select(i => i.Id).ToList();
            db.PaymentTerms.Where(x => !listids.Contains(x.Id) && x.Id != cod.Id).ToList().ForEach(
                x =>
                {
                    db.Traders.Where(t => t.PayId == x.Id).ToList().
                        ForEach(t => t.PaymentTerms = cod);
                    db.PaymentTerms.Remove(x);
                });

            cod.Order = 0;
            foreach (var item in list)
            {
                if (item.Id == db.AppConfig.DefaultPTId) continue;
                order++;

                if (item.Id != 0)
                {
                    item.Order = order;
                    db.Entry(item).State = EntityState.Modified;
                }
                else
                {
                    item.Order = order;
                    db.PaymentTerms.Add(item);
                }
            }
            ViewBag.ProtectedId = cod.Id;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [NeedRead]
        public ActionResult ExportAllOrders()
        {
            var csv = new OrderManager(db, MyBusiness, 0).GetAllOrdersCSV();

            var filedate = DateTime.UtcNow.AddHours(double.Parse(MyBusiness.Timezone));
            var name = string.Format("orders_{0:yyyyMMddHHmmss}.csv", filedate);
            return File(Encoding.UTF8.GetBytes(csv), "text/csv; charset=utf-8", name);
        }

        [NeedWrite]
        public ActionResult EditTypes(IEnumerable<TraderType> list)
        {
            var order = 0;

            if (!ModelState.IsValid || list == null)
                return RedirectToAction("Index");

            var defType = db.TraderTypes.Find(db.AppConfig.DefaultTTId);

            var listids = list.Select(i => i.Id).ToList();
            db.TraderTypes.Where(x => !listids.Contains(x.Id) && x.Id != defType.Id).ToList().ForEach(
                x =>
                {
                    db.Traders.Where(t => t.TypeId == x.Id).ToList().
                        ForEach(t => t.Type = defType);
                    db.Prices.OfType<TraderTypePrice>().Where(p => p.TraderTypeId == x.Id).
                        ToList().ForEach(p => db.Prices.Remove(p));
                    db.TraderTypes.Remove(x);
                });

            //defType.Order = 0;
            foreach (var item in list)
            {
                if (item.Id == db.AppConfig.DefaultPTId) continue;
                order++;

                if (item.Id != 0)
                {
                    //item.Order = order;
                    db.Entry(item).State = EntityState.Modified;
                }
                else
                {
                    //item.Order = order;
                    db.TraderTypes.Add(item);
                }
            }
            ViewBag.ProtectedId = defType.Id;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [ChlebyMenu("AdminSendInv")]
        public ActionResult Finish()
        {
            var model = new List<FinishVM>();

            var bes = db.BusinessEmployees.OrderBy(x => x.LastName).ThenBy(x => x.LastName).ToList();
            model.AddRange(bes.Select(x => new FinishVM
                {
                    Company = MyBusiness.TradingName ?? MyBusiness.LegalName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Phone = x.Phone,
                    Email = Membership.GetUser(x.ExternalAccountData).Email,
                    Guid = x.ExternalAccountData
                }));

            var tes = db.TraderEmployees.ToList();
            model.AddRange(tes.Select(x => new FinishVM
            {
                Company = x.Trader.Name,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Phone = x.Phone,
                Email = Membership.GetUser(x.ExternalAccountData).Email,
                Guid = x.ExternalAccountData
            }).OrderBy(x => x.Company).ThenBy(x => x.LastName).ThenBy(x => x.LastName));

            return View(model);
        }

        [HttpPost]
        public ActionResult Finish(Dictionary<Guid, bool> send)
        {
            if (MyBusiness.HandwalkStep == 0)
                return RedirectToAction("Index");

            foreach (var kvp in send)
            {
                if (!kvp.Value) continue;
                var user = Membership.GetUser(kvp.Key);
                if (user == null || user == Membership.GetUser()) continue;

                user.IsApproved = false;
                Membership.UpdateUser(user);

                Employee emp = db.BusinessEmployees.FirstOrDefault(x => x.ExternalAccountData == kvp.Key);
                if (emp == null)
                    emp = db.TraderEmployees.FirstOrDefault(x => x.ExternalAccountData == kvp.Key);
                if (emp == null) continue;

                CreateInvitation(user, emp);
            }

            if (MyBusiness.HandwalkStep != 0)
            {
                ViewBag.HandwalkFinish = true;
                MyBusiness.HandwalkStep = 0;
                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

                return View("HandwalkFinish");
            }
            else return RedirectToAction("Finish");
        }

        [NeedRead]
        public ActionResult Templates()
        {
            var model = db.MailTemplates.OrderBy(x => x.Name).ToList();
            return View(model);
        }

        [NeedWrite]
        [ChlebyMenu("MailTemplates")]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult TemplateEdit(int id)
        {
            var mt = db.MailTemplates.Find(id);
            if (mt == null)
                return HttpNotFound();

            return PartialView(mt);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult TemplateEdit(MailTemplate mt)
        {
            var dbmt = db.MailTemplates.AsNoTracking().Single(x => x.Id == mt.Id);
            mt.Name = dbmt.Name;
            mt.Send = dbmt.Send;
            ModelState.Clear();
            TryValidateModel(mt);
            if (ModelState.IsValid)
            {
                db.Entry(mt).State = EntityState.Modified;
                db.SaveChanges();
            }

            // return PartialView(mt);
            return RedirectToAction("Templates");
        }

        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ToggleLive(int id, bool live)
        {
            var item = db.MailTemplates.Find(id);
            if (item == null)
                return Json(new { msg = "wrong id" }, JsonRequestBehavior.AllowGet);
            item.Send = live;

            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return Json(new { msg = "db error - " + e.Message, value = item.Send }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { msg = "ok", value = live }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [NeedWrite]
        public ActionResult UploadLogo(HttpPostedFileBase file)
        {
            var fn = TrySaveImage(file, "file", 200, 200);
            if (fn != null)
                MyBusiness.Logo = fn;

            object ret;
            if (!ModelState.IsValid)
            {
                ret = new { result = false, err = ModelState["file"].Errors.First().ErrorMessage };
            }
            else
            {
                ret = new { result = true, fn };
                db.Entry(MyBusiness).State = EntityState.Modified;
                db.SaveChanges();
                HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();
            }
            return Content(new JavaScriptSerializer().Serialize(ret), "text/html");
        }

        [NeedRead]
        public ActionResult ManageAccSoftware(int id = -1)
        {
            if (id == -1)
                id = MyBusiness.AccSoftware;
            switch (id)
            {
                case 0: //qb
                    return View("QBNothing");
                case 1: //xero
                    return RedirectToAction("Index", "XeroSettings");
            }
            return HttpNotFound();
        }

        [NeedWrite]
        public ActionResult Restore(int id)
        {
            var mt = db.MailTemplates.Find(id);
            if (mt == null) return HttpNotFound();

            var defmt = MvcApplication.DefaultMailTemplates.First(x => x.Name == mt.Name);
            mt.Subject = defmt.Subject;
            mt.Template = defmt.Template;
            db.SaveChanges();

            return RedirectToAction("Templates");
        }

        public ActionResult HandwalkIntro()
        {
            return View("~/Views/Business/HandwalkIntro.cshtml");
        }
    }
}