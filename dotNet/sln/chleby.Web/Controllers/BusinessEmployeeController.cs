﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using AutoMapper;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //ModuleCategory: ADMIN
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Employees")]
    [ChlebyMenu("AdminUsers")]
    public class BusinessEmployeeController : ChlebyController
    {
        //
        // GET: /Employees/

        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModel<BusinessEmployeeAccount>
                          {
                              Index = GetBusinessEmployeeAccounts(),
                              Create = null,
                              Edit = null
                          };
            return View(cvm);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult FavAdminDash(int bemploy, int id_fav)
        {
            db.BusinessEmployees.Find(bemploy).Favourite = id_fav;
            db.SaveChanges();
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            };
        }

        private IEnumerable<BusinessEmployeeAccount> GetBusinessEmployeeAccounts()
        {
            var model = new List<BusinessEmployeeAccount>();
            foreach (var be in db.BusinessEmployees)
            {
                var user = Membership.GetUser(be.ExternalAccountData);
                if (user == null)
                    DebugReport("user=null, id={0}", be.ExternalAccountData);
                else
                    model.Add(new BusinessEmployeeAccount
                                  {
                                      BusinessRole = be.BusinessRole,
                                      FirstName = be.FirstName,
                                      ExternalAccountData = be.ExternalAccountData,
                                      Id = be.Id,
                                      LastName = be.LastName,
                                      Iban = be.Iban,
                                      Phone = be.Phone,
                                      UserName = user.UserName
                                  });
            }
            return model;
        }

        //
        // POST: /Employees/Create

        public ActionResult Create()
        {
            return PartialView();
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult Create(BusinessEmployeeAccount bea)
        {
            ComboViewModel<BusinessEmployeeAccount> cvm;
            if (Membership.GetUser(bea.UserName) != null)
                ModelState.AddModelError("UserName", "User exists");

            if (ModelState.IsValid)
            {
                var password = Membership.GeneratePassword(20, 5);
                try
                {
                    var user = Membership.CreateUser(bea.UserName, password, bea.UserName);
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                    Roles.AddUserToRole(bea.UserName, Defines.Roles.Employee);
                    var be = new BusinessEmployee
                        {
                            BusinessRole = bea.BusinessRole,
                            ExternalAccountData = (Guid)user.ProviderUserKey,
                            FirstName = bea.FirstName,
                            LastName = bea.LastName,
                            Phone = bea.Phone,
                            Iban = bea.Iban
                        };
                    db.BusinessEmployees.Add(be);

                    if (MyBusiness.HandwalkStep == 0) //w handwalku nie wysylamy od razu
                        CreateInvitation(user, be);

                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(chleby.Web.Resources.Strings.Controller_Business_Employee_Error_Creation, e);
                    cvm = new ComboViewModel<BusinessEmployeeAccount>
                    {
                        Index = GetBusinessEmployeeAccounts(),
                        Create = bea,
                        Edit = null
                    };
                    return View("Index", cvm);
                }
                SetAlert(Severity.info, chleby.Web.Resources.Strings.Controller_Business_Employee_Remember_remember + " (" + bea.UserName + ")");
                if (Request.UrlReferrer != null)
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            cvm = new ComboViewModel<BusinessEmployeeAccount>
            {
                Index = GetBusinessEmployeeAccounts(),
                Create = bea,
                Edit = null
            };
            return View("Index", cvm);
        }

        //
        // GET: /Employees/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            BusinessEmployee businessemployee = db.BusinessEmployees.Find(id);
            if (businessemployee == null)
            {
                return HttpNotFound();
            }
            var user = Membership.GetUser(businessemployee.ExternalAccountData);
            if (user == null)
                return HttpNotFound();

            var model = new BusinessEmployeeAccount
            {
                BusinessRole = businessemployee.BusinessRole,
                FirstName = businessemployee.FirstName,
                ExternalAccountData = businessemployee.ExternalAccountData,
                Id = id,
                LastName = businessemployee.LastName,
                Phone = businessemployee.Phone,
                Iban = businessemployee.Iban,
                UserName = user.UserName,
                Password = "DONTEDIT",
                Password2 = "DONTEDIT"
            };
            return PartialView(model);
        }

        //
        // POST: /Employees/Edit/5

        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(int id, BusinessEmployeeAccount bea)
        {
            var oldbe = db.BusinessEmployees.Find(id);

            var user = Membership.GetUser(oldbe.ExternalAccountData);
            if (user == null)
                return HttpNotFound();

            if (user.UserName != bea.UserName)
            {
                var byemail = Membership.GetUserNameByEmail(bea.UserName);
                var byname = Membership.GetUser(bea.UserName);
                if (byemail != null || byname != null)
                {
                    ModelState.AddModelError("UserName", chleby.Web.Resources.Strings.Controller_BusinessController_Email_Used);
                }
            }

            if (ModelState.IsValid)
            {
                oldbe.BusinessRole = bea.BusinessRole;
                oldbe.FirstName = bea.FirstName;
                oldbe.LastName = bea.LastName;
                oldbe.Iban = bea.Iban;
                oldbe.Phone = bea.Phone;
                db.Entry(oldbe).State = EntityState.Modified;
                db.SaveChanges();

                if (bea.Password != "DONTEDIT")
                {
                    user.ChangePassword(user.ResetPassword(), bea.Password);
                }

                if (user.UserName != bea.UserName)
                {
                    user.Email = bea.UserName;
                    Membership.UpdateUser(user);
                    AccountController.ChangeUserName(user.UserName, bea.UserName);
                }

                if (Request.UrlReferrer != null)
                {
                    //return Redirect(Request.UrlReferrer.ToString());
                    return RedirectToAction("Index"); 
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            var cvm = new ComboViewModel<BusinessEmployeeAccount>
            {
                Index = GetBusinessEmployeeAccounts(),
                Create = null,
                Edit = bea,
            };
            return View("Index", cvm);
        }
        //
        // GET: /Employees/Delete/5

        [NeedWrite]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            string userName = User.Identity.Name;
            BusinessEmployee businessemployee = db.BusinessEmployees.Find(id);
            var user = Membership.GetUser(businessemployee.ExternalAccountData);
            if (user == null)
                return HttpNotFound();
            if (Roles.IsUserInRole(user.UserName, Defines.Roles.SuperAdmin))
            {
                SetAlert(Severity.info, chleby.Web.Resources.Strings.BusinessEmpluyee_Index_Deleting_Administrator);
                return RedirectToAction("Index");
            }
            db.BusinessEmployees.Remove(businessemployee);
            db.SaveChanges();
            Membership.DeleteUser(user.UserName);
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [NeedWrite]
        public ActionResult Import()
        {
            ViewBag.What = "users";
            return PartialView();
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return Import(str);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult Import(string csv, bool save = false)
        {
            ViewBag.csv = csv;
            ViewBag.What = "users";
            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var beas = new List<BusinessEmployeeAccount>();
            foreach (var tline in lines)
            {
                var bea = new BusinessEmployeeAccount();
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 6)
                {
                    errors.Add(Tuple.Create(curline, -1, chleby.Web.Resources.Strings.Controller_BusinessEmployee_WrongField));
                    continue;
                }

                if (string.IsNullOrWhiteSpace(fields[0]))
                {
                    errors.Add(Tuple.Create(curline, 1, chleby.Web.Resources.Strings.Controller_BusinessEmployee_emptyemail));
                    continue;
                }

                if (!EmailAddressAttribute.Valid(fields[0]))
                {
                    errors.Add(Tuple.Create(curline, 1, chleby.Web.Resources.Strings.Controller_BusinessEmployee_Invalid_Mail));
                    continue;
                }

                if (Membership.GetUser(fields[0]) != null)
                {
                    //errors.Add(Tuple.Create(curline, 1, chleby.Web.Resources.Strings.Controller_BusinessEmployee_User_Used));
                    continue;
                }

                bea.UserName = fields[0].Trim();

                if (string.IsNullOrWhiteSpace(fields[1]))
                {
                    errors.Add(Tuple.Create(curline, 2, chleby.Web.Resources.Strings.Controller_BusinessEmployee_Empty_Firstname));
                    continue;
                }

                bea.FirstName = fields[1].Trim();

                if (string.IsNullOrWhiteSpace(fields[2]))
                {
                    errors.Add(Tuple.Create(curline, 3, chleby.Web.Resources.Strings.Controller_BusinessEmployee_Empty_Lastname));
                    continue;
                }

                bea.LastName = fields[2].Trim();

                //optional
                bea.Phone = fields[3].Trim();
                bea.BusinessRole = fields[3].Trim();
                bea.Iban = fields[4].Trim();

                beas.Add(bea);
            }

            if (errors.Count == 0 && save)
            {
                foreach (var bea in beas)
                {
                    var be = Mapper.Map<BusinessEmployee>(bea);
                    var user = Membership.CreateUser(bea.UserName, Membership.GeneratePassword(8, 1), bea.UserName);
                    user.IsApproved = false;
                    SetAllRoles(bea.UserName, AppName);
                    Membership.UpdateUser(user);
                    be.ExternalAccountData = (Guid)user.ProviderUserKey;
                    db.BusinessEmployees.Add(be);
                }
                db.SaveChanges();
                SetAlert(Severity.info, chleby.Web.Resources.Strings.Controller_Business_Employee_Remember_remember + "s");
                return RedirectToAction("Index");
            }

            ViewBag.errors = errors;
            return PartialView("Import");
        }

        [NonAction]
        public static void SetAllRoles(string user, string appName)
        {
            var modules = (from um in MvcApplication.UsedModules
                           where um.App.Name == appName
                           select um.Module).Union(
                                (from mr in MvcApplication.ModuleRegister
                                 where mr.IsCore
                                 select mr)).Distinct().ToList();
            foreach (var mod in modules)
            {
                try { Roles.AddUserToRole(user, mod.Name + Defines.Roles.WriteSuffix); }
                catch { }
                try { Roles.AddUserToRole(user, mod.Name + Defines.Roles.ReadSuffix); }
                catch { }
            }
        }

        [NeedWrite]
        public ActionResult DwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Email*,First name*,Last Name*,Telephone,Role,Iban
"), "text/csv; charset=utf-8","ClientUsers.csv");
        }
    }
}