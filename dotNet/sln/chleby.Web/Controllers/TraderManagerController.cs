﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using chleby.Web.Models;
using System.Web.Routing;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("TraderManager")]
    public class TraderManagerController : ChlebyController
    {


        //
        // GET: /TraderManager/

        public ActionResult Index()
        {
            var tradermanagers = db.TraderManagers.Include(t => t.Trader);
            return View(tradermanagers.ToList());
        }

        //
        // GET: /TraderManager/Details/5

        public ActionResult Details(int id = 0)
        {
            TraderManager tradermanager = db.TraderManagers.Find(id);
            if (tradermanager == null)
            {
                return HttpNotFound();
            }
            return View(tradermanager);
        }

        //
        // GET: /TraderManager/Create

        public ActionResult Create()
        {

            ViewBag.TraderId = new SelectList(db.Traders, "Id", "Name");
            return View();
        }

        //
        // POST: /TraderManager/Create

        [HttpPost]
        public ActionResult Create(TraderManager tradermanager)
        {
            if (ModelState.IsValid)
            {
                tradermanager.Date = DateTime.UtcNow;
                db.TraderManagers.Add(tradermanager);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TraderId = new SelectList(db.Traders, "Id", "Name", tradermanager.TraderId);
            return View(tradermanager);
        }

        //
        // GET: /TraderManager/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TraderManager tradermanager = db.TraderManagers.Find(id);
            if (tradermanager == null)
            {
                return HttpNotFound();
            }
            ViewBag.TraderId = new SelectList(db.Traders, "Id", "Name", tradermanager.TraderId);
            return View(tradermanager);
        }

        //
        // POST: /TraderManager/Edit/5

        [HttpPost]
        public ActionResult Edit(TraderManager tradermanager)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tradermanager).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TraderId = new SelectList(db.Traders, "Id", "Name", tradermanager.TraderId);
            return View(tradermanager);
        }

        //
        // GET: /TraderManager/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TraderManager tradermanager = db.TraderManagers.Find(id);
            if (tradermanager == null)
            {
                return HttpNotFound();
            }
            return View(tradermanager);
        }

        //
        // POST: /TraderManager/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TraderManager tradermanager = db.TraderManagers.Find(id);
            db.TraderManagers.Remove(tradermanager);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}