﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    [ChlebyModule("Settings")]
    public class XeroSettingsController : ChlebyController
    {
        public ActionResult Index()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var taxes = db.XeroTaxMappings.OrderBy(x => x.Name).ToList();
            ViewBag.ProtectedId = 0;
            ViewBag.Action = "EditTaxes";
            ViewBag.Controller = "XeroSettings";
            var model = new XeroSettings
                {
                    Config = MyBusiness.XeroConfig,
                    Taxes = taxes
                };
            return View(model);
        }

        [NeedWrite]
        [HttpPost]
        public ActionResult Index(XeroSettings model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (model.Taxes == null)
            {
                model.Taxes = new List<XeroTaxMapping>();
            }
            var listids = model.Taxes.Select(i => i.Id).ToList();
                db.XeroTaxMappings.Where(x => !listids.Contains(x.Id)).ToList().
                   ForEach(x => db.XeroTaxMappings.Remove(x));

            foreach (var item in model.Taxes)
            {
                if (item.Id != 0)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                else
                {
                    db.XeroTaxMappings.Add(item);
                }
            }

            MyBusiness.XeroConfig = model.Config;
            db.Entry(MyBusiness).State = EntityState.Modified;
            db.SaveChanges();
            HttpContext.Cache[AppName + "__Business"] = db.Businesses.First();

            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
