﻿using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Transactions;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using AutoMapper;
using Ionic.Zip;
using chleby.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using MigraDoc;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using WebGrease.Css.Extensions;
using System.Reflection;
using chleby.Web.Resources;
using System.Data.Objects;
using chleby.Web.ViewModel;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Invoices")]
    [ChlebyMenu("AdminFinance")]
    public class InvoicesController : ChlebyController
    {
        private string _invoicePeriod = "u";
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            var yearsSnapshot = db.SnapshotOrderPositions.
                Select(x => SqlFunctions.DatePart("yyyy", x.EndDate)).
                Distinct().ToList();

            var yearsInvoice = db.Invoices.Select(x => x.Year).Distinct().ToList();
            var years = new List<int?>(yearsSnapshot);
            years.AddRange(yearsInvoice.Select(year => (int?)year));

            ViewBag.yearsInvoice = yearsInvoice;
            ViewBag.yearsSnapshot = yearsSnapshot;
            ViewBag.years = years.Distinct();
            ViewBag.traders = db.Traders.ToList();
            ViewBag.DeliveryId = db.AppConfig.DeliveryId;

            if (RouteData.Values["invoiceperiod"] == null || RouteData.Values["invoiceperiod"].ToString() == "u")
            {
                _invoicePeriod = Settings["Default"]["InvoicePeriod"];
            }
            else
            {
                _invoicePeriod = RouteData.Values["invoiceperiod"].ToString();
            }
            ViewBag.invoicePeriod = _invoicePeriod;
        }

        private ActionResult IndexWeek(int? year)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            var model = GetIndexWeek(nowyear);
            ViewBag.curyear = nowyear;

            return View("IndexWeek", model.ToList());
        }

        private List<InvoiceWeekSummary> GetIndexWeek(int nowyear)
        {
            var deliveryId = db.AppConfig.DeliveryId;

            var model = db.SnapshotOrderPositions.
                           Where(x => SqlFunctions.DatePart("yyyy", x.EndDate) == nowyear).ToList().
                           GroupBy(x => CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                               x.EndDate,
                               CalendarWeekRule.FirstDay,
                               (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"))).
                           OrderBy(x => x.Key).
                           Select(x => new
                               {
                                   x.Key,
                                   Count = x.GroupBy(y => y.Address.TraderId).Count(),
                                   Price = x.Where(y => y.Item_Id != deliveryId).
                                             Sum(y => y.Price * y.MadeCount),
                                   Vat = x.Sum(y => y.Price * y.MadeCount * y.Vat / 100),
                                   DelPrice = x.Where(y => y.Item_Id == deliveryId).
                                                Sum(y => y.Price * y.MadeCount)
                               }).
                           AsEnumerable().Select(x => new InvoiceWeekSummary
                               {
                                   Week = x.Key,
                                   Count = x.Count,
                                   Price = x.Price,
                                   Delivery = x.DelPrice,
                                   Vat = x.Vat
                               }).ToList();
            return model;
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        private ActionResult PublishedWeek(int? year, bool? published)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            ViewBag.curyear = nowyear;
            var model = GetPublishedWeekInfo(published, nowyear);

            return View("IndexWeekPublished", model.ToList());
        }

        private IEnumerable<InvoiceWeekSummary> GetPublishedWeekInfo(bool? published, int nowyear)
        {
            var hasValue = published.HasValue;
            var model = db.Invoices.
                           Where(x => x.Year == nowyear && !x.Cloned && !x.Deleted && (!hasValue || x.Published == published) && !x.MyClones.Any())
                          .ToList()
                          .GroupBy(x => CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                               x.Date,
                               CalendarWeekRule.FirstDay,
                               (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"))).
                           Select(x => new InvoiceWeekSummary
                               {
                                   Week = x.Key,
                                   Price = x.Sum(y => y.ItemsPrice),
                                   Delivery = x.Sum(y => y.DeliveryPrice),
                                   Vat = x.Sum(y => y.DeliveryVat + y.Vat),
                                   Count = x.Count()
                               });
            return model;
        }

        private IEnumerable<InvoiceWeekSummary> GetPublishedDayInfo(bool? published, int nowyear)
        {
            var hasValue = published.HasValue;
            var model = db.Invoices.
                           Where(x => x.Year == nowyear && !x.Cloned && !x.Deleted && (!hasValue || x.Published == published) && !x.MyClones.Any())
                          .ToList()
                          .GroupBy(x => x.Date).
                           Select(x => new InvoiceWeekSummary
                           {
                               Day = x.Key,
                               DayLocal = ToClientTime(x.Key),
                               Price = x.Sum(y => y.ItemsPrice),
                               Delivery = x.Sum(y => y.DeliveryPrice),
                               Vat = x.Sum(y => y.DeliveryVat + y.Vat),
                               Count = x.Count()
                           });
            return model;
        }


        private ActionResult IndexMonth(int? year)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            var model = GetIndexMonth(nowyear);
            ViewBag.curyear = nowyear;

            return View("IndexMonth", model.ToList());
        }

        private ActionResult IndexDay(int? year)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            var model = GetIndexDay(nowyear);
            ViewBag.curyear = nowyear;

            return View("IndexDay", model.ToList());
        }


        private List<InvoiceWeekSummary> GetIndexMonth(int nowyear)
        {
            var deliveryId = db.AppConfig.DeliveryId;

            var model = db.SnapshotOrderPositions.
                           Where(x => SqlFunctions.DatePart("yyyy", x.EndDate) == nowyear).
                           GroupBy(x => SqlFunctions.DatePart("mm", x.EndDate)).
                           OrderBy(x => x.Key).
                           Select(x => new
                               {
                                   x.Key,
                                   Count = x.GroupBy(y => y.Address.TraderId).Count(),
                                   Price = x.Where(y => y.Item_Id != deliveryId).
                                             Sum(y => y.Price * y.MadeCount),
                                   Vat = x.Sum(y => y.Price * y.MadeCount * y.Vat / 100),
                                   DelPrice = x.Where(y => y.Item_Id == deliveryId).
                                                Sum(y => y.Price * y.MadeCount)
                               }).
                           AsEnumerable().Select(x => new InvoiceWeekSummary
                               {
                                   Month = x.Key,
                                   Count = x.Count,
                                   Price = x.Price,
                                   Delivery = x.DelPrice,
                                   Vat = x.Vat
                               }).ToList();
            return model;
        }


        private IEnumerable<InvoiceWeekSummary> GetIndexDay(int nowyear)
        {
            var deliveryId = db.AppConfig.DeliveryId;

            var model = db.SnapshotOrderPositions.
                           Where(x => SqlFunctions.DatePart("yyyy", x.EndDate) == nowyear).
                           GroupBy(x => EntityFunctions.TruncateTime(x.EndDate)).
                           OrderBy(x => x.Key).
                           Select(x => new
                               {
                                   x.Key,
                                   Count = x.GroupBy(y => y.Address.TraderId).Count(),
                                   Price = (decimal?) x.Where(y => y.Item_Id != deliveryId).
                                             Sum(y => y.Price * y.MadeCount),
                                   Vat = x.Sum(y => y.Price * y.MadeCount * y.Vat / 100),
                                   DelPrice = (decimal?)x.Where(y => y.Item_Id == deliveryId).
                                                Sum(y => y.Price * y.MadeCount)
                               });
            var enumer = model.
                           AsEnumerable().Select(x => new InvoiceWeekSummary
                           {
                               Day = x.Key,
                               DayLocal = ToClientTime(x.Key),
                               Count = x.Count,
                               Price = x.Price ?? 0,
                               Delivery = x.DelPrice ?? 0,
                               Vat = x.Vat
                           }).ToList();

            foreach (InvoiceWeekSummary summary in enumer)
            {
                DateTime? utc = summary.Day;
                if (utc != null)
                {
                    DateTime local = ToClientTime(utc.Value);
                    summary.DayLocal = local;
                }
            }

            return enumer;
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        private ActionResult PublishedMonth(int? year, bool? published)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            var model = GetPublishedMonthInfo(published, nowyear);

            ViewBag.curyear = nowyear;

            return View("IndexMonthPublished", model.ToList());
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        private ActionResult PublishedDay(int? year, bool? published)
        {
            var nowyear = DateTime.Now.Year;
            if (year.HasValue)
                nowyear = year.Value;

            var model = GetPublishedDayInfo(published, nowyear);

            ViewBag.curyear = nowyear;

            return View("IndexDayPublished", model.ToList());
        }


        private IQueryable<InvoiceWeekSummary> GetPublishedMonthInfo(bool? published, int nowyear)
        {
            var hasValue = published.HasValue;
            var model = db.Invoices.
                           Where(x => !x.Cloned && !x.Deleted && x.Year == nowyear && (!hasValue || x.Published == published) && !x.MyClones.Any()).
                           GroupBy(x => x.Date.Month).
                           Select(x => new InvoiceWeekSummary
                               {
                                   Month = x.Key,
                                   Price = x.Sum(y => y.ItemsPrice),
                                   Delivery = x.Sum(y => y.DeliveryPrice),
                                   Vat = x.Sum(y => y.DeliveryVat + y.Vat),
                                   Count = x.Count()
                               });
            return model;
        }

        [NeedRead]
        public ActionResult Index(int? year)
        {
            switch (_invoicePeriod)
            {
                case "w":
                    return IndexWeek(year);
                case "m":
                    return IndexMonth(year);
                case "d":
                    return IndexDay(year);
                default:
                    return IndexWeek(year);
            }
        }

        [NeedRead]
        public ActionResult Dashboard11(int? year, int? curyear)
        {
            if (year == null)
                year = DateTime.Now.Year;

            ViewBag.curyear = year;
            ViewBag.year = year;
            switch (_invoicePeriod)
            {
                case "w":
                    return PartialView(GetIndexWeek(year ?? DateTime.Now.Year));
                case "m":
                    return PartialView(GetIndexMonth(year ?? DateTime.Now.Year));
                default:
                    return PartialView(GetIndexWeek(year ?? DateTime.Now.Year));
            }
        }

        [NeedRead]
        public ActionResult Dashboard12(string dtstart, string dtend)
        {
            DateTime start;
            DateTime end;
            ActionResult actionResult;
            if (dahsboardinit(dtstart, dtend, out start, out end, out actionResult)) return actionResult;
            return PartialView(PDFs.GetSalesReportInvoiceData(db, start, end, null, null));
        }

        private bool dahsboardinit(string dtstart, string dtend, out DateTime start, out DateTime end,
                                   out ActionResult actionResult)
        {
            end = start = DateTime.Now;
            actionResult = null;
            IEnumerable<Tuple<int, int>> weeks = ViewBag.Weeks = GetWeeksPublished(null);
            IEnumerable<Tuple<int, int>> months = ViewBag.Months = GetMonthsPublished(null);
            IDictionary<DateTime, DateTime> days = ViewBag.Days = GetDaysPublished(null);

            var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");

            if (dtstart == null)
            {
                switch (_invoicePeriod)
                {
                    default:
                    case "w":
                        {
                            var fw = weeks.FirstOrDefault();
                            if (fw == null)
                            {
                                actionResult = HttpNotFound();
                                return true;
                            }
                            dtstart = WeekHelper.FirstDateOfWeek(fw.Item1, fw.Item2, dow).ToStringUrl();
                            dtend = WeekHelper.FirstDateOfWeek(fw.Item1, fw.Item2, dow).AddDays(6).ToStringUrl();
                        }
                        break;
                    case "m":
                        {
                            var fw = months.FirstOrDefault();
                            if (fw == null)
                            {
                                actionResult = HttpNotFound();
                                return true;
                            }
                            dtstart = new DateTime(fw.Item1, fw.Item2, 1).ToStringUrl();
                            dtend = new DateTime(fw.Item1, fw.Item2, WeekHelper.GetMonthLen(fw.Item2, fw.Item1)).ToStringUrl();
                        }
                        break;
                    case "d":
                        {
                            var fw = days.FirstOrDefault();
                            if (fw.Key == default(DateTime))
                            {
                                actionResult = HttpNotFound();
                                return true;
                            }
                            dtstart = fw.Key.ToStringUrl();
                            dtend = fw.Key.ToStringUrl();
                        }
                        break;
                }
            }
            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            start = DateTimeExtensions.FromStringUrl(dtstart);
            ViewBag.startdate = start;
            end = DateTimeExtensions.FromStringUrl(dtend);
            ViewBag.enddate = end;

            return false;
        }

        [NeedRead]
        public ActionResult Dashboard21(string dtstart, string dtend)
        {
            ViewBag.Weeks = GetWeeksPublished(null);
            ViewBag.Months = GetMonthsPublished(null);
            ViewBag.Days = GetDaysPublished(null);
            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            var start = DateTimeExtensions.FromStringUrl(dtstart);
            ViewBag.startdate = start;
            var end = DateTimeExtensions.FromStringUrl(dtend);
            ViewBag.enddate = end;
            return PartialView(PDFs.GetItemReportInvoiceData(db, start, end, null).ToList());
        }

        [NeedRead]
        public ActionResult Dashboard22(string period)
        {
            if (string.IsNullOrWhiteSpace(period))
                period = _invoicePeriod;
            var cutoff = DateTime.Now.AddMonths(-11);
            cutoff = new DateTime(cutoff.Year, cutoff.Month, 1);
            switch (period) //no coz, sql mowi spierdalaj ;x
            {
                case "m":
                default:
                    {
                        var data = from inv in db.Invoices
                                   where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                   group inv by SqlFunctions.DatePart("mm", inv.Date)
                                       into bymonth
                                       select new
                                           {
                                               k = bymonth.Key,
                                               v = bymonth.Sum(x => x.TotalPrice)
                                           };
                        return Json(data.ToDictionary(ks => CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[ks.k.Value], es => es.v), "text/html",
                                    JsonRequestBehavior.AllowGet);
                    }
                case "w":
                    {
                        var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
                        var data = from localinv in
                                       (
                                           from inv in db.Invoices
                                           where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                           select new
                                            {
                                                inv.Date,
                                                inv.TotalPrice
                                            }).ToList()
                                   group localinv by CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                                                       localinv.Date,
                                                       CalendarWeekRule.FirstDay, dow)
                                       into bymonth
                                       select new
                                       {
                                           k = bymonth.Key,
                                           v = bymonth.Sum(x => x.TotalPrice)
                                       };
                        return Json(data.ToDictionary(ks => Strings.Global_Week_Short + " " + ks.k.ToString(CultureInfo.InvariantCulture), es => es.v), "text/html",
                                    JsonRequestBehavior.AllowGet);
                    }
                case "d":
                    {
                        var data = from inv in db.Invoices
                                   where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                   group inv by inv.Date
                                       into bymonth
                                       select new
                                       {
                                           k = bymonth.Key,
                                           v = bymonth.Sum(x => x.TotalPrice)
                                       };
                        return Json(data.ToDictionary(ks => ks.k.ToString("d"), es => es.v), "text/html",
                                    JsonRequestBehavior.AllowGet);
                    }
            }
        }

        [NeedRead]
        public ActionResult Dashboard22i(string period)
        {
            if (string.IsNullOrWhiteSpace(period))
                period = _invoicePeriod;
            var cutoff = DateTime.Now.AddMonths(-11);
            cutoff = new DateTime(cutoff.Year, cutoff.Month, 1);
            switch (period) //no coz, sql mowi spierdalaj ;x
            {
                case "m":
                default:
                    {
                        var data = from row in db.InvoiceRows
                                   let inv = row.Invoice
                                   where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                   group row by row.Text
                                       into bytext
                                       select new
                                           {
                                               k = bytext.Key,
                                               v = from row in bytext
                                                   let inv = row.Invoice
                                                   group row by SqlFunctions.DatePart("mm", inv.Date)
                                                       into bymonth
                                                       select new
                                                           {
                                                               k = bymonth.Key,
                                                               v = bymonth.Sum(x => x.Price + x.Vat),
                                                           }
                                           };

                        return Json(data.ToDictionary(ks => ks.k.ToString(CultureInfo.InvariantCulture),
                                                      es => es.v.ToDictionary(ks2 => CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[ks2.k.Value], es2 => es2.v)),
                                    "text/html", JsonRequestBehavior.AllowGet);
                    }
                case "w":
                    { //TODO: wykminic jak uwzglednic nasz chujowy tydzien
                        var data = from row in db.InvoiceRows
                                   let inv = row.Invoice
                                   where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                   group row by row.Text
                                       into bytext
                                       select new
                                       {
                                           k = bytext.Key,
                                           v = from row in bytext
                                               let inv = row.Invoice
                                               group row by SqlFunctions.DatePart("wk", inv.Date)
                                                   into bymonth
                                                   select new
                                                   {
                                                       k = bymonth.Key,
                                                       v = bymonth.Sum(x => x.Price + x.Vat),
                                                   }
                                       };

                        return Json(data.ToDictionary(ks => ks.k.ToString(CultureInfo.InvariantCulture),
                                                      es => es.v.ToDictionary(ks2 => Resources.Strings.Global_Week_Short + " " + ks2.k.ToString(), es2 => es2.v)),
                                    "text/html", JsonRequestBehavior.AllowGet);
                    }
                case "d":
                    {
                        var data = from row in db.InvoiceRows
                                   let inv = row.Invoice
                                   where !inv.MyClones.Any() && inv.Date >= cutoff && inv.Published
                                   group row by row.Text
                                       into bytext
                                       select new
                                       {
                                           k = bytext.Key,
                                           v = from row in bytext
                                               let inv = row.Invoice
                                               group row by inv.Date
                                                   into bymonth
                                                   select new
                                                   {
                                                       k = bymonth.Key,
                                                       v = bymonth.Sum(x => x.Price + x.Vat),
                                                   }
                                       };

                        return Json(data.ToDictionary(ks => ks.k.ToString(CultureInfo.InvariantCulture),
                                                      es => es.v.ToDictionary(ks2 => ks2.k.ToString("d"), es2 => es2.v)),
                                    "text/html", JsonRequestBehavior.AllowGet);
                    }
            }
        }

        public ActionResult Dashboard31(string dtstart, string dtend, bool? published)
        {
            DateTime start;
            DateTime end;
            ActionResult actionResult;
            if (dahsboardinit(dtstart, dtend, out start, out end, out actionResult)) return actionResult;
            return PartialView(PDFs.GetItemReportInvoiceData(db, start, end, published));
        }

        public ActionResult Dashboard32(bool? published, int year = 0)
        {
            if (year == 0)
                year = DateTime.Now.Year;

            IEnumerable<InvoiceWeekSummary> model;
            switch (_invoicePeriod)
            {
                case "w":
                default:
                    model = GetPublishedWeekInfo(published, year);
                    break;
                case "m":
                    model = GetPublishedMonthInfo(published, year);
                    break;
            }
            ViewBag.year = year;
            ViewBag.published = published;
            return PartialView(model);
        }

        [NeedRead]
        public ActionResult Published(int? year, bool? published)
        {
            ViewBag.published = published;
            switch (_invoicePeriod)
            {
                case "w":
                    return PublishedWeek(year, published);
                case "m":
                    return PublishedMonth(year, published);
                case "d":
                    return PublishedDay(year, published);
                default:
                    return PublishedWeek(year, published);
            }
        }

        private IEnumerable<Tuple<int, int>> GetWeeks()
        {
            var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
            return (from pos in db.SnapshotOrderPositions
                    where pos.MadeCount > 0
                    select pos.EndDate).ToList().Select(pos => new
                    {
                        year = pos.Year,
                        week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                               pos.Date, CalendarWeekRule.FirstDay, dow)
                    }).Distinct().
                    OrderByDescending(x => x.year).
                    ThenByDescending(x => x.week).ToList().
                    Select(x => new Tuple<int, int>(x.year, x.week));
        }

        private IEnumerable<Tuple<int, int>> GetWeeksPublished(bool? published)
        {
            var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
            var hasValue = published.HasValue;
            return (from pos in db.Invoices
                    where !pos.Cloned && !pos.Deleted && (!hasValue || pos.Published == published) && !pos.MyClones.Any()
                    select pos.Date).ToList().Select(pos => new
                    {
                        year = pos.Year,
                        week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                               pos.Date, CalendarWeekRule.FirstDay, dow)
                    }).Distinct().
                 OrderByDescending(x => x.year).
                 ThenByDescending(x => x.week).ToList().
                 Select(x => new Tuple<int, int>(x.year, x.week));
        }

        private IEnumerable<Tuple<int, int>> GetMonths()
        {
            return (from pos in db.SnapshotOrderPositions
                    where pos.MadeCount > 0
                    select new
                    {
                        year = SqlFunctions.DatePart("yyyy", pos.EndDate),
                        month = SqlFunctions.DatePart("mm", pos.EndDate),
                    }).Distinct().
                    OrderByDescending(x => x.year).
                    ThenByDescending(x => x.month).ToList().
                    Select(x => new Tuple<int, int>(x.year.Value, x.month.Value));
        }

        private IEnumerable<Tuple<int, int>> GetMonthsPublished(bool? published)
        {
            var hasValue = published.HasValue;
            return (from pos in db.Invoices
                    where !pos.Cloned && !pos.Deleted && (!hasValue || pos.Published == published) && !pos.MyClones.Any()
                    select new
                    {
                        year = SqlFunctions.DatePart("yyyy", pos.Date),
                        month = SqlFunctions.DatePart("mm", pos.Date),
                    }).Distinct().
                 OrderByDescending(x => x.year).
                 ThenByDescending(x => x.month).ToList().
                 Select(x => new Tuple<int, int>(x.year.Value, x.month.Value));
        }

        [NeedRead]
        public ActionResult Show(string dtstart, string dtend)
        {
            if (dtend == "auto")
                dtend = dtstart;
            if (dtstart == null || dtend == null)
                return RedirectToAction("Index");
            dtstart = dtstart.Replace("/", "");
            dtend = dtend.Replace("/", "");

            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var model = GetSnapshotSummary(db, start, end);
            foreach (var iwd in model)
            {
                var dmgitems = db.DmgItems.Where(x => x.AddressId == iwd.Address.Id);
                if (dmgitems.Any(d => d.AddressId == iwd.Address.Id &&
                    d.DeliveryDate >= start && d.DeliveryDate <= end))
                    iwd.HasComplaints = true;
            }

            ViewBag.totalsum = model.Sum(x => x.ItemPrice + x.DeliveryPrice + x.VatPrice);
            ViewBag.totalitems = model.Sum(x => x.ItemPrice);
            ViewBag.totaldel = model.Sum(x => x.DeliveryPrice);
            ViewBag.totalvat = model.Sum(x => x.VatPrice);
            ViewBag.LastNumber = Settings["Invoice"]["LastNumber"];
            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            ViewBag.startdate = start;
            ViewBag.enddate = end;

            ViewBag.StartDateLocal = ToClientTime(start);
            ViewBag.EndDateLocal = ToClientTime(end);

            switch (_invoicePeriod)
            {
                case "d":
                    ViewBag.daysDict = GetDays();
                    break;
                case "m":
                    ViewBag.months = GetMonths();
                    break;
                case "w":
                    ViewBag.weeks = GetWeeks();
                    break;
            }

            return View("Show_Range", model.ToList());
        }

        public static List<InvoiceWeekData> GetSnapshotSummary(ChlebyContext db, DateTime start, DateTime end)
        {
            var deliveryId = db.AppConfig.DeliveryId;

            var model = db.SnapshotOrderPositions.
                Where(x => EntityFunctions.TruncateTime(x.EndDate) >= EntityFunctions.TruncateTime(start) 
                    && EntityFunctions.TruncateTime(x.EndDate) <= EntityFunctions.TruncateTime(end) 
                    && x.Item_Id != deliveryId).
                GroupBy(x => x.Address).
                Select(x => new
                {
                    Price = x.Sum(i => i.Price * i.MadeCount),
                    Vat = x.Sum(i => i.Price * i.MadeCount * i.Vat / 100),
                    Address = x.Key,
                    ZP = x.Any(i => i.Price == 0),
                    MC = x.Any(i => i.MadeCount != i.OrderedCount),
                    NO = x.Any(i => i.Order == null)
                }).AsEnumerable().
                Select(x => new InvoiceWeekData
                {
                    Address = x.Address,
                    ItemPrice = x.Price,
                    VatPrice = x.Vat,
                    HasZeroPrice = x.ZP,
                    HasManualChanges = x.MC,
                    HasNoOrder = x.NO
                }).ToList();

            var delivs = db.SnapshotOrderPositions.
                Where(x => EntityFunctions.TruncateTime(x.EndDate) >= EntityFunctions.TruncateTime(start)
                    && EntityFunctions.TruncateTime(x.EndDate) <= EntityFunctions.TruncateTime(end)
                    && x.Item_Id == deliveryId).Select(
                               x => new { x.Address_Id, x.MadeCount, x.Price, x.Vat });
            foreach (var deliv in delivs)
            {
                var iwd = model.FirstOrDefault(x => x.Address.Id == deliv.Address_Id);
                if (iwd == null) continue;
                iwd.DeliveryPrice += deliv.MadeCount * deliv.Price;
                iwd.VatPrice += deliv.MadeCount * deliv.Price * deliv.Vat / 100;
            }
            return model;
        }

        //THIS IS VERY WRONG
        //a to dlaczego:
        //   --- http request ---
        //1) linki leca do showpublished(start,end, published? [, redirect=true])
        //2) ta akcja gdy redirect=true robi redirecta na:
        //2) a) showunpublished(start,end) <- published=false
        //2) b) showppublished(start,end) <- published=true
        //2) c) showsalesdata(start,end) <- published=null
        //   --- http request ---
        //3) akcje a)b)c) jedyne co robia to wolaja pierwotna akcje z redirect=false
        //4) akcja pierwotna robi cala robote
        //
        // wiec podsumowujac, zeby zmienic urle na rozne i jednoczesnie nie kopiowac kodu
        // posunalem sie do jednego zewnetrzenego i jednego wewnetrznego redirecta ...
        // a wszystko po to zeby axol mogl sobie podmienic po jednym slowie w helptexcie do nich ...
        //
        // a do tego menu jest ustawione z dupy ale who cares

        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult ShowUnpublished(string dtstart, string dtend)
        {
            return ShowPublished(dtstart, dtend, false, false);
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult ShowPPublished(string dtstart, string dtend)
        {
            return ShowPublished(dtstart, dtend, true, false);
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult ShowSalesData(string dtstart, string dtend)
        {
            return ShowPublished(dtstart, dtend, null, false);
        }

        [NeedRead]
        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult ShowPublished(string dtstart, string dtend, bool? published, bool redirect = true)
        {
            if (redirect)
            {
                if (published.HasValue)
                {
                    if (published == true)
                        return RedirectToAction("ShowPPublished", new { dtstart, dtend });
                    else
                        return RedirectToAction("ShowUnpublished", new { dtstart, dtend });
                }
                return RedirectToAction("ShowSalesData", new { dtstart, dtend });
            }

            if (dtend == "auto")
                dtend = dtstart;
            if (dtstart == null || dtend == null)
                return RedirectToAction("Published", new { published });
            dtstart = dtstart.Replace("/", "");
            dtend = dtend.Replace("/", "");

            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var model = GetInvoiceSummary(db, start, end, published).Where(x => x.Child == null);

            ViewBag.published = published;

            ViewBag.totalsum = model.Sum(x => x.ItemPrice + x.DeliveryPrice + x.VatPrice);
            ViewBag.totalitems = model.Sum(x => x.ItemPrice);
            ViewBag.totaldel = model.Sum(x => x.DeliveryPrice);
            ViewBag.totalvat = model.Sum(x => x.VatPrice);
            ViewBag.LastNumber = Settings["Invoice"]["LastNumber"];
            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            ViewBag.startdate = start;
            ViewBag.enddate = end;

            ViewBag.StartDateLocal = ToClientTime(start);
            ViewBag.EndDateLocal = ToClientTime(end);

            switch (_invoicePeriod)
            {
                case "d":
                    ViewBag.days = GetDaysPublished(published);
                    break;
                case "m":
                    ViewBag.months = GetMonthsPublished(published);
                    break;
                case "w":
                    ViewBag.weeks = GetWeeksPublished(published).ToList();
                    break;
            }

            return View("Show_RangePublished", model.ToList());
        }

        public IDictionary<DateTime, DateTime> GetDaysPublished(bool? published)
        {
            var hasValue = published.HasValue;
            IList<DateTime> allDates = (from inv in db.Invoices
                    where !inv.Cloned && !inv.Deleted && (!hasValue || inv.Published == published) && !inv.MyClones.Any()
                    orderby inv.Date descending
                    select inv.Date).Distinct().ToList();
            IDictionary<DateTime, DateTime> datesWithLocal = new Dictionary<DateTime, DateTime>();

            // localize all end
            foreach (DateTime? utc in allDates)
            {
                if (utc != null)
                {
                    datesWithLocal[utc.Value] = ToClientTime(utc.Value);
                }
            }

            return datesWithLocal;

        }

        public IDictionary<DateTime, DateTime> GetDays()
        {
            IList<DateTime?> allDates = (from pos in db.SnapshotOrderPositions
                    orderby pos.EndDate descending
                    select EntityFunctions.TruncateTime(pos.EndDate)).Distinct().ToList();
            IDictionary<DateTime, DateTime> datesWithLocal = new Dictionary<DateTime, DateTime>();
            // localize all end
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            foreach (DateTime? utc in allDates)
            {
                if (utc != null)
                {
                    datesWithLocal[utc.Value] = TimeZoneInfo.ConvertTimeFromUtc(utc.Value, timeZoneInfo);
                }
            }

            return datesWithLocal;
        }

        public List<InvoiceWeekData> GetInvoiceSummary(ChlebyContext db, DateTime start, DateTime end, bool? published = null)
        {
            var hasValue = published.HasValue;
            var invoices = db.Invoices.
                              Where(x => EntityFunctions.TruncateTime(x.Date) >= EntityFunctions.TruncateTime(start)
                                  && EntityFunctions.TruncateTime(x.Date) <= EntityFunctions.TruncateTime(end));
            var model = new List<InvoiceWeekData>(
                from invoice in invoices
                where (!hasValue || invoice.Published == published)
                orderby invoice.Date, invoice.InvoiceNumber, invoice.Revision
                select new InvoiceWeekData
                    {
                        Address = invoice.DeliveryAddress,
                        DeliveryPrice = invoice.DeliveryPrice,
                        ItemPrice = invoice.ItemsPrice,
                        VatPrice = invoice.Vat + invoice.DeliveryVat,
                        Published = invoice.Published,
                        Cloned = invoice.Cloned,
                        Deleted = invoice.Deleted,
                        Revision = invoice.Revision,
                        Id = invoice.Id,
                        InvoiceNumber = invoice.InvoiceNumber,
                        Child = invoice.MyClones.FirstOrDefault(),
                        Parent = invoice.MyParent,
                        Date = invoice.Date,
                        Exported = invoice.Exported
                    });

            foreach (InvoiceWeekData inv in model)
            {
                inv.DateLocal = ToClientTime(inv.Date);
            }

            return model;
        }

        [NeedRead]
        public ActionResult RangeInvoice(string dtstart, string dtend, int address)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            var addr = db.Addresses.Find(address);
            if (addr == null)
                return RedirectToAction("Show", new { dtstart, dtend });
            // var weekdata = db.SnapshotOrderPositions.
            // Where(x => x.EndDate >= start && x.EndDate <= end && x.Address.TraderId == addr.TraderId).
            // GroupBy(x => x.Address);

            var weekdata = db.SnapshotOrderPositions.Where(
                x => EntityFunctions.TruncateTime(x.EndDate) >= EntityFunctions.TruncateTime(start)
                  && EntityFunctions.TruncateTime(x.EndDate) <= EntityFunctions.TruncateTime(end) && x.Address.Id == addr.Id).
              GroupBy(x => x.Address);

            ViewBag.AllItems = db.Items.OrderBy(x => x.Name).ToList();
            ViewBag.LastNumber = Settings["Invoice"]["LastNumber"];
            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            ViewBag.enddate = end;
            ViewBag.startdate = start;
            ViewBag.addrs = db.SnapshotOrderPositions.
                Where(x => EntityFunctions.TruncateTime(x.EndDate) >= EntityFunctions.TruncateTime(start)
                    && EntityFunctions.TruncateTime(x.EndDate) <= EntityFunctions.TruncateTime(end) &&
                           x.Item_Id != db.AppConfig.DeliveryId).
                           Select(x => new { x.Address.Address1, x.Address_Id, x.Address.Trader.PrimaryName, x.Address.Trader.TradingName }).Distinct().
                           ToDictionary(ks => ks.Address_Id, ks =>
                               Tuple.Create(
                                    ks.Address1,
                                    string.IsNullOrWhiteSpace(ks.TradingName) ? ks.PrimaryName : ks.TradingName));

            ViewBag.EndDateLocal = ToClientTime(end);
            ViewBag.StartDateLocal = ToClientTime(start);

            return View(weekdata.ToList());
        }

        [NeedWrite]
        public ActionResult EditNumber()
        {
            var lastnumber = Convert.ToInt32(Settings["Invoice"]["LastNumber"]);
            ViewBag.LastNumber = lastnumber + 1;
            return PartialView();
        }

        [HttpPost]
        [NeedWrite]
        public ActionResult EditNumber(int? invoicelastnumber)
        {

            if (!invoicelastnumber.HasValue)
                return Json(new { result = "error", data = "invalid number" });
            invoicelastnumber--;
            Settings["Invoice"]["LastNumber"] = invoicelastnumber.Value.ToString(CultureInfo.InvariantCulture);
            Log(new AuditLog { Action = "setnumber", IntValue = invoicelastnumber });
            return Json(new { result = "ok", data = invoicelastnumber });
        }

        [NeedWrite]
        public ActionResult SetData(Dictionary<string, int> pos,
            Dictionary<string, bool> ack, Dictionary<string, decimal?> price, string action, string dtstart, string dtend)
        {
            var changes = new List<OrderDiff>();
            foreach (var kvp in pos)
            {
                var id = int.Parse(kvp.Key);
                var p = db.SnapshotOrderPositions.Find(id);
                if (p != null)
                {
                    if (price.ContainsKey(p.Item.Id.ToString(CultureInfo.InvariantCulture)) && price[p.Item.Id.ToString()].HasValue)
                        p.Price = price[p.Item.Id.ToString(CultureInfo.InvariantCulture)].Value;
                    var cnt = Math.Max(0, kvp.Value);
                    if (cnt != p.MadeCount)
                        changes.Add(new OrderDiff(p.Adhoc, p, cnt, p.MadeCount));
                    p.MadeCount = cnt;
                    //var dt = p.EndDate.ToShortDateString();
                    //TODO: FIX p.Delivery = delivery[dt];
                }
            }

            if (ack != null && !ack.ContainsKey("controller"))
                foreach (var kvp in ack)
                {
                    var id = int.Parse(kvp.Key);
                    db.DmgItems.Find(id).Ack = kvp.Value;
                }

            SimpleProductionController.NotifyChanges(db, changes, AppName, MyBusiness);
            db.SaveChanges();
            if (action == "saveback")
                return RedirectToAction("Show", new { dtstart, dtend });
            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());
            else
                return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult CreateRowRange(int addressId, int itemId, Dictionary<string, int> days, string dtstart, string dtend)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            var address = db.Addresses.Find(addressId);
            if (address == null)
                return DebugReport("address=null");
            var item = db.Items.Find(itemId);
            if (item == null)
                return DebugReport("item=null");

            var cur = start;
            do
            {
                var curstr = cur.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
                var value = 0;
                if (days.ContainsKey(curstr))
                {
                    value = days[curstr];
                    var existing =
                        db.SnapshotOrderPositions.FirstOrDefault(x => x.EndDate == cur &&
                                                                      x.Address.Id == addressId && x.Item.Id == itemId);
                    if (existing != null && value > 0)
                    {
                        existing.MadeCount = value;
                        goto NoCoz;
                        //return DebugReport("item already set");
                    }
                }
                var pos = new SnapshotOrderPosition
                {
                    OrderedCount = 0,
                    MadeCount = value,
                    Price = item.GetItemPrice(address.Trader).Value,
                    Vat = item.Vat,
                    Address = address,
                    Adhoc = true,
                    EndDate = cur,
                    StartDate = cur,
                    Item = item,
                    Order = null,
                };
                db.SnapshotOrderPositions.Add(pos);
            NoCoz: //no coz, chujowo..
                cur = cur.AddDays(1);
            } while (cur <= end);
            db.SaveChanges();

            if (Request.UrlReferrer != null)
                return Redirect(Request.UrlReferrer.ToString());
            else
                return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult Publish(int addrId, string dtstart, string dtend, DateTime date,
            bool mark = true, bool autosave = true, int number = 0)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            var address = db.Addresses.Find(addrId);
            if (address == null)
                return RedirectToAction("Show", new { dtstart, dtend });

            if (number == 0)
                number = Settings["Invoice"].Get<int>("LastNumber") + 1;

            if (db.Invoices.Any(x => x.InvoiceNumber == number))
            {
                SetAlert(Severity.error, "number " + number + " already used");
                return RedirectToAction("RangeInvoice", new { dtstart, dtend, address = addrId });
            }
            var trader = address.Trader;

            
            var invoice = new Invoice();
            var invaddr = trader.Address.FirstOrDefault(x => x.AddressIsPrimary) ?? trader.Address.First();
            invoice.Address = invaddr;
            invoice.Date = date;
            invoice.PaymentTerm = trader.PaymentTerms.Name;
            invoice.Trader = trader;
            invoice.Year = date.Year;
            invoice.StartDate = start;
            invoice.EndDate = end;
            invoice.InvoiceNumber = number;
            if (db.DmgItems.Any(x => x.DeliveryDate >= start && x.DeliveryDate <= end && x.AddressId == addrId))
            {
                invoice.Published = false; //sa dmgitemy wiec unpublished
                SetAlert(Severity.warning, "There are unprocessed return on this invoice. It is marked as unpublished for now.");
            }
            else
            {
                invoice.Published = mark;
            }
            invoice.DeliveryAddress = address;
            invoice.Rows = new Collection<InvoiceRow>();

            var orders = db.SnapshotOrderPositions.
                Where(x => EntityFunctions.TruncateTime(x.EndDate) >= EntityFunctions.TruncateTime(start)
                    && EntityFunctions.TruncateTime(x.EndDate) <= EntityFunctions.TruncateTime(end) &&
                    x.Address.Id == address.Id);
            // if (!orders.Any()) continue;
            var firstDate = orders.Min(x => x.EndDate);
            var lastDate = orders.Max(x => x.EndDate);
            var items = orders.Select(x => x.Item).OrderBy(x => x.Name).Distinct().ToList();

            foreach (var item in items)
            {
                var itemorders = orders.Where(r => r.Item.Id == item.Id);
                var rowdata = from o in itemorders
                              group o by 0
                                  into g
                                  select new
                                  {
                                      Count = g.Sum(i => i.MadeCount),
                                      Price = g.Sum(i => i.Price * i.MadeCount),
                                      Vat = g.Sum(i => i.Vat / 100 * i.Price * i.MadeCount),
                                  };
                var onerowdata = rowdata.First();
                var row = new InvoiceRow();
                row.Count = onerowdata.Count;
                row.Price = onerowdata.Price;
                row.Vat = onerowdata.Vat;
                row.Item = item;
                row.Text = item.Name;
                row.DayData = new Collection<InvoiceRowDayData>();
                foreach (var io in itemorders)
                {
                    row.DayData.Add(new InvoiceRowDayData
                        {
                            Date = io.EndDate,
                            Count = io.MadeCount,
                            Price = io.Price,
                            Vat = io.Vat
                        });
                }
                invoice.Rows.Add(row);
            }

            var curDate = firstDate;
            while (curDate <= lastDate)
            {
                var pos = orders.FirstOrDefault(x => x.EndDate == curDate && x.Item_Id == db.AppConfig.DeliveryId);
                if (pos != null)
                {
                    invoice.DeliveryPrice += pos.MadeCount > 0 ? pos.Price : 0;
                    invoice.DeliveryVat += pos.MadeCount > 0 ? pos.Price * pos.Vat / 100m : 0;
                    invoice.DeliveryCount += pos.MadeCount > 0 ? 1 : 0;
                }
                curDate = curDate.AddDays(1);
            }

            //TODO: DELIVERYCHUJ
            invoice.ItemsPrice += invoice.Rows.Where(x => x.Item.Id != db.AppConfig.DeliveryId).Sum(x => x.Price);
            invoice.Vat += invoice.Rows.Where(x => x.Item.Id != db.AppConfig.DeliveryId).Sum(x => x.Vat);
            invoice.TotalPrice += invoice.ItemsPrice + invoice.Vat;

            orders.ToList().ForEach(x =>
                {
                    db.SnapshotOrderPositions.Remove(x);
                    if (x.Order is AdhocOrder)
                        db.Orders.Remove(x.Order);
                });

            Log("publish", invoice.Id);

            db.Invoices.Add(invoice);
            //db.Settings.First(x => x.Name == "Invoice_LastNumber").Value = number;
            Settings["Invoice"]["LastNumber"] = number.ToString(CultureInfo.InvariantCulture);

            if (autosave)
                db.SaveChanges();
            // return RedirectToAction("Show", new { dtstart, dtend });
            return RedirectToAction("Dashboard");
        }

        [NeedRead]
        public ActionResult List()
        {
            return View(db.Invoices.ToList());
        }

        [NeedWrite]
        public ActionResult EditRowRange(int itemId, int addressId, string dtstart, string dtend)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            var address = db.Addresses.Find(addressId);
            if (address == null)
                return DebugReport("address=null");
            var item = db.Items.Find(itemId);
            if (item == null)
                return DebugReport("item=null");

            var model = db.SnapshotOrderPositions.
                Where(x => x.EndDate >= start && x.EndDate <= end &&
                x.Item.Id == itemId && x.Address.Id == addressId);

            ViewBag.dtstart = dtstart;
            ViewBag.dtend = dtend;
            return PartialView(model.ToList());
        }

        [HttpPost]
        [NeedWrite]
        public ActionResult EditRowRange(Dictionary<string, int> count, Dictionary<string, decimal> price, string dtstart, string dtend)
        {
            var changes = new List<OrderDiff>();
            SnapshotOrderPosition p = null;
            foreach (var kvp in count)
            {
                var id = int.Parse(kvp.Key);
                p = db.SnapshotOrderPositions.Find(id);
                if (p != null)
                {
                    var cnt = count[p.Id.ToString(CultureInfo.InvariantCulture)];
                    p.Price = price[p.Id.ToString(CultureInfo.InvariantCulture)];
                    if (cnt != p.MadeCount)
                        changes.Add(new OrderDiff(p.Adhoc, p, cnt, p.MadeCount));
                    p.MadeCount = cnt;
                }
            }
            SimpleProductionController.NotifyChanges(db, changes, AppName, MyBusiness);
            db.SaveChanges();
            //TODO: gdzie wrocic jak p=null
            //nie wracamy bo w modalu xD
            return RedirectToAction("RangeInvoice", new { dtstart, dtend, address = p.Address.Id });
        }

        [NeedRead]
        public ActionResult PDF(int id)
        {
            var invoice = db.Invoices.Find(id);
            var ms = new MemoryStream();

            SavePdfToStream(invoice, ms);

            return File(ms, "application/pdf", invoice.InvoiceNumber.ToString() + ".pdf");
        }

        private void SavePdfToStream(Invoice invoice, Stream ms)
        {
            var b = MyBusiness;
            var pdfs = new PDFs(b);
            var doc = pdfs.CreateInvoiceDoc(invoice, db);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            pdr.Save(ms, false);
        }

        public ActionResult Batch(Dictionary<string, bool> batch, string dtstart, string dtend, string action, DateTime? date)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            var tids = batch.Where(x => x.Value).Select(x => int.Parse(x.Key)).ToList();
            switch (action)
            {
                case "delete":
                    db.SnapshotOrderPositions.
                        Where(x => tids.Contains(x.Address.TraderId) &&
                            x.EndDate >= start && x.EndDate <= end).
                        ToList().ForEach(x => db.SnapshotOrderPositions.Remove(x));
                    db.SaveChanges();
                    break;
                case "publish":
                    var invs = db.Invoices.Where(x => tids.Contains(x.Id));
                    foreach (var inv in invs)
                        inv.Published = true;
                    db.SaveChanges();
                    return RedirectToAction("Dashboard");
                case "pdf":
                    return GetPDFs(tids, dtstart, dtend);
            }

            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return RedirectToAction("Dashboard");
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult PublishPrep(Dictionary<int, bool> batch, string dtstart, string dtend)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart.Replace("/", ""));
            var end = DateTime.Now;
            if (dtend == "auto")
            {
                end = start;
            }
            else
            {
                end = DateTimeExtensions.FromStringUrl(dtend.Replace("/", ""));
            }

            if (batch == null)
                return RedirectToAction("Show", new { dtstart, dtend });

            var model = new InvoicePrepModel();

            model.DataInPast = db.SnapshotOrderPositions.Any(x => x.EndDate < start);
            model.TakenNumbers = db.Invoices.Select(x => x.InvoiceNumber).ToList();

            var summary = GetSnapshotSummary(db, start, end);

            var list = new List<InvoiceWeekData>();
            foreach (var kvp in batch)
                list.Add(summary.First(x => x.Address.Id == kvp.Key));
            model.Summary = list;
            foreach (var iwd in model.Summary)
                iwd.Published = batch[iwd.Address.Id];

            model.FirstNumber = Settings["Invoice"].Get<int>("LastNumber");

            ViewBag.dtend = end;
            return PartialView(model);
        }

        public ActionResult PublishPrepDo(Dictionary<int, InvoicePrepRetModel> info, string date, string dtstart, string dtend, bool sendMail)
        {
            DateTime dateParsed = DateTime.ParseExact(date, MyBusiness.DateFormat, CultureInfo.InvariantCulture);

            var numbers = info.Select(kvp => kvp.Value.Number);

            if (db.Invoices.Any(x => numbers.Contains(x.InvoiceNumber)))
            {
                var batch = info.ToDictionary(ks => ks.Key, es => es.Value.Publish);
                return PublishPrep(batch, dtstart, dtend);
            }

            Alerts.Clear();
            var good = true;
            foreach (var kvp in info)
            {
                var tid = kvp.Key;
                var num = kvp.Value.Number;
                if (!db.Invoices.Any(x => x.InvoiceNumber == num))
                    Publish(tid, dtstart, dtend, dateParsed.Date, kvp.Value.Publish, false, num);
                else
                {
                    good = false;
                    break;
                }
            }

            if (good)
            {
                db.SaveChanges();

                if (sendMail)
                {
                    foreach (var number in numbers)
                    {
                        var stream = new MemoryStream(8192);
                        var invoice = db.Invoices.First(x => x.InvoiceNumber == number);
                        var emp = invoice.Address.TraderEmployees.FirstOrDefault();
                        if (emp == null || !invoice.Published) continue;

                        SavePdfToStream(invoice, stream);
                        new Mail(db, "newInvoice", Membership.GetUser(emp.ExternalAccountData).Email, new
                            {
                                invoice
                            })
                            .AddAttachment(new Attachment(stream, "invoice-" + number + ".pdf", "application/pdf"))
                            .Send();
                    }
                }

                //return RedirectToAction("Published", date.Year);
                return RedirectToAction("Dashboard", dateParsed.Year);
            }
            else
            {
                throw new Exception("number used");
            }
        }

        public ActionResult GetPDFs(IEnumerable<int> ids, string dtstart, string dtend)
        {
            var start = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);

            IQueryable<Invoice> invoices;
            if (ids == null)
                invoices = db.Invoices.Where(x => x.Date >= start && x.Date <= end);
            else
                invoices = db.Invoices.Where(x => ids.Contains(x.Id));
            var zip = new ZipFile();
            foreach (var invoice in invoices.ToList())
            {
                var ms = new MemoryStream();
                SavePdfToStream(invoice, ms);
                var name = string.Format("invoice_{0}_{1}-{2}_{3}_{4}.pdf", ReplaceInvalidChars(invoice.Trader.Name),
                    invoice.InvoiceNumber, invoice.Revision, invoice.Week, invoice.Year);
                if (!zip.ContainsEntry(name))
                    zip.AddEntry(name, ms);
                else
                    _log.Error("duplicate invoice " + name);
            }
            var zipstream = new MemoryStream();
            zip.Save(zipstream);
            return File(zipstream.ToArray(), "application/pdf",
                        string.Format("invoices_{0}_{1}.zip", dtstart, dtend));
        }

        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult Edit(int id)
        {
            ViewBag.invoiceid = id;
            var invoice = db.Invoices.Find(id);

            if (invoice == null)
                return HttpNotFound();

            ViewBag.parents = invoice.Parents;
            ViewBag.clones = invoice.AllClones;
            ViewBag.clones.Reverse();

            if (invoice.Published)
                return PDF(id);

            var delivId = db.AppConfig.DeliveryId;

            var icm = new InvoiceCreateModel();
            icm.Date = invoice.Date.Date;
            icm.EndDate = invoice.EndDate.Date;
            icm.StartDate = invoice.StartDate.Date;

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            icm.DateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.Date, timeZoneInfo);
            icm.EndDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.EndDate, timeZoneInfo);
            icm.StartDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.StartDate, timeZoneInfo);

            icm.InvoiceAddressId = invoice.Address_Id;
            icm.PaymentTerm = invoice.PaymentTerm;
            icm.OldNumber = invoice.InvoiceNumber;

            icm.DeliveryAddressId = invoice.DeliveryAddress_Id;
            foreach (var row in invoice.Rows)
            {
                if (row.Item != null && row.Item.Id == delivId) //delivery
                {
                    foreach (var dd in row.DayData)
                    {
                        if (icm.DeliveryCount.ContainsKey(dd.Date.Date)) continue; //fix na bugged ordery stare
                        icm.DeliveryCount.Add(dd.Date.Date, (int)dd.Count);
                        icm.DeliveryPrice.Add(dd.Date.Date, dd.Price);
                    }
                }
                else //normal item
                {
                    var counts = new Dictionary<DateTime, decimal>();
                    var prices = new Dictionary<DateTime, decimal>();
                    foreach (var dd in row.DayData)
                    {
                        if (counts.ContainsKey(dd.Date.Date)) continue; //fix na bugged ordery stare
                        counts.Add(dd.Date.Date, (int)dd.Count);
                        prices.Add(dd.Date.Date, dd.Price);
                    }
                    icm.FieldCount.Add(P.Create(row.Text, counts));
                    icm.FieldPrice.Add(P.Create(row.Text, prices));
                    icm.Notes.Add(icm.FieldCount.Count - 1, row.Info);
                    icm.Vats.Add(icm.FieldCount.Count - 1, row.DayData.Average(x => x.Vat));
                }
            }
                      

            ViewBag.addrs = db.Addresses.ToList();
            ViewBag.items = db.Items.ToList();
            ViewBag.pts = db.PaymentTerms.ToList();
            ViewBag.id = id;
            
            IList<DmgItem> complaints = db.DmgItems.
               Where(x => EntityFunctions.TruncateTime(x.DeliveryDate) >= EntityFunctions.TruncateTime(icm.StartDate)
                   && EntityFunctions.TruncateTime(x.DeliveryDate) <= EntityFunctions.TruncateTime(icm.EndDate) &&
               x.AddressId == icm.DeliveryAddressId && x.AddressId == icm.DeliveryAddressId).ToList();

            foreach (DmgItem item in complaints)
            {
                item.DeliveryDateLocal = TimeZoneInfo.ConvertTimeFromUtc(item.DeliveryDate, timeZoneInfo);
            }

            ViewBag.complaints = complaints;

            ViewBag.returnUrl = Request.UrlReferrer == null
                ? Url.Action("ShowPublished")
                : Request.UrlReferrer.ToString();
            return View("NewCreate", icm);
        }

        public ActionResult DelRow(int id, int rowId)
        {
            var invoice = db.Invoices.Find(id);
            if (invoice == null)
                return HttpNotFound();

            if (invoice.Published || invoice.Deleted)
                throw new InvalidOperationException(Strings.Controller_InvoicesController_Invoice_Published);

            var row = invoice.Rows.First(x => x.Id == rowId);
            invoice.Rows.Remove(row);
            db.InvoiceRows.Remove(row);
            db.SaveChanges();
            return RedirectToAction("Edit", new { id });
        }

        [ChlebyMenu("AdminCreateInvoice")]
        public ActionResult NewCreate2(int id = 0)
        {
            if (id > 0) //create z id = edit
                return Edit(id);

            var model = new InvoiceCreateViewModel();

            return View(model);
        }

        [ChlebyMenu("AdminCreateInvoice")]
        public ActionResult Edit2(int id)
        {
            var model = new InvoiceCreateViewModel();
            
            return View(model);
        }


        [ChlebyMenu("AdminCreateInvoice")]
        public ActionResult NewCreate(int id = 0)
        {
            ViewBag.invoiceid = id;
            if (id > 0) //create z id = edit
                return Edit(id);

            var icm = new InvoiceCreateModel();
            //icm.Number = db.Invoices.Max(x => x.InvoiceNumber) + 1;
            var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
            icm.StartDate = DateTime.UtcNow.GetStartOfWeek(dow).Date;
            icm.EndDate = icm.StartDate.AddDays(6).Date;
            icm.Date = icm.EndDate;

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            icm.DateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.Date, timeZoneInfo);
            icm.EndDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.EndDate, timeZoneInfo);
            icm.StartDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.StartDate, timeZoneInfo);

            icm.InvoiceAddressId = icm.DeliveryAddressId = db.Traders.First().Address.First(x => x.AddressIsPrimary).Id;
            FillDeliveryPrice(icm);
            ViewBag.addrs = db.Addresses.ToList();
            ViewBag.items = db.Items.ToList();
            ViewBag.pts = db.PaymentTerms.ToList();
            ViewBag.complaints = db.DmgItems.
                Where(x => x.DeliveryDate >= icm.StartDate && x.DeliveryDate <= icm.EndDate &&
                x.AddressId == icm.DeliveryAddressId && x.AddressId == icm.DeliveryAddressId).ToList();
            return View(icm);
        }

        [HttpPost]
        [ChlebyMenu("AdminCreateInvoice")]
        public ActionResult NewCreate(InvoiceCreateModel icm, string action, string returnUrl,
            List<string> names, Dictionary<int, Dictionary<DateTime, decimal>> fc,
            Dictionary<int, Dictionary<DateTime, decimal>> fp,
            List<int> complaints, int id = 0)
        {
            ViewBag.invoiceid = id;
            icm.FieldCount = new List<P<string, Dictionary<DateTime, decimal>>>();
            icm.FieldPrice = new List<P<string, Dictionary<DateTime, decimal>>>();
            if (fc != null)
            {
                foreach (var kvp in fc)
                {
                    var name = names[kvp.Key];
                    icm.FieldCount.Add(P.Create(name, kvp.Value));
                    icm.FieldPrice.Add(P.Create(name, fp[kvp.Key]));
                }
            }

            if (action.StartsWith("up_"))
            {
                int swap = int.Parse(action.Substring(3));
                if (swap > 0 && swap < icm.FieldCount.Count)
                {
                    icm.FieldCount.Swap(swap, swap - 1);
                    icm.FieldPrice.Swap(swap, swap - 1);
                }
            }
            if (action.StartsWith("down_"))
            {
                int swap = int.Parse(action.Substring(5));
                if (swap < icm.FieldCount.Count - 1)
                {
                    icm.FieldCount.Swap(swap, swap + 1);
                    icm.FieldPrice.Swap(swap, swap + 1);
                }
            }

            if (id != 0)
            {
                var invoice = db.Invoices.Find(id);
                if (invoice == null)
                    return HttpNotFound();
                ViewBag.parents = invoice.Parents;
                ViewBag.clones = invoice.AllClones;
                ViewBag.clones.Reverse();
            }
            if (action == "addrow")
            {
                int itemId = Convert.ToInt32(icm.NewItemId);
                var item = db.Items.SingleOrDefault(x => x.Id == itemId);
                if (item != null)
                {
                    var addr = db.Addresses.Find(icm.DeliveryAddressId);
                    var price = item.GetItemPrice(addr.Trader).Value;

                    var rowcnt = icm.NewRow.ToDictionary(ks => ks.Key, es => es.Value);
                    var rowprice = rowcnt.ToDictionary(kvp => kvp.Key, kvp => price);

                    icm.FieldCount.Add(P.Create(item.Name, rowcnt));
                    icm.FieldPrice.Add(P.Create(item.Name, rowprice));
                    icm.Vats.Add(icm.FieldCount.Count - 1, item.Vat);
                }
            }
            else if (action.StartsWith("delete_"))
            {
                int pos = int.Parse(action.Substring(7));
                icm.FieldCount.RemoveAt(pos);
                icm.FieldPrice.RemoveAt(pos);
            }

            var newfc = new List<P<string, Dictionary<DateTime, decimal>>>();
            var newfp = new List<P<string, Dictionary<DateTime, decimal>>>();

            for (int i = 0; i < icm.FieldCount.Count; ++i)
            {
                if (icm.FieldCount[i].Value.Sum(x => x.Value) != 0)
                {
                    newfc.Add(icm.FieldCount[i]);
                    newfp.Add(icm.FieldPrice[i]);
                }
            }

            for (int i = 0; i < icm.FieldCount.Count; ++i)
                if (!icm.Notes.ContainsKey(i))
                    icm.Notes.Add(i, null);

            if (complaints != null && complaints.Count > 0 && action == "applydi")
            {
                foreach (var diid in complaints)
                {
                    var di = db.DmgItems.Find(diid);
                    if (di == null || di.Ack) continue;

                    if (icm.StartDate > di.DeliveryDate || icm.EndDate < di.DeliveryDate) continue;

                    var ourMinusRowCands = icm.FieldCount.Where(x => x.Key == di.Item.Name && x.Value.Any(y => y.Value < 0));
                    P<string, Dictionary<DateTime, decimal>> ourMinusRow = null;
                    foreach (var row in ourMinusRowCands)
                    {
                        var idx = icm.FieldCount.IndexOf(row);
                        if (icm.Notes[idx] == di.Note)
                        {
                            ourMinusRow = row;
                            break;
                        }
                    }
                    if (ourMinusRow != null)
                    {
                        var idx = icm.FieldCount.IndexOf(ourMinusRow);
                        if (ourMinusRow.Value.ContainsKey(di.DeliveryDate))
                        {
                            ourMinusRow.Value[di.DeliveryDate] -= di.Quantity;
                            icm.FieldPrice[idx].Value[di.DeliveryDate] = di.PricePerItem;
                        }
                        else
                        {
                            ourMinusRow.Value.Add(di.DeliveryDate, -di.Quantity);
                            icm.FieldPrice[idx].Value.Add(di.DeliveryDate, -di.PricePerItem);
                        }
                    }
                    else
                    {
                        icm.FieldCount.Add(new P<string, Dictionary<DateTime, decimal>>(
                            di.Item.Name, new Dictionary<DateTime, decimal> { { di.DeliveryDate, -di.Quantity } }));
                        icm.FieldPrice.Add(new P<string, Dictionary<DateTime, decimal>>(
                            di.Item.Name, new Dictionary<DateTime, decimal> { { di.DeliveryDate, di.PricePerItem } }));

                        icm.Notes.Add(icm.FieldCount.Count - 1, di.Note);
                        icm.Vats.Add(icm.FieldCount.Count - 1, di.Item.Vat);
                    }
                    di.Ack = true;
                    action = "savedb";
                }
            }

            var curDate = icm.StartDate;
            for (int i = 0; i < icm.FieldCount.Count; ++i)
            {
                if (!icm.Notes.ContainsKey(i))
                {
                    icm.Notes.Add(i, null);
                }
                curDate = icm.StartDate;
                do
                {
                    if (!icm.FieldCount[i].Value.ContainsKey(curDate))
                        icm.FieldCount[i].Value.Add(curDate, 0);
                    if (!icm.FieldPrice[i].Value.ContainsKey(curDate))
                    {
                        var key = icm.FieldCount[i].Key;
                        var item = db.Items.SingleOrDefault(x => x.Name == key);
                        if (item != null)
                        {
                            var addr = db.Addresses.Find(icm.DeliveryAddressId);
                            var price = item.GetItemPrice(addr.Trader).Value;
                            icm.FieldPrice[i].Value.Add(curDate, price);
                        }
                        else
                        {
                            var addr = db.Addresses.Find(icm.DeliveryAddressId);
                            var price = icm.FieldPrice[i].Value.First().Value;
                            icm.FieldPrice[i].Value.Add(curDate, price);

                        }
                    }
                    curDate = curDate.AddDays(1);
                } while (curDate <= icm.EndDate);
            }

            FillDeliveryPrice(icm);

            if (action == "savedb" || action == "publish")
            {
                Invoice invoice;
                if (id != 0) //edytujemy istniejaca
                {
                    invoice = db.Invoices.Find(id);
                    Log("edit", invoice.Id);
                }
                else
                {
                    invoice = new Invoice();
                    Log("create", invoice.Id);
                }

                if (action == "publish")
                    invoice.PublishClone();

                invoice.Address_Id = icm.InvoiceAddressId;
                if (id != 0)
                {
                    if (!CheckValidInvoiceNumber(invoice, icm.OldNumber))
                        throw new Exception("number used!");
                    invoice.InvoiceNumber = icm.OldNumber;
                }
                else
                {
                    invoice.InvoiceNumber = Settings["Invoice"].Get<int>("LastNumber") + 1;
                }
                invoice.PaymentTerm = icm.PaymentTerm;
                invoice.Year = icm.Date.Year;
                invoice.StartDate = icm.StartDate;
                invoice.EndDate = icm.EndDate;
                invoice.Date = icm.Date;

                if (id != 0)
                {
                    //invoice.Rows.ToList().ForEach(x => {
                    //    x.DayData.ForEach(dd => db.InvoiceRowDayDatas.Remove(dd));
                    //    db.InvoiceRows.Remove(x);
                    //});
                    IList<InvoiceRow> rows = invoice.Rows.ToList();
                    foreach (InvoiceRow row in rows)
                    {
                        IList<InvoiceRowDayData> days = row.DayData.ToList();
                        foreach (InvoiceRowDayData dayData in days)
                        {
                            db.InvoiceRowDayDatas.Remove(dayData);
                        }
                        db.InvoiceRows.Remove(row);
                    }
                }

                invoice.Rows = new Collection<InvoiceRow>();
                invoice.ItemsPrice = 0;
                invoice.Vat = 0;
                invoice.TotalPrice = 0;
                for (int i = 0; i < icm.FieldCount.Count; ++i)
                {
                    var counts = icm.FieldCount[i].Value.Where(x => x.Key >= icm.StartDate && x.Key <= icm.EndDate).ToDictionary(ks => ks.Key, es => es.Value);
                    var prices = icm.FieldPrice[i].Value.Where(x => x.Key >= icm.StartDate && x.Key <= icm.EndDate).ToDictionary(ks => ks.Key, es => es.Value);
                    var row = new InvoiceRow();
                    var key = icm.FieldCount[i].Key;
                    row.Item = db.Items.SingleOrDefault(x => x.Name == key);
                    row.Count = counts.Sum(x => x.Value);
                    row.Info = icm.Notes[i];
                    row.DayData = new Collection<InvoiceRowDayData>();
                    foreach (var info in counts) //info.key=data, info.value=count, prices[info.key]=price
                    {
                        row.DayData.Add(new InvoiceRowDayData
                            {
                                Date = info.Key,
                                Count = info.Value,
                                Price = prices[info.Key],
                                Vat = icm.Vats[i]
                            });
                        row.Price += prices[info.Key] * info.Value;
                    }
                    row.Vat = row.Price * icm.Vats[i] / 100;
                    row.Text = key;
                    invoice.Rows.Add(row);
                }

                var delivRow = new InvoiceRow();
                var deliv = db.Entities.Find(db.AppConfig.DeliveryId);
                var delivVat = Settings["PriceData"].Get<decimal>("DeliveryVat");
                var delivCounts = icm.DeliveryCount.Where(x => x.Key >= icm.StartDate && x.Key <= icm.EndDate).ToDictionary(ks => ks.Key, es => es.Value);
                var delivPrices = icm.DeliveryPrice.Where(x => x.Key >= icm.StartDate && x.Key <= icm.EndDate).ToDictionary(ks => ks.Key, es => es.Value);

                delivRow.Item = deliv;
                delivRow.Count = delivCounts.Sum(x => x.Value);
                delivRow.DayData = new Collection<InvoiceRowDayData>();
                foreach (var info in delivCounts) //info.key=data, info.value=count, prices[info.key]=price
                {
                    delivRow.DayData.Add(new InvoiceRowDayData
                    {
                        Date = info.Key,
                        Count = info.Value,
                        Price = delivPrices[info.Key],
                        Vat = delivVat
                    });
                    delivRow.Price += delivPrices[info.Key] * info.Value;
                }
                delivRow.Vat = delivRow.Price * delivVat / 100;
                delivRow.Text = deliv.Name;
                invoice.Rows.Add(delivRow);
                invoice.DeliveryCount = (int)delivRow.Count;
                invoice.DeliveryPrice = delivRow.Price;
                invoice.DeliveryVat = delivRow.Vat;

                FillInvoice(db, invoice, icm.DeliveryAddressId);

                decimal sum = invoice.Rows.Sum(row => row.Count * row.Price);
                if (sum == 0)
                {
                    SetAlert(Severity.error, Strings.Invoice_CantSaveEmpty);
                }
                else
                {
                    if (id == 0)
                        db.Invoices.Add(invoice);
                    db.SaveChanges();
                    SetAlert(Severity.info, Strings.InvoiceSaved);
                    if (id == 0)
                        Settings["Invoice"]["LastNumber"] = invoice.InvoiceNumber.ToString(CultureInfo.InvariantCulture);
                }
            }

            if (action == "publish")
            {
                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    return Redirect("SalesData");
                }
                return Redirect(returnUrl);
            }
            ViewBag.addrs = db.Addresses.ToList();
            ViewBag.items = db.Items.ToList();
            ViewBag.pts = db.PaymentTerms.ToList();
            ViewBag.id = id;
            ViewBag.complaints = db.DmgItems.
               Where(x => x.DeliveryDate >= icm.StartDate && x.DeliveryDate <= icm.EndDate &&
               x.AddressId == icm.DeliveryAddressId && x.AddressId == icm.DeliveryAddressId).ToList();

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            icm.DateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.Date, timeZoneInfo);
            icm.EndDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.EndDate, timeZoneInfo);
            icm.StartDateLocal = TimeZoneInfo.ConvertTimeFromUtc(icm.StartDate, timeZoneInfo);

            return View(icm);
        }

        private bool CheckValidInvoiceNumber(Invoice invoice, int newNumber)
        {
            var parents = new List<int>();
            var node = invoice;
            do
            {
                parents.Add(node.Id);
            } while ((node = node.MyParent) != null);

            if (invoice.InvoiceNumber != newNumber)
            {
                if (db.Invoices.Where(x => !parents.Contains(x.Id)).Any(x => x.InvoiceNumber == newNumber))
                    return false;
            }
            return true;
        }

        private void FillDeliveryPrice(InvoiceCreateModel icm)
        {
            DateTime curDate = icm.StartDate;
            do
            {
                if (!icm.DeliveryCount.ContainsKey(curDate))
                    icm.DeliveryCount.Add(curDate, 0);
                if (!icm.DeliveryPrice.ContainsKey(curDate))
                {
                    var addr = db.Addresses.Find(icm.DeliveryAddressId);
                    icm.DeliveryPrice.Add(curDate, addr.DeliveryPrice);
                }
                curDate = curDate.AddDays(1);
            } while (curDate <= icm.EndDate);
        }

        public static void FillInvoice(ChlebyContext db, Invoice invoice, int delivAddrId = 0)
        {
            var invoiceaddr = db.Addresses.Find(invoice.Address_Id).Trader.Address.FirstOrDefault(x => x.AddressIsPrimary);
            if (invoiceaddr == null)
                invoiceaddr = db.Addresses.Find(invoice.Address_Id);

            invoice.Year = invoice.Date.Year;
            invoice.Address = invoiceaddr;
            invoice.PaymentTerm = invoice.Address.Trader.PaymentTerms.Name;
            invoice.Trader = invoice.Address.Trader;

            invoice.DeliveryAddress = db.Addresses.Find(delivAddrId == 0 ? invoice.Address_Id : delivAddrId);

            invoice.ItemsPrice = invoice.Rows.Where(x => x.Item == null || x.Item.Id != db.AppConfig.DeliveryId).Sum(x => x.Price);
            invoice.Vat = invoice.Rows.Where(x => x.Item == null || x.Item.Id != db.AppConfig.DeliveryId).Sum(x => x.Vat);
            invoice.TotalPrice = invoice.ItemsPrice + invoice.Vat;

            foreach (var row in invoice.Rows)
            {
                if (row.DmgItemId > 0)
                {
                    row.DmgItem = db.DmgItems.Find(row.DmgItemId);
                    row.DmgItem.Ack = true;
                }
            }
        }

        public static string ReplaceInvalidChars(string str)
        {
            foreach (var c in Path.GetInvalidFileNameChars())
            {
                str = str.Replace(c, '_');
            }
            return str;
        
        }

        //public ActionResult DeleteBatch(int[] id, string dtstart, string dtend)
        //{
        //}

        public ActionResult Delete(int id, string dtstart, string dtend)
        {
            var invoice = db.Invoices.Find(id);
            if (invoice == null)
                return RedirectToAction("ShowPublished", new { dtstart, dtend });

            if (invoice.Published || invoice.MyClones.Any())
            {
                invoice.Deleted = true;
                Log("markdelete", invoice.Id);
            }
            else
            {
                var prev = invoice.MyParent;
                if (prev != null)
                    prev.Cloned = false;
                //foreach (var row in invoice.Rows.ToList())
                //{
                //    db.InvoiceRows.Remove(row);
                //}
                IList<InvoiceRow> rows = invoice.Rows.ToList();
                foreach (InvoiceRow row in rows)
                {
                    IList<InvoiceRowDayData> days = row.DayData.ToList();
                    foreach (InvoiceRowDayData dayData in days)
                    {
                        db.InvoiceRowDayDatas.Remove(dayData);
                    }
                    db.InvoiceRows.Remove(row);
                }
                db.Invoices.Remove(invoice);
                Log("delete", invoice.Id);
            }
            db.SaveChanges();

            return RedirectToAction("ShowPublished", new { dtstart, dtend });
        }

        public ActionResult Clone(int id)
        {
            var invoice = db.Invoices.Find(id);

            var clone = invoice.Clone();

            db.Invoices.Add(clone);
            Log("clone", invoice.Id);
            db.SaveChanges();

            return null;
        }

        public ActionResult PublishClone(int id, string dtstart, string dtend)
        {
            var invoice = db.Invoices.Find(id);

            invoice.PublishClone();
            Log("clonepublish", invoice.Id);
            db.SaveChanges();
            return RedirectToAction("ShowPublished", new { dtstart, dtend });
        }


        public ActionResult EditAndClone(int id)
        {
            var invoice = db.Invoices.Find(id);
            if (invoice == null)
                return HttpNotFound();
            if (invoice.Cloned)
            {
                return RedirectToAction("Edit", new { id = id });
            }
            else
            {
                var clone = invoice.Clone();

                db.Invoices.Add(clone);
                Log("cloneedit", invoice.Id);
                db.SaveChanges();
                return RedirectToAction("Edit", new { id = clone.Id });
            }
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult CheckNumber(int number)
        {
            var errs = new List<string>();
            var lastnumber = Settings["Invoice"].Get<int>("LastNumber");
            if (lastnumber + 1 != number)
                errs.Add(string.Format(Strings.Controller_InvoicesController_New_number + " ({0}) != " + Strings.Controller_InvoicesController_Prop_number + " ({1})", number, lastnumber + 1));

            if (db.Invoices.Any(x => x.InvoiceNumber == number))
                errs.Add(string.Format(Strings.Controller_InvoicesController_NumberUsed + " ({0})", number));

            return Json(errs.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [NeedRead]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Export(string dtstart, string dtend)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart.Replace("/", ""));
            var end = DateTimeExtensions.FromStringUrl(dtend.Replace("/", ""));
            ViewBag.dtstart = start;
            ViewBag.dtend = end;

            var query = from invoice in db.Invoices
                        where invoice.Date >= start && invoice.Date <= end && !invoice.Deleted
                        select invoice;

            return ExportView(query);
        }

        [NeedRead]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult ExportByNums(string dtstart, string dtend, int numstart, int numend)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart.Replace("/", ""));
            var end = DateTimeExtensions.FromStringUrl(dtend.Replace("/", ""));
            ViewBag.dtstart = start;
            ViewBag.dtend = end;

            var query = from invoice in db.Invoices
                        where invoice.InvoiceNumber >= numstart && invoice.InvoiceNumber <= numend && !invoice.Deleted
                        select invoice;

            return ExportView(query);
        }

        private ActionResult ExportView(IQueryable<Invoice> query)
        {
            var invoices = (from invoice in query
                            orderby invoice.Date, invoice.InvoiceNumber, invoice.Revision
                            select new
                            {
                                invoice.Id,
                                invoice.InvoiceNumber,
                                invoice.Date,
                                invoice.Trader.PrimaryName,
                                invoice.Trader.TradingName,
                                invoice.DeliveryAddress.Address1,
                                invoice.ItemsPrice,
                                invoice.Vat,
                                invoice.DeliveryPrice,
                                invoice.DeliveryVat,
                                invoice.Published,
                                invoice.Revision,
                                invoice.Exported
                            }).ToList().Select(x => new InvoiceWeekData
                {
                    Address = new Address { Address1 = x.Address1 },
                    TraderName = string.IsNullOrWhiteSpace(x.TradingName) ? x.PrimaryName : x.TradingName,
                    DeliveryPrice = x.DeliveryPrice,
                    ItemPrice = x.ItemsPrice,
                    VatPrice = x.Vat + x.DeliveryVat,
                    Id = x.Id,
                    Published = x.Published,
                    InvoiceNumber = x.InvoiceNumber,
                    Date = x.Date,
                    Revision = x.Revision,
                    Exported = x.Exported
                });

            return PartialView("Export", invoices);
        }

        [NeedRead]
        public ActionResult ExportDo(IEnumerable<int> ids, string dtstart, string dtend)
        {
            switch (MyBusiness.AccSoftware)
            {
                case 0: //qb
                    return QBExport(ids, dtstart, dtend);
                case 1: //xero
                    return XeroExport(ids, dtstart, dtend);
            }
            return HttpNotFound();
        }

        [NeedRead]
        [HttpPost]
        public ActionResult QBExport(IEnumerable<int> ids, string dtstart, string dtend)
        {
            var csv = new StringBuilder();
            if (ids == null || !ids.Any())
                return Export(dtstart, dtend);

            var invoices = from invoice in db.Invoices
                           where ids.Contains(invoice.Id)
                           select invoice;

            foreach (var invoice in invoices.ToList())
            {
                invoice.Exported = true;

                csv.AppendLine(string.Join(";", new string[]
                {
                    Strings.Global_Yes,
                    invoice.EndDate.ToShortDateString(),
                    Strings.Global_Invoice,
                    invoice.InvoiceNumber.ToString(CultureInfo.InvariantCulture),
                    invoice.Trader.Name,
                    invoice.Address.Address1,
                    invoice.Address.Address2,
                    invoice.Address.City,
                    invoice.Address.Post,
                    invoice.DeliveryAddress.Address1,
                    invoice.DeliveryAddress.Address2,
                    invoice.DeliveryAddress.City,
                    invoice.DeliveryAddress.Post,
                    "", //item
                    "", //desc
                    Strings.Controller_InvoicesController_Acc_Receivable, //account
                    "", //qty
                    "", //each
                    (invoice.TotalPrice+invoice.DeliveryPrice+invoice.DeliveryVat).ToString("##.##"), //amount
                    "Y", //taxable
                }));

                var vats = new Dictionary<decimal, decimal>();
                foreach (var row in invoice.Rows)
                {
                    if (row.Count == 0) continue;
                    csv.AppendLine(";;;;;;;;;;;;;" + string.Join(";", new string[]
                    {
                        row.Text,
                        "",
                        Strings.Global_Sales,
                        (-row.Count).ToString("F0"),
                        (row.Price/row.Count).ToString("F2"),
                        (-row.Price).ToString("F2"),
                        "Y"
                    }));

                    var vat = row.Vat / row.Price * 100;
                    if (vat != 0)
                    {
                        if (vats.ContainsKey(vat))
                            vats[vat] += row.Vat;
                        else
                            vats.Add(vat, row.Vat);
                    }
                }

                //TODO: skad urodzic POPRAWNY procentowy vat itemow?

                foreach (var kvp in vats)
                {
                    csv.AppendLine(";;;;;;;;;;;;;" + string.Join(";", new string[]
                    {
                        "Standard Sales", //item 
                        "", //padding
                        "VAT Liability", //const
                        "", //padding
                        kvp.Key.ToString("F2") + "%", //vat %
                        (-kvp.Value).ToString("F2"), //vat sum
                        "" //padding
                    }));
                }

                csv.AppendLine(";;;;;;;;;;;;;" + string.Join(";", new string[]
                    {
                        "Zero-Rated Sales", //item 
                        "", //padding
                        "VAT Liability", //const
                        "", //padding
                        "0%", //vat %
                        "", //vat sum
                        "" //padding
                    }));
            }

            db.SaveChanges();

            return File(Encoding.UTF8.GetBytes(csv.ToString()), "text/csv", "export-" + ids.Min() + "-" + ids.Max() + ".csv");
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public JsonResult CheckInvoiceNumber(int id, int number)
        {
            var invoice = db.Invoices.Find(id);
            var good = false;

            if (invoice != null)
            {
                good = CheckValidInvoiceNumber(invoice, number);
                if (!good)
                    return Json(Strings.Controller_InvoiceController_Number_Used, JsonRequestBehavior.AllowGet);
            }
            else //nowa faktura
            {
                good = !db.Invoices.Any(x => x.InvoiceNumber == number);
                if (!good)
                    return Json(Strings.Controller_InvoiceController_Number_Used, JsonRequestBehavior.AllowGet);

                good = db.Invoices.Max(x => x.InvoiceNumber) < number;
                if (!good)
                    return Json("Number < other", JsonRequestBehavior.AllowGet);
            }

            return Json(good, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GoToInvoiceNumber(int? number)
        {

            if (number != null)
            {
                Invoice invoice = null;
                var invs = db.Invoices.Where(x => x.InvoiceNumber == number);
                if (invs.Any())
                {
                    var maxrev = invs.Max(y => y.Revision);
                    invoice = invs.Single(x => x.Revision == maxrev);
                }
                else
                {
                    SetAlert(Severity.error, Strings.Invoices_NumberNotFound);
                    if (Request.UrlReferrer != null)
                    {
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                    return RedirectToAction("Dashboard", "Invoices");
                }

                if (invoice.Published)
                {
                    return RedirectToAction("EditAndClone", "Invoices",
                                            new
                                            {
                                                id = invoice.Id,
                                                dtstart = invoice.StartDate.ToStringUrl(),
                                                dtend = invoice.EndDate.ToStringUrl()
                                            });
                    //return RedirectToAction("PDF", "Invoices", new { id = invoice.Id });
                }
                else
                {
                    return RedirectToAction("Edit", "Invoices",
                                            new
                                                {
                                                    id = invoice.Id,
                                                    dtstart = invoice.StartDate.ToStringUrl(),
                                                    dtend = invoice.EndDate.ToStringUrl()
                                                });
                }
            }
            else
            {
                SetAlert(Severity.error, Strings.Invoices_NoNumberEntered);
            }
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return RedirectToAction("Dashboard", "Invoices");

        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetTraderAddresses(int id)
        {
            var a = db.Addresses.Find(id);
            if (a == null)
            {
                return HttpNotFound();
            }
            var tid = a.Trader.Id;
            var addrs = from addr in db.Addresses
                        where addr.TraderId == tid
                        select new { addr.Id, addr.Address1 };
            return Json(addrs.ToDictionary(ks => ks.Id.ToString(CultureInfo.InvariantCulture), es => es.Address1), JsonRequestBehavior.AllowGet);
        }

        [NeedRead]
        public ActionResult SalesReportSnapshot(string dtstart, string dtend)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var ms = new MemoryStream();
            var pdfs = new PDFs(MyBusiness);
            var doc = pdfs.SalesReportSnapshot(db, start, end, _invoicePeriod[0],
                (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"));
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        [NeedRead]
        public ActionResult SalesReportInvoice(string dtstart, string dtend, bool? published, string order)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var ms = new MemoryStream();
            var pdfs = new PDFs(MyBusiness);
            var doc = pdfs.SalesReportInvoice(db, start, end, _invoicePeriod[0],
                (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"), published, order);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        [NeedRead]
        public ActionResult ItemReportSnapshot(string dtstart, string dtend)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var ms = new MemoryStream();
            var pdfs = new PDFs(MyBusiness);
            var doc = pdfs.SalesItemsSnapshot(db, start, end, _invoicePeriod[0],
                (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"));
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        [NeedRead]
        public ActionResult ItemReportInvoice(string dtstart, string dtend, bool? published)
        {
            var start = DateTimeExtensions.FromStringUrl(dtstart);
            var end = DateTimeExtensions.FromStringUrl(dtend);

            var ms = new MemoryStream();
            var pdfs = new PDFs(MyBusiness);
            var doc = pdfs.SalesItemsInvoice(db, start, end, _invoicePeriod[0],
                (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay"), published);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        [NeedRead]
        [ChlebyMenu("AdminSalesData")]
        public ActionResult SalesData(int? year)
        {
            return Published(year, null);
        }

        [NeedRead]
        [ChlebyMenu("AdminUnpublishedInvoices")]
        public ActionResult UnpublishedInvoices(int? year)
        {
            return Published(year, false);
        }

        [NeedRead]
        [ChlebyMenu("AdminPublishedInvoices")]
        public ActionResult PublishedInvoices(int? year)
        {
            return Published(year, true);
        }

        [ChlebyMenu("AdminFinanceDashboard")]
        [HttpPost]
        public ActionResult Dashboard(List<string> dashboard, int? year, string dtstart, string dtend)
        {
            if (dashboard != null)
                Settings["Finance"]["Dashboard"] = string.Join(",", dashboard);
            return Dashboard(year, dtstart, dtend);
        }

        [ChlebyMenu("AdminFinanceDashboard")]
        public ActionResult DashboardReset()
        {
            Settings["Finance"]["Dashboard"] = "sales,snapshot,sales-unpublished,sales-published,sales-data,sales-bycustomer,sales-item,sales-byitem";
            return RedirectToAction("Dashboard");
        }

        [ChlebyMenu("AdminFinanceDashboard")]
        public ActionResult Dashboard(int? year, string dtstart, string dtend)
        {
            //dtstart & dtend get!
            ViewBag.year = ViewBag.curyear = year ?? DateTime.Now.Year;
            ViewBag.dashboard = Settings["Finance"]["Dashboard"];
            ViewBag.anyReturns = db.DmgItems.Any(x => !x.Ack);
            var now = DateTime.Now.Date;
            DateTime start, end, exportstart, exportend;
            var lastmaybe = (from pos in db.Invoices
                             where !pos.Cloned && !pos.Deleted && !pos.MyClones.Any()
                             select (DateTime?)pos.Date).Min();
            var last = DateTime.Now.Date;
            if (lastmaybe.HasValue) last = lastmaybe.Value;
            switch (_invoicePeriod)
            {
                case "d":
                    ViewBag.days = GetDays().ToList();
                    exportstart = exportend = last;
                    end = start = now;
                    break;
                case "w":
                    ViewBag.weeks = GetWeeks().ToList();
                    var dow = (DayOfWeek)Settings["Invoice"].Get<int>("FirstDay");
                    exportstart = WeekHelper.FirstDateOfWeek(last.Year,
                        CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(
                            last, CalendarWeekRule.FirstDay, dow),
                        dow);
                    exportend = exportstart.AddDays(6);
                    start = now.GetStartOfWeek(dow);
                    end = start.AddDays(6);
                    break;
                case "m":
                default:
                    ViewBag.months = GetMonths().ToList();

                    exportstart = new DateTime(last.Year, last.Month, 1);
                    exportend = new DateTime(last.Year, last.Month, WeekHelper.GetMonthLen(last.Month, last.Year));
                    start = new DateTime(now.Year, now.Month, 1);
                    end = new DateTime(now.Year, now.Month, WeekHelper.GetMonthLen(now.Month, now.Year));
                    break;
            }

            ViewBag.exportstart = exportstart.ToStringUrl();
            ViewBag.exportend = exportend.ToStringUrl();
            if (dtstart == null || dtend == null)
            {
                ViewBag.startdate = start;
                ViewBag.enddate = end;
                ViewBag.dtstart = start.ToStringUrl();
                ViewBag.dtend = end.ToStringUrl();
            }
            else
            {
                ViewBag.startdate = DateTime.ParseExact(dtstart, "ddMMyyyy", CultureInfo.InvariantCulture);
                ViewBag.enddate = DateTime.ParseExact(dtend, "ddMMyyyy", CultureInfo.InvariantCulture);
                ViewBag.dtstart = dtstart;
                ViewBag.dtend = dtend;
            }

            return View();
        }

        [NeedRead]
        public ActionResult ReturnsReport()
        {
            return View(db.DmgItems.Where(x => !x.Ack).ToList());
        }

        [NeedRead]
        [HttpPost]
        public ActionResult XeroExport(IEnumerable<int> ids, string dtstart, string dtend)
        {
            var csv = new StringBuilder();
            if (ids == null || !ids.Any())
                return Export(dtstart, dtend);

            var invoices = from invoice in db.Invoices
                           where ids.Contains(invoice.Id)
                           select invoice;

            csv.AppendLine(
                "ContactName,EmailAddress,POAddressLine1,POAddressLine2,POAddressLine3,POAddressLine4,POCity,PORegion,POPostalCode,POCountry,InvoiceNumber,Reference,InvoiceDate,DueDate,Total,InventoryItemCode,Description,Quantity,UnitAmount,Discount,AccountCode,TaxType,TaxAmount,TrackingName1,TrackingOption1,TrackingName2,TrackingOption2");


            foreach (var invoice in invoices.ToList())
            {
                invoice.Exported = true;

                var prefix = string.Join(",", new string[]
                    {
                        "\"" + invoice.Trader.Name + "-" + invoice.DeliveryAddress.Address1 + "\"",
                        ",,,,,,,,", //addr details
                        invoice.InvoiceNumber.ToString(CultureInfo.InvariantCulture),
                        "",//reference
                        invoice.Date.ToString("MM/dd/yyyy"),
                        "",//due date
                        "", //total
                        "", //<EOF>
                    });
                var vats = new Dictionary<decimal, decimal>();
                foreach (var row in invoice.Rows)
                {
                    if (row.Count == 0) continue;
                    var name = "";
                    var vat = 0m;
                    if (row.Item != null)
                    {
                        if (row.Item is Item)
                        {
                            name = (string.IsNullOrWhiteSpace(row.Item.LegacyCode)
                                        ? ((Item)row.Item).ItemGroup.Name.SafeLeft(14) + "-" +
                                          row.Item.Name.SafeLeft(15)
                                        : row.Item.LegacyCode.SafeLeft(30));
                            vat = ((Item)row.Item).Vat;
                        }
                        else
                        {
                            name = (string.IsNullOrWhiteSpace(row.Item.LegacyCode)
                                        ? row.Item.Name.SafeLeft(30)
                                        : row.Item.LegacyCode.SafeLeft(30));
                            vat = Settings["PriceData"].Get<decimal>("DeliveryVat");
                        }
                    }
                    else
                    {
                        name = row.Text.SafeLeft(30);
                        vat = row.DayData.Average(x => x.Vat);
                    }

                    var taxmap = db.XeroTaxMappings.ToDictionary(ks => ks.Tax, vs => vs.Name);
                    if (!taxmap.ContainsKey(vat))
                    {
                        taxmap.Add(vat, "!UNK!");
                    }
                    csv.AppendLine(prefix + string.Join(",", new string[]
                    {
                        "\"" + name + "\"",
                        "\"" + row.Text + "\"",
                        row.Count.ToString(CultureInfo.InvariantCulture),
                        (row.Price/row.Count).ToString(CultureInfo.InvariantCulture),
                        "", //discount
                        MyBusiness.XeroConfig.SalesAccount.ToString(CultureInfo.InvariantCulture),
                        taxmap[vat],
                        ",,,,"
                    }));
                }
            }

            db.SaveChanges();

            return File(Encoding.UTF8.GetBytes(csv.ToString()), "text/csv", "export-" + ids.Min() + "-" + ids.Max() + ".csv");
        }

        public ActionResult FindInvoice(int addrid, string dt)
        {
            var addr = db.Addresses.Find(addrid);

            if (addr == null || dt == null)
            {
                SetAlert(Severity.error, Strings.Invoices_Returns_Bad_Addr_Date);
                return RedirectToAction("ReturnsReport");
            }

            var date = DateTimeExtensions.FromStringUrl(dt);

            var inv = db.Invoices.FirstOrDefault(x => x.DeliveryAddress_Id == addrid && date >= x.StartDate && date <= x.EndDate);
            if (inv == null)
            {
                SetAlert(Severity.error, Strings.Invoices_Returns_Invoice_Not_Found);
                return RedirectToAction("ReturnsReport");
            }

            if (inv.Published)
                return EditAndClone(inv.Id);
            else
                return Edit(inv.Id);
        }
    }
}

