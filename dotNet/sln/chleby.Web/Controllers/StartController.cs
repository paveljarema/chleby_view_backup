﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using chleby.Web.Areas.AppAdmin.Controllers;
using chleby.Web.Areas.AppAdmin.Models;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    public class StartController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            var lang = requestContext.RouteData.Values["lang"].ToString();
            CultureInfo ci;
            try
            {
                if (lang == "u" && requestContext.HttpContext.Request.UserLanguages != null && requestContext.HttpContext.Request.UserLanguages.Length > 0)
                    ci = new CultureInfo(requestContext.HttpContext.Request.UserLanguages[0]);
                else
                    ci = new CultureInfo(lang);
            }
            catch
            {
                ci = new CultureInfo("en-GB");
            }
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = ci;
            base.Initialize(requestContext);
        }

        public ActionResult IndexOld()
        {
            return View("Index_old");
        }

        public ActionResult Index()
        {
            return View("Index");
        }


        public ActionResult TryStreamline()
        {
            return View();
        }


        [HttpPost]
        public ActionResult TryStreamline(AppInfo info, string humanName)
        {
            ViewBag.HumanName = humanName;
            humanName = humanName.ToLower();
            info.Name = "";
            for (int i = 0; i < humanName.Length; ++i)
            {
                if (humanName[i] >= 'a' && humanName[i] <= 'z')
                    info.Name += humanName[i];
                if (info.Name.Length > 16) break;
            }

            info.ConnStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.Replace("Database=chleby", "Database=chleby_" + info.Name);
            info.DeliveryName = "Delivery";
            info.DefaultPT = "COD";
            info.DefaultTT = "Default";
            ModelState.Clear();
            TryValidateModel(info);
            if (MvcApplication.AppRegister.Any(x => x.Name == info.Name))
            {
                ModelState.AddModelError("HumanName", "name already used!");
            }
            if (ModelState.IsValid)
            {
                var jss = new JavaScriptSerializer();

                var qa = new QueuedApp
                    {
                        AppData = jss.Serialize(info),
                        Token = Guid.NewGuid()
                    };
                using (var maindb = new MainAppContext())
                {
                    maindb.QueuedApps.Add(qa);
                    maindb.SaveChanges();
                }
                var msg = new MailMessage(Defines.MailSender, info.AdminUser);
                msg.Subject = "Welcome to Streamline​.";
                msg.IsBodyHtml = true;
                msg.BodyEncoding = msg.SubjectEncoding = Encoding.UTF8;
                msg.Body = string.Format(@"Hi {2},<br/>
<br/>
Thanks for creating an account with Mountain Stream Cloud Software to use Streamline.<br/>
<br/>
<a href='{0}/Start/TryStreamlineConfirm/{1}'>Sign in</a> to get started.<br/>
<br/>
We're here to help you every step of the way. Contact us anytime at help@mountainstream.ms or call  00 48 730 790 709 <br/>
<br/>
Team<br/>
<br/>
Mountain Stream", GetBaseUrl(), qa.Token, info.AdminUser);
                SmtpHelper.Send(msg);
                return View("TryStreamlineWaitForMail", info);
            }
            return View(info);
        }

        public string GetBaseUrl()
        {
            var request = HttpContext.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        public ActionResult TryStreamlineConfirm(Guid id)
        {
            using (var maindb = new MainAppContext())
            {
                var qa = maindb.QueuedApps.FirstOrDefault(x => x.Token == id);
                if (qa == null) return HttpNotFound();

                var jss = new JavaScriptSerializer();
                var info = jss.Deserialize<AppInfo>(qa.AppData);

                MainController.CreateAppImpl(maindb, info);
                SmtpHelper.Send(Defines.MailSender, Defines.OwnerMailAddress, "new account", string.Format(
@"new account:
name: {0},
user: {1},
pass: {2},
person: {3} {4}", info.Name, info.AdminUser, info.AdminPassword, info.FirstName, info.LastName));
                maindb.QueuedApps.Remove(qa);
                maindb.SaveChanges();
                return RedirectToAction("Index", "Home", new { app = info.Name });
            }
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(FrontPageContactModel model)
        {
            if (ModelState.IsValid)
            {
                SmtpHelper.Send(Defines.MailSender,
                    Defines.OwnerMailAddress,
                    "message from contact form",
                    string.Format("Email: {0}\nName: {1}\n\n{2}", model.Email, model.BusinessName, model.Content));
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Features()
        {
            return View();
        }

        public ActionResult Pricing()
        {
            return View();
        }

        public ActionResult Disclaimer()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }


        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult StyleTutorial()
        {
            return View();
        }

        public ActionResult StyleTutorialModal()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult SendMail(EmailField model)
        {
            //dirty hax ;(
            if (model != null &&
                !string.IsNullOrWhiteSpace(model.Email) &&
                EmailAddressAttribute._regex.IsMatch(model.Email))
            {
                //SmtpHelper.Send(Defines.MailSender, "sales@mountainstream.ms", "new contact", model.Email);
                SmtpHelper.Send(Defines.MailSender, "pavel.jarema@gmail.com", "new contact", model.Email);
                lock (typeof(StartController))
                {
                    System.IO.File.AppendAllText(Server.MapPath("~/contactMails.txt"),
                        model.Email + '\n');
                }
                return View();
            }
            return View("Index", model);
        }


    }
}
