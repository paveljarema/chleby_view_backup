﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using MigraDoc;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Rounds")]
    [ChlebyMenu("Routes")]
    public class RoundsController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.listagrup = db.AddressGroups.Where(i => !i.Addresses.Any()).ToList();
            ViewBag.bezgrupy = db.Addresses.
                Where(addr => addr.AddressGroupId == null).ToList();
            ViewBag.usedAddresses = db.Invoices.
                Select(x => x.DeliveryAddress_Id).
                Union(db.Invoices.Select(x => x.Address_Id)).Distinct().ToList();

        }

        [NeedRead]
        public ActionResult PDF(int id)
        {
            var b = db.Businesses.First();
            var round = db.AddressGroups.Find(id);
            var pdfs = new PDFs(b);
            var doc = pdfs.CreateAddressGroupDoc(round);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            var ms = new MemoryStream();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        //
        // GET: /Rounds/
        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModelPenta<Address, IGrouping<AddressGroup, Address>, AddressGroup>
            {
                Index = db.Addresses.Where(x => x.Trader.Live == 1).GroupBy(x => x.AddressGroup).ToList(),
                Create = null,
                Edit = null,
                Create2 = null,
                Edit2 = null
            };
            var lista = (List<IGrouping<AddressGroup, Address>>)cvm.Index;
            lista.AddRange(db.AddressGroups.Where(i => !i.Addresses.Any()).AsEnumerable().
                Select(listag => new FakeGrouping<AddressGroup, Address>(listag)));
            cvm.Index = cvm.Index.OrderBy(x => x.Key).ToList(); //TO MA BYć WSZYNDZIE
            return View(cvm);
        }

        [NeedWrite]
        public ActionResult SaveOrder(Dictionary<string, int?> address, string hwbeh)
        {
            int temp = 0;
            if (address != null)
            {
                foreach (var kvp in address)
                {
                    Address adr = db.Addresses.Find(int.Parse(kvp.Key));
                    adr.RoundOrder = temp;
                    temp++;
                    adr.AddressGroupId = kvp.Value;
                }
                db.SaveChanges();
                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_RoundsController_OrderSaved);
            }
            if (MyBusiness.HandwalkStep > 0)
            {
                if (hwbeh == "stay")
                    return RedirectToAction("Index");
                else if (hwbeh == "prev")
                    return RedirectToAction("HandwalkPrev", "Internal");
                else
                    return RedirectToAction("HandwalkNext", "Internal");
            }
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Rounds/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(AddressGroup addressGroup)
        {
            var adr = new AddressGroup();
            adr = addressGroup;
            if (db.AddressGroups.Any())
            {
                adr.Order = db.AddressGroups.Max(x => x.Order);
                adr.Order++;
            }
            else
            {
                adr.Order = 0;
            }

            if (ModelState.IsValid)
            {
                db.AddressGroups.Add(adr);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(adr);
        }


        [NeedWrite]
        [ActionName("Up")]
        public ActionResult Up(int id)
        {
            AddressGroup adr = db.AddressGroups.Find(id);
            if (adr.Order > 0)
            {
                var order = --adr.Order;
                db.AddressGroups.First(g => g.Order == order && g.Id != id).Order++;
                db.Entry(adr).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [NeedWrite]
        [ActionName("Down")]
        public ActionResult Down(int id)
        {
            AddressGroup adr = db.AddressGroups.Find(id);
            if (db.AddressGroups.Max(x => x.Order) != adr.Order)
            {
                adr.Order++;
                db.AddressGroups.First(g => g.Order == adr.Order && g.Id != id).Order--;
                db.Entry(adr).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [NeedWrite]
        public ActionResult Edit(int id = 0)
        {
            AddressGroup addressgroup = db.AddressGroups.Find(id);
            if (addressgroup == null)
            {
                return HttpNotFound();
            }

            return PartialView(addressgroup);
        }

        //
        // POST: /Rounds/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(AddressGroup addressgroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addressgroup).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "Rounds");

            }

            return PartialView(addressgroup);
        }

        //
        // GET: /Rounds/Delete/5
        //[NeedWrite]
        //public ActionResult Delete(int id = 0)
        //{
        //    Address address = db.Addresses.Find(id);
        //    if (address == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(address);
        //}

        //

        // POST: /Rounds/Delete/5

        [NeedWrite]
        public ActionResult DeleteAddress(int id)
        {
            Address adr = db.Addresses.Find(id);
            adr.AddressGroupId = null;
            db.SaveChanges();
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [NeedWrite]
        public ActionResult Delete(int id)
        {
            AddressGroup adrgroup = db.AddressGroups.Find(id);

            foreach (var other in db.AddressGroups)
            {
                if (other.Order > adrgroup.Order)
                {
                    other.Order--;
                }
            }
            db.AddressGroups.Remove(adrgroup);
            db.SaveChanges();
            if (Request.UrlReferrer != null)
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}