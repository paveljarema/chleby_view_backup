﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MigraDoc.Rendering;
using chleby.Web.Models;
using System.Data.Objects;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("SimpleDist")]
    [ChlebyMenu("AdminDistribution")]
    public class SimpleDistController : ChlebyController
    {
        [NeedRead]
        public ActionResult Index(string date = null)
        {
            var reportDate = DateTime.UtcNow.Date;

            var business = db.Businesses.First();
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(business.Timezone);

            if (date != null)
            {
                reportDate = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);

                //set hour and minutes from crafting deadline
                var deadlineLocal = TimeZoneInfo.ConvertTimeFromUtc(business.CraftingDeadline, timeZoneInfo);
                reportDate = reportDate.AddHours(deadlineLocal.Hour);
                reportDate = reportDate.AddMinutes(deadlineLocal.Minute);

                // convert to utc time
                reportDate = TimeZoneInfo.ConvertTimeToUtc(reportDate, timeZoneInfo);
            }
            else if (db.SnapshotOrderPositions.Any())
            {
                reportDate = db.SnapshotOrderPositions.Where(x => x.MadeCount != 0 || x.OrderedCount != 0).Select(x => x.EndDate).Distinct().OrderByDescending(x => x).First();
            }

            

            var snapshotPositions = 
                (from pos in db.SnapshotOrderPositions
                 where (pos.EndDate.Year == reportDate.Date.Year && pos.EndDate.Month == reportDate.Date.Month && pos.EndDate.Day == reportDate.Date.Day) &&
                    (pos.OrderedCount > 0 || pos.MadeCount > 0) && pos.Item is Item
                    select pos).ToList();

            ViewBag.date = TimeZoneInfo.ConvertTimeFromUtc(reportDate, timeZoneInfo);
            ViewBag.year = reportDate.Year;

            if (business.UsingTradingProps)
            {
                ViewBag.GmapsAddress = business.TradingCity + "," + business.TradingPost + "," + business.TradingAddress1 + "%20" + business.TradingAddress2 + "%20" + business.TradingAddress3;
            }
            else
            {
                ViewBag.GmapsAddress = business.LegalCity + "," + business.LegalPost + "," + business.LegalAddress1 + "%20" + business.LegalAddress2 + "%20" + business.LegalAddress3;
            }

            var dfi = DateTimeFormatInfo.CurrentInfo;
            var cal = dfi.Calendar;
            var myCWR = CultureInfo.InvariantCulture.DateTimeFormat.CalendarWeekRule;

            ViewBag.week = cal.GetWeekOfYear(reportDate.AddDays(1), myCWR, System.DayOfWeek.Monday);
            //HARDCODED MONDAY AS FIRST DAY OF WEEK OMG
            IList<DateTime> endDates = db.SnapshotOrderPositions.Where(x => x.MadeCount != 0 || x.OrderedCount != 0).Select(x => x.EndDate).Distinct().OrderByDescending(x => x).ToList();
            IList<DateTime> endDates2 = new List<DateTime>();
            // localize all end dates
            for (int i = 0; i < endDates.Count; i++)
            {
                endDates[i] = TimeZoneInfo.ConvertTime(endDates[i], timeZoneInfo);
                if (!endDates2.Contains(endDates[i].Date))
                {
                    endDates2.Add(endDates[i].Date);
                }
            }
            ViewBag.alldates = endDates2;

            return View(snapshotPositions);//.Select(x => new DistIndexModel(x.Address, x.Count, x.Items, x.CountMade)));
        }


        //TODO: poza returne, kopipasta z simpleprod/editsave
        public ActionResult Save(Dictionary<string, int> pos)
        {
            if (pos.Count == 0)
                return HttpNotFound();

            int sum = 0;
            var itemid = 0;
            var changes = new List<OrderDiff>();
            foreach (var kvp in pos)
            {
                var val = Math.Max(kvp.Value, 0);
                var p = db.SnapshotOrderPositions.Find(int.Parse(kvp.Key));
                if (p.MadeCount != val)
                    changes.Add(new OrderDiff(p.Adhoc, p, p.MadeCount, val));
                p.MadeCount = val;
                if (itemid == 0)
                    itemid = p.Item_Id;

                sum += val;
            }
            db.SaveChanges();
            SimpleProductionController.NotifyChanges(db, changes, AppName, MyBusiness);
            return RedirectToAction("Index");
        }

        public ActionResult WeightPerGroup(string dt)
        {
            var date = DateTime.Now.AddDays(1).Date;
            if (dt != null)
                date = DateTimeExtensions.FromStringUrl(dt);

            var days = (from pos in db.SnapshotOrderPositions
                        where (pos.OrderedCount > 0 || pos.MadeCount > 0) && pos.Item is Item
                        orderby pos.EndDate descending
                        select pos.EndDate).Distinct().ToList();

            var model = from pos in db.SnapshotOrderPositions
                        where (pos.OrderedCount > 0 || pos.MadeCount > 0) && pos.EndDate == date && pos.Item is Item
                        let item = pos.Item as Item
                        group pos by item.ItemGroup
                        into bygroup
                        select bygroup;

            ViewBag.date = date;
            ViewBag.days = days;
            return View(model.ToList());
        }

        [NeedRead]
        public ActionResult PDFList()
        {
            var dates = db.SnapshotOrderPositions.Where(pos => pos.OrderedCount > 0).Select(x => EntityFunctions.TruncateTime(x.EndDate)).Distinct().OrderByDescending(x => x).ToList();

            IDictionary<DateTime, DateTime> datesDict = new Dictionary<DateTime, DateTime>();

            // localize all end
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            foreach (DateTime? utc in dates)
            {
                if (utc != null)
                {
                    datesDict[utc.Value] = TimeZoneInfo.ConvertTimeFromUtc(utc.Value, timeZoneInfo);
                }
            }

            return View(datesDict);
        }

        [NeedRead]
        public ActionResult PDF(string date)
        {

            var dt = DateTime.UtcNow;
            if (db.SnapshotOrderPositions.Any())
            {
                dt = db.SnapshotOrderPositions.Select(x => x.EndDate).Distinct().OrderByDescending(x => x).First();
            }

            if (date == null)
            {
                date = dt.ToStringUrl();
            }
            var b = db.Businesses.First();
            var pdfs = new PDFs(b);
            var doc = pdfs.CreateDeliveryNotesDoc(date, db);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();
            var ms = new MemoryStream();
            pdr.Save(ms, false);

            return File(ms, "application/pdf");
        }

        [NeedRead]
        public ActionResult PrintNote(string date)
        {
            return View("DeliveryNote");
        }
    }
}
