﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using chleby.Web.Resources;
using System.Data.Entity;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("OrderOverride")]
    [ChlebyMenu("TraderCart")]
    public class OrderOverrideController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_tid == -1)
            {
                RedirectToAction("Index", "Home").ExecuteResult(ControllerContext);
                requestContext.HttpContext.Response.End();
                return;
            }
        }

        //
        // GET: /Orders/
        [NeedRead]
        public ActionResult Index(int id = 0, string date = null, string name=null)
        {
            var trader = db.Traders.Find(_tid);
            if (!trader.Address.Any())
                return RedirectToAction("Index", "Address");
            ViewBag.cartName = name;
            ViewBag.b = this.MyBusiness;

            DateTime utcToday = DateTime.UtcNow.Date;

            var selectedDate = utcToday.AddDays(1);
            var pushdate = false;
            DateTime firstGoodDate = utcToday;
            if (date != null)
            {
                selectedDate = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                int mindelay = db.Items.Where(x => x.Live).Min(x => x.CraftingTime);
                firstGoodDate = selectedDate.AddDays(mindelay +
                                    (MyBusiness.CraftingDeadline.TimeOfDay < DateTime.UtcNow.TimeOfDay ? 1 : 0));
                pushdate = true;
            }

            ViewBag.date = selectedDate;
            ViewBag.DateLocal = ToClientTime(selectedDate);
            ViewBag.overridestart = utcToday.AddDays(1); //should be last day avail before snapshot
            
            //stos danych wyrzuconych z initialize
            var items = db.Items.Where(x => x.Live).ToList().Where(it =>
            {
                var vi = it.Visibility.FirstOrDefault(x => x.TraderId == _tid);
                if (vi == null) return true;
                else return !vi.HiddenByAdmin;

            }).ToList();
            ViewBag.Trader = trader;

            ViewBag.Addresses = trader.Address.ToList();
            ViewBag.GroupedItems = items.GroupBy(ks => ks.ItemGroup).
                OrderBy(x => x.Key).ToList();
            items.Sort(new ItemListComparator((Trader)ViewBag.Trader));
            ViewBag.AllItems = items;

            ViewBag.HasEmployees = trader.Employees.Any();

            if (trader.Address.Any())
            {
                if (id == 0)
                {
                    // ReSharper disable PossibleNullReferenceException
                    id = trader.Address.FirstOrDefault().Id;
                    // ReSharper restore PossibleNullReferenceException
                }

                var validdays = new Dictionary<DateTime, ValidDayReason>();
                ViewBag.validdays = validdays;
                var addr = db.Addresses.Find(id);
                for (int i = 1; i <= int.Parse(Settings["Order"]["FuturePeriod"]); ++i)
                {
                    var thatday = utcToday.AddDays(i);
                    var thatdow = (int)thatday.DayOfWeek - 1;
                    if (thatdow < 0) thatdow += 7;

                    if (db.BusinessHolidays.Any(x => thatday >= x.StartDate && thatday <= x.EndDate))
                        validdays.Add(thatday, ValidDayReason.BusinessHoliday);
                    else if (addr.AddressHolidays.Any(x => thatday >= x.StartDate && thatday <= x.EndDate))
                        validdays.Add(thatday, ValidDayReason.AddressHoliday);
                    else if (!MyBusiness.DeliveryDays.Contains(thatdow))
                        validdays.Add(thatday, ValidDayReason.NoWork);
                    else
                        validdays.Add(thatday, ValidDayReason.OK);
                }

                if (pushdate)
                {
                    foreach (var day in validdays)
                    {
                        if (day.Key.Date >= firstGoodDate.Date && day.Value == ValidDayReason.OK)
                        {
                            firstGoodDate = day.Key;
                            break;
                        }
                    }

                    ViewBag.date = firstGoodDate;
                    ViewBag.DateLocal = ToClientTime(firstGoodDate);
                }
                var guid = (Guid)Membership.GetUser().ProviderUserKey;
                var carts = db.SavedCarts.Where(x => x.UserId == guid).ToList();
                var goodCarts = new List<SavedCart>();
                var jss = new JavaScriptSerializer();
                foreach (var cart in carts)
                {
                    dynamic obj = jss.DeserializeObject(cart.Content);
                    if (obj["addrId"] == id)
                        goodCarts.Add(cart);
                }

                ViewBag.MinDelivery = MyBusiness.MinDelivery;
                ViewBag.DeliveryVat = Settings["PriceData"]["DeliveryVat"];
                ViewBag.carts = goodCarts;

                // get local dates
                IDictionary<DateTime, DateTime> localDates = new Dictionary<DateTime, DateTime>();
                foreach (DateTime utcDate in validdays.Keys)
                {
                    localDates[utcDate] = ToClientTime(utcDate);
                }
                ViewBag.LocalDates = localDates;
                return View(trader.Address.Where(x => x.Id == id).ToList());
            }
            else
            {
                return View();
            }

        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult DeleteCart(string name, int addrId)
        {
            var userid = (Guid)Membership.GetUser().ProviderUserKey;
            var c = db.SavedCarts.FirstOrDefault(x => x.Name == name && x.UserId == userid);
            if (c != null)
            {
                db.SavedCarts.Remove(c);
                db.SaveChanges();
            }
            var carts = db.SavedCarts.Where(x => x.UserId == userid);
            var jss = new JavaScriptSerializer();
            var goodCarts = new List<SavedCart>();

           

            foreach (var cart in carts)
            {
                dynamic obj = jss.DeserializeObject(cart.Content);
                if (obj["addrId"] == addrId)
                    goodCarts.Add(cart);
            }

            var placeholder = new SavedCart();
            placeholder.Content = "";
            placeholder.Name = goodCarts.Any() ? Strings.OrderOverride_LoadCartPlaceholder : Strings.OrderOverride_LoadCartPlaceholderNoCarts;
            goodCarts.Reverse();
            goodCarts.Add(placeholder);
            goodCarts.Reverse();
           
            return Json(goodCarts,JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveOverride(string dt, int addrId, Dictionary<string, int?> items, bool delivery, string note,
            string action, string cartName)
        {
            var date = DateTime.ParseExact(dt, "ddMMyyyy", CultureInfo.InvariantCulture);

            var addr = db.Addresses.Find(addrId);
            if (addr == null)
                return HttpNotFound();

            var dow = (int)date.DayOfWeek - 1;
            if (dow < 0)
                dow += 7;
            var so = db.Orders.OfType<StandingOrder>().
                FirstOrDefault(x => x.Day == dow && x.Address.Id == addrId);
            var ao = db.Orders.OfType<AdhocOrder>().
                FirstOrDefault(x => x.Date == date && x.Address.Id == addrId);

            if (action == "save")
            {
                if (string.IsNullOrWhiteSpace(cartName))
                {
                    SetAlert(Severity.error, Strings.OrderOverride_CartNameIsReqired);
                    return RedirectToAction("Index", new { date = dt, id = addrId });
                }

                var userid = (Guid) Membership.GetUser().ProviderUserKey;
                var json = new JavaScriptSerializer().Serialize(new {dt, addrId, items, delivery, note});
                if (db.SavedCarts.Any(x => x.UserId == userid && x.Name == cartName))
                {
                    SetAlert(Severity.error, Strings.Controller_OrderOverride_NameUsed);
                    return RedirectToAction("Index", new { date = dt, id = addrId });   
                }
                db.SavedCarts.Add(new SavedCart
                {
                    UserId = userid,
                    Content = json,
                    Name = cartName
                });
                db.SaveChanges();
                return RedirectToAction("Index", new { date = dt, id = addrId, name=cartName });
            }

            var ordermgr = new OrderManager(db, MyBusiness, _tid);

            foreach (var kvp in items)
            {
                var itemId = int.Parse(kvp.Key);
                if (!kvp.Value.HasValue) //pusto wiec wywalamy
                {
                    if (ao != null) //jest adhoc
                    {
                        var aopos = ao.Items.FirstOrDefault(x => x.Item.Id == itemId);
                        if (aopos != null) //z naszym itemem
                        {
                            ordermgr.DeleteAdhocPos(aopos.Id); //to w pizdu go
                        }
                    }
                }
                else //ciolek cos wpisal
                {
                    if (so != null) //i mamy standing
                    {
                        var sopos = so.Items.FirstOrDefault(x => x.Item.Id == itemId);
                        //bez naszego itemu lub z inna iloscia
                        if (sopos == null || sopos.OrderedCount != kvp.Value)
                        {
                            //to zrob adhoca
                            ordermgr.AddAdhoc(addrId, itemId, date, kvp.Value.Value, delivery, note);
                        }
                        else //standing ma tyle co wpisalismy
                        {
                            if (ao != null) //ale jest adhoc
                            {
                                var aopos = ao.Items.FirstOrDefault(x => x.Item.Id == itemId);
                                if (aopos != null) //z naszym itemem
                                {
                                    //to w pizdu go
                                    ordermgr.DeleteAdhocPos(aopos.Id);
                                }
                            }
                        }
                    }
                    else //i nie ma standinga
                    {
                        //to zrob adhoca
                        ordermgr.AddAdhoc(addrId, itemId, date, kvp.Value.Value, delivery, note);
                    }
                }
            }

            SetAlert(Severity.success, Strings.OrderOverride_OrderPlaced);

            return RedirectToAction("Index", new { date = date.ToString("ddMMyyyy") });
        }

        public ActionResult DeleteCartOrder(string date, int addrId, int itemId)
        {
            var dt = DateTimeExtensions.FromStringUrl(date);
            var addr = db.Addresses.Find(addrId);

            if (addr.TraderId != _tid)
                return HttpNotFound(); //jestesmy traderem i nie zgadza sie id -> foch

            var ordermgr = new OrderManager(db, MyBusiness, _tid);
            var pos = db.OrderPositions.Include(x=>x.Order).FirstOrDefault(x => x.Item_Id == itemId &&
                                                   x.Order.Address_Id == addrId &&
                                                   (x.Order as AdhocOrder).Date == dt);

            if (pos != null && !db.SnapshotOrderPositions.Any(x => x.Order_Id == pos.Order.Id))
            {
                ordermgr.DeleteAdhocPos(pos.Id);
            }

            return RedirectToAction("CartSummary");
        }

        [ChlebyMenu("TraderPendingCart")]
        public ActionResult CartSummary()
        {
            //var last = DateTime.ParseExact(
            //        MvcApplication.Settings[db.AppConfig.Name]["Snapshot"]["LastDate"],
            //        "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

            var last = MyBusiness.LastSnapshotDate ?? DateTime.MinValue;
            ViewBag.Lastsnap = last;

            var future = from order in db.Orders.OfType<AdhocOrder>()
                         where order.Date > last && order.Address.TraderId == _tid
                         select order.Id;

            var snaps = from snappos in db.SnapshotOrderPositions
                        where snappos.Order is AdhocOrder && snappos.Address.TraderId == _tid
                        let order = snappos.Order as AdhocOrder
                        select order.Id;

            var ordersId = future.Union(snaps);

            var obj = from order in db.Orders.OfType<AdhocOrder>()
                      where ordersId.Contains(order.Id)
                      from pos in order.Items
                      //where pos.OrderedCount > 0
                      group pos by order.Date
                          into byday
                          select new
                          {
                              date = byday.Key,
                              byaddr = byday.GroupBy(x => x.Order.Address).
                              Select(x => new
                              {
                                  addr = x.Key,
                                  items = x.GroupBy(y => y.Item)
                              })
                          };

            IDictionary<DateTime, DateTime> localDates = new Dictionary<DateTime, DateTime>();
            foreach (var o in obj)
            {
                localDates[o.date] = ToClientTime(o.date);
            }
            ViewBag.LocalDates = localDates;

            return View("~/Views/SimpleProduction/CartSummary.cshtml", obj.ToDictionary(
                ks => ks.date,
                es => es.byaddr.ToDictionary(
                    ks2 => ks2.addr,
                    es2 => es2.items)));
        }

        [NeedRead]
        [ChlebyMenu("TraderTotalOrder")]
        public ActionResult TotalSummary(int week = 0, int year = 0)
        {
            if (year == 0) year = DateTime.Now.Year;
            if (week == 0) week = DateTime.Now.GetWeek();

            var start = WeekHelper.FirstDateOfWeek(year, week, DayOfWeek.Monday);
            var end = start.AddDays(7);
            var sos = from order in db.Orders.OfType<StandingOrder>()
                      from pos in order.Items
                      where pos.OrderedCount > 0 && pos.Order.Address.TraderId == _tid
                      group pos by order.Day
                          into byday
                          select byday;

            var sosBA = from order in db.Orders.OfType<StandingOrder>()
                        from pos in order.Items
                        where pos.OrderedCount > 0 && pos.Order.Address.TraderId == _tid
                        group pos by pos.Item
                            into g
                            select new
                            {
                                k = g.Key,
                                v = g.GroupBy(x => x.Order.Address)
                            };

            var aos = from order in db.Orders.OfType<AdhocOrder>()
                      where order.Date >= start && order.Date < end
                      from pos in order.Items
                      where pos.OrderedCount > 0 && pos.Order.Address.TraderId == _tid
                      group pos by order.Date
                          into bydate
                          select bydate;

            var aosBA = from order in db.Orders.OfType<AdhocOrder>()
                        where order.Date >= start && order.Date < end
                        from pos in order.Items
                        where pos.OrderedCount > 0 && pos.Order.Address.TraderId == _tid
                        group pos by pos.Item
                            into g
                            select new
                            {
                                k = g.Key,
                                v = g.GroupBy(x => x.Order.Address)
                            };

            DateTime firstAO = DateTime.Now.Date;
            var firstAOquery = (from order in db.Orders.OfType<AdhocOrder>()
                                where order.Date.Year == year && order.Address.TraderId == _tid
                                select order.Date);
            if (firstAOquery.Any())
                firstAO = firstAOquery.Min();

            ViewBag.sosBA = sosBA.ToDictionary(ks => ks.k, es => es.v);
            ViewBag.aosBA = aosBA.ToDictionary(ks => ks.k, es => es.v);
            ViewBag.week = week;
            ViewBag.year = year;
            ViewBag.date = start;
            ViewBag.firstWeek = firstAO.GetWeek();
            ViewBag.lastWeek = DateTime.Now.AddDays(Settings["Order"].Get<int>("FuturePeriod")).GetWeek();
            return View("~/Views/SimpleProduction/TotalSummary.cshtml", Tuple.Create(
                sos.ToDictionary(ks => ks.Key, es => es.ToList()),
                aos.ToDictionary(ks => ks.Key, es => es.ToList())
                ));
        }
    }
}
