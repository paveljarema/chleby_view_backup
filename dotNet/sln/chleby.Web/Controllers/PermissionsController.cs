﻿using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using chleby.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace chleby.Web.Controllers
{
    public class PermissionsController : ChlebyController
    {
        [OutputCache(Location = OutputCacheLocation.None)]
        [CustomPerm("userperm")]
        public ActionResult EmployeeEdit(int id, bool admin)
        {
            if (Roles.IsUserInRole(Defines.Roles.Trader))
                return HttpNotFound();

            var modules = (from um in MvcApplication.UsedModules
                           where um.App.Name == AppName
                           select um.Module).Union(
                   (from mr in MvcApplication.ModuleRegister
                    where mr.IsCore
                    select mr)).Where(p => p.Admin == admin && p.Category != "-").Distinct().ToList();

            ViewBag.User = db.BusinessEmployees.SingleOrDefault(t => t.Id == id);
            var user = FromBEId(id);
            if (user == null)
                return HttpNotFound();
            ViewBag.UserName = user.UserName;
            ViewBag.Target = "Employee";
            ViewBag.AdminPerms = admin;
            return PartialView("Edit", modules);
        }

        //
        // POST: /Permissions/Edit/5

        [HttpPost]
        [CustomPerm("userperm")]
        public ActionResult EmployeeEdit(int id, Dictionary<string, string> module, Dictionary<string, bool> perm)
        {
            Session["alerts"] = new List<UIAlert>();
            try
            {
                var user = FromBEId(id);
                if (user == null)
                    return HttpNotFound();
                foreach (var kvp in module)
                {
                    try { Roles.RemoveUserFromRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix); }
                    catch { }
                    try { Roles.RemoveUserFromRole(user.UserName, kvp.Key + Defines.Roles.WriteSuffix); }
                    catch { }

                    if (kvp.Value == "read")
                    {
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix);
                    }
                    else if (kvp.Value == "write")
                    {
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix);
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.WriteSuffix);
                    }
                }

                foreach (var kvp in perm)
                {
                    try { Roles.RemoveUserFromRole(user.UserName, Defines.Roles.CustomPrefix + kvp.Key); }
                    catch { }
                    if (kvp.Value)
                        Roles.AddUserToRole(user.UserName, Defines.Roles.CustomPrefix + kvp.Key);
                }

                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_Permissions_PermSaved);
                return RedirectToAction("Index", "BusinessEmployee");
            }
            catch
            {
                SetAlert(Severity.error, chleby.Web.Resources.Strings.Controller_Permissions_PermNotSaved);
                return RedirectToAction("Index", "BusinessEmployee");
            }
        }

        private MembershipUser FromBEId(int id)
        {
            var dbUser = db.BusinessEmployees.SingleOrDefault(t => t.Id == id);
            if (dbUser == null)
                return null;
            return Membership.GetUser(dbUser.ExternalAccountData);
        }


        [OutputCache(Location = OutputCacheLocation.None)]
        [CustomPerm("traderperm")]
        public ActionResult TraderEdit(int id)
        {
            var modules = (from um in MvcApplication.UsedModules
                           where um.App.Name == AppName
                           select um.Module).Union(
                   (from mr in MvcApplication.ModuleRegister
                    where mr.IsCore
                    select mr)).Where(p => !p.Admin && p.Category!="-").Distinct().ToList();

            ViewBag.User = db.TraderEmployees.SingleOrDefault(t => t.Id == id);
            var user = FromTEId(id);
            if (user == null)
                return HttpNotFound();
            ViewBag.UserName = user.UserName;
            ViewBag.Target = "Trader";
            ViewBag.AdminPerms = false;
            return PartialView("Edit", modules);
        }

        //
        // POST: /Permissions/Edit/5

        [HttpPost]
        [CustomPerm("traderperm")]
        public ActionResult TraderEdit(int id, Dictionary<string, string> module, Dictionary<string,bool> perm)
        {
            try
            {
                var user = FromTEId(id);
                if (user == null)
                    return HttpNotFound();
                foreach (var kvp in module)
                {
                    try { Roles.RemoveUserFromRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix); }
                    catch { }
                    try { Roles.RemoveUserFromRole(user.UserName, kvp.Key + Defines.Roles.WriteSuffix); }
                    catch { }

                    if (kvp.Value == "read")
                    {
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix);
                    }
                    else if (kvp.Value == "write")
                    {
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.ReadSuffix);
                        Roles.AddUserToRole(user.UserName, kvp.Key + Defines.Roles.WriteSuffix);
                    }

                }

                foreach (var kvp in perm)
                {
                    try { Roles.RemoveUserFromRole(user.UserName, Defines.Roles.CustomPrefix + kvp.Key); }
                    catch {}
                    if (kvp.Value)
                        Roles.AddUserToRole(user.UserName, Defines.Roles.CustomPrefix + kvp.Key);
                }
                SetAlert(Severity.success, chleby.Web.Resources.Strings.Controller_Permissions_PermSaved);
                return RedirectToAction("Index", "TraderEmployees");
            }
            catch
            {
                return RedirectToAction("Index", "TraderEmployees");
            }
        }

        private MembershipUser FromTEId(int id)
        {
            var dbUser = db.TraderEmployees.SingleOrDefault(t => t.Id == id);
            if (dbUser == null || dbUser.TraderId != _tid)
                return null;
            return Membership.GetUser(dbUser.ExternalAccountData);
        }
    }
}
