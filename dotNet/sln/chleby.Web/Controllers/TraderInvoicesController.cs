﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using chleby.Web.Models;
using Ionic.Zip;
using MigraDoc.Rendering;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("CInvoices")]
    [ChlebyMenu("TraderInvoices")]
    public class TraderInvoicesController : ChlebyController
    {
        //
        // GET: /TraderInvoices/

        public ActionResult Index()
        {
            IList<Invoice> invoices = db.Invoices.Where(x => x.Trader != null &&
                x.Trader.Id == _tid && x.Published && !x.Deleted).OrderByDescending(x => x.Date).ToList();
            IList<Invoice> filtered = new List<Invoice>();
            foreach (Invoice inv in invoices)
            {
                if (!filtered.Any(i => i.InvoiceNumber == inv.InvoiceNumber))
                {
                    int latestRev = invoices.Where(i => i.InvoiceNumber == inv.InvoiceNumber).Max(i => i.Revision);
                    filtered.Add(invoices.Where(i => i.InvoiceNumber == inv.InvoiceNumber && i.Revision == latestRev).FirstOrDefault());
                }
                
            }
            return View(filtered);
        }

        public ActionResult PdfAll()
        {
            var invoices = db.Invoices.Where(x => x.Trader != null && x.Trader.Id == _tid
                && x.Published && !x.Deleted).ToList();

            if (!invoices.Any())
            {
                return RedirectToAction("Index");
            }

            var zip = new ZipFile();
            foreach (var invoice in invoices)
            {
                var ms = GetInvoicePdf(invoice);
                var name = string.Format("invoice_{0}_{1}-{2}_{3}_{4}.pdf",
                    InvoicesController.ReplaceInvalidChars(invoice.Trader.Name),
                    invoice.InvoiceNumber, invoice.Revision, invoice.Week, invoice.Year);
                if (!zip.ContainsEntry(name))
                    zip.AddEntry(name, ms);
                else
                    _log.Error("duplicate invoice " + name);
            }
            var zipstream = new MemoryStream();
            zip.Save(zipstream);
            return File(zipstream.ToArray(), "application/pdf",
                        string.Format("invoices_{0}.zip", invoices[0].Trader.Name));
        }

        public ActionResult Pdf(int id)
        {
            var invoice = db.Invoices.Find(id);
            if (invoice.Trader == null || invoice.Trader.Id != _tid)
                return HttpNotFound();

            var ms = GetInvoicePdf(invoice);

            return File(ms, "application/pdf", invoice.InvoiceNumber + ".pdf");
        }

        private MemoryStream GetInvoicePdf(Invoice invoice)
        {
            var ms = new MemoryStream();
            var pdfs = new PDFs(MyBusiness);
            var doc = pdfs.CreateInvoiceDoc(invoice, db);
            var pdr = new PdfDocumentRenderer();
            pdr.Document = doc;
            pdr.RenderDocument();
            pdr.WriteDocumentInformation();

            pdr.Save(ms, false);
            return ms;
        }
    }
}
