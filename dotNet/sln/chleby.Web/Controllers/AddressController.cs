﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using AutoMapper;
using Newtonsoft.Json;
using chleby.Web.Models;
using System.Collections.ObjectModel;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("Addresses")]
    [ChlebyMenu("TraderAddresses")]
    public class AddressController : ChlebyController
    {
        //
        // GET: /Address/

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.HasEmployees = db.TraderEmployees.Any(x => x.TraderId == _tid);
            ViewBag.PossibleTraders = db.TraderEmployees.Where(x => x.TraderId == _tid).ToList();
            ViewBag.PossibleRounds = db.AddressGroups.OrderBy(x => x.Order).ToList();
            ViewBag.usedAddresses = db.Invoices.
                Select(x => x.DeliveryAddress_Id).
                Union(db.Invoices.Select(x => x.Address_Id)).Distinct().ToList();
        }

        [NeedRead]
        public ActionResult Export()
        {
            var trader = db.Traders.Find(_tid);
            var list = db.Addresses.Where(te => te.TraderId == _tid || _tid == -1).ToList();
            var name = string.Format("{0}_{1}_export_{2}.csv", trader.Name,
                                     "Address",
                                     DateTime.Now.ToString("yy-MM-dd-HH-mm"));
            return File(System.Text.Encoding.UTF8.GetBytes(CsvExporter.Export(list)), "text/plain; charset=utf-8", name);
        }

        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModel<Address>
            {
                Index = db.Addresses.Where(te => te.TraderId == _tid || _tid == -1).ToList(),
                Create = null,
                Edit = null
            };
            return View(cvm);
        }

        //
        // GET: /Address/Create

        [OutputCache(Location = OutputCacheLocation.None)]
        [NeedWrite]
        public ActionResult Create(int tid)
        {
            var trader = db.Traders.Find(tid);
            if (trader == null)
                return HttpNotFound();

            ViewBag.PossibleTraders = trader.Employees;
            var addr = new Address();
            addr.TraderEmployees.Add(trader.Employees.First());
            return PartialView("CreatePopup", addr);
        }


        [NeedWrite]
        [HttpPost]
        public ActionResult Create(Address address, int? wybrany)
        {
            if (ModelState.IsValid && wybrany.HasValue)
            {
                address.TraderId = _tid;

                if (Roles.IsUserInRole(Defines.Roles.Trader))
                {
                    address.LegacyCode = null;
                    address.DeliveryPrice = 0;
                    address.Live = false;
                    address.DeliveryDayValue = 0;
                    address.MapLink = null;
                }
                db.Addresses.Add(address);
                db.SaveChanges();

                address = db.Addresses.Include(x => x.TraderEmployees).Single(x => x.Id == address.Id);
                if (address.TraderEmployees != null) address.TraderEmployees.Clear();
                else address.TraderEmployees = new Collection<TraderEmployee>();
                db.SaveChanges();

                address.TraderEmployees.Add(db.TraderEmployees.Find(wybrany));
                if (_tid > 0)
                {
                    var trader = db.Traders.Find(_tid);
                    if (trader.Address.Count() == 1)
                        trader.Address.First().AddressIsPrimary = true;
                }
                Log("create", address.Id);
                db.SaveChanges();
                if (Roles.IsUserInRole(Defines.Roles.Trader))
                {
                    new Mail(db, "newAddress", MyBusiness.MailConfirmAddress, new
                        {
                            trader = address.Trader.Name,
                            addr = address
                        }).Send();
                }

                return RedirectToAction("Index");
            }

            if (!wybrany.HasValue)
            {
                ModelState.AddModelError("wybrany", chleby.Web.Resources.Strings.Controller_AddressController_Person_Responsible);
            }

            var cvm = new ComboViewModel<Address>
                {
                    Index = db.Addresses.Where(te => te.TraderId == _tid || _tid == -1).ToList(),
                    Create = address,
                    Edit = null
                };
            return View("Index", cvm);
        }

        [NeedWrite]
        public ActionResult SetPrimary(int id = 0)
        {
            foreach (var adr in db.Addresses.Where(x => x.TraderId == _tid || _tid == -1).ToList())
            {
                adr.AddressIsPrimary = false;
            }
            var address = db.Addresses.Single(x => x.Id == id && (x.TraderId == _tid || _tid == -1));
            address.AddressIsPrimary = true;
            Log("setprimary", address.Id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Address/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            var address = db.Addresses.SingleOrDefault(x => x.Id == id && (x.TraderId == _tid || _tid == -1));
            if (address == null)
                return HttpNotFound();
            return PartialView(address);
        }

        //
        // POST: /Address/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(Address address, int wybrany)
        {
            var oldAddress = db.Addresses.SingleOrDefault(x => x.Id == address.Id && (x.TraderId == _tid || _tid == -1));
            if (oldAddress == null)
                return HttpNotFound();
            if (ModelState.IsValid)
            {
                address.TraderId = _tid <= 0 ? oldAddress.TraderId : _tid;

                if (Roles.IsUserInRole(Defines.Roles.Trader))
                {
                    address.DeliveryPrice = oldAddress.DeliveryPrice;
                    address.AddressGroupId = oldAddress.AddressGroupId;
                    address.Live = oldAddress.Live;
                    address.DeliveryDayValue = oldAddress.DeliveryDayValue;
                    address.MapLink = oldAddress.MapLink;
                }
                else
                {
                    if (oldAddress.AddressGroupId != address.AddressGroupId)
                        address.RoundOrder = 640 * 1024; //640kb powinno wystarczyc kazdemu :)
                    if (address.DeliveryDayValue != oldAddress.DeliveryDayValue)
                    {
                        var emp = address.TraderEmployees.FirstOrDefault();
                        if (emp != null)
                        {
                            var user = Membership.GetUser(emp.ExternalAccountData);
                            if (user != null)
                            {
                                var mail = user.Email;
                                new Mail(db, "addressDayChanged", mail, new
                                {
                                    address,
                                    emp,
                                    dd = string.Join(",", address.DeliveryDays.Select(x => x.GetShortDayName()))
                                }).Send();
                            }
                        }
                    }
                }

                Log("edit", address.Id);
                db.SaveChanges();

                // addres trader employee work around
                if (wybrany > 0)
                {
                    db.Database.ExecuteSqlCommand("delete AddressTraderEmployees where Address_Id = {0}", address.Id);
                    db.Database.ExecuteSqlCommand("insert into AddressTraderEmployees(Address_Id, TraderEmployee_Id) values ({0}, {1})", address.Id, wybrany);
                }
                if (Request.IsAjaxRequest())
                {
                    return Json(new { ok = true });
                }

                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<Address>
            {
                Index = db.Addresses.Where(te => te.TraderId == _tid || _tid == -1).ToList(),
                Create = null,
                Edit = address
            };
            return View("Index", cvm);
        }

        //
        // POST: /Address/Delete/5
        [NeedWrite]
        public ActionResult Delete(int id = 0, bool fromroutes = false)
        {
            if (db.Invoices.Any(x => x.Address_Id == id || x.DeliveryAddress_Id == id))
                throw new Exception(chleby.Web.Resources.Strings.Controller_AddressController_Invoice_Exists);

            var address = db.Addresses.Single(x => x.Id == id && (x.TraderId == _tid || _tid == -1));
            db.SnapshotOrderPositions.Where(x => x.Address.Id == id).ToList().ForEach(x => db.SnapshotOrderPositions.Remove(x));

            if (address.AddressIsPrimary)
            {
                var adrfirst = address.Trader.Address.FirstOrDefault();
                if (adrfirst != null)
                    adrfirst.AddressIsPrimary = true;
            }
            db.Addresses.Remove(address);
            Log("delete", address.Id);
            db.SaveChanges();
            if (fromroutes)
                return RedirectToAction("Index", "Rounds");
            else
                return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Import()
        {
            ViewBag.What = "addresses";
            return PartialView();
        }

        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return Import(str);
        }

        [HttpPost]
        public ActionResult Import(string csv, bool save = false)
        {
            ViewBag.csv = csv;
            ViewBag.What = "addresses";

            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var addrs = new List<Address>();
            var rounds = new List<AddressGroup>();
            foreach (var tline in lines)
            {
                var addr = new Address();
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 10)
                {
                    errors.Add(Tuple.Create(curline, -1, "wrong field count"));
                    continue;
                }

                var tradername = fields[0].Trim().Normalize(NormalizationForm.FormKD);
                if (string.IsNullOrWhiteSpace(fields[0]))
                {
                    errors.Add(Tuple.Create(curline, 1, "empty customer"));
                    continue;
                }
                var trader = db.Traders.FirstOrDefault(x => x.PrimaryName == tradername);
                if (trader == null)
                {
                    errors.Add(Tuple.Create(curline, 1, "invalid customer"));
                    continue;
                }
                if (!trader.Address.Any() && addrs.All(x => x.Trader != trader))
                    addr.AddressIsPrimary = true;

                addr.Trader = trader;

                var person = trader.Employees.FirstOrDefault();
                if (person == null)
                {
                    errors.Add(Tuple.Create(curline, 1, "no employees"));
                    continue;
                }
                addr.TraderEmployees.Add(person);

                if (string.IsNullOrWhiteSpace(fields[1]))
                {
                    errors.Add(Tuple.Create(curline, 2, "empty address1"));
                    continue;
                }
                addr.Address1 = fields[1].Trim();
                addr.Address2 = fields[2].Trim();
                addr.Address3 = fields[3].Trim();

                if (string.IsNullOrWhiteSpace(fields[4]))
                {
                    errors.Add(Tuple.Create(curline, 5, "empty city"));
                    continue;
                }
                addr.City = fields[4].Trim();

                if (string.IsNullOrWhiteSpace(fields[5]))
                {
                    errors.Add(Tuple.Create(curline, 6, "empty postcode"));
                    continue;
                }
                addr.Post = fields[5].Trim();

                if (db.Addresses.Any(x =>
                                     x.Address1 == addr.Address1 &&
                                     x.City == addr.City &&
                                     x.Post == addr.Post &&
                                     x.TraderId == addr.Trader.Id))
                {
                    continue; //omijamy uzytego juz
                }

                var roundname = fields[6].Trim().Normalize(NormalizationForm.FormKD);
                if (!string.IsNullOrWhiteSpace(roundname))
                {
                    var ag = db.AddressGroups.FirstOrDefault(x => x.Name == roundname);
                    if (ag == null)
                    {
                        ag =
                            rounds.FirstOrDefault(
                                x => x.Name.Equals(roundname, StringComparison.InvariantCultureIgnoreCase));
                        if (ag == null)
                        {
                            ag = new AddressGroup() {Name = roundname};
                            rounds.Add(ag);
                        }
                    }
                    addr.AddressGroup = ag;
                }

                if (string.IsNullOrWhiteSpace(fields[7]))
                {
                    errors.Add(Tuple.Create(curline, 8, "empty delivery price"));
                    continue;
                }
                var price = 0m;
                if (!decimal.TryParse(fields[7], NumberStyles.Any, CultureInfo.InvariantCulture, out price))
                {
                    errors.Add(Tuple.Create(curline, 8, "invalid delivery price"));
                    continue;
                }
                addr.DeliveryPrice = price;

                addr.Live = true;
                addr.DeliveryDayValue = 255;
                addr.SpecialNote = fields[8].Trim();
                addr.MapLink = fields[9].Trim();

                addrs.Add(addr);
            }

            if (errors.Count == 0 && save)
            {
                rounds.ForEach(x => db.AddressGroups.Add(x));
                addrs.ForEach(x => db.Addresses.Add(x));
                db.SaveChanges();
                if (_tid == -1)
                {
                    return RedirectToAction("Index", "TurboTraders");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.errors = errors;
            return PartialView("Import");
        }

        [NeedWrite]
        public ActionResult DwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Customer*,Address1*,Address2,Address3,City*,Postcode*,Route,Delivery Price*,Delivery Notes,GoogleMaps Link
" + string.Join("\n", db.Traders.Select(x => x.PrimaryName))), "text/csv; charset=utf-8", "CustomerAddresses.csv");
        }
    }
}