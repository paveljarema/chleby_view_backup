﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using chleby.Web.Models;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Employee)]
    [ChlebyModule("Holidays")]
    [ChlebyMenu("AdminCalendar")]
    public class AllHolidayController : ChlebyController
    {
        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModel<BusinessHoliday>
            {
                Index = db.BusinessHolidays.ToList(),
                Create = null,
                Edit = null
            };
            ViewBag.Trader = db.Traders.ToList();
            ViewBag.TraderHoliday = db.AddressHolidays.ToList();
            return View(cvm);
        }

        //
        // GET: /BusinessHoliday/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(BusinessHoliday businessHoliday, string calendarView, string calendarMonth, string calendarYear)
        {
            Session["CalendarView"] = calendarView;
            Session["CalendarMonth"] = calendarMonth;
            Session["CalendarYear"] = calendarYear;
            if (ModelState.IsValid)
            {
                db.BusinessHolidays.Add(businessHoliday);
                Log("create", businessHoliday.Id);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<BusinessHoliday>
            {
                Index = db.BusinessHolidays.ToList(),
                Create = businessHoliday,
                Edit = null
            };
            return View("Index", cvm);
        }



        //
        // GET: /BusinessHoliday/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id = 0)
        {
            BusinessHoliday businessholiday = db.BusinessHolidays.Find(id);
            if (businessholiday == null)
            {
                return HttpNotFound();
            }
            ViewBag.Action = "Edit";
            return PartialView(businessholiday);
        }

        //
        // POST: /BusinessHoliday/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(BusinessHoliday businessHoliday, string calendarView, string calendarMonth, string calendarYear)
        {
            Session["CalendarView"] = calendarView;
            Session["CalendarMonth"] = calendarMonth;
            Session["CalendarYear"] = calendarYear;
            if (ModelState.IsValid)
            {
                db.Entry(businessHoliday).State = EntityState.Modified;
                Log("edit", businessHoliday.Id);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<BusinessHoliday>
            {
                Index = db.BusinessHolidays.ToList(),
                Create = null,
                Edit = businessHoliday
            };

            return View("Index", cvm);
        }

        //
        // GET: /BusinessHoliday/Delete/5

        [NeedWrite]
        public ActionResult Delete(int id = 0)
        {
            BusinessHoliday businessholiday = db.BusinessHolidays.Find(id);
            db.BusinessHolidays.Remove(businessholiday);
            Log("delete", businessholiday.Id);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}