using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI;
using AutoMapper;
using chleby.Web.Models;
using chleby.Web.Resources;

namespace chleby.Web.Controllers
{
    //[ChlebyAuthorize(ReqRoles = RequiredRole.Both)]
    [ChlebyModule("CEmployees")]
    [ChlebyMenu("TraderUsers")]
    public class TraderEmployeesController : ChlebyController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

        }
        [NeedRead]
        public ActionResult Export()
        {
            var trader = db.Traders.Find(_tid);
            var list = new List<TraderEmployeeAccount>();
            foreach (var be in db.TraderEmployees.Where(te => te.TraderId == _tid).ToList())
            {
                var user = Membership.GetUser(be.ExternalAccountData);
                if (user == null)
                    DebugReport("trader user=null, id={0}", be.ExternalAccountData);
                else
                    list.Add(new TraderEmployeeAccount
                    {
                        BusinessRole = be.BusinessRole,
                        FirstName = be.FirstName,
                        ExternalAccountData = be.ExternalAccountData,
                        Id = be.Id,
                        LastName = be.LastName,
                        Phone = be.Phone,
                        Note = be.Note,
                        UserName = user.UserName,
                        Addresses = be.Addresses.ToList(),
                        TraderId = be.TraderId
                    });
            }
            var name = string.Format("{0}_{1}_export_{2}.csv", trader.Name,
                                     "TraderEmployee",
                                     DateTime.Now.ToString("yy-MM-dd-HH-mm"));
            return File(System.Text.Encoding.UTF8.GetBytes(CsvExporter.Export(list)), "text/plain; charset=utf-8", name);
        }

        [NeedRead]
        public ActionResult Index()
        {
            var cvm = new ComboViewModel<TraderEmployeeAccount>
            {
                Index = GetTraderEmployeeAccounts(),
                Create = null,
                Edit = null
            };
            return View(cvm);
        }

        private IEnumerable<TraderEmployeeAccount> GetTraderEmployeeAccounts()
        {
            var model = new List<TraderEmployeeAccount>();
            foreach (var be in db.TraderEmployees.Where(te => te.TraderId == _tid).ToList())
            {
                var user = Membership.GetUser(be.ExternalAccountData);
                if (user == null)
                    DebugReport("trader user=null, id={0}", be.ExternalAccountData);
                else
                    model.Add(new TraderEmployeeAccount
                    {
                        BusinessRole = be.BusinessRole,
                        FirstName = be.FirstName,
                        ExternalAccountData = be.ExternalAccountData,
                        Id = be.Id,
                        LastName = be.LastName,
                        Phone = be.Phone,
                        Note = be.Note,
                        UserName = user.UserName,
                        Addresses = be.Addresses.ToList(),
                        TraderId = be.TraderId,
                        MainContact = be.MainContact
                    });
            }
            return model;
        }

        //
        // GET: /TraderEmployees/Create
        [NeedWrite]
        public ActionResult Create()
        {
            //ViewBag.PossibleTraders = db.Traders;
            return View();
        }

        //
        // POST: /TraderEmployees/Create
        [NeedWrite]
        [HttpPost]
        public ActionResult Create(TraderEmployeeAccount bea)
        {
            ComboViewModel<TraderEmployeeAccount> cvm;
            if (Membership.GetUser(bea.UserName) != null)
                ModelState.AddModelError("UserName", "User exists");

            if (ModelState.IsValid)
            {
                var password = Membership.GeneratePassword(8, 1);
                try
                {
                    var user = Membership.CreateUser(bea.UserName, password, bea.UserName);
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                    Roles.AddUserToRole(bea.UserName, Defines.Roles.Trader);
                    var te = new TraderEmployee
                    {
                        BusinessRole = bea.BusinessRole,
                        ExternalAccountData = (Guid)user.ProviderUserKey,
                        FirstName = bea.FirstName,
                        LastName = bea.LastName,
                        Phone = bea.Phone,
                        Note = bea.Note,
                        TraderId = _tid,
                        MainContact = (Roles.IsUserInRole(Defines.Roles.Employee) || 
                                         Roles.IsUserInRole(Defines.Roles.SuperAdmin)) 
                                         ? bea.MainContact : false
                    };
                    db.TraderEmployees.Add(te);

                    var trader = db.Traders.Find(_tid);

                    if (MyBusiness.HandwalkStep == 0) //w handwalku nie wysylamy od razu
                        CreateInvitation(user, te);

                    db.SaveChanges();

                    //pierwszemu dodajemy full uprawnienia dla kategorii trader
                    trader = db.Traders.Find(_tid);
                    if (trader.Employees.Count == 1)
                    {
                        SetAllRoles(bea.UserName, AppName);
                    }
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(chleby.Web.Resources.Strings.Controller_Business_Employee_Error_Creation, e);
                    _log.Error("err in traderemployee tid=" + _tid, e);
                    Membership.DeleteUser(bea.UserName);
                    cvm = new ComboViewModel<TraderEmployeeAccount>
                    {
                        Index = GetTraderEmployeeAccounts(),
                        Create = bea,
                        Edit = null
                    };
                    return View("Index", cvm);
                }
                SetAlert(Severity.info, chleby.Web.Resources.Strings.Controller_Trader_Employee_Remember_remember + " (" + bea.UserName + ")");
                return RedirectToAction("Index");
            }

            cvm = new ComboViewModel<TraderEmployeeAccount>
            {
                Index = GetTraderEmployeeAccounts(),
                Create = bea,
                Edit = null
            };
            return View("Index", cvm);
        }

        public static void SetAllRoles(string user, string appName)
        {
            var modules = (from um in MvcApplication.UsedModules
                           where um.App.Name == appName
                           select um.Module).Union(
                                (from mr in MvcApplication.ModuleRegister
                                 where mr.IsCore
                                 select mr)).Where(p => !p.Admin).Distinct().ToList();
            foreach (var mod in modules)
            {
                try { Roles.AddUserToRole(user, mod.Name + Defines.Roles.WriteSuffix); }
                catch { }
                try { Roles.AddUserToRole(user, mod.Name + Defines.Roles.ReadSuffix); }
                catch { }
            }
        }

        //
        // GET: /TraderEmployees/Edit/5
        [NeedWrite]
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Edit(int id)
        {
            var traderemployee = db.TraderEmployees.SingleOrDefault(x => x.Id == id && x.TraderId == _tid);
            if (traderemployee == null)
                return HttpNotFound();
            //ViewBag.PossibleTraders = db.Traders;
            var user = Membership.GetUser(traderemployee.ExternalAccountData);
            if (user == null)
                return HttpNotFound();
            var model = new TraderEmployeeAccount()
            {
                BusinessRole = traderemployee.BusinessRole,
                FirstName = traderemployee.FirstName,
                ExternalAccountData = traderemployee.ExternalAccountData,
                Id = id,
                LastName = traderemployee.LastName,
                Phone = traderemployee.Phone,
                Note = traderemployee.Note,
                UserName = user.UserName,
                Password = "DONTEDIT",
                Password2 = "DONTEDIT",
                MainContact = traderemployee.MainContact
            };
            return PartialView(model);
        }

        //
        // POST: /TraderEmployees/Edit/5
        [NeedWrite]
        [HttpPost]
        public ActionResult Edit(int id, TraderEmployeeAccount traderemployee)
        {
            var oldte = db.TraderEmployees.SingleOrDefault(x => x.Id == id && x.TraderId == _tid);
            if (oldte == null)
                return HttpNotFound();

            var user = Membership.GetUser(oldte.ExternalAccountData);
            if (user == null)
                return HttpNotFound();

            if (user.UserName != traderemployee.UserName)
            {
                var byemail = Membership.GetUserNameByEmail(traderemployee.UserName);
                var byname = Membership.GetUser(traderemployee.UserName);
                if (byemail != null || byname != null)
                {
                    ModelState.AddModelError("UserName", "Email already used.");
                }
            }

            if (ModelState.IsValid)
            {
                oldte.BusinessRole = traderemployee.BusinessRole;
                oldte.FirstName = traderemployee.FirstName;
                oldte.LastName = traderemployee.LastName;
                oldte.Note = traderemployee.Note;
                oldte.Phone = traderemployee.Phone;
                if (Roles.IsUserInRole(Defines.Roles.Employee) || Roles.IsUserInRole(Defines.Roles.SuperAdmin))
                {
                    if (traderemployee.MainContact && !oldte.MainContact)
                    {
                        var mainContacts = db.TraderEmployees.Where(te => te.TraderId == _tid && te.MainContact && te.Id != traderemployee.Id).ToList();

                        // find other main contact
                        foreach (TraderEmployee main in mainContacts)
                        {
                            SetAlert(Severity.warning, string.Format(Strings.TraderEmployees_Main_Contatc_Changed, main.FirstName + " " + main.LastName));
                            main.MainContact = false;
                        }
                    }
                    oldte.MainContact = traderemployee.MainContact;
                }
                db.Entry(oldte).State = EntityState.Modified;
                db.SaveChanges();

                if (traderemployee.Password != "DONTEDIT")
                {
                    user.ChangePassword(user.ResetPassword(), traderemployee.Password);
                    Membership.UpdateUser(user);
                }

                if (user.UserName != traderemployee.UserName)
                {
                    user.Email = traderemployee.UserName;
                    Membership.UpdateUser(user);
                    AccountController.ChangeUserName(user.UserName, traderemployee.UserName);
                }
                return RedirectToAction("Index");
            }
            var cvm = new ComboViewModel<TraderEmployeeAccount>
            {
                Index = GetTraderEmployeeAccounts(),
                Create = null,
                Edit = traderemployee,
            };
            return View("Index", cvm);
        }

        //
        // GET: /TraderEmployees/Delete/5
        [NeedWrite]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var traderemployee = db.TraderEmployees.SingleOrDefault(x => x.Id == id && x.TraderId == _tid);
            if (traderemployee.MainContact)
            {
                SetAlert(Severity.error, Strings.TraderEmployees_Cant_Delete_Main_Contact);
            }
            else
            {
                if (traderemployee == null || traderemployee.TraderId != _tid)
                    return HttpNotFound();

                var user = Membership.GetUser(traderemployee.ExternalAccountData);
                if (user == null)
                    return HttpNotFound();

                foreach (var addr in traderemployee.Addresses)
                {
                    addr.TraderEmployees.Clear();
                    var newte = addr.Trader.Employees.FirstOrDefault();
                    if (newte != null)
                        addr.TraderEmployees.Add(newte);
                }

                db.TraderEmployees.Remove(traderemployee);
                db.SaveChanges();
                Membership.DeleteUser(user.UserName);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Import()
        {
            ViewBag.What = "customer users";
            return PartialView();
        }

        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase file)
        {
            var bytes = new byte[file.ContentLength];
            var ms = new MemoryStream(bytes);
            file.InputStream.CopyTo(ms);
            var str = Encoding.UTF8.GetString(bytes);
            return Import(str);
        }

        [HttpPost]
        public ActionResult Import(string csv, bool save = false)
        {
            ViewBag.csv = csv;
            ViewBag.What = "customer users";
            var lines = new List<string>(csv.Split('\n'));
            if (lines.Count > 0)
                lines.RemoveAt(0);
            var errors = new List<Tuple<int, int, string>>();
            var curline = 0;
            var teas = new List<TraderEmployeeAccount>();
            foreach (var tline in lines)
            {
                var tea = new TraderEmployeeAccount();
                curline++;
                var line = tline.Trim();
                var fields = line.SplitCsv();
                if (fields.Length <= 1)
                    continue;
                if (fields.Length != 7)
                {
                    errors.Add(Tuple.Create(curline, -1, "wrong field count"));
                    continue;
                }

                if (string.IsNullOrWhiteSpace(fields[4]))
                {
                    errors.Add(Tuple.Create(curline, 5, "empty email"));
                    continue;
                }

                if (!EmailAddressAttribute.Valid(fields[4]))
                {
                    errors.Add(Tuple.Create(curline, 5, "invalid email"));
                    continue;
                }

                if (Membership.GetUser(fields[4]) != null)
                {
                    //errors.Add(Tuple.Create(curline, 5, "user already used"));
                    //omijamy uzytego po cichu
                    continue;
                }
                tea.UserName = fields[4].Trim();

                if (string.IsNullOrWhiteSpace(fields[1]))
                {
                    errors.Add(Tuple.Create(curline, 2, "empty firstname"));
                    continue;
                }
                tea.FirstName = fields[1].Trim();

                if (string.IsNullOrWhiteSpace(fields[2]))
                {
                    errors.Add(Tuple.Create(curline, 3, "empty lastname"));
                    continue;
                }
                tea.LastName = fields[2].Trim();

                tea.BusinessRole = fields[3].Trim();

                var tradername = fields[0].Trim().Normalize(NormalizationForm.FormKD);
                if (string.IsNullOrWhiteSpace(fields[0]))
                {
                    errors.Add(Tuple.Create(curline, 1, "empty customer"));
                    continue;
                }
                var trader = db.Traders.FirstOrDefault(x => x.PrimaryName == tradername);
                if (trader == null)
                {
                    errors.Add(Tuple.Create(curline, 1, "invalid customer"));
                    continue;
                }
                tea.Trader = trader;

                //optional
                tea.Phone = fields[5].Trim();
                tea.Note = fields[6].Trim();

                teas.Add(tea);
            }

            if (errors.Count == 0 && save)
            {
                var first = true;
                foreach (var tea in teas)
                {
                    var te = Mapper.Map<TraderEmployee>(tea);
                    if (first)
                        te.MainContact = true;
                    var user = Membership.CreateUser(tea.UserName, Membership.GeneratePassword(8, 1), tea.UserName);
                    user.IsApproved = false;
                    Roles.AddUserToRole(tea.UserName, Defines.Roles.Trader);
                    SetAllRoles(tea.UserName, AppName);
                    Membership.UpdateUser(user);
                    te.ExternalAccountData = (Guid)user.ProviderUserKey;
                    db.TraderEmployees.Add(te);
                    first = false;
                }
                SetAlert(Severity.info, chleby.Web.Resources.Strings.Controller_Trader_Employee_Remember_remember + "s");
                db.SaveChanges();
                if (_tid == -1)
                {
                    return RedirectToAction("Index", "TurboTraders");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.errors = errors;
            return PartialView("Import");
        }

        [NeedWrite]
        public ActionResult DwlTemplate()
        {
            return File(Encoding.UTF8.GetBytes(
@"Customer*,First name*,Last Name*,Role,Email*,Telephone,Notes
" + string.Join("\n",db.Traders.Select(x => x.PrimaryName))), "text/csv; charset=utf-8", "CustomerUsers.csv");
        }
    }
}