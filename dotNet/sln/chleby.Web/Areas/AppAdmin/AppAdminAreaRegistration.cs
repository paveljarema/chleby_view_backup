﻿using System.Web.Mvc;

namespace chleby.Web.Areas.AppAdmin
{
    public class AppAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AppAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AppAdmin_default",
                "AppAdmin/{controller}/{action}/{id}",
                new { controller="Main", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
