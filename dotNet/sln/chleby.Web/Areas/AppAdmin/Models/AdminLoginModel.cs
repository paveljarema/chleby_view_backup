﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Areas.AppAdmin.Models
{
    public class AdminLoginModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "Model_UsernameRequired")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "Remember")]
        public bool RememberMe { get; set; }
    }
}