﻿using System.Collections.Generic;
using chleby.Web.Models;

namespace chleby.Web.Areas.AppAdmin.Models
{
    public class AppStats
    {
        public decimal? SumInvoices { get; set; }

        public int SumTraders { get; set; }

        public int HandwalkStep { get; set; }

        public IEnumerable<AuditLog> LogEvents { get; set; }

        public string Currency { get; set; }

        public string Name { get; set; }
    }
}