﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using chleby.Web.Resources;

namespace chleby.Web.Areas.AppAdmin.Models
{
    public class AppInfo
    {
        [Required]
        [RegularExpression("[a-z]{3,16}", ErrorMessageResourceType = typeof(Strings), ErrorMessageResourceName = "UrlName")]
        public string Name { get; set; }

        [Required]
        public string ConnStr { get; set; }

        [Required]
        [RegularExpression(EmailAddressAttribute._pattern,ErrorMessageResourceType = typeof(Resources.Strings),ErrorMessageResourceName = "ValidEmail")]
        public string AdminUser { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 6)]
        public string AdminPassword { get; set; }

        [Required]
        public string DefaultPT { get; set; }

        [Required]
        public string DefaultTT { get; set; }

        [Required]
        public string DeliveryName { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
    }

    public class QueuedApp
    {
        [Key]
        public int Id { get; set; }

        public Guid Token { get; set; }

        public string AppData { get; set; }
    }
}