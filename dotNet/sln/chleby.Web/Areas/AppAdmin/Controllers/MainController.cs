﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Xml.Serialization;
using chleby.Web.Areas.AppAdmin.Models;
using chleby.Web.Controllers;
using chleby.Web.Mantis;
using chleby.Web.Models;
using System.Data.SqlClient;

namespace chleby.Web.Areas.AppAdmin.Controllers
{
    [Authorize]
    public class MainController : Controller
    {
        //
        // GET: /AppAdmin/Main/

        readonly MainAppContext db = new MainAppContext();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Membership.ApplicationName = "global";
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            if (returnUrl != null && !returnUrl.ToLower().StartsWith("/appadmin"))
            {
                var uri = new Uri(new Uri("http://xxx.com"), returnUrl);
                if (uri.Segments.Length >= 1)
                {
                    var seg = uri.Segments[1];
                    return RedirectToAction("Login", "Account", new { app = seg.Substring(0, seg.Length - 1), area = "" });
                }
            }
            return View("Login");
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(AdminLoginModel model, string returnUrl)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index");

            if (ModelState.IsValid && Membership.ValidateUser(model.UserName, model.Password))
            {
                Session.Abandon();
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe, Url.Content("~/AppAdmin"));
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ViewBag.error = chleby.Web.Resources.Strings.Controler_Main_Password;
            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Main");
            }
        }

        public ActionResult Index()
        {
            return View(MvcApplication.AppRegister);
        }

        public ActionResult BackupUserData(int id = 0)
        {
            var app = MvcApplication.AppRegister.Find(x => x.Id == id);
            if (app == null)
                return HttpNotFound();

            string data = "";
            using (var conn = OpenMainAppConnection())
            {
                Guid appId;

                data += ": APPLICATION\n";
                data += GetApplicationData(conn, app.Name, out appId);
                data += ": USERS\n";
                data += GetTableDataByApp(conn, appId, "Users", 5);
                data += ": ROLES\n";
                data += GetTableDataByApp(conn, appId, "Roles", 4);
                data += ": MEMBERSHIPS\n";
                data += GetTableDataByApp(conn, appId, "Memberships", 19);
                data += ": USERSINROLES\n";
                data += GetUsersInRolesByApp(conn, appId);
            }
            return Content(data, "text/plain");
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult BackupAppData(int id)
        {
            var app = MvcApplication.AppRegister.Find(x => x.Id == id);
            var match = Regex.Match(app.DBConnectionString, @"Database=(\w+);?");
            var db = match.Groups[1].Value;
            using (var conn = OpenMainAppConnection())
            {
                var path = Server.MapPath(string.Format("~/backups/{0}_{1:yyyyMMddHHmmss}", db, DateTime.UtcNow));
                var ret = BackupDatabase(conn, db, path);
                return Json(new { result = ret, info = ret ? "Complete" : "Failed" }, JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult BackupList()
        {
            var path = Server.MapPath("~/backups/");
            var model = new DirectoryInfo(path).EnumerateFiles();
            return PartialView(model);
        }

        private SqlConnection OpenMainAppConnection()
        {
            var connstr = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            var conn = new SqlConnection(connstr.ConnectionString);
            conn.Open();
            return conn;
        }

        private static string GetApplicationData(SqlConnection conn, string name, out Guid appId)
        {
            string data = "";
            using (var cmd = new SqlCommand("SELECT TOP 1 * FROM Applications WHERE ApplicationName = @app", conn))
            {
                cmd.Parameters.Add("app", SqlDbType.NVarChar).Value = "app_" + name;
                using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                {
                    appId = new Guid();
                    while (reader.Read())
                    {
                        var vals = new object[3];
                        reader.GetValues(vals);
                        appId = (Guid)vals[0];
                        data += string.Join("::", vals) + "\n";
                    }
                }
            }
            return data;
        }

        private static bool BackupDatabase(SqlConnection conn, string name, string file)
        {
            bool ret = true;
            using (var cmd = new SqlCommand("BACKUP DATABASE @db TO DISK = @file WITH NOINIT", conn))
            {
                cmd.Parameters.Add("db", SqlDbType.NVarChar).Value = name;
                cmd.Parameters.Add("file", SqlDbType.NVarChar).Value = file;
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                    ret = false;
                }
            }
            return ret;
        }

        private static string GetTableDataByApp(SqlConnection conn, Guid appId, string table, int rowCount)
        {
            string data = "";
            using (var cmd = new SqlCommand("SELECT * FROM " + table + " WHERE ApplicationId = @app", conn))
            {
                cmd.Parameters.Add("app", SqlDbType.UniqueIdentifier).Value = appId;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var vals = new object[rowCount];
                        reader.GetValues(vals);
                        data += string.Join("::", vals) + "\n";
                    }
                }
            }
            return data;
        }

        private static string GetUsersInRolesByApp(SqlConnection conn, Guid appId)
        {
            string data = "";
            var roles = new List<Guid>();
            using (var cmd = new SqlCommand("SELECT RoleId FROM Roles WHERE ApplicationId = @app", conn))
            {
                cmd.Parameters.Add("app", SqlDbType.UniqueIdentifier).Value = appId;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        roles.Add(reader.GetGuid(0));
                }
            }
            using (var cmd = new SqlCommand("SELECT * FROM UsersInRoles", conn))
            {
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var userId = reader.GetGuid(0);
                        var roleId = reader.GetGuid(1);
                        if (roles.Contains(roleId))
                        {
                            data += string.Format("{0}::{1}\n", userId, roleId);
                        }
                    }
                }
            }
            return data;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult BugList(string app)
        {
            var mantis = new Helper(app);
            mantis.Connect();
            ViewBag.app = app;
            return View(mantis);
        }
#if !DEBUG
        [OutputCache(Duration = 60)]
#else
        [OutputCache(Location = OutputCacheLocation.None)]
#endif
        public ActionResult BugDetails(string id, string app)
        {
            var mantis = new Helper(app);
            mantis.Connect();
            return PartialView(mantis.GetIssue(id));
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult BugClose(string id, string app)
        {
            var mantis = new Helper(app);
            mantis.Connect();
            var ret = mantis.CloseIssue(id);
            return Json(new { result = ret, info = ret ? "Complete" : "Failed" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateApp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateApp(AppInfo info)
        {
            if (ModelState.IsValid && MvcApplication.AppRegister.All(x => x.Name != info.Name))
            {
                CreateAppImpl(db, info);

                return RedirectToAction("Index");
            }
            return View(info);
        }

        public static void CreateAppImpl(MainAppContext db, AppInfo info)
        {
            if (db.AppRegister.Any(x => x.Name == info.Name)) throw new Exception("app already exists");
            var app = new AppRegister
                {
                    DBConnectionString = info.ConnStr,
                    DefaultPTId = 1,
                    DefaultTTId = 1,
                    DeliveryId = 1,
                    Name = info.Name
                };
            db.AppRegister.Add(app);
            db.SaveChanges();
            MvcApplication.UpdateCachedDB();
            ///INSTALL
            var appdb = new ChlebyContext(app);
            appdb.Businesses.Add(new Business
                {
                    CraftingDeadline = new DateTime(2000, 1, 1, 23, 55, 0),
                    Currency = "$",
                    DeliveryDayValue = 127,
                    LegalName = "!!!GUWNOFIX!!!",
                    LegalAddress1 = "!!!GUWNOFIX!!!",
                    LegalCity = "!!!GUWNOFIX!!!",
                    LegalPost = "!!!GUWNOFIX!!!",
                    LegalPhone = "!!!GUWNOFIX!!!",
                    Timezone = "Romance Standard Time",
                    UsingTradingProps = false,
                    HandwalkStep = -1,
                    SendOrdersBackup = false,
                    MailConfirmAddress = info.AdminUser,
                    CurrencyNegativePattern = 0,
                    CurrencyPositivePattern = 0,
                    XeroConfig = new XeroConfig { PurchaseAccount = 0, SalesAccount = 0 },
                    AccSoftware = 0,
                    NIP = "!!!GUWNOFIX!!!",
                    REGON = "!!!GUWNOFIX!!!",
                    LogoOnInvoice = false
                });

            appdb.Units.Add(new UnitOfMeasure
                {
                    Name = "---"
                });

            appdb.SaveChanges();

            appdb.Entities.Add(new WorldEntity
                {
                    BaseUnit = appdb.Units.First(x => x.Name == "---"),
                    Name = info.DeliveryName,
                });

            appdb.PaymentTerms.Add(new PaymentTerm
                {
                    Name = info.DefaultPT
                });

            appdb.TraderTypes.Add(new TraderType
                {
                    Name = info.DefaultTT
                });

            appdb.Settings.Add(new Settings { Name = "ShowAttr_Weight", Value = "all" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_MagicPrice", Value = "all" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_Vat", Value = "all" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_CraftingTime", Value = "all" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_AdditionalInfo", Value = "internal" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_Tags", Value = "internal" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_BarcodeType", Value = "none" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_BarcodeValue", Value = "none" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_LegacyCode", Value = "internal" });
            appdb.Settings.Add(new Settings { Name = "ShowAttr_BaseUnit", Value = "none" });
            appdb.Settings.Add(new Settings
                {
                    Name = "ShowAttrOrder_value",
                    Value = "Weight,MagicPrice,Vat,CraftingTime,AdditionalInfo,Tags,BarcodeType,BarcodeValue,LegacyCode"
                });
            appdb.Settings.Add(new Settings { Name = "PriceData_DeliveryVat", Value = "20" });
            appdb.Settings.Add(new Settings { Name = "Snapshot_LastDate", Value = "01/01/2000 13:00:00" });
            appdb.Settings.Add(new Settings { Name = "Invoice_LastNumber", Value = "1" });
            appdb.Settings.Add(new Settings { Name = "Default_PaymentTerm", Value = "0" });
            appdb.Settings.Add(new Settings { Name = "Default_TraderType", Value = "0" });
            appdb.Settings.Add(new Settings { Name = "Order_FuturePeriod", Value = "14" });
            appdb.Settings.Add(new Settings { Name = "Default_InvoicePeriod", Value = "d" });
            appdb.Settings.Add(new Settings { Name = "Invoice_FirstDay", Value = "0" });
            appdb.Settings.Add(new Settings
                {
                    Name = "Finance_Dashboard",
                    Value = "sales,sales-item,snapshot,sales-bycustomer,sales-byitem,sales-data,sales-unpublished,sales-published"
                });

            foreach (var mt in MvcApplication.DefaultMailTemplates)
            {
                appdb.Entry(mt).State = EntityState.Detached;
                mt.Id = 0;
                appdb.MailTemplates.Add(mt);
            }

            Membership.ApplicationName = "app_" + info.Name;
            var user = Membership.CreateUser(info.AdminUser, info.AdminPassword, info.AdminUser);
            appdb.BusinessEmployees.Add(new BusinessEmployee
                {
                    FirstName = info.FirstName,
                    LastName = info.LastName,
                    ExternalAccountData = (Guid)user.ProviderUserKey
                });
            appdb.SaveChanges();

            using (var conn = new SqlConnection(info.ConnStr))
            {
                conn.Open();
                using (var cmd = new SqlCommand("DROP TABLE [dbo].[__MigrationHistory]", conn))
                    cmd.ExecuteNonQuery();
                using (var cmd = new SqlCommand("ALTER TABLE [dbo].[Invoices] ADD UNIQUE([InvoiceNumber],[Revision])", conn))
                    cmd.ExecuteNonQuery();
                conn.Close();
            }

            MvcApplication.UpdateCachedDB();

            InternalController.UpdateRolesForApp(info.Name);

            Roles.AddUserToRole(info.AdminUser, Defines.Roles.SuperAdmin);
        }

        //[AllowAnonymous]
        //public ActionResult CreateGlobalAdmin()
        //{
        //    Membership.ApplicationName = "global";
        //    var password = Membership.GeneratePassword(20, 0);
        //    if (Membership.GetUser("globaladmin") == null)
        //    {
        //        Membership.CreateUser("globaladmin", password);
        //        return Content("WRITE DOWN THIS!\n USER: globaladmin\n PASSWORD: " + password, "text/plain");
        //    }
        //    return HttpNotFound();
        //}

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult BackupDownload(string file)
        {
            file = file.Replace("/", "").Replace(".", "");
            return File(Server.MapPath("~/backups/" + file), "application/octet-stream", file);
        }

        public ActionResult Stats()
        {
            var stats = new List<AppStats>();
            foreach (var app in MvcApplication.AppRegister)
            {
                using (var appdb = new ChlebyContext(app))
                {
                    var b = appdb.Businesses.AsNoTracking().FirstOrDefault();
                    if (b == null) continue;

                    var sumInvoices = (from inv in appdb.Invoices.AsNoTracking()
                                       where !inv.Deleted && !inv.MyClones.Any()
                                       select inv.TotalPrice).Cast<decimal?>().Sum();
                    var sumTraders = appdb.Traders.AsNoTracking().Count();
                    var handwalkStep = b.HandwalkStep;
                    var currency = b.Currency;
                    var logEvents = (from log in appdb.AuditLog.AsNoTracking()
                                     where log.Module == "Login"
                                     orderby log.Date descending
                                     select log).ToList();
                    stats.Add(new AppStats
                        {
                            Name = app.Name,
                            SumInvoices = sumInvoices,
                            SumTraders = sumTraders,
                            HandwalkStep = handwalkStep,
                            LogEvents = logEvents,
                            Currency = currency
                        });
                }
            }
            return View(stats);
        }
    }
}
