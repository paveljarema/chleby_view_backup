﻿using System.Linq;
using System.Web.Mvc;
using System.Web.UI;

namespace chleby.Web.Areas.AppAdmin.Controllers
{
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class ValidationController : Controller
    {
        public JsonResult CheckName(string name)
        {
            if (name == "default" || MvcApplication.AppRegister.Any(x => x.Name == name))
            {
                return Json("already used", JsonRequestBehavior.AllowGet);
            }
            else
                return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}