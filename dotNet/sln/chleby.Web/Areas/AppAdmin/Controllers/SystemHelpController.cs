﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using chleby.Web.Models;

namespace chleby.Web.Areas.AppAdmin.Controllers
{
    public class SystemHelpController : Controller
    {
        public static Dictionary<string, IEnumerable<Tuple<bool, MethodInfo, int>>> Controllers;
        public static Dictionary<int, MethodInfo> Handles = new Dictionary<int, MethodInfo>();
        static SystemHelpController()
        {
            int i = 0;
            Controllers = (from type in Assembly.GetExecutingAssembly().GetTypes()
                            where type.IsSubclassOf(typeof(ChlebyController))
                            where type.Name != "InternalController"
                            orderby type.Name
                            select new
                                {
                                    name = type.Name,
                                    actions = from method in type.GetMethods(
                                        BindingFlags.DeclaredOnly |
                                        BindingFlags.Instance |
                                        BindingFlags.Public)
                                              where method.ReturnType == typeof(ActionResult)
                                              let nextI = i++
                                              let _ = ((Func<int>)(() => { Handles.Add(nextI, method); return 0; }))()
                                              orderby method.Name
                                              select Tuple.Create(
                                                  method.MyGetCustomAttribute<HttpPostAttribute>() != null,
                                                  method,
                                                  nextI)
                                }).Where(x => x.actions.Any()).
                                ToDictionary(ks => ks.name, es => es.actions);
        }

        public ActionResult Index()
        {
            return View(Controllers);
        }

        public ActionResult Edit(int id)
        {
            if (!Handles.ContainsKey(id))
                return HttpNotFound();

            var method = Handles[id];

            var help = MvcApplication.Help.Find(x => x.Controller == method.DeclaringType.Name && x.Action == method.ToString());
            if (help == null)
            {
                using (var db = new MainAppContext())
                {
                    help = db.SystemHelp.Add(new SystemHelp
                        {
                            Action = method.ToString(),
                            Controller = method.DeclaringType.Name,
                            Content = ""
                        });
                    db.SaveChanges();
                }
                MvcApplication.UpdateCachedDB();
            }

            return View(Tuple.Create(id, method, help));
        }

        [HttpPost]
        public ActionResult Edit(int id, SystemHelp sh)
        {
            if (!Handles.ContainsKey(id))
                return HttpNotFound();

            var method = Handles[id];

            using (var db = new MainAppContext())
            {
                var ctl = method.DeclaringType.Name;
                var act = method.ToString();
                var help =db.SystemHelp.First(x => 
                        x.Controller == ctl && x.Action == act);
                help.Content = sh.Content;
                db.SaveChanges();
                MvcApplication.UpdateCachedDB();
            }

            return RedirectToAction("Index");
        }
    }
}
