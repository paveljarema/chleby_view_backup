using chleby.Web.Models;

namespace chleby.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ChlebyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ChlebyContext context)
        {
            context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Addresses] DROP CONSTRAINT [FK_dbo.Addresses_dbo.AddressGroups_AddressGroupId]");
            context.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Addresses] ADD CONSTRAINT [FK_dbo.Addresses_dbo.AddressGroups_AddressGroupId] FOREIGN KEY ([AddressGroupId]) REFERENCES [dbo].[AddressGroups] ([Id]) ON UPDATE NO ACTION ON DELETE SET NULL");
        }

    }

    internal sealed class ConfigurationMain : DbMigrationsConfiguration<MainAppContext>
    {
        public ConfigurationMain()
        {
            AutomaticMigrationsEnabled = true;
        }
    }
}
