﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Web;

namespace chleby.Web
{
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string ToStringUrl(this DateTime dt)
        {
            return dt.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
        }

        public static DateTime FromStringUrl(string dt)
        {
            return DateTime.ParseExact(dt, "ddMMyyyy", CultureInfo.InvariantCulture);
        }
    }

    public class EnumDescriptionAttribute : Attribute
    {
        public string Name { get; private set; }
        public Type ResType { get; private set; }

        public EnumDescriptionAttribute(string name)
        {
            Name = name;
        }

        public EnumDescriptionAttribute(Type resType)
        {
            ResType = resType;
        }
    }

    public static class EnumExtensions
    {
        public static string Description(this Enum value)
        {
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(EnumDescriptionAttribute),
                                                       false);
            if (attributes.Length > 0)
            {
                var attr = (EnumDescriptionAttribute)attributes[0];
                if (attr.ResType == null)
                    return attr.Name;
                else
                {
                    var res = new ResourceManager(attr.ResType);
                    return res.GetString("Enum_" + value.GetType().Name + "_" + value.ToString());
                }
            }
            else return value.ToString();
        }
    }

    public static class AttrExt
    {
        private static readonly Dictionary<Type, ResourceManager> _resources = new Dictionary<Type, ResourceManager>();

        public static string GetName(this DisplayAttribute da)
        {
            if (da.ResourceType != null)
            {
                if (!_resources.ContainsKey(da.ResourceType))
                    _resources.Add(da.ResourceType, new ResourceManager(da.ResourceType));
                    return _resources[da.ResourceType].GetString(da.Name);
            }
            else 
                return da.Name;
        }
    }

    public static class StringExt
    {
        public static string[] SplitCsv(this string r)
        {
            string[] c;
            List<string> resp = new List<string>();
            bool cont = false;
            string cs = "";

            c = r.Split(new char[] { ',' }, StringSplitOptions.None);

            foreach (string y in c)
            {
                string x = y;


                if (cont)
                {
                    // End of field
                    if (x.EndsWith("\""))
                    {
                        cs += "," + x.Substring(0, x.Length - 1);
                        resp.Add(cs);
                        cs = "";
                        cont = false;
                        continue;

                    }
                    else
                    {
                        // Field still not ended
                        cs += "," + x;
                        continue;
                    }
                }

                // Fully encapsulated with no comma within
                if (x.StartsWith("\"") && x.EndsWith("\""))
                {
                    if ((x.EndsWith("\"\"") && !x.EndsWith("\"\"\"")) && x != "\"\"")
                    {
                        cont = true;
                        cs = x;
                        continue;
                    }

                    resp.Add(x.Substring(1, x.Length - 2));
                    continue;
                }

                // Start of encapsulation but comma has split it into at least next field
                if (x.StartsWith("\"") && !x.EndsWith("\""))
                {
                    cont = true;
                    cs += x.Substring(1);
                    continue;
                }

                // Non encapsulated complete field
                resp.Add(x);

            }

            return resp.ToArray();

        }

        public static string SafeLeft(this string s, int count)
        {
            if (s.Length < count) return s;
            else return (s.Substring(0, count));
        }
    }

    public static class ListExt
    {
        public static void Swap<T>(this List<T> that, int a, int b)
        {
            var temp = that[a];
            that[a] = that[b];
            that[b] = temp;
        }
    }
}