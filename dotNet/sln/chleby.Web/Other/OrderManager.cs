﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using chleby.Web.Models;
using log4net;

namespace chleby.Web
{
    public class OrderManager
    {
        private readonly ChlebyContext _db;
        private readonly Business _business;
        private readonly int _tid;
        public static readonly ILog _log = LogManager.GetLogger(typeof(OrderManager));

        public OrderManager(ChlebyContext db, Business business, int tid)
        {
            _db = db;
            _business = business;
            _tid = tid;
            DontSave = false;
            DontLog = false;
        }

        public bool DontSave { get; set; }

        public bool DontLog { get; set; }

        public void AddStanding(int addressId, int itemId, Dictionary<int, int> countByDays, Dictionary<int, bool> delivByDays = null)
        {
            var changes = new List<OrderDiff>();

            for (int day = 0; day < 7; ++day)
                if (!countByDays.ContainsKey(day))
                    countByDays.Add(day, -1);

            var orders = _db.Orders.OfType<StandingOrder>().
                Where(o => o.Address_Id == addressId).ToList();

            if (orders.Any())
            {
                //istnieja ordery na dany adres wiec dodajemy itemy do niego
                foreach (var order in orders)
                {
                    //if (order.Items.Any(x => x.Item_Id == itemId))
                    //    throw new ArgumentException("Item already ordered");

                    for (int day = 0; day < 7; ++day)
                    {
                        if (order.Day != day) continue; //nie ten order

                        var oldpos = order.Items.FirstOrDefault(x => x.Item_Id == itemId);
                        if (oldpos != null && countByDays[day] >= 0)
                        {
                            var oldcnt = oldpos.OrderedCount;
                            oldpos.OrderedCount = _business.DeliveryDays.Contains(day) ? countByDays[day] : 0;
                            if (!DontSave)
                                changes.Add(new OrderDiff(false, oldpos, countByDays[day], oldcnt));
                            if (!DontLog)
                                _log.DebugFormat("modyfing {0}{1} to {2}@{3} (from {4})", oldpos.OrderedCount, oldpos.Item.Name, order.Day,
                                             order.Address.Id, oldcnt);
                        }
                        else if(oldpos == null)
                        {
                            var pos = new OrderPosition
                                {
                                    Item = _db.Items.Find(itemId),
                                    Adhoc = false,
                                    OrderedCount = Math.Max(0, _business.DeliveryDays.Contains(day) ? countByDays[day] : 0),
                                    Order = order
                                };
                            order.Items.Add(pos);
                            if (!DontSave)
                                changes.Add(new OrderDiff(false, pos, countByDays[day], 0));
                            if (!DontLog)
                                _log.DebugFormat("adding {0}{1} to {2}@{3}", pos.OrderedCount, pos.Item.Name, order.Day,
                                             order.Address.Id);
                        }

                        if (delivByDays != null && delivByDays.ContainsKey(day))
                            order.Delivery = delivByDays[day];
                    }
                }
            }
            else
            {
                //nie ma ordera wiec tworzymy 7 orderow na kazdy dzien
                foreach (var kvp in countByDays)
                {
                    var order = new StandingOrder
                        {
                            Address = _db.Addresses.Find(addressId),
                            Day = kvp.Key
                        };
                    if (delivByDays != null && delivByDays.ContainsKey(kvp.Key))
                        order.Delivery = delivByDays[kvp.Key];
                    _db.Orders.Add(order);
                    if (!DontLog)
                        _log.DebugFormat("creating so {0}@{1}", order.Day, order.Address.Id);

                    order.Items = new Collection<OrderPosition>();
                    var newcount = _business.DeliveryDays.Contains(kvp.Key) ? kvp.Value : 0;
                    var pos = new OrderPosition
                        {
                            Item = _db.Items.Find(itemId),
                            Adhoc = false,
                            OrderedCount = Math.Max(0, newcount),
                            Order = order
                        };
                    order.Items.Add(pos);
                    if (!DontLog)
                        _log.DebugFormat("adding {0}{1} to new {2}@{3}",
                        pos.OrderedCount, pos.Item.Name, order.Day, order.Address.Id);
                    if (!DontSave)
                        changes.Add(new OrderDiff(false, pos, newcount, 0));
                }
            }

            if (!DontSave)
            {
                _db.SaveChanges();
                NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            }
        }

        public void DeleteStanding(int addressId, int itemId)
        {
            var changes = new List<OrderDiff>();

            //usun item z orderu
            _db.OrderPositions.OfType<OrderPosition>().
                Where(x => x.Item_Id == itemId && x.Order.Address_Id == addressId &&
                    x.Order is StandingOrder).
                ToList().ForEach(pos =>
                {
                    changes.Add(new OrderDiff(false, pos, 0, pos.OrderedCount));
                    _db.OrderPositions.Remove(pos);
                });
            _db.SaveChanges();

            DeleteEmptyOrders();

            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            _db.SaveChanges();
        }

        private void DeleteEmptyOrders()
        {
            //wyrzuc puste ordery
            _db.Orders.Where(o => !o.Items.Any()).
                ToList().ForEach(o =>
                    {
                        _db.SnapshotOrderPositions.Where(p => p.Order_Id == o.Id).ToList().ForEach(
                            p => p.Order = null);
                        _db.Orders.Remove(o);
                    });
        }

        public void ModifyStanding(Dictionary<int, int> countByPos, Dictionary<int, bool> delivery)
        {
            var changes = new List<OrderDiff>();

            var posids = countByPos.Keys;
            var byItem = _db.OrderPositions.Where(pos => posids.Contains(pos.Id)).
                GroupBy(ks => ks.Item.Id).ToList();

            foreach (var gr in byItem)
            {
                //jesli jakis item ma same zera to go usun
                bool isZero = countByPos.Where(kvp => gr.Any(pos => pos.Id == kvp.Key)).
                    All(kvp => kvp.Value == 0);
                if (isZero)
                    DeleteStanding(gr.Key, gr.First().Order.Address.Id);
            }

            foreach (var kvp in countByPos)
            {
                int id = kvp.Key;
                var pos = _db.OrderPositions.Find(id);
                //sprawdz czy order nalezy do wlasciwego tradera
                if (pos != null && pos.Order.Address.TraderId == _tid)
                {
                    //jesli w dany dzien nie pracuje firma to ignoruj
                    if (!_business.DeliveryDays.Contains(((StandingOrder)pos.Order).Day))
                        continue;

                    changes.Add(new OrderDiff(false, pos, Math.Max(0, kvp.Value), pos.OrderedCount));
                    pos.OrderedCount = Math.Max(0, kvp.Value);

                    //ustaw delivery w orderze
                    var day = ((StandingOrder)pos.Order).Day;
                    pos.Order.Delivery = delivery[day];
                }
            }

            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            _db.SaveChanges();
        }

        public void AddAdhoc(int addressId, int itemId, DateTime date, int count, bool delivery, string note)
        {
            var changes = new List<OrderDiff>();

            var order = _db.Orders.OfType<AdhocOrder>().
                FirstOrDefault(o => o.Date == date && o.Address_Id == addressId);

            var dow = (int)date.DayOfWeek - 1;
            if (dow < 0) dow += 7;
            if (!_business.DeliveryDays.Contains(dow)) return;

            if (_db.AddressHolidays.Any(x => x.StartDate >= date && x.EndDate <= date) ||
                _db.BusinessHolidays.Any(x => x.StartDate >= date && x.EndDate <= date)) return;

            //nie bylo adhoca na dany dzien
            if (order == null)
            {
                order = new AdhocOrder
                {
                    Address = _db.Addresses.Find(addressId),
                    Date = date,
                    Delivery = delivery,
                    Note = note,
                };
                order.Items = new Collection<OrderPosition>
                    {
                        new OrderPosition
                            {
                                Adhoc = true,
                                Item = _db.Items.Find(itemId),
                                OrderedCount = count,
                                Order = order
                            }
                    };
                _db.Orders.Add(order);
                changes.Add(new OrderDiff(true, order.Items.First(), count, 0));
            }
            else //jest adhoc, modyfikujmy go
            {
                if (order.Items.Any(x => x.Item.Id == itemId)) //byl ten item, zmien wartosc
                {
                    var pos = order.Items.First(x => x.Item.Id == itemId);
                    changes.Add(new OrderDiff(true, pos, count, pos.OrderedCount));
                    pos.OrderedCount = count;
                    pos.Order.Delivery = delivery;
                }
                else //nie ma itemu w orderze, dodaj
                {
                    var pos = new OrderPosition
                    {
                        Adhoc = true,
                        Item = _db.Items.Find(itemId),
                        OrderedCount = count,
                        Order = order
                    };
                    order.Items.Add(pos);
                    pos.Order.Delivery = delivery;
                    changes.Add(new OrderDiff(true, pos, count, 0));
                }
            }

            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            _db.SaveChanges();
        }

        public void DeleteAdhocPos(int posId)
        {
            var changes = new List<OrderDiff>();
            var pos = _db.OrderPositions.FirstOrDefault(x => x.Order.Address.TraderId == _tid && x.Id == posId);
            if (pos == null)
                throw new ArgumentException("Invalid posId");
            changes.Add(new OrderDiff(true, pos, 0, pos.OrderedCount));
            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            pos.Order.Items.Remove(pos);

            if (!pos.Order.Items.Any())
            {
                foreach (var it in pos.Order.Items)
                {
                    _db.SnapshotOrderPositions.Where(p => p.Order_Id == pos.Order.Id).ToList().
                        ForEach(p => p.Order = null);
                }
                _db.Orders.Remove(pos.Order);
            }
            _db.OrderPositions.Remove(pos);
            _db.SaveChanges();
        }

        public void DeleteAdhoc(int orderId)
        {
            var changes = new List<OrderDiff>();
            var order = _db.Orders.OfType<AdhocOrder>().FirstOrDefault(x => x.Address.TraderId == _tid && x.Id == orderId);
            if (order == null)
                throw new ArgumentException("Invalid orderId");
            foreach (var pos in order.Items)
                changes.Add(new OrderDiff(false, pos, 0, pos.OrderedCount));
            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
            foreach (var it in order.Items)
            {
                _db.SnapshotOrderPositions.Where(p => p.Order_Id == order.Id).ToList().
                    ForEach(p => p.Order = null);
            }
            _db.Orders.Remove(order);
            _db.SaveChanges();
        }

        public void ModifyAdhoc(Dictionary<int, int> countByPos, bool delivery, string note)
        {
            var changes = new List<OrderDiff>();
            var snapshoter = new Snapshoter(_db, new DefaultSnapshotParameterProvider(_db, _db.AppConfig.Name));
            foreach (var kvp in countByPos)
            {
                int id = kvp.Key;
                var pos = _db.OrderPositions.Find(id);
                if (pos.Order.Address.TraderId == _tid)
                {
                    changes.Add(new OrderDiff(true, pos, kvp.Value, pos.OrderedCount));

                    //nadpisz potencjalnie przeszle snapshoty
                    snapshoter.CreateOverrideSnapshot(pos.Order.Address.Id,
                        ((AdhocOrder)pos.Order).Date, note, kvp.Value,
                        pos.Item, pos.Order.Address.Trader, delivery, _business);

                    if (kvp.Value == 0)
                    {
                        var owner = pos.Order.Address.Id;
                        var ao = (AdhocOrder)pos.Order;
                        var dow = (int)ao.Date.DayOfWeek;
                        if (dow < 0) dow += 7;
                        var itemId = pos.Item.Id;
                        var soExists = _db.Orders.OfType<StandingOrder>().
                            Any(x => x.Address.Id == owner && x.Day == dow && x.Items.Any(i => i.Item.Id == itemId));
                        if (!soExists)
                            DeleteAdhoc(pos.Id);
                        else
                            pos.OrderedCount = 0;
                    }
                    else
                    {
                        pos.OrderedCount = kvp.Value;
                        pos.Order.Delivery = delivery;
                        pos.Order.Note = note;
                    }
                }
            }
            _db.SaveChanges();
            NotifyChanges(changes, _db, _db.AppConfig.Name, _business);
        }

        public static void NotifyChanges(List<OrderDiff> changes, ChlebyContext mydb, string app, Business myBusiness)
        {
            //var dtz = double.Parse(mydb.Businesses.First().Timezone);
            var user = Membership.GetUser();
            var username = user == null ? "nobody" : user.UserName;

            var changedlist = changes.Where(x => x.OldCount != x.NewCount);
            if (!changedlist.Any()) return;

            var cacheKey = "ordersNotify_" + app + "_" + changes.First().Trader;
            var cache = (List<OrderDiff>)System.Web.HttpContext.Current.Cache[cacheKey];
            if (cache == null)
            {
                cache = new List<OrderDiff>();
                System.Web.HttpContext.Current.Cache.Add(
                    cacheKey, cache,
                    null, DateTime.MaxValue, Defines.OrderNotifyPeriod,
                    CacheItemPriority.NotRemovable, (k, v, r) =>
                        {
                            var sochanges = ((IEnumerable<OrderDiff>)v).Where(x => !x.AdHoc).ToList();
                            if (sochanges.Any())
                            {
                                if (!string.IsNullOrWhiteSpace(myBusiness.MailConfirmAddress))
                                {
                                    new Mail(mydb, "orderChange", myBusiness.MailConfirmAddress, new
                                        {
                                            date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                            user = username,
                                            changes = sochanges
                                        }).Send();
                                }

                                new Mail(mydb, "orderChange", changes.First().Email, new
                                    {
                                        date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                        user = username,
                                        changes = sochanges
                                    }).Send();
                            }

                            var aochanges = ((IEnumerable<OrderDiff>)v).Where(x => x.AdHoc).ToList();
                            if (aochanges.Any())
                            {
                                if (!string.IsNullOrWhiteSpace(myBusiness.MailConfirmAddress))
                                {
                                    new Mail(mydb, "cartChange", myBusiness.MailConfirmAddress, new
                                        {
                                            date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                            user = username,
                                            changes = aochanges
                                        }).Send();
                                }

                                new Mail(mydb, "cartChange", changes.First().Email, new
                                    {
                                        date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Defines.Timezones[myBusiness.Timezone]),
                                        user = username,
                                        changes = aochanges
                                    }).Send();
                            }
                        });
            }
            cache.AddRange(changedlist);
        }

        public string GetAllOrdersCSV()
        {
            var objSO = from order in _db.Orders.OfType<StandingOrder>()
                        from pos in order.Items
                        where pos.OrderedCount > 0
                        select new
                        {
                            day = order.Day + 1,
                            qty = pos.OrderedCount,
                            item = pos.Item.LegacyCode ?? pos.Item.Name,
                            addr = order.Address.LegacyCode ?? pos.Order.Address.Address1
                        };

            var objAO = from order in _db.Orders.OfType<AdhocOrder>()
                        from pos in order.Items
                        select new
                        {
                            date = order.Date,
                            qty = pos.OrderedCount,
                            item = pos.Item.LegacyCode ?? pos.Item.Name,
                            addr = order.Address.LegacyCode ?? pos.Order.Address.Address1
                        };

            return CsvExporter.Export(objSO) + CsvExporter.Export(objAO);
        }
    }
}