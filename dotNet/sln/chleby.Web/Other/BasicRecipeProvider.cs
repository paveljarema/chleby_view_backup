﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using chleby.Web.Models;

namespace chleby.Web
{
    public class BasicRecipeProvider : RecipeProvider
    {
        public BasicRecipeProvider(WorldEntity entity, ChlebyContext db) : base(entity, db) { }

        public override IEnumerable<string> GetSteps()
        {
            var recipe = _db.BasicRecipes.FirstOrDefault(x => x.Entity_Id == _entity.Id);
            if (recipe != null)
                return new List<string>
                    {
                        string.Format("{0} -> {1}{2} from {3} for {4:N2}",
                        _entity.Name,
                        recipe.SuppliedEntity.Amount,
                        recipe.SuppliedEntity.Unit.Name,
                        recipe.SuppliedEntity.Supplier.Name,
                        recipe.SuppliedEntity.Price)
                    };
            else
                return base.GetSteps();
        }

        public override IEnumerable<Supplier> GetSuppliers()
        {
            return _db.SuppliedEntities.
                Where(x => x.Entity_Id == _entity.Id).
                Select(x => x.Supplier).ToList();
        }

        public override IEnumerable<SuppliedEntity> GetSupplyInfo()
        {
            return _db.SuppliedEntities.
                Where(x => x.Entity_Id == _entity.Id).ToList();
        }
    }
}