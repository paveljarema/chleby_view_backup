﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace chleby.Web
{
    public abstract class ChlebyAuthorizeAttribute : AuthorizeAttribute
    {
        public string Name { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var mod = MvcApplication.ModuleRegister.Find(x => x.Name == Name);

            if (mod != null)
            {
                if (mod.Admin)
                    return httpContext.User.IsInRole(Defines.Roles.Employee) ||
                           httpContext.User.IsInRole(Defines.Roles.SuperAdmin);
                else
                    return httpContext.User.IsInRole(Defines.Roles.Employee) ||
                           httpContext.User.IsInRole(Defines.Roles.Trader) ||
                           httpContext.User.IsInRole(Defines.Roles.SuperAdmin);
            }

            return false;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                {
                    { "app", filterContext.RouteData.Values["app"] },
                    { "controller", "Account" },
                    { "action", "Login" },
                    { "ReturnUrl", filterContext.HttpContext.Request.RawUrl }
                });
            }
        }

    }

    /*public enum RequiredRole
    {
        Employee,
        Trader,
        Both
    }*/

    public class NeedReadAttribute : ChlebyAuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var rd = httpContext.Request.RequestContext.RouteData;
            var typename = rd.Values["controller"] + "Controller";
            var module = ChlebyModuleAttribute.GetByControllerName(typename);
            if (module != null)
            {
                return httpContext.User.IsInRole(module.Name + Defines.Roles.ReadSuffix) ||
                       httpContext.User.IsInRole(Defines.Roles.SuperAdmin);
            }
            else
            {
                return false; // base.AuthorizeCore(httpContext);
            }
        }

    }

    public class CustomPermAttribute : ChlebyAuthorizeAttribute
    {
        private readonly string _perm;

        public CustomPermAttribute(string perm)
        {
            _perm = perm;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.User.IsInRole(Defines.Roles.CustomPrefix + _perm) ||
                    httpContext.User.IsInRole(Defines.Roles.SuperAdmin);
        }

    }

    public class NeedWriteAttribute : ChlebyAuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var rd = httpContext.Request.RequestContext.RouteData;
            var typename = rd.Values["controller"] + "Controller";
            var module = ChlebyModuleAttribute.GetByControllerName(typename);
            if (module != null)
            {
                return httpContext.User.IsInRole(module.Name + Defines.Roles.WriteSuffix) ||
                    httpContext.User.IsInRole(Defines.Roles.SuperAdmin);
            }
            else
            {
                return false; // base.AuthorizeCore(httpContext);
            }
        }
    }
}