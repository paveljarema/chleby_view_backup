﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using chleby.Web.Models;
using chleby.Web.Controllers;
using System.Reflection;

namespace chleby.Web
{
    public abstract class MultiController : ChlebyController
    {
        public abstract dynamic InitData(RequestContext requestContext);

        protected ActionResult MultiView(Type parentController = null, string parentAction = "Index", Action<dynamic> callbackModel = null)
        {
            if (parentController == null)
            {
                var attr = this.GetType().MyGetCustomAttribute<ChlebyModuleAttribute>();
                parentController = attr.Parent;
            }
            var model = MultiModel.Dispatch(parentController, this.ControllerContext.RequestContext);
            this.RewriteTo(model);
            if (callbackModel != null)
                callbackModel(model[GetType()].Model);
            var parentName = parentController.Name.Replace("Controller", "");
            return View("~/Views/" + parentName + "/" + parentAction + ".cshtml", model);
        }
    }

    public class ModelData
    {
        public dynamic Model { get; set; }
        public ViewDataDictionary ViewData { get; set; }
    }

    public static class MultiModel
    {
        public static void RewriteTo(this MultiController mc, IDictionary<Type, ModelData> model)
        {
            var me = model[mc.GetType()];
            foreach (var ms in mc.ModelState)
                me.ViewData.ModelState.Add(ms.Key, ms.Value);
            foreach (var vd in mc.ViewData)
            {
                if (me.ViewData.ContainsKey(vd.Key))
                    me.ViewData[vd.Key] = vd.Value;
                else
                    me.ViewData.Add(vd.Key, vd.Value);
            }
        }

        public static IDictionary<Type, ModelData> Dispatch(Type parentType, RequestContext requestContext)
        {
            var dict = new Dictionary<Type, ModelData>();
            var attrs = parentType.MyGetCustomAttributes<HasChildAttribute>();

            foreach (var attr in attrs)
            {
                dynamic controller = Activator.CreateInstance(attr.ChildType);
                var model = controller.InitData(requestContext);
                var data = controller.ViewData;
                dict.Add(attr.ChildType, new ModelData
                {
                    Model = model,
                    ViewData = data
                });
            }
            return dict;
        }
    }
}