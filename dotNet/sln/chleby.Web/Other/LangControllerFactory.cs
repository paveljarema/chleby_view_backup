﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace chleby.Web
{
    public class LangControllerFactory : DefaultControllerFactory
    {

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            var vLang = requestContext.RouteData.Values["lang"];
            var ci = (CultureInfo)new CultureInfo("en-GB").Clone();
            if (vLang != null)
            {
                var lang = vLang.ToString();
                try
                {
                    if (lang == "u" && requestContext.HttpContext.Request.UserLanguages != null &&
                        requestContext.HttpContext.Request.UserLanguages.Length > 0)
                    {
                        var langCookie = requestContext.HttpContext.Request.Cookies["userLang"];
                        if (langCookie != null)
                            ci = (CultureInfo)new CultureInfo(langCookie.Value).Clone();
                        else
                            ci = (CultureInfo)new CultureInfo(requestContext.HttpContext.Request.UserLanguages[0]).Clone();
                    }
                    else
                        ci = (CultureInfo)new CultureInfo(lang).Clone();
                }
                catch
                { }
            }
            ci.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
            ci.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            ci.DateTimeFormat.DateSeparator = "/";
            ci.DateTimeFormat.ShortTimePattern = "HH:mm";
            ci.DateTimeFormat.TimeSeparator = ":";
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = ci;

            return base.GetControllerInstance(requestContext, controllerType);
        }

    }
}
