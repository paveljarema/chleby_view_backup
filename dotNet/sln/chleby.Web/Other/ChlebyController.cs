﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using chleby.Web.Mantis;
using chleby.Web.Models;
using log4net;
using System.Reflection;

namespace chleby.Web
{
    public abstract class ChlebyController : Controller
    {
        protected string AppName { get; private set; }
        protected ChlebyContext db { get; private set; }
        private bool _invalidApp = false;
        protected static readonly ILog _log = LogManager.GetLogger(typeof(ChlebyController));
        protected SettingsManager Settings;
        protected int _tid;
        protected List<UIAlert> Alerts { get; private set; }
        protected Business MyBusiness 
        {
            get
            {
                var myBusiness = (Business)HttpContext.Cache[AppName + "__Business"];
                DateTime? lastUpdate = (DateTime?)HttpContext.Cache[AppName + "__LastUpdate"];
                if (lastUpdate == null)
                {
                    lastUpdate = DateTime.MinValue;
                }
                DateTime utcNow = DateTime.UtcNow;
                if (myBusiness == null || (utcNow - lastUpdate.Value).Minutes > 5)
                {
                    HttpContext.Cache[AppName + "__Business"] = myBusiness = db.Businesses.AsNoTracking().First();
                    HttpContext.Cache[AppName + "__LastUpdate"] = DateTime.UtcNow;
                }

                return myBusiness;
            }
        }

        /// <summary>
        /// Converts utc date to client timezone
        /// </summary>
        /// <param name="utc">Utc date</param>
        /// <returns>Trader timezone date</returns>
        protected DateTime ToClientTime(DateTime utc)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
            return TimeZoneInfo.ConvertTimeFromUtc(utc, timeZoneInfo);
        }

        /// <summary>
        /// Converts utc date to client timezone
        /// </summary>
        /// <param name="utc">Utc date</param>
        /// <returns>Trader timezone date</returns>
        protected DateTime? ToClientTime(DateTime? utc)
        {
            if (utc != null)
            {
                var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(MyBusiness.Timezone);
                return TimeZoneInfo.ConvertTimeFromUtc(utc.Value, timeZoneInfo);
            }

            return null;
        }

        public static int GetTraderId()
        {
            var tid = 0;
            using (var db = ChlebyContext.ForCurrentApp())
            {
                tid = int.Parse(System.Web.HttpContext.Current.Request.
                    RequestContext.RouteData.Values["tid"].ToString(), CultureInfo.InvariantCulture);
                if (Roles.IsUserInRole(Defines.Roles.Trader))
                {
                    var user = Membership.GetUser();
                    if (user != null)
                    {
                        var uid = (Guid)user.ProviderUserKey;
                        tid = db.TraderEmployees.
                            Where(te => te.ExternalAccountData == uid).
                            Select(x => x.TraderId).First();
                    }
                }
            }
            return tid;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.AppName = null;

            AppName = requestContext.RouteData.Values["app"].ToString();
            var app = MvcApplication.AppRegister.FirstOrDefault(a => a.Name == AppName);
            if (app == null)
            {
                var view = View("InvalidApp");
                view.ViewBag.AppName = AppName;
                view.ViewBag.Apps = MvcApplication.AppRegister;
                view.ExecuteResult(ControllerContext);
                _invalidApp = true;
                requestContext.HttpContext.Response.End();
                return;
            }
            Membership.ApplicationName = "app_" + AppName;
            Roles.ApplicationName = "app_" + AppName;
            db = new ChlebyContext(app); //changed DB to db cause defaultcontrollers like them :3

            ViewBag.Business = MyBusiness;

            _tid = GetTraderId();

            if (_tid != -1)
            {
                ViewBag.TraderName = db.Traders.Where(x => x.Id == _tid).ToList().Select(x => x.Name).FirstOrDefault();
                if (ViewBag.TraderName == null)
                    _tid = -1;
            }

            string controllerName = requestContext.RouteData.GetRequiredString("controller");
            string actionName = requestContext.RouteData.GetRequiredString("action");
            ViewBag.ContrName = controllerName;
            ViewBag.ActionName = actionName;
            ViewBag.Tid = _tid;
            //minski ty chuju!
            var testingHB = db.HelpBubles.FirstOrDefault(x => x.name == controllerName + "." + actionName);
            if (testingHB == null)
            {
                HelpBuble newHB = new HelpBuble();
                newHB.name = controllerName + "." + actionName;
                newHB.value = "";
                db.HelpBubles.Add(newHB);
                db.SaveChanges();
            }
            else
            {
                var hb = db.HelpBubles.FirstOrDefault(x => x.name == controllerName + "." + actionName);
                ViewBag.HelperLine = hb == null ? "" : hb.value;
            }

            ViewBag.Settings = Settings = MvcApplication.Settings[AppName];
            Alerts = GetObjectFromSession<List<UIAlert>>("alerts");
        }

        protected T GetObjectFromSession<T>(string name) where T : new()
        {
            var obj = Session[name];
            if (obj == null)
            {
                obj = new T();
                Session[name] = obj;
            }
            return (T)obj;
        }

        protected void SetAlert(Severity status, string msg)
        {
            Alerts.Clear();
            Alerts.Add(new UIAlert { Status = status, Msg = msg });
        }

        protected void AddAlert(Severity status, string msg)
        {
            Alerts.Add(new UIAlert { Status = status, Msg = msg });
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.HandwalkFinish = false;
            if (_invalidApp)
            {
                filterContext.Result = new EmptyResult();
                return;
            }
            if (filterContext.RouteData.Values["controller"].ToString() != "Internal")
                _log.InfoFormat("[" + User.Identity.Name + "] " +
                    filterContext.RouteData.Values.Aggregate("", (s, kvp) =>
                        s + string.Format("{0}: {1}; ", kvp.Key, kvp.Value)));
            base.OnActionExecuting(filterContext);

            if (HttpContext.Cache[AppName + "__BusinessCraftingDeadline"] == null)
                //HttpContext.Cache[AppName + "__BusinessCraftingDeadline"] = MyBusiness.CraftingDeadline;
                HttpContext.Cache.Add(AppName + "__BusinessCraftingDeadline", MyBusiness.CraftingDeadline, null, DateTime.MaxValue, TimeSpan.FromMinutes(2),
                    System.Web.Caching.CacheItemPriority.Normal, null);

            Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencySymbol = MyBusiness.Currency;           
            Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyPositivePattern = MyBusiness.CurrencyPositivePattern;
            Thread.CurrentThread.CurrentUICulture.NumberFormat.CurrencyNegativePattern = MyBusiness.CurrencyNegativePattern;

            ViewBag.HandwalkStep = MyBusiness.HandwalkStep;
#if DEBUG
            if (!Request.QueryString.AllKeys.Contains("disable"))
            {
#endif
                var controller = filterContext.RouteData.Values["controller"].ToString();
                var action = filterContext.RouteData.Values["action"].ToString();
                if (MyBusiness.HandwalkStep == -1 && controller != "Internal" && controller != "Account" && controller!="Business")
                {
                    filterContext.Result = new RedirectResult(Url.Action("HandwalkIntro", "Business"));
                }
                if (MyBusiness.HandwalkStep > 0)
                {
                    if (controller != "Internal" && controller != "Account" &&
                        !Defines.HandwalkSteps[MyBusiness.HandwalkStep].Any(
                        t => t.Item1.Equals(controller, StringComparison.InvariantCultureIgnoreCase) &&
                             t.Item2.Equals(action, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        var first = Defines.HandwalkSteps[MyBusiness.HandwalkStep].First();
                        filterContext.Result = new RedirectResult(Url.Action(first.Item2, first.Item1));
                    }
                }
#if DEBUG
            }
#endif
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            ViewBag.AppName = AppName;
            ViewBag.Mailto = MyBusiness.MailConfirmAddress;

            ViewBag.ActiveMenuId = FindActiveMenuId();

            ViewBag.MyBusiness = MyBusiness;

            if (MyBusiness.UsingTradingProps)
            {
                ViewBag.BusinessName = MyBusiness.TradingName;
            }
            else
            {
                ViewBag.BusinessName = MyBusiness.LegalName;
            }
            ViewBag.CurrencySymbol = MyBusiness.Currency;

            var attrs = this.GetType().GetCustomAttributes(typeof(ChlebyModuleAttribute), true);
            ViewBag.AdminModule = false;
            if (attrs.Length == 1)
            {
                var attr = (ChlebyModuleAttribute)attrs[0];
                ViewBag.ModuleName = attr.Name;
                if (attr.Parent != null)
                {
                    var parentAttr = attr.Parent.MyGetCustomAttribute<ChlebyModuleAttribute>(true);
                    ViewBag.ModuleName = parentAttr.Name;
                    ViewBag.ChildModule = attr.Name;
                }
                ViewBag.AdminModule = MvcApplication.ModuleRegister.Find(x => x.Name == attr.Name).Admin;
            }

            //woo automatic title everywhere where not set!
            if (ViewBag.Title == null && MvcApplication.ModuleRegister.Any())
            {
                ModuleRegister module = MvcApplication.ModuleRegister.FirstOrDefault(x => x.Name == ViewBag.ModuleName);
                if (module != null)
                {
                    if (module.Admin)
                    {
                        ViewBag.Title =
                      string.Format("{1}",
                      ViewBag.TraderName, module.HumanName, ViewBag.BusinessName);
                    }
                    else
                    {
                        ViewBag.Title =
                      string.Format("{0} - {1} - {2}",
                      ViewBag.TraderName, module.HumanName, ViewBag.BusinessName);
                    }

                }
            }

            var rdc = new ReflectedControllerDescriptor(GetType());
            var ad = rdc.FindAction(this.ControllerContext, this.ControllerContext.RouteData.Values["action"].ToString());
            var action = (ad as ReflectedActionDescriptor).MethodInfo.ToString();
            var syshelp = MvcApplication.Help.Find(x =>
                x.Controller == this.GetType().Name &&
                x.Action == action);
            ViewBag.SystemHelp = syshelp == null ? "" : syshelp.Content;

            base.OnResultExecuting(filterContext);
        }

        private string FindActiveMenuId()
        {
            var rd = Request.RequestContext.RouteData.Values;
            var ctl = rd["controller"].ToString();
            var act = rd["action"].ToString();
            var ctltype = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(x => x.Name == ctl + "Controller");
            if (ctltype != null)
            {
                var methods = ctltype.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
                var actinfo = methods.FirstOrDefault(x => x.Name.ToUpperInvariant() == act.ToUpperInvariant());
                if (actinfo != null)
                {
                    var mattr = actinfo.MyGetCustomAttribute<ChlebyMenuAttribute>();
                    if (mattr != null)
                        return mattr.Id;
                }
                var cattr = ctltype.MyGetCustomAttribute<ChlebyMenuAttribute>();
                if (cattr != null)
                    return cattr.Id;
            }
            return "";
        }

        protected ActionResult DebugReport(string format, params object[] args)
        {
            _log.DebugFormat(format, args);
            return Content("DEBUG: " + string.Format(format, args));
        }

        public ActionResult BugReport()
        {
            ViewBag.info = this.RouteData.Values.Aggregate("", (s, k) => s + k.Key + ": " + k.Value + "\n");
            ViewBag.info += "TID: " + Session["TraderId"] + "/" + _tid + "\n";
            ViewBag.info += "Roles: " + string.Join(", ", Roles.GetRolesForUser()) + "\n";
            ViewBag.info += "User: " + Membership.GetUser() + "\n";
            ViewBag.info += "Referer: " + Request.UrlReferrer;

            //var mantis = new Helper(AppName);

            ViewBag.sevs = new string[] { "low", "medium", "high" };

            ViewBag.email = Membership.GetUser().UserName;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BugReport(string geninfo, string userinfo,
            string cat, string prio, string repr, string sev)
        {
            var sw = new StreamWriter(Server.MapPath("~/logs/bug_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
            sw.WriteLine(geninfo);
            sw.WriteLine(userinfo);
            sw.Close();

            var issue = new IssueData();
            issue.additional_information = geninfo;
            issue.description = userinfo;
            if (string.IsNullOrWhiteSpace(userinfo))
                issue.summary = "[empty]";
            else
                issue.summary = userinfo.Split('\n').FirstOrDefault();
            //var mantis = new Helper(AppName);

            //issue.category = cat;
            ////issue.category = mantis.Categories.FirstOrDefault();
            //issue.priority = new ObjectRef { id = prio };
            //// issue.priority = mantis.Priorities.FirstOrDefault();
            //issue.reproducibility = new ObjectRef { id = repr };
            //// issue.reproducibility = new ObjectRef {id = "100"};
            //issue.severity = new ObjectRef { id = sev };
            ////issue.severity = new ObjectRef { id = "10" };

            //mantis.Connect();
            //var ret = mantis.AddIssue(issue);

            SetAlert(Severity.success, chleby.Web.Resources.Strings.ChlebyController_Bug_Report);
            return RedirectToAction("Index");
        }

        protected string TrySaveImage(HttpPostedFileBase uploadedfile, string fieldname, int x = 100, int y = 100)
        {
            string ret = null;
            if (uploadedfile != null)
            {
                var file = uploadedfile;
                var fn = Guid.NewGuid().ToString("N") + ".upload";

                try
                {
                    Image img = new Bitmap(file.InputStream);
                    if (img.Width < x)
                        x = img.Width;
                    if (img.Height < y)
                        y = img.Height;
                    Image logo = new Bitmap(x, y);
                    Graphics g = Graphics.FromImage(logo);
                    g.DrawImage(img, 0, 0, x, y);
                    logo.Save(Server.MapPath("~/img/" + fn), ImageFormat.Png);
                    ret = fn;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(fieldname, "Bad image " + e.Message);
                    ret = null;
                }
            }
            return ret;
        }

        protected void CreateInvitation(MembershipUser user, Employee emp)
        {
            var info = new UserPwdResetInfo();
            info.UserId = (Guid)user.ProviderUserKey;
            info.ResetDate = DateTime.Now;
            info.Token = Guid.NewGuid();
            db.UserPwdResetInfos.Add(info);

            string template = "";
            if (emp is BusinessEmployee)
                template = "pwdFirstSetMail";
            else if (emp is TraderEmployee)
            {
                template = ((TraderEmployee)emp).MainContact ? "pwdFirstSetTraderFirstMail" : "pwdFirstSetTraderMail";
            }

            new Mail(db, template, user.Email, new
                {
                    user = user.UserName,
                    link = Url.Action("Invitation", "Account", new { id = info.Token }),
                    name = emp.FirstName + " " + emp.LastName,
                    host = HttpContext.Request.ServerVariables["HTTP_HOST"]
                }).Send();
        }

        protected void Log(AuditLog log)
        {
            log.Module = log.Module ?? GetType().MyGetCustomAttribute<ChlebyModuleAttribute>().Name;
            log.User = log.User ?? User.Identity.Name;
            db.AuditLog.Add(log);
        }

        protected void Log(string action, int? objectId = null)
        {
            Log(new AuditLog { Action = action, ObjectId = objectId });
        }

        public ActionResult Lang()
        {
            var lang = RouteData.Values["lang"].ToString();
            var langCookie = new HttpCookie("userLang",lang);
            langCookie.Path = Url.Content("~/" + AppName);
            Response.AppendCookie(langCookie);

            var uri = Request.UrlReferrer;
            if (uri != null)
                return Redirect(uri.ToString());
            else
                return RedirectToAction("Index", "Home", new {lang});
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            ILog _logger = LogManager.GetLogger(typeof(ChlebyController));
            if (filterContext.Exception != null)
            {
                _logger.ErrorFormat("Unhandled exception! {0} \n {1}", filterContext.Exception.Message,
                    filterContext.Exception.StackTrace);
            }

        }
    }
}