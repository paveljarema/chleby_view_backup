﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using chleby.Web.Models;

namespace chleby.Web
{
    public abstract class RecipeProvider
    {
        protected WorldEntity _entity;
        protected ChlebyContext _db;

        protected RecipeProvider(WorldEntity entity, ChlebyContext db)
        {
            _entity = entity;
            _db = db;
        }

        public virtual IEnumerable<string> GetSteps()
        {
            return new string[] { };
        }

        public virtual IEnumerable<Supplier> GetSuppliers()
        {
            return new Supplier[] { };
        }

        public virtual IEnumerable<SuppliedEntity> GetSupplyInfo()
        {
            return new SuppliedEntity[] { };
        }
    }

    public class NullRecipeProvider : RecipeProvider
    {
        public NullRecipeProvider(WorldEntity entity, ChlebyContext db) : base(entity, db) {}
    }
}
