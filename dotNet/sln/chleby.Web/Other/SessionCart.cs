﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using chleby.Web.Models;

namespace chleby.Web
{
    public class SessionCart
    {
        public SessionCart()
        {
            Items = new Dictionary<Item, int>();
            Errors = new Dictionary<int, bool>();
            Date = DateTime.Today.AddDays(3);
        }

        public Dictionary<Item, int> Items { get; set; }
        public Dictionary<int, bool> Errors { get; set; }

        [Required]
        public bool Delivery { get; set; }

        public int AddressId { get; set; }

        public decimal DeliveryPrice { get; set; }

        public string Note { get; set; }

        public void Add(Item item, int qty)
        {
            if (Items.ContainsKey(item))
                Items[item] += qty;
            else
            {
                Items.Add(item, qty);
                Errors.Add(item.Id, false);
            }
        }

        public void Set(Item item, int qty)
        {
            if (Items.ContainsKey(item))
                Items[item] = qty;
            else
            {
                Items.Add(item, qty);
                Errors.Add(item.Id, false);
            }
        }

        public void Delete(Item item)
        {
            if (Items.ContainsKey(item))
            {
                Items.Remove(item);
                Errors.Remove(item.Id);
            }
        }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }
    }
}