﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Net.Mail;
using System.Web;
using DotNetOpenAuth.Messaging;
using chleby.Web.Models;
using log4net;

namespace chleby.Web
{
    public class Mail
    {
        private readonly TextTemplate _template;
        private readonly string _recipient;
        private readonly dynamic _data;
        private readonly MailTemplate _dbobj;
        private readonly TextTemplate _subjectTemplate;

        public List<Attachment> Attachments { get; private set; }

        protected static readonly ILog _log = LogManager.GetLogger(typeof(Mail));

        public Mail(ChlebyContext db, string name, string recipient, dynamic data = null)
        {
            _recipient = recipient;
            _data = data;
            Attachments = new List<Attachment>();

            _dbobj = db.MailTemplates.FirstOrDefault(x => x.Name == name);
            if (_dbobj == null)
                throw new ArgumentException("template doesn't exist in database");

            _template = TextTemplate.FromString(_dbobj.Template);
            _subjectTemplate = TextTemplate.FromString(_dbobj.Subject);
        }

        public Mail AddAttachment(Attachment attachment)
        {
            Attachments.Add(attachment);
            return this;
        }

        public Mail AddAttachment(string content, string name)
        {
            Attachments.Add(Attachment.CreateAttachmentFromString(content, name));
            return this;
        }

        public void Send()
        {
            if (!_dbobj.Send)
            {
                _log.InfoFormat("ignoring mail {0} (disabled in global settings) to {1}",
                    _dbobj.Name, _recipient);
                return;
            }

            try
            {
                var msg = new MailMessage(
                    Defines.MailSender,
                    _recipient, //Defines.OwnerMailAddress,
                    _subjectTemplate.Format(_data ?? new object()),
                    _template.Format(_data ?? new object()));
                msg.Attachments.AddRange(Attachments);

                SmtpHelper.Send(msg);
            }
            catch (Exception e)
            {
                _log.Warn("error while sending mail", e);
            }
        }
    }
}