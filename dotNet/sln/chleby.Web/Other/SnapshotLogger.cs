﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using chleby.Web.Models;
using log4net;

namespace chleby.Web.Other
{
    public interface ISnapshotLogger
    {
        void Log(string msg);
    }

    public class SnapshotLogger : ISnapshotLogger
    {
        public ILog Logger
        {
            get
            {
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }

        public ILog _logger;

        public void Log(string msg)
        {
            Logger.Info(msg);
        }
    }


    public static class SnapshotLoggerExtension
    {
        public static string GetLoggingMessage(this AdhocOrder adhoc)
        {
            StringBuilder msg = new StringBuilder(string.Format("Adhoc Order: id: {0}, address: {1}, date: {2}, delivery: {3}, note: {4}", adhoc.Id, adhoc.Address.Address1, adhoc.Date, adhoc.Delivery, adhoc.Note));
            msg.AppendLine(string.Format("Items (count: {0}):", adhoc.Items.Count));
            foreach (OrderPosition item in adhoc.Items)
            {
                msg.AppendLine(string.Format("Item: id: {0}, name: {1}", item.Id, item.Item.Name));
            }

            return msg.ToString();
        }

        public static string GetLogginMessage(this StandingOrder sorder)
        {
            StringBuilder msg = new StringBuilder(string.Format("StandingOrder: id: {0}, address: {1}, day: {2}, delivery: {3}, note: {4}", sorder.Id, sorder.Address.Address1, sorder.Day, sorder.Delivery, sorder.Note));

            msg.AppendLine(string.Format("Items (count: {0}):", sorder.Items.Count));
            foreach (OrderPosition item in sorder.Items)
            {
                msg.AppendLine(item.GetLogginMessage());
            }

            return msg.ToString();
        }


        public static string GetLogginMessage(this SnapshotOrderPosition sorder)
        {
            return string.Format("SnapshotOrderPosition: id: {0}, address: {1}, startDate: {2}, endDate: {3}, item: {4}, price: {5}", sorder.Id, sorder.Address.Address1, sorder.StartDate, sorder.EndDate, sorder.Item.Name, sorder.Price);
        }

        public static string GetLogginMessage(this OrderPosition item)
        {
            return string.Format("Item: id: {0}, name: {1}", item.Id, item.Item.Name);
        }
    }
}