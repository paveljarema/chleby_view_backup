﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace chleby.Web.Mantis
{
    public class Helper
    {
        private readonly string _user = ConfigurationManager.AppSettings["mantisUser"];
        private readonly string _pass = ConfigurationManager.AppSettings["mantisPass"];
        private readonly string _projId = ConfigurationManager.AppSettings["mantisProject"];
        private string _filter;

        private readonly MantisConnectPortTypeClient _client;
        private readonly string _app;
        private ObjectRef _appIdField;

        private static string[] _catCache;
        private static ObjectRef[] _sevCache;
        private static ObjectRef[] _reprCache;
        private static ObjectRef[] _prioCache;
        private static ObjectRef[] _statusCache;
        private static ObjectRef[] _acclvlCache;
        private static AccountData[] _userCache;

        public string[] Categories { get { return GetCategories(); } }
        public ObjectRef[] Reproducibilities { get { return GetReproducibilities(); } }
        public ObjectRef[] Priorities { get { return GetPriorities(); } }
        public ObjectRef[] Severities { get { return GetSeverities(); } }
        public ObjectRef[] Statuses { get { return GetStatuses(); } }
        public AccountData[] Users { get { return GetUsers(); } }

        public Helper(string app)
        {
            _client = new MantisConnectPortTypeClient();
            _app = app;
        }

        public void Connect()
        {
            _appIdField = _client.mc_project_get_custom_fields(_user, _pass, _projId).
                First(x => x.field.name == "auto_appId").field;

            var filters = _client.mc_filter_get(_user, _pass, _projId);
            var filter = filters.FirstOrDefault(x => x.name == "api_" + _app);
            if (filter != null)
                _filter = filter.id;
        }

        private string[] GetCategories()
        {
            if (_catCache != null)
                return _catCache;

            return _catCache = _client.mc_project_get_categories(_user, _pass, _projId);
        }

        public IssueData[] GetIssues(int pageNumber, int pageSize)
        {
            return _client.mc_filter_get_issues(_user, _pass, _projId, _filter, "1", "10");
        }

        public IssueHeaderData[] GetIssueHeaders(int pageNumber, int pageSize)
        {
            return _client.mc_filter_get_issue_headers(_user, _pass, _projId, _filter, "1", "10");
        }

        public IssueHeaderData[] GetIssueHeaders()
        {
            var headers = new List<IssueHeaderData>();
            if (_filter == null)
                return headers.ToArray();
            int page = 1;
            int len = 0;
            do
            {
                //retarded mantis fix, jak pusto to zwracal pierwsza strone
                IssueHeaderData[] partial = GetIssueHeaders(page++, 10);
                len = partial.Count(x => headers.All(i => i.id != x.id));
                if (len == 0) break;

                headers.AddRange(partial);
            } while (len > 0);
            return headers.ToArray();
        }

        public IssueData[] GetIssues()
        {
            var issues = new List<IssueData>();
            if (_filter == null)
                return issues.ToArray();
            int page = 1;
            int len = 0;
            do
            {
                //retarded mantis fix, jak pusto to zwracal pierwsza strone
                IssueData[] partial = GetIssues(page++, 10);
                len = partial.Count(x => issues.All(i => i.id != x.id));
                if (len == 0) break;

                issues.AddRange(partial);
            } while (len > 0);
            return issues.ToArray();
        }

        public IssueData GetIssue(string id)
        {
            return _client.mc_issue_get(_user, _pass, id);
        }

        private ObjectRef[] GetReproducibilities()
        {
            if (_reprCache != null)
                return _reprCache;

            return _reprCache = _client.mc_enum_reproducibilities(_user, _pass);
        }

        private ObjectRef[] GetStatuses()
        {
            if (_statusCache != null)
                return _statusCache;

            return _statusCache = _client.mc_enum_status(_user, _pass);
        }

        private ObjectRef[] GetSeverities()
        {
            if (_sevCache != null)
                return _sevCache;

            return _sevCache = _client.mc_enum_severities(_user, _pass);
        }

        private ObjectRef[] GetPriorities()
        {
            if (_prioCache != null)
                return _prioCache;

            return _prioCache = _client.mc_enum_priorities(_user, _pass);
        }

        private AccountData[] GetUsers()
        {
            if (_userCache != null)
                return _userCache;

            _acclvlCache = _client.mc_enum_access_levels(_user, _pass);
            return _userCache = _client.mc_project_get_users(_user, _pass, _projId, _acclvlCache.First(x => x.name == "viewer").id);
        }

        public string AddIssue(IssueData issue)
        {
            issue.project = new ObjectRef { id = _projId };
            issue.custom_fields = new CustomFieldValueForIssueData[1];
            issue.custom_fields[0] = new CustomFieldValueForIssueData();
            issue.custom_fields[0].field = _appIdField;
            issue.custom_fields[0].value = _app;
            return _client.mc_issue_add(_user, _pass, issue);
        }

        public bool CloseIssue(string id)
        {
            var issue = GetIssue(id);
            issue.status = Statuses.First(x => x.name == "closed");
            return _client.mc_issue_update(_user, _pass, id, issue);
        }
    }
}