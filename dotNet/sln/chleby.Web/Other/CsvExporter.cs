﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;

namespace chleby.Web
{
    public static class CsvExporter
    {
        public static string Export<T>(IEnumerable<T> obj, bool withHeader = true)
        {
            var sb = new StringBuilder();
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            if (withHeader)
            {
                foreach (PropertyDescriptor prop in props)
                {
                    if (prop.PropertyType == typeof(string) ||
                        prop.PropertyType.GetInterface(typeof(IEnumerable).FullName) == null)
                        sb.AppendFormat("{0};", prop.Name);
                }
                sb.AppendLine();
            }
            foreach (var o in obj)
            {
                foreach (PropertyDescriptor prop in props)
                {
                    if (prop.PropertyType == typeof(string) || prop.PropertyType.GetInterface(typeof(IEnumerable).FullName) == null)
                        sb.AppendFormat("{0};", prop.GetValue(o) != null ? prop.GetValue(o).ToString().Trim() : "null");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}