﻿using System;
using System.Configuration;
using System.Messaging;
using System.Net;
using System.Net.Mail;

namespace chleby.Web
{
    /// <summary>
    /// Klasa ułatwiająca wysyłanie maili z konta ustawionego w konfiguracji serwera
    /// </summary>
    public static class SmtpHelper
    {
        private const int _maxAttempts = 3;
        private const string MSMQName = @".\private$\chleby.mail";

        /// <summary>
        /// Wysyła mail, parametry powinny mieć poprawny format
        /// </summary>
        /// <param name="from">mail nadawcy, pozwala zdefiniowac nazwę wyświetlaną u klienta</param>
        /// <param name="recipients">mail odbiorcy</param>
        /// <param name="subject">temat maila</param>
        /// <param name="body">zawartość maila</param>
        
        public static void Send(string @from, string recipients, string subject, string body)
        {
            SmtpClient smtpClient = HookupClient();
            MailMessage mail = new MailMessage();
            //Setting From , To and CC
            mail.From = new MailAddress(from, from);
            mail.To.Add(new MailAddress(recipients));
            mail.Body = body;
            mail.Subject = subject; 

            smtpClient.Send(mail);
            //Send(new MailMessage(from, recipients, subject, body));
        }

        /// <summary>
        /// Wysyła mail z gotowego obiektu reprezentującego wiadomość
        /// </summary>
        /// <param name="message">wiadomość do wysłania</param>
        public static void Send(MailMessage message)
        {
            try
            {
                /* var qmsg = new Message();
                qmsg.Body = new common.SerializeableMailMessage(message);
                qmsg.AppSpecific = _maxAttempts;
                qmsg.Extension = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                using (var queue = new MessageQueue(MSMQName, QueueAccessMode.Send))
                {
                    qmsg.Formatter = new BinaryMessageFormatter();
                    queue.Send(qmsg);
                } */
                SmtpClient smtpClient = HookupClient();
                smtpClient.Send(message);
            }
            catch { }
        }

        private static SmtpClient HookupClient()
        {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
            smtpClient.Credentials = new System.Net.NetworkCredential("pavel.jarema@gmail.com", "karma108");
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;
            return smtpClient;
        }
    }
}