﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using chleby.Web.Models;
using System.Collections.ObjectModel;
namespace chleby.Web
{
    public static class Defines
    {
        public const string DataDirectory = "~/data";

        public const string MailSender = "mountainstream.line@gmail.com";

        public const string OwnerMailAddress = "mountainstream.line@gmail.com";

        public const string CronCacheKey = "ywosazc5zculk4ynzcigam3ywobelhc2onorc1";

        public static readonly TimeSpan OrderNotifyPeriod = new TimeSpan(0, 0, 15);

        public static Dictionary<string, TimeZoneInfo> Timezones { get; private set; }

        public static Dictionary<AttrDataType, string> AttrTypeNames { get; private set; }

        public static Dictionary<int, List<Tuple<string, string>>> HandwalkSteps = new Dictionary<int, List<Tuple<string, string>>>() {
            { 1, new List<Tuple<string,string>> {
                Tuple.Create("Business", "Company"),
                Tuple.Create("Business", "EditCompany"),
                } 
            },
            { 2, new List<Tuple<string,string>> {
                Tuple.Create("Business", "Index"),
                Tuple.Create("Business", "Edit"),
                Tuple.Create("Business", "Logo"),
                Tuple.Create("Business", "UploadLogo"),
                } 
            },
            { 3, new List<Tuple<string,string>> {
                Tuple.Create("Business", "Invoicing"),
                Tuple.Create("Business", "EditInvoice"),
                Tuple.Create("Business", "ManagePaymentTerms"),
                Tuple.Create("Business", "EditPaymentList"),
                Tuple.Create("Business", "ManageAccSoftware"),
                Tuple.Create("XeroSettings", "Index"),
                Tuple.Create("Business", "ManageTT"),
                Tuple.Create("Business", "EditTypes"),
                } 
            },
            { 4, new List<Tuple<string,string>> {
                Tuple.Create("BusinessEmployee", "Index"),
                Tuple.Create("BusinessEmployee", "Create"),
                Tuple.Create("BusinessEmployee", "Edit"),
                Tuple.Create("BusinessEmployee", "Delete"),
                Tuple.Create("BusinessEmployee", "Import"),
                Tuple.Create("BusinessEmployee", "ImportFile"),
                Tuple.Create("BusinessEmployee", "DwlTemplate"),
                Tuple.Create("Permissions", "EmployeeEdit"),
                } 
            },
            { 5, new List<Tuple<string, string>>
                {
                    Tuple.Create("Rounds", "Index"),
                    Tuple.Create("Rounds", "Create"),
                    Tuple.Create("Rounds", "Edit"),
                    Tuple.Create("Rounds", "Delete"),
                    Tuple.Create("Rounds", "Down"),
                    Tuple.Create("Rounds", "Up"),
                    Tuple.Create("Rounds", "SaveOrder"), 
                    Tuple.Create("Address", "Index"),
                    Tuple.Create("Address", "Create"),
                    Tuple.Create("Address", "Edit"),
                    Tuple.Create("Address", "Delete"),
                    Tuple.Create("Address", "SetPrimary"),
                    Tuple.Create("Address", "Import"),
                    Tuple.Create("Address", "ImportFile"),
                    Tuple.Create("Address", "DwlTemplate"),
                }},
            { 6, new List<Tuple<string,string>> {
                Tuple.Create("TurboTraders", "Index"),
                Tuple.Create("TurboTraders", "PDF"),
                Tuple.Create("Traders", "Create"),
                Tuple.Create("Traders", "Edit"),
                Tuple.Create("Traders", "Delete"),
                Tuple.Create("Traders", "Import"),
                Tuple.Create("Traders", "ImportFile"),
                Tuple.Create("TraderEmployees", "Index"),
                Tuple.Create("TraderEmployees", "Create"),
                Tuple.Create("TraderEmployees", "Edit"),
                Tuple.Create("TraderEmployees", "Delete"),
                Tuple.Create("TraderEmployees", "Import"),
                Tuple.Create("TraderEmployees", "ImportFile"),
                Tuple.Create("Permissions", "TraderEdit"),
                Tuple.Create("Address", "Index"),
                Tuple.Create("Address", "Create"),
                Tuple.Create("Address", "Edit"),
                Tuple.Create("Address", "Delete"),
                Tuple.Create("Address", "SetPrimary"),
                Tuple.Create("Address", "Import"),
                Tuple.Create("Address", "ImportFile"),
                Tuple.Create("Rounds", "Index"),
                Tuple.Create("Rounds", "Create"),
                Tuple.Create("Rounds", "Edit"),
                Tuple.Create("Rounds", "Delete"),
                Tuple.Create("Rounds", "Down"),
                Tuple.Create("Rounds", "Up"),
                Tuple.Create("Rounds", "SaveOrder"),
                Tuple.Create("Traders", "DwlTemplate"),
                Tuple.Create("TraderEmployees", "DwlTemplate"),
                Tuple.Create("Address", "DwlTemplate"),
                } 
            },
            { 7, new List<Tuple<string,string>> {
                Tuple.Create("AttrMeta", "Index"),
                Tuple.Create("AttrMeta", "Base"),
                } 
            },
            { 8, new List<Tuple<string,string>> {
                Tuple.Create("AttrMeta", "Custom"),
                Tuple.Create("AttrMeta", "Create"),
                Tuple.Create("AttrMeta", "Edit"),
                Tuple.Create("AttrMeta", "Delete"),
                Tuple.Create("AttrMeta", "SetOrder"),
                } 
            },
            { 9, new List<Tuple<string,string>> {
                Tuple.Create("Item", "Index"),
                Tuple.Create("Item", "Create"),
                Tuple.Create("Item", "Edit"),
                Tuple.Create("Item", "Delete"),
                Tuple.Create("Item", "CreateGroup"),
                Tuple.Create("Item", "EditGroup"),
                Tuple.Create("Item", "DeleteGroup"),
                Tuple.Create("Item", "Up"),
                Tuple.Create("Item", "Down"),
                Tuple.Create("Item", "ToggleLive"),
                Tuple.Create("Item", "BatchEdit"),
                Tuple.Create("Item", "AttrEdit"),
                Tuple.Create("Item", "Import"),
                Tuple.Create("Item", "ImportFile"),
                Tuple.Create("Item", "Image"),
                Tuple.Create("Item", "DwlTemplate"),
                } 
            },
            { 10, new List<Tuple<string,string>> {
                Tuple.Create("AllHoliday", "Index"),
                Tuple.Create("AllHoliday", "Create"),
                Tuple.Create("AllHoliday", "Edit"),
                Tuple.Create("AllHoliday", "Delete"),
                } 
            },
            {11, new List<Tuple<string, string>> {
                    Tuple.Create("SimpleProduction", "TotalSummary"),
                    Tuple.Create("SimpleProduction", "SOImport"),
                    Tuple.Create("SimpleProduction", "SOImportFile"),
                    Tuple.Create("SimpleProduction", "AOImport"),
                    Tuple.Create("SimpleProduction", "AOImportFIle"),
                    Tuple.Create("SimpleProduction", "AODwlTemplate"),
                    Tuple.Create("SimpleProduction", "SODwlTemplate"),
                }
            },
            { 12, new List<Tuple<string,string>> {
                Tuple.Create("Business", "Finish"),
                } 
            },
        };

        public static List<string> CurrencyPositivePatterns = new List<string> { "$0.00", "0.00$", "$ 0.00", "0.00 $" };

        public static List<string> CurrencyNegativePatterns = new List<string> 
        { "($0.00)", "-$0.00", "$-0.00", "$0.00-",
          "(0.00$)", "-0.00$", "0.00-$", "0.00$-",
          "-0.00 $", "-$ 0.00", "0.00 $-", "$ 0.00-",
          "$ -0.00", "0.00- $", "($ 0.00)", "(0.00 $)"
        };

        static Defines()
        {
            Timezones = new Dictionary<string, TimeZoneInfo>();
            ReadOnlyCollection<TimeZoneInfo> infos = TimeZoneInfo.GetSystemTimeZones();
            foreach (TimeZoneInfo info in infos)
            {
                Timezones.Add(info.Id, info);
            }

            //Timezones.Add("-12", "[UTC - 12] Baker Island Time");
            //Timezones.Add("-11", "[UTC - 11] Niue Time, Samoa Standard Time");
            //Timezones.Add("-10", "[UTC - 10] Hawaii-Aleutian Standard Time, Cook Island Time");
            //Timezones.Add("-9.5", "[UTC - 9:30] Marquesas Islands Time");
            //Timezones.Add("-9", "[UTC - 9] Alaska Standard Time, Gambier Island Time");
            //Timezones.Add("-8", "[UTC - 8] Pacific Standard Time");
            //Timezones.Add("-7", "[UTC - 7] Mountain Standard Time");
            //Timezones.Add("-6", "[UTC - 6] Central Standard Time");
            //Timezones.Add("-5", "[UTC - 5] Eastern Standard Time");
            //Timezones.Add("-4.5", "[UTC - 4:30] Venezuelan Standard Time");
            //Timezones.Add("-4", "[UTC - 4] Atlantic Standard Time");
            //Timezones.Add("-3", "[UTC - 3:30] Newfoundland Standard Time");
            //Timezones.Add("-3.5", "[UTC - 3] Amazon Standard Time, Central Greenland Time");
            //Timezones.Add("-2", "[UTC - 2] Fernando de Noronha Time, South Georgia &amp; the South Sandwich Islands Time");
            //Timezones.Add("-1", "[UTC - 1] Azores Standard Time, Cape Verde Time, Eastern Greenland Time");
            //Timezones.Add("0", "[UTC] Western European Time, Greenwich Mean Time");
            //Timezones.Add("1", "[UTC + 1] Central European Time, West African Time");
            //Timezones.Add("2", "[UTC + 2] Eastern European Time, Central African Time");
            //Timezones.Add("3", "[UTC + 3] Moscow Standard Time, Eastern African Time");
            //Timezones.Add("3.5", "[UTC + 3:30] Iran Standard Time");
            //Timezones.Add("4", "[UTC + 4] Gulf Standard Time, Samara Standard Time");
            //Timezones.Add("4.5", "[UTC + 4:30] Afghanistan Time");
            //Timezones.Add("5", "[UTC + 5] Pakistan Standard Time, Yekaterinburg Standard Time");
            //Timezones.Add("5.5", "[UTC + 5:30] Indian Standard Time, Sri Lanka Time");
            //Timezones.Add("5.75", "[UTC + 5:45] Nepal Time");
            //Timezones.Add("6", "[UTC + 6] Bangladesh Time, Bhutan Time, Novosibirsk Standard Time");
            //Timezones.Add("6.5", "[UTC + 6:30] Cocos Islands Time, Myanmar Time");
            //Timezones.Add("7", "[UTC + 7] Indochina Time, Krasnoyarsk Standard Time");
            //Timezones.Add("8", "[UTC + 8] Chinese Standard Time, Australian Western Standard Time, Irkutsk Standard Time");
            //Timezones.Add("8.75", "[UTC + 8:45] Southeastern Western Australia Standard Time");
            //Timezones.Add("9", "[UTC + 9] Japan Standard Time, Korea Standard Time, Chita Standard Time");
            //Timezones.Add("9.5", "[UTC + 9:30] Australian Central Standard Time");
            //Timezones.Add("10", "[UTC + 10] Australian Eastern Standard Time, Vladivostok Standard Time");
            //Timezones.Add("10.5", "[UTC + 10:30] Lord Howe Standard Time");
            //Timezones.Add("11", "[UTC + 11] Solomon Island Time, Magadan Standard Time");
            //Timezones.Add("11.5", "[UTC + 11:30] Norfolk Island Time");
            //Timezones.Add("12", "[UTC + 12] New Zealand Time, Fiji Time, Kamchatka Standard Time");
            //Timezones.Add("12.75", "[UTC + 12:45] Chatham Islands Time");
            //Timezones.Add("13", "[UTC + 13] Tonga Time, Phoenix Islands Time");
            //Timezones.Add("14", "[UTC + 14] Line Island Time");
            
        }

        public class Roles
        {
            public const string SuperAdmin = "superadmin";
            public const string Employee = "employee";
            public const string Trader = "trader";
            public const string ReadSuffix = "_read";
            public const string WriteSuffix = "_write";
            public const string CustomPrefix = "custom_";
        }
    }
}

