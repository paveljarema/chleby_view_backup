﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ChlebyMenuAttribute : Attribute
    {
        public string Id { get; set; }
        public ChlebyMenuAttribute(string id)
        {
            Id = id;
        }
    }
}