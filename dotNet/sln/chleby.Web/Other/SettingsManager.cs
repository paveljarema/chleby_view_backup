﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using chleby.Web.Models;

namespace chleby.Web
{
    public class SettingsManager:IDisposable
    {
        private readonly ChlebyContext _db;
        private readonly Dictionary<string, SettingsCategory> _cats = new Dictionary<string, SettingsCategory>();

        public SettingsManager(ChlebyContext db)
        {
            _db = db;
            var names = _db.Settings.Select(x => x.Name).ToList();
            foreach (var name in names)
            {
                var cat = name.Split('_')[0];
                if (!_cats.ContainsKey(cat))
                    _cats.Add(cat, new SettingsCategory(_db, cat));
            }
        }

        public SettingsCategory this[string category]
        {
            get
            {
                if (!_cats.ContainsKey(category))
                    throw new ArgumentException("Invalid category name");
                return _cats[category];
            }
        }

        public void Dispose()
        {
            _cats.Clear();
            _db.Dispose();
        }
    }

    public class SettingsCategory
    {
        private readonly string _category;
        private readonly ChlebyContext _db;
        private readonly Dictionary<string, string> _data;

        public SettingsCategory(ChlebyContext db, string category)
        {
            _db = db;
            _category = category;
            _data = (from sett in db.Settings
                      where sett.Name.StartsWith(_category + "_")
                      select sett).ToDictionary(ks => ks.Name, es => es.Value);
        }

        public string this[string name]
        {
            get
            {
                return _data[_category + "_" + name];
            }
            set {
                lock (_data)
                {
                    _data[_category + "_" + name] = value;
                    _db.Settings.First(x => x.Name == _category + "_" + name).Value = value;
                    _db.SaveChanges();
                }
            }
        }

        public T Get<T>(string name)
        {
            var fullname = _category + "_" + name;
            if (!_data.ContainsKey(fullname))
                throw new ArgumentException("Invalid setting name");

            return (T)Convert.ChangeType(_data[_category + "_" + name], typeof(T), CultureInfo.InvariantCulture);
        }
    }
}