﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace chleby.Web
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues, string @class = "")
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var name = ExpressionHelper.GetExpressionText(expression);
            string fullName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            var sb = new StringBuilder();

            if (listOfValues != null)
            {
                foreach (SelectListItem item in listOfValues)
                {
                    var id = string.Format(
                        "{0}_{1}",
                        fullName,
                        item.Value
                    );

                    //var radio = htmlHelper.RadioButtonFor(expression, item.Value, new { id = id }).ToHtmlString();
                    var radio =
                        htmlHelper.RadioButton(fullName, item.Value, item.Selected, new { id = id, @class }).
                            ToHtmlString();
                    sb.AppendFormat(
                        "<label class='radio' >{2}{1}</label> ",
                        id,
                        HttpUtility.HtmlEncode(item.Text),
                        radio
                    );
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }

}