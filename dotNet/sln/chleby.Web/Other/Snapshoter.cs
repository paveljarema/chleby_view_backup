﻿#define SAVETODB
#undef SAVETODB

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Security;
using chleby.Web.Models;
using log4net;
using chleby.Web.Other;
using System.Configuration;
using System.Data.Objects;

namespace chleby.Web
{
    public interface ISnapshotParameterProvider
    {
        DateTime LastSnapshotDate { get; set; }
        decimal DeliveryVat { get; }
        SnapshoterMode Mode { get; }

        /// <summary>
        /// Gets current date
        /// </summary>
        DateTime CurrentDate { get; }
    }

    public class DefaultSnapshotParameterProvider : ISnapshotParameterProvider
    {
        public DefaultSnapshotParameterProvider(ChlebyContext db, string appName)
        {
            _db = db;
            _currentClient = db.Businesses.First();
            _appName = appName;
        }

        private readonly ChlebyContext _db;
        private readonly Business _currentClient;
        private string _appName;

        public DateTime LastSnapshotDate
        {
            get
            {
                return _currentClient.LastSnapshotDate ?? DateTime.MinValue;
            }
            set
            {
                _currentClient.LastSnapshotDate = value;
            }
        }

        public decimal DeliveryVat
        {
            get
            {
                return MvcApplication.Settings[_appName]["PriceData"].Get<decimal>("DeliveryVat");
            }
        }


        public SnapshoterMode Mode
        {
            get
            {
                if (string.Compare(ConfigurationManager.AppSettings["SnapshoterMode"], "Test", true) == 0)
                {
                    return SnapshoterMode.Test;
                }
                return SnapshoterMode.Production;
            }
        }


        public DateTime CurrentDate
        {
            get { return DateTime.UtcNow; }
        }
    }

    public class Snapshoter
    {
        private readonly ChlebyContext _db;
        private static readonly ILog _log = LogManager.GetLogger(typeof(Snapshoter));
        private readonly ISnapshotParameterProvider _parameterProvider;
        private ISnapshotLogger _snapshotLogger;

        public ISnapshotLogger SnapshotLogger
        {
            get
            {
                return _snapshotLogger;
            }
            set { _snapshotLogger = value; }
        }

        public Snapshoter(ChlebyContext db, ISnapshotParameterProvider parameterProvider)
        {
            _db = db;
            _parameterProvider = parameterProvider;
        }

        public void Snapshot(DateTime snapshotDate)
        {
            lock (MvcApplication.Cron)
            {
                _log.Info("Snapshot!");
                if (_parameterProvider.Mode == SnapshoterMode.Test)
                {
                    SnapshotLogger.Log(string.Format("====================================== Making snapshot for {0} ========================", _db.AppConfig.Name));
                }
                Membership.ApplicationName = _db.AppConfig.Name;
                var itemDelivery = _db.Entities.Find(_db.AppConfig.DeliveryId);
                var b = _db.Businesses.FirstOrDefault();
                if (b == null)
                {
                    _log.ErrorFormat("no business in app {0}", _db.AppConfig.Name);
                    return;
                }
                var minPrice = b.MinDelivery;
                var deliveryDays = b.DeliveryDays.ToList();
                var businessHolidays = _db.BusinessHolidays.ToList();
                var dayOfWeek = (int)snapshotDate.DayOfWeek - 1;
                while (dayOfWeek < 0) dayOfWeek += 7;

                //check local time, if different date, chenge day of week
                var business = _db.Businesses.First();
                var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(business.Timezone);
                var localSnapshotDate = TimeZoneInfo.ConvertTimeFromUtc(snapshotDate, timeZoneInfo);

                if (localSnapshotDate.Day != snapshotDate.Day)
                {
                    if (localSnapshotDate.Day < snapshotDate.Day)
                    {
                        dayOfWeek--;
                    }
                    else
                    {
                        dayOfWeek++;
                    }
                }

                var delays = _db.Items.Where(x => x.Live).Select(x => x.CraftingTime).Distinct().ToList();
                if (_parameterProvider.Mode == SnapshoterMode.Test)
                {
                    string msg = string.Empty;
                    foreach (int delay in delays)
                    {
                        msg += delay + ", ";
                    }
                    SnapshotLogger.Log("Got delays: " + msg);
                }
                _log.DebugFormat("Got {0} delays", delays.Count);

                //decimal.Parse(_db.Settings.First(x => x.Name == "PriceData_DeliveryVat").Value);

                var adHocOrders = _db.Orders.OfType<AdhocOrder>().ToList();
                if (_parameterProvider.Mode == SnapshoterMode.Test)
                {
                    if (adHocOrders.Count > 0)
                    {
                        SnapshotLogger.Log(string.Format("AdhocOrders count {0}:", adHocOrders.Count));
                        foreach (AdhocOrder adhoc in adHocOrders)
                        {
                            SnapshotLogger.Log(adhoc.GetLoggingMessage());
                        }
                    }
                    else
                    {
                        SnapshotLogger.Log("No adhoc orders");
                    }
                }
                _log.DebugFormat("Got {0} AdhocOrders", adHocOrders.Count);
                foreach (var delay in delays)
                {
                    var endDate = snapshotDate.AddDays(1 + delay);
                    if (_parameterProvider.Mode == SnapshoterMode.Test)
                    {
                        SnapshotLogger.Log("End Date is : " + endDate);
                    }
                    var holiday = businessHolidays.Where(bh => bh.StartDate <= endDate && bh.EndDate >= endDate).FirstOrDefault();
                    //if (bhs.Any(bh => bh.StartDate <= endDate && bh.EndDate >= endDate))
                    if (holiday != null)
                    {
                        if (_parameterProvider.Mode == SnapshoterMode.Test)
                        {
                            SnapshotLogger.Log(string.Format("End Date {0} is at holiday {1} - {2}: ", endDate, holiday.StartDate, holiday.EndDate));
                        }
                        _log.DebugFormat("BusinessHoliday {0} - {1} for delay {2}", holiday.StartDate, holiday.EndDate, endDate);
                        continue;
                    }
                    var stos = _db.Orders.OfType<StandingOrder>().Where(
                        o => o.Day == (dayOfWeek + delay + 1) % 7 &&
                             o.Items.Any(i => i.Item.CraftingTime == delay) &&
                             o.Address.Live &&
                             o.Address.Trader.Live == 1 &&
                             !o.Address.AddressHolidays.Any(ah => EntityFunctions.TruncateTime(ah.StartDate) <= EntityFunctions.TruncateTime(endDate)
                             && EntityFunctions.TruncateTime(ah.EndDate) >= EntityFunctions.TruncateTime(endDate))).ToList();

                    if (_parameterProvider.Mode == SnapshoterMode.Test)
                    {
                        SnapshotLogger.Log(string.Format("StandingOrders count {0}:", stos.Count));
                        foreach (StandingOrder sto in stos)
                        {
                            SnapshotLogger.Log(sto.GetLogginMessage());
                        }
                    }
                    _log.DebugFormat("Got {0} StandingOrders", stos.Count);
                    foreach (var sto in stos)
                    {
                        if (!deliveryDays.Contains(sto.Day))
                        {
                            _log.DebugFormat("No delivery at day {0}", sto.Day);
                            if (_parameterProvider.Mode == SnapshoterMode.Test)
                            {
                                SnapshotLogger.Log(string.Format("No delivery at day {0}", sto.Day));
                            }
                            continue;
                        }
                        if (!sto.Address.DeliveryDays.Contains(sto.Day)) //dni pracy adresu
                        {
                            _log.DebugFormat("No delivery at day {0} in {1}", sto.Day, sto.Address.Address1);
                            if (_parameterProvider.Mode == SnapshoterMode.Test)
                            {
                                SnapshotLogger.Log(string.Format("No delivery at day {0} in {1}", sto.Day, sto.Address.Address1));
                            }
                            continue;
                        }



                        var its = sto.Items.Where(i => i.Item.CraftingTime == delay).ToList();

                        if (_parameterProvider.Mode == SnapshoterMode.Test)
                        {
                            SnapshotLogger.Log("Found items count:" + its.Count());
                        }
                        bool itemAdded = false;
                        foreach (var it in its)
                        {
                            var vi = it.Item.Visibility.FirstOrDefault(x => x.TraderId == sto.Address.TraderId);
                            if (!it.Item.Live || (vi != null && vi.HiddenByAdmin)) continue;

                            if (it.OrderedCount != 0)
                            {
                                var snapshot = new SnapshotOrderPosition
                                {
                                    Item = it.Item,
                                    OrderedCount = it.OrderedCount,
                                    MadeCount = it.OrderedCount,
                                    Price = it.Item.GetItemPrice(sto.Address.Trader).Value,
                                    Vat = it.Item.Vat,
                                    Address = sto.Address,
                                    StartDate = snapshotDate,
                                    EndDate = endDate,
                                    Order = sto,
                                    Note = sto.Note
                                };
#if !SAVETODB
                                if (!CheckIfExists(snapshot))
                                {
                                    _db.SnapshotOrderPositions.Add(snapshot);
                                    itemAdded = true;
                                }
                                
#endif
                                if (_parameterProvider.Mode == SnapshoterMode.Test)
                                {
                                    SnapshotLogger.Log("Added snapshot position:" + it.GetLogginMessage());
                                }
                            }
                        }

                        if (itemAdded)
                        {
                            _log.DebugFormat("Items found creating delivery");
                            var delivery = new SnapshotOrderPosition
                            {
                                Address = sto.Address,
                                Adhoc = false,
                                EndDate = endDate,
                                StartDate = snapshotDate,
                                Item = itemDelivery,
                                MadeCount = sto.Delivery ? 1 : 0,
                                OrderedCount = sto.Delivery ? 1 : 0,
                                Order = sto,
                                Price = sto.Address.DeliveryPrice,
                                Vat = _parameterProvider.DeliveryVat
                            };
                            if (_parameterProvider.Mode == SnapshoterMode.Test)
                            {
                                SnapshotLogger.Log("Added delivery: " + delivery.GetLogginMessage());
                            }
                            _log.DebugFormat("Saved delivery {0}", delivery);
                            _db.SnapshotOrderPositions.Add(delivery);
                        }

                    }
#if !SAVETODB
                    if (_parameterProvider.Mode != SnapshoterMode.Test)
                    {
                        _db.SaveChanges();
                    }

#endif
                    var aos = adHocOrders.Where(a => a.Date.Date == endDate.Date &&
                             a.Address.Live &&
                             a.Address.Trader.Live == 1).ToList();
                    _log.DebugFormat("Got {0} AdhocOrders", aos.Count);
                    if (_parameterProvider.Mode == SnapshoterMode.Test)
                    {
                        SnapshotLogger.Log("Processing AdHocs: count: " + aos.Count);
                    }
                    foreach (var ao in aos)
                    {
                        var aodow = (int)ao.Date.DayOfWeek - 1;
                        if (aodow < 0) aodow += 7;
                        if (!deliveryDays.Contains(aodow))
                        {
                            _log.DebugFormat("No delivery at {0} ", aodow);
                            continue;
                        }
                        if (!ao.Address.DeliveryDays.Contains(aodow)) //dni pracy adresu
                        {
                            _log.DebugFormat("No delivery at {0} in {1}", aodow, ao.Address);
                            continue;
                        }
                        foreach (var it in ao.Items)
                        {
                            var vi = it.Item.Visibility.FirstOrDefault(x => x.TraderId == ao.Address.TraderId);
                            if (!it.Item.Live || (vi != null && vi.HiddenByAdmin))
                            {
                                _log.DebugFormat("Item {0} is hidden, ignoring...", it);
                                continue;
                            }
                            var stpos = _db.SnapshotOrderPositions.FirstOrDefault(pos =>
                                pos.Address.Id == ao.Address.Id &&
                                pos.Item.Id == it.Item.Id &&
                                EntityFunctions.TruncateTime(pos.EndDate) == EntityFunctions.TruncateTime(ao.Date));
                            if (stpos == null)
                            {
                                var delivery = new SnapshotOrderPosition
                                {
                                    Address = ao.Address,
                                    Adhoc = false,
                                    EndDate = endDate,
                                    StartDate = snapshotDate,
                                    Item = itemDelivery,
                                    MadeCount = ao.Delivery ? 1 : 0,
                                    OrderedCount = ao.Delivery ? 1 : 0,
                                    Order = ao,
                                    Price = ao.Address.DeliveryPrice,
                                    Vat = _parameterProvider.DeliveryVat
                                };
                                _log.DebugFormat("Adding SnapshotOrderPosition {0}", delivery);
                                _log.Debug(delivery);
#if !SAVETODB
                                _db.SnapshotOrderPositions.Add(delivery);
                                if (_parameterProvider.Mode == SnapshoterMode.Test)
                                {
                                    SnapshotLogger.Log("Adding delivery for AdHoc: " + delivery.GetLogginMessage());
                                }
#endif
                                var snapshot = new SnapshotOrderPosition
                                    {
                                        Item = it.Item,
                                        OrderedCount = it.OrderedCount,
                                        MadeCount = it.OrderedCount,
                                        Price = it.Item.GetItemPrice(ao.Address.Trader).Value,
                                        Vat = it.Item.Vat,
                                        Address = ao.Address,
                                        StartDate = snapshotDate,
                                        EndDate = endDate,
                                        Order = ao,
                                        Adhoc = true,
                                        Note = ao.Note
                                    };
#if !SAVETODB
                                _db.SnapshotOrderPositions.Add(snapshot);
                                if (_parameterProvider.Mode == SnapshoterMode.Test)
                                {
                                    SnapshotLogger.Log("Adding Snapshot for AdHoc: " + snapshot.GetLogginMessage());
                                }
#endif
                                _log.DebugFormat("Adding SnapshotOrderPosition {0}", snapshot);
                            }
                            else
                            {
                                stpos.OrderedCount = it.OrderedCount;
                                stpos.MadeCount = it.OrderedCount;
                                stpos.Adhoc = true;
                                _log.DebugFormat("Modifing SnapshotOrderPosition {0}", stpos);
                            }
                        }
                    }
                    //TODO: przetestowac to!
                    var poss = from pos in _db.SnapshotOrderPositions
                               where EntityFunctions.TruncateTime(pos.EndDate) == EntityFunctions.TruncateTime(endDate)
                               && pos.Item is Item
                               group pos by pos.Address into byaddr
                               select byaddr;
                    foreach (var byaddr in poss.ToList())
                    {
                        try
                        {
                            var addr = byaddr.Key;
                            var pricesum = byaddr.Sum(x =>
                                                      x.Item.GetItemPrice(addr.Trader).Value);
                            var emp = byaddr.Key.TraderEmployees.First();
                            var user = Membership.GetUser(emp.ExternalAccountData);
                            if (user == null) continue;
                            var email = user.Email;
                            if (pricesum < minPrice)
                            {
                                _db.SnapshotOrderPositions.Local.
                                    Where(
                                        x =>
                                        EntityFunctions.TruncateTime(x.EndDate) == EntityFunctions.TruncateTime(endDate)
                                        && x.Item_Id == itemDelivery.Id && x.Address_Id == byaddr.Key.Id).
                                    ToList().ForEach(x => x.OrderedCount = x.MadeCount = 0);
                                new Mail(_db, "minDeliveryWarningErr", email, new
                                    {
                                        poss = byaddr.ToList(),
                                        minPrice
                                    }).Send();
                                new Mail(_db, "minDeliveryWarningErr", b.MailConfirmAddress, new
                                    {
                                        poss = byaddr.ToList(),
                                        minPrice
                                    }).Send();
                            }
                            else
                            {
                                new Mail(_db, "minDeliveryWarningOk", email, new
                                    {
                                        poss = byaddr.ToList(),
                                        minPrice
                                    }).Send();
                                new Mail(_db, "minDeliveryWarningOk", b.MailConfirmAddress, new
                                    {
                                        poss = byaddr.ToList(),
                                        minPrice
                                    }).Send();
                            }
                        }
                        catch (Exception e)
                        {
                            _log.Error("error while sending snapshot info mails", e);
                        }
                    }
                }
#if !SAVETODB
                if (_parameterProvider.Mode != SnapshoterMode.Test)
                {
                    _db.SaveChanges();
                }
#endif
                //_db.Settings.First(x => x.Name == "Snapshot_LastDate").Value 
                //MvcApplication.Settings[_db.AppConfig.Name]["Snapshot"]["LastDate"] = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                _parameterProvider.LastSnapshotDate = DateTime.UtcNow;
#if !SAVETODB
                if (_parameterProvider.Mode != SnapshoterMode.Test)
                {
                    _db.SaveChanges();
                }
#endif
#if !SAVETODB
                FixDelivery();
#endif
                try
                {
                    SendOrdersMail();
                }
                catch (Exception e)
                {
                    _log.Error("error around sendordermail", e);
                }
                if (_parameterProvider.Mode == SnapshoterMode.Test)
                {
                    SnapshotLogger.Log(string.Format("====================================== Snapshot finished for {0} ========================", _db.AppConfig.Name));
                }
            }

        }

        /// <summary>
        /// Checks if snapshot order position exists in db
        /// </summary>
        /// <param name="position">Snapshot order position</param>
        /// <returns>True if exists, otherwise false</returns>
        private bool CheckIfExists(SnapshotOrderPosition position)
        {
            return _db.SnapshotOrderPositions.Any(pos =>
                pos.Address_Id == position.Address.Id &&
                EntityFunctions.TruncateTime(pos.EndDate) == EntityFunctions.TruncateTime(position.EndDate) &&
                pos.Item_Id == position.Item.Id
                );
        }

        internal void SendOrdersMail()
        {
            var b = _db.Businesses.First();
            if (!b.SendOrdersBackup)
                return;

            try
            {
                var csv = new OrderManager(_db, b, 0).GetAllOrdersCSV();
                new Mail(_db, "backup", b.MailConfirmAddress, new { date = DateTime.UtcNow }).
                    AddAttachment(csv, "orders.csv").
                    Send();
            }
            catch (Exception e)
            {
                _log.Warn("error while sending order backup email to " + b.MailConfirmAddress, e);
            }
        }

        public void FixDelivery()
        {
            var deliveryId = _db.AppConfig.DeliveryId;

            var list = new List<Tuple<int, DateTime>>();
            foreach (var pos in _db.SnapshotOrderPositions.Where(x => x.Item_Id == deliveryId).ToList())
            {
                var key = Tuple.Create(pos.Address_Id, pos.EndDate);
                if (list.Contains(key))
                {
                    _db.SnapshotOrderPositions.Remove(_db.SnapshotOrderPositions.Find(pos.Id));
                }
                else
                {
                    list.Add(key);
                }
            }
            if (_parameterProvider.Mode != SnapshoterMode.Test)
            {
                _db.SaveChanges();
            }
        }

        public void CreateOverrideSnapshot(int addressId, DateTime? date, string note, int count,
                                    Item item, Trader trader, bool delivery, Business business)
        {
            //pozwalamy tworzyc tylko wtedy, kiedy wiemy ze snapshot nie nadpisze nam nigdy tego
            if (!item.CanOrder(business, date.Value))
            {
                var order = _db.Orders.OfType<AdhocOrder>().
                    FirstOrDefault(x => EntityFunctions.TruncateTime(x.Date) == EntityFunctions.TruncateTime(date.Value) &&
                            x.Address_Id == addressId &&
                            x.Items.Any(p => p.Item_Id == item.Id));
                //var delvat = decimal.Parse(_db.Settings.First(x => x.Name == "PriceData_DeliveryVat").Value);
                //var delvat = MvcApplication.Settings[_db.AppConfig.Name]["PriceData"].Get<decimal>("DeliveryVat");
                var oldpos = _db.SnapshotOrderPositions.FirstOrDefault(
                        x => EntityFunctions.TruncateTime(x.EndDate) == EntityFunctions.TruncateTime(date.Value)
                            && x.Address_Id == addressId && x.Item_Id == item.Id);
                if (oldpos != null)
                {
                    oldpos.Adhoc = true;
                    oldpos.OrderedCount = count;
                }
                else
                {
                    var orderDate = date.Value.AddDays(-item.CraftingTime - 1);

                    var backpos = new SnapshotOrderPosition
                    {
                        Address_Id = addressId,
                        Adhoc = true,
                        EndDate = date.Value,
                        Item = item,
                        OrderedCount = count,
                        MadeCount = count,
                        Note = note,
                        Vat = item.Vat,
                        Price = item.GetItemPrice(trader).Value,
                        StartDate = orderDate,
                        Order = order
                    };
                    _db.SnapshotOrderPositions.Add(backpos);
                }

                var deliveryId = _db.AppConfig.DeliveryId;

                var delpos = _db.SnapshotOrderPositions.FirstOrDefault(
                        x => EntityFunctions.TruncateTime(x.EndDate) == EntityFunctions.TruncateTime(date.Value)
                            && x.Address_Id == addressId && x.Item_Id == deliveryId);
                if (delpos != null)
                {
                    delpos.Adhoc = true;
                    delpos.OrderedCount = delivery ? 1 : 0;
                }
                else
                {
                    delpos = new SnapshotOrderPosition
                    {
                        Address_Id = addressId,
                        Adhoc = true,
                        EndDate = date.Value,
                        Item = _db.Entities.Find(deliveryId),
                        OrderedCount = delivery ? 1 : 0,
                        MadeCount = delivery ? 1 : 0,
                        Note = note,
                        Vat = _parameterProvider.DeliveryVat,
                        Price = item.GetItemPrice(trader).Value,
                        StartDate = date.Value,
                        Order = order
                    };
                    _db.SnapshotOrderPositions.Add(delpos);
                }
            }
            _db.SaveChanges();
        }
    }

    public class MultiSnapshoter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MultiSnapshoter));
        public void Do()
        {

            //lock (MvcApplication.Cron)
            //{
            //    _log.Info("multisnapshoter::do");
            //    foreach (var app in MvcApplication.AppRegister)
            //    {

            //        var db = new ChlebyContext(app);
            //        Business b;

            //        try
            //        {
            //            b = db.Businesses.First();
            //        }
            //        catch
            //        {
            //            _log.Error("No one to snapshot in application " + app.Name);
            //            continue;
            //        }
            //        var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            //        dt = dt.AddHours(b.CraftingDeadline.Hour);
            //        dt = dt.AddMinutes(b.CraftingDeadline.Minute);

            //        var last = DateTime.ParseExact(
            //            MvcApplication.Settings[db.AppConfig.Name]["Snapshot"]["LastDate"],
            //            "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            //        var utc = DateTime.UtcNow;

            //        _log.InfoFormat("Comparing {0} last to {1} current.", last.ToString(), utc.ToString());

            //        if (last.Date >= dt.Date)
            //        {
            //            _log.InfoFormat("snapshot for {0} already done at {1}", app.Name, last);
            //        }
            //        else if (utc >= dt)
            //        {
            //            _log.InfoFormat("doing snapshot for {0} at {1}", app.Name, utc);
            //            try
            //            {
            //            new Snapshoter(db, new DefaultSnapshotParameterProvider(db.AppConfig.Name)).Snapshot(utc);
            //            }
            //            catch (Exception e)
            //            {
            //                _log.Error("error while doing snapshot", e);
            //            }
            //        }
            //    }
            //}
        }
    }
}