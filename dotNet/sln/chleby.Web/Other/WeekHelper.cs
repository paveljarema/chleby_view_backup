﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace chleby.Web
{
    public static class WeekHelper
    {
        public static DateTime FirstDateOfWeek(int year, int weekOfYear, DayOfWeek dow)
        {
            var dt = new DateTime(year, 1, 1).AddDays((weekOfYear - 1) * 7);
            return GetStartOfWeek(dt, dow);
        }

        public static int GetWeek(this DateTime date, DayOfWeek dow = DayOfWeek.Monday)
        {
            var cal = CultureInfo.InvariantCulture.Calendar;
            return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, dow);
        }

        public static int GetMonthLen(int month, int year)
        {
            var cal = CultureInfo.InvariantCulture.Calendar;
            return cal.GetDaysInMonth(year, month);
        }

        public static string GetMonthName(int month)
        {
            return new DateTime(2000, month, 1).ToString("MMMM");
        }

        public static DateTime GetStartOfWeek(this DateTime date, DayOfWeek dow)
        {
            var mydow = (int)date.DayOfWeek - (int)dow;
            while (mydow < 0) // było if, lepiej while
                mydow += 7;
            return date.AddDays(-mydow);
        }

        public static string GetShortDayName(this int wd)
        {
            wd++;
            while (wd >= 7) wd -= 7; //było if, lepiej while
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedDayName((DayOfWeek) wd);
        }

        public static string GetLongDayName(this int wd)
        {
            wd++;
            while (wd >= 7) wd -= 7; //było if, lepiej while. w sumie widziałem w bazie 8 dzień tygodnia
            return CultureInfo.CurrentCulture.DateTimeFormat.GetDayName((DayOfWeek)wd);
        }

        public static string GetLongDayName(this int wd, CultureInfo c)
        {
            wd++;
            while (wd >= 7) wd -= 7; //było if, lepiej while
            return c.DateTimeFormat.GetDayName((DayOfWeek)wd);
        }
    }
}