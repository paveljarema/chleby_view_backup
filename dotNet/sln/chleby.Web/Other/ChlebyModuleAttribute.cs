﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace chleby.Web
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ChlebyModuleAttribute : ChlebyAuthorizeAttribute
    {
        //public string Category { get; set; }
        //public bool IsCore { get; set; }
        public Type Parent { get; set; }

        public ChlebyModuleAttribute(string name)
        {
            Name = name;
        }

        public static ChlebyModuleAttribute GetByControllerName(string typename)
        {
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Name.ToLower() == typename.ToLower()).ToList();
            if (types.Count == 1)
            {
                return types[0].MyGetCustomAttribute<ChlebyModuleAttribute>(true);
            }
            return null;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class HasChildAttribute : Attribute
    {
        public Type ChildType { get; set; }

        public HasChildAttribute(Type childType)
        {
            ChildType = childType;
        }
    }
}