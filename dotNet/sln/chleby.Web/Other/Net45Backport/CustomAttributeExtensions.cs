﻿// Type: System.Reflection.CustomAttributeExtensions
// Assembly: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
// Assembly location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll

using System;
using System.Collections.Generic;

namespace System.Reflection
{
    /// <summary>
    /// Contains static methods for retrieving custom attributes.
    /// </summary>
    public static class CustomAttributeExtensions
    {
        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static Attribute MyGetCustomAttribute(this Assembly element, Type attributeType)
        {
            return Attribute.GetCustomAttribute(element, attributeType);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The module to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static Attribute MyGetCustomAttribute(this Module element, Type attributeType)
        {
            return Attribute.GetCustomAttribute(element, attributeType);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static Attribute MyGetCustomAttribute(this MemberInfo element, Type attributeType)
        {
            return Attribute.GetCustomAttribute(element, attributeType);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static Attribute MyGetCustomAttribute(this ParameterInfo element, Type attributeType)
        {
            return Attribute.GetCustomAttribute(element, attributeType);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this Assembly element) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The module to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this Module element) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The member to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this MemberInfo element) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this ParameterInfo element) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified member, and optionally inspects the ancestors of that member.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static Attribute MyGetCustomAttribute(this MemberInfo element, Type attributeType, bool inherit)
        {
            return Attribute.GetCustomAttribute(element, attributeType, inherit);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified parameter, and optionally inspects the ancestors of that parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute matching <paramref name="attributeType"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static Attribute MyGetCustomAttribute(this ParameterInfo element, Type attributeType, bool inherit)
        {
            return Attribute.GetCustomAttribute(element, attributeType, inherit);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified member, and optionally inspects the ancestors of that member.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this MemberInfo element, bool inherit) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T), inherit);
        }

        /// <summary>
        /// Retrieves a custom attribute of a specified type that is applied to a specified parameter, and optionally inspects the ancestors of that parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A custom attribute that matches <paramref name="T"/>, or null if no such attribute is found.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static T MyGetCustomAttribute<T>(this ParameterInfo element, bool inherit) where T : Attribute
        {
            return (T)CustomAttributeExtensions.MyGetCustomAttribute(element, typeof(T), inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this Assembly element)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The module to inspect.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this Module element)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this MemberInfo element)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this ParameterInfo element)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified member, and optionally inspects the ancestors of that member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> that match the specified criteria, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this MemberInfo element, bool inherit)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes that are applied to a specified parameter, and optionally inspects the ancestors of that parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this ParameterInfo element, bool inherit)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this Assembly element, Type attributeType)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The module to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this Module element, Type attributeType)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this MemberInfo element, Type attributeType)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this ParameterInfo element, Type attributeType)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this Assembly element) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The module to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this Module element) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this MemberInfo element) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this ParameterInfo element) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T));
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified member, and optionally inspects the ancestors of that member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this MemberInfo element, Type attributeType, bool inherit)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType, inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified parameter, and optionally inspects the ancestors of that parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="attributeType"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static IEnumerable<Attribute> MyGetCustomAttributes(this ParameterInfo element, Type attributeType, bool inherit)
        {
            return (IEnumerable<Attribute>)Attribute.GetCustomAttributes(element, attributeType, inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified member, and optionally inspects the ancestors of that member.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this MemberInfo element, bool inherit) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T), inherit);
        }

        /// <summary>
        /// Retrieves a collection of custom attributes of a specified type that are applied to a specified parameter, and optionally inspects the ancestors of that parameter.
        /// </summary>
        /// 
        /// <returns>
        /// A collection of the custom attributes that are applied to <paramref name="element"/> and that match <paramref name="T"/>, or an empty collection if no such attributes exist.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param><typeparam name="T">The type of attribute to search for.</typeparam>

        public static IEnumerable<T> MyGetCustomAttributes<T>(this ParameterInfo element, bool inherit) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.MyGetCustomAttributes(element, typeof(T), inherit);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified assembly.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The assembly to inspect.</param><param name="attributeType">The type of the attribute to search for.</param>

        public static bool IsDefined(this Assembly element, Type attributeType)
        {
            return Attribute.IsDefined(element, attributeType);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified module.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The module to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static bool IsDefined(this Module element, Type attributeType)
        {
            return Attribute.IsDefined(element, attributeType);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified member.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static bool IsDefined(this MemberInfo element, Type attributeType)
        {
            return Attribute.IsDefined(element, attributeType);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified parameter.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param>

        public static bool IsDefined(this ParameterInfo element, Type attributeType)
        {
            return Attribute.IsDefined(element, attributeType);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified member, and, optionally, applied to its ancestors.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The member to inspect.</param><param name="attributeType">The type of the attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>
        public static bool IsDefined(this MemberInfo element, Type attributeType, bool inherit)
        {
            return Attribute.IsDefined(element, attributeType, inherit);
        }

        /// <summary>
        /// Indicates whether custom attributes of a specified type are applied to a specified parameter, and, optionally, applied to its ancestors.
        /// </summary>
        /// 
        /// <returns>
        /// true if an attribute of the specified type is applied to <paramref name="element"/>; otherwise, false.
        /// </returns>
        /// <param name="element">The parameter to inspect.</param><param name="attributeType">The type of attribute to search for.</param><param name="inherit">true to inspect the ancestors of <paramref name="element"/>; otherwise, false. </param>

        public static bool IsDefined(this ParameterInfo element, Type attributeType, bool inherit)
        {
            return Attribute.IsDefined(element, attributeType, inherit);
        }
    }
}
