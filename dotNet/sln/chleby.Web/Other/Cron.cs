﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace chleby.Web
{
    public class Cron
    {
        public List<Job> Jobs { get; private set; }

        public Cron()
        {
            Jobs = new List<Job>();
        }

        public void ExecuteJobs()
        {
            var queue = new Queue<Job>();
            foreach (var job in Jobs)
            {
                if (job.WillExecute())
                    queue.Enqueue(job);
            }

            if (queue.Count == 0) return;

            lock (typeof(Cron))
            {
                while (queue.Count > 0)
                {
                    queue.Dequeue().Execute();
                }
            }
        }

        public abstract class Job
        {
            private static readonly ILog _log = LogManager.GetLogger(typeof(Job));
            public string Name { get; private set; }
            public Action Action { get; private set; }
            public abstract bool WillExecute();

            protected DateTime LastExecuted { get; private set; }
            public void Execute()
            {
                _log.InfoFormat("executing job {0} => {1}", Name, Action.Method);
                Action();
                LastExecuted = DateTime.UtcNow;
                _log.InfoFormat("executing job {0} [{1}] @ {2}", Name, GetType().Name, DateTime.UtcNow);
            }

            protected Job(string name, Action action)
            {
                Name = name;
                Action = action;
                LastExecuted = new DateTime(1970, 1, 1);
            }
        }

        public class HourlyJob : Job
        {
            public HourlyJob(string name, Action action) : base(name, action) { }

            public override bool WillExecute()
            {
                return LastExecuted.Hour != DateTime.UtcNow.Hour;
            }
        }

        public class DailyJob : Job
        {
            public DailyJob(string name, Action action) : base(name, action) { }

            public override bool WillExecute()
            {
                return LastExecuted.Day != DateTime.UtcNow.Day;
            }
        }

        public class MinuteJob : Job
        {
            public MinuteJob(string name, Action action) : base(name, action) { }

            public override bool WillExecute()
            {
                return LastExecuted.Minute != DateTime.UtcNow.Minute;
            }
        }
    }
}