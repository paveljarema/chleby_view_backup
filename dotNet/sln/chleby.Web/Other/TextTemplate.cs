﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
using MigraDoc.DocumentObjectModel;
using chleby.Web.Models;

namespace chleby.Web
{
    /// <summary>
    /// Prosty system szablonów tekstowych, jego planowane użycie to proste szablony plain textowe, aktualnie używane w mailach, pola szablonu mają format {nazwa}
    /// </summary>
    public class TextTemplate
    {
        private readonly string _template;
        private string _filledTemplate;

        private TextTemplate(string content)
        {
            _template = content;
        }

        /// <summary>
        /// Ładuje szablon z pliku znajdującego się w katalogu z danymi serwera
        /// </summary>
        /// <param name="template">nazwa szablonu, rozszerzenie i ścieżka zostanie dodana automatycznie</param>
        /// <returns>obiekt reprezentujący szablon</returns>
        public static TextTemplate FromFile(string template)
        {
            var path = template;
            if (!Path.IsPathRooted(template))
                path = Defines.DataDirectory + "\\" + template + ".t";
            path = System.Web.Hosting.HostingEnvironment.MapPath(path);
            return new TextTemplate(File.ReadAllText(path));
        }

        /// <summary>
        /// Ładuje szablon z przekazanego stringa
        /// </summary>
        /// <param name="content">zawartość szablonu</param>
        /// <returns>obiekt reprezentujący szablon</returns>
        public static TextTemplate FromString(string content)
        {
            return new TextTemplate(content);
        }

        /// <summary>
        /// Formatuje szablon według przekazanych parametrów
        /// </summary>
        /// <param name="obj">dowolny obiekt, pola w szablonie zostaną podmienione na wartości odpowiednich właściwości tego obiektu. Format pola to {obj.prop1.prop2} lub {obj.prop1.prop2:format}</param>
        /// <returns>obiekt reprezentujący szablon</returns>
        public TextTemplate Format(dynamic obj)
        {
            _filledTemplate = ReplaceFields(_template, obj, 0, 0);
            return this;
        }

        private string ReplaceFields(string template, dynamic obj, int level, int count)
        {
            var matches = Regex.Matches(template,
@"{
   (?<selector>[^ :}\[ ]+)
   (
     (?<format>:[^}]+) | ( \[" + level + @"\[ (?<nested>.+?) \]" + level + @"\] )
   )?
}", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
            foreach (Match match in matches)
            {
                var parts = new Queue<string>(match.Groups["selector"].Value.Split('.'));
                var format = match.Groups["format"].Value;
                var nested = match.Groups["nested"].Value;
                if (format.Length > 1) format = format.Substring(1);

                var curObj = obj;
                curObj = RecurseProps(curObj, parts, level, count);

                string val = "";
                if (nested.Length > 0)
                {
                    var elCount = 0;
                    foreach (var elem in curObj)
                    {
                        val += ReplaceFields(nested, elem, level + 1, ++elCount);
                    }
                }
                else
                {   
                    try
                    {
                        val = curObj.ToString(format);
                    }
                    catch
                    {
                        val = curObj.ToString();
                    }
                }

                template = template.Replace(match.Value, val);
            }

            return template;
        }

        private object RecurseProps(dynamic obj, Queue<string> tail, int level, int count)
        {
            if (tail.Count == 0)
                return obj;
            if (tail.Count == 1) //specjane przypadki
            {
                switch (tail.Peek())
                {
                    case "$":
                        return obj;
                    case "#":
                        return count;
                    case "@":
                        return level;
                }
            }

            var props = TypeDescriptor.GetProperties(obj);
            var propName = tail.Dequeue();
            
            if (propName == "*" && obj is IEnumerable)
                return Enumerable.Count(obj);

            var prop = props[propName];

            obj = prop.GetValue(obj) ?? "";

            return RecurseProps(obj, tail, level, count);
        }

        /// <summary>
        /// Formatuje szablon według przekazanych parametrów
        /// </summary>
        /// <param name="dictionary">mapa pól na wartości, wartość może być dowolnym obiektem, przed podmianą będzie wykonana na nim metoda ToString()</param>
        /// <returns>obiekt reprezentujący szablon</returns>
        public TextTemplate Format(Dictionary<string, object> dictionary)
        {
            _filledTemplate = _template;
            foreach (var kvp in dictionary)
                _filledTemplate = _filledTemplate.Replace("{" + kvp.Key + "}",
                    kvp.Value == null ? "" : kvp.Value.ToString());

            return this;
        }

        /// <summary>
        /// Renderuje szablon
        /// </summary>
        /// <returns>string z wyrenderowanym szablonem</returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(_filledTemplate))
                return _template;
            else
                return _filledTemplate;
        }

        /// <summary>
        /// Operator konwersji na string
        /// </summary>
        /// <param name="tt">obiekt szablonu</param>
        /// <returns>string z wyrenderowanym szablonem</returns>
        public static implicit operator string(TextTemplate tt)
        {
            return tt.ToString();
        }
    }
}