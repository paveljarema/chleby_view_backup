﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace chleby.Web
{
    public struct FunctionDef
    {
        public string name;
        public int argsnum;
        public Func<double[],double> handler;
        public FunctionDef(string _name, int _argsnum)
        {
            name = _name;
            argsnum = _argsnum;
            handler = null;
        }
    }

    class Parser
    {
        public enum Ops { Empty, Add, Sub, Mul, Div, Pow, Mod };
        public enum SymbolType { Empty, Num, Op, Func, LBracket, RBracket, Comma };
        public Func<string, double> UnknownVariable;

        public struct Symbol
        {
            public SymbolType type;
            public Ops op;
            public double val;
            public string func;
            public override string ToString()
            {
                //string ret = type.ToString() + " ";
                string ret = "";
                if (type == SymbolType.Op) //ret += op.ToString();
                {
                    switch (op)
                    {
                        case Ops.Pow: ret += "^"; break;
                        case Ops.Mul: ret += "*"; break;
                        case Ops.Div: ret += "/"; break;
                        case Ops.Add: ret += "+"; break;
                        case Ops.Sub: ret += "-"; break;
                        case Ops.Mod: ret += "%"; break;
                    }
                }
                if (type == SymbolType.Num) ret += val.ToString(CultureInfo.InvariantCulture);
                if (type == SymbolType.Func) ret += func;
                return ret;
            }
        }

        readonly List<FunctionDef> _functions = new List<FunctionDef>();

        public List<FunctionDef> Functions
        {
            get { return _functions; }
        }

        public static string TryParse(string expr)
        {
            string ret = null;
            var parser = new Parser();
            parser.UnknownVariable += name => 0;
            try
            {
                var q = parser.Parse(expr);
                parser.Execute(q);
            }
            catch (Exception e)
            {
                ret = e.Message;
            }
            return ret;
        }

        public double Execute(Queue<Symbol> output)
        {
            Stack<double> stack = new Stack<double>();

            while (output.Count != 0)
            {
                Symbol s = output.Dequeue();
                switch (s.type)
                {
                    case SymbolType.Num:
                        stack.Push(s.val);
                        break;
                    case SymbolType.Op:
                        double a = stack.Pop();
                        double b = stack.Pop();
                        switch (s.op)
                        {
                            case Ops.Pow:
                                stack.Push((double)Math.Pow(b, a)); break;
                            case Ops.Mul:
                                stack.Push(b * a); break;
                            case Ops.Div:
                                stack.Push(b / a); break;
                            case Ops.Add:
                                stack.Push(b + a); break;
                            case Ops.Sub:
                                stack.Push(b - a); break;
                            case Ops.Mod:
                                stack.Push(b % a); break;
                        }
                        break;
                    case SymbolType.Func:
                        foreach (FunctionDef function in _functions)
                        {
                            if (function.name == s.func)
                            {
                                double[] args = new double[function.argsnum];
                                for (int i = 0; i < function.argsnum; ++i)
                                    args[i] = stack.Pop();
                                stack.Push(function.handler(args.Reverse().ToArray()));
                                goto breakup; //wyjdz z foreacha i switcha
                            }
                        }

                        FunctionDef vd = new FunctionDef();
                        double val = double.NaN;
                        try
                        {
                            val = UnknownVariable(s.func);
                        }
                        catch
                        {
                        }
                        vd.name = s.func;
                        vd.argsnum = 0;
                        vd.handler = doubles => val;
                        _functions.Add(vd);
                        stack.Push(val);
                    //}
                    breakup:
                        break;
                }
            }
            return stack.Pop();
        }

        public Queue<Symbol> Parse(string expr)
        {
            Queue<Symbol> output = new Queue<Symbol>();
            Stack<Symbol> opstack = new Stack<Symbol>();
            int pos = 0;
            expr = expr.Replace(" ", null);

            Symbol s;
            do
            {
                s = ReadSymbol(expr, ref pos);
                switch (s.type)
                {
                    case SymbolType.Num:
                        output.Enqueue(s);
                        break;
                    case SymbolType.Func:
                        opstack.Push(s);
                        break;
                    case SymbolType.Comma:
                        while (PeekStack(opstack).type != SymbolType.LBracket)
                        {
                            if (opstack.Count == 0) break;
                            output.Enqueue(opstack.Pop());
                        }
                        break;
                    case SymbolType.Op:
                        if (output.Count == 0 || opstack.Count > 0)
                        {
                            if (s.op == Ops.Sub)
                            {
                                opstack.Push(new Parser.Symbol() { type = SymbolType.Func, func = "neg" });
                                break;
                            }
                        }
                        while (GoodOp(s, PeekStack(opstack)))
                        {
                            if (opstack.Count == 0) break;
                            output.Enqueue(opstack.Pop());
                        }
                        opstack.Push(s);
                        break;
                    case SymbolType.LBracket:
                        opstack.Push(s);
                        break;
                    case SymbolType.RBracket:
                        while (PeekStack(opstack).type != SymbolType.LBracket)
                        {
                            if (opstack.Count == 0) break;
                            output.Enqueue(opstack.Pop());
                        }
                        if (opstack.Count > 0) opstack.Pop();
                        break;
                }
            } while (s.type != SymbolType.Empty);

            while (PeekStack(opstack).type != SymbolType.Empty)
                output.Enqueue(opstack.Pop());

            return output;
        }

        #region Helper functions
        bool GoodOp(Symbol o1, Symbol o2)
        {
            //funkcje sa git. tylko czemu?
            if (o1.type == SymbolType.Func || o2.type == SymbolType.Func) return true;

            if (o1.op != Ops.Pow) //lewolaczny
            {
                if (GetPrecedence(o1.op) <= GetPrecedence(o2.op))
                    return true;
            }
            else //prawolaczny (^)
            {
                if (GetPrecedence(o1.op) < GetPrecedence(o2.op))
                    return true;
            }
            return false;
        }

        Symbol PeekStack(Stack<Symbol> stack)
        {
            if (stack.Count == 0) return new Symbol();
            else return stack.Peek();
        }

        int GetPrecedence(Ops op)
        {
            switch (op)
            {
                case Ops.Pow:
                    return 2;
                case Ops.Mul:
                case Ops.Div:
                case Ops.Mod:
                    return 1;
                case Ops.Add:
                case Ops.Sub:
                    return 0;
                default:
                    return -1;
            }
        }

        Symbol ReadSymbol(string expr, ref int pos)
        {
            Symbol ret = new Symbol();
            string smb = "";
            char[] c_num = "1234567890.,".ToCharArray();
            char[] c_op = "+-*/^%".ToCharArray();
            char[] c_func = "abcdefghijklmnopqrstuvwxyzABCEDEFGHIJKLMNOPQRSTUVWXYZ1234567980_".ToCharArray();
            char[] c_sel = c_num;

            if (expr.Length == pos) return new Symbol();

            else if (expr[pos] == '(')
            {
                ret.type = SymbolType.LBracket;
                ++pos;
                return ret;
            }

            else if (expr[pos] == ')')
            {
                ret.type = SymbolType.RBracket;
                ++pos;
                return ret;
            }

            else if (expr[pos] == ';')
            {
                ret.type = SymbolType.Comma;
                ++pos;
                return ret;
            }

            else if (c_num.Contains(expr[pos]))
            {
                ret.type = SymbolType.Num;
                c_sel = c_num;
            }

            else if (c_op.Contains(expr[pos]))
            {
                ret.type = SymbolType.Op;
                c_sel = c_op;
            }

            else if (c_func.Contains(expr[pos]))
            {
                ret.type = SymbolType.Func;
                c_sel = c_func;
            }

            while (c_sel.Contains(expr[pos]))
            {
                smb += expr[pos];
                ++pos;
                if (pos == expr.Length) break;
            }

            if (ret.type == SymbolType.Op)
            {
                switch (smb)
                {
                    case "+": ret.op = Ops.Add; break;
                    case "-": ret.op = Ops.Sub; break;
                    case "*": ret.op = Ops.Mul; break;
                    case "/": ret.op = Ops.Div; break;
                    case "^": ret.op = Ops.Pow; break;
                    case "%": ret.op = Ops.Mod; break;
                }
            }

            if (ret.type == SymbolType.Num) ret.val += double.Parse(smb);
            if (ret.type == SymbolType.Func) ret.func = smb;

            return ret;
        }
        #endregion
    }
}