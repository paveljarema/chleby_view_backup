﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace chleby.Web
{
    public abstract class ChlebyPage<T> : WebViewPage<T>
    {
        public SettingsManager Settings
        {
            get
            {
                return (SettingsManager)ViewBag.Settings;
            }
        }

        public bool CanRead()
        {
            return User.IsInRole(ViewBag.ModuleName + Defines.Roles.ReadSuffix) || IsSuperAdmin();
        }

        public bool CanRead(string moduleName)
        {
            return User.IsInRole(moduleName + Defines.Roles.ReadSuffix) || IsSuperAdmin();
        }

        public bool CanRead(Type controllerType)
        {
            return User.IsInRole(controllerType.MyGetCustomAttribute<ChlebyModuleAttribute>().Name + Defines.Roles.ReadSuffix) || IsSuperAdmin();
        }

        public bool CanWrite()
        {
            return User.IsInRole(ViewBag.ModuleName + Defines.Roles.WriteSuffix) || IsSuperAdmin();
        }

        public bool CanWrite(string moduleName)
        {
            return User.IsInRole(moduleName + Defines.Roles.WriteSuffix) || IsSuperAdmin();
        }

        public bool CanWrite(Type controllerType)
        {
            return User.IsInRole(controllerType.MyGetCustomAttribute<ChlebyModuleAttribute>().Name + Defines.Roles.WriteSuffix) || IsSuperAdmin();
        }

        public bool IsSuperAdmin()
        {
            return User.IsInRole(Defines.Roles.SuperAdmin);
        }

        public bool IsEmployee()
        {
            return User.IsInRole(Defines.Roles.Employee);
        }

        public bool IsTrader()
        {
            return User.IsInRole(Defines.Roles.Trader);
        }
    }

    public abstract class ChlebyPage : WebViewPage
    {

    }
}