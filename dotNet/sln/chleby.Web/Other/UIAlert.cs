﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web
{
    public enum Severity { success, error, warning, info };

    public class UIAlert
    {
        public Severity Status { get; set; }
        public string Msg { get; set; }
    }
}