﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Globalization;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using chleby.Web.Models;
using System.Web.Security;
using chleby.Web.Controllers;
using chleby.Web.Resources;
using System.Data.Objects;

namespace chleby.Web
{
    public class PDFs
    {
        private readonly Business _business;

        public PDFs(Business business)
        {
            _business = business;
        }

        public Document CreateCustomerList(List<Trader> trader, int live)
        {
            var doc = new Document();

            doc.Info.Title = Strings.CustomerList;
            doc.Styles["Normal"].Font.Size = "8pt";
            var section = doc.AddSection();
            section.PageSetup.BottomMargin = "3cm";
            section.PageSetup.TopMargin = "2cm";
            section.Footers.Primary.AddParagraph(_business.InvoiceFooter ?? "").Format.Alignment = ParagraphAlignment.Center;
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);

            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            var headname = section.AddParagraph(Strings.CustomerList + " - ");
            switch (live)
            {
                case 0:
                    headname.AddText(Strings.NotLive);
                    break;
                case 1:
                    headname.AddText(Strings.Live);
                    break;
                case 2:
                    headname.AddText(Strings.Traders_Sales_Lead);
                    break;
            }
            headname.Format.Font = new Font("Verdana", "16pt");
            headname.Format.SpaceAfter = "1cm";
            headname.AddTab();
            headname.Format.AddTabStop("16cm", TabAlignment.Right);


            var table = section.AddTable();
            table.KeepTogether = true;
            table.Borders.Style = BorderStyle.Single;
            table.Borders.Color = Colors.LightGray;

            table.AddColumn("4cm", ParagraphAlignment.Left);
            table.AddColumn("3cm", ParagraphAlignment.Left);
            table.AddColumn("3cm", ParagraphAlignment.Left);
            table.AddColumn("6cm", ParagraphAlignment.Left);

            var headrow = table.AddRow();
            headrow.VerticalAlignment = VerticalAlignment.Center;
            headrow.Height = "1cm";
            headrow.Cells[1].AddParagraph(Strings.InvoiceContact);
            headrow.Cells[2].AddParagraph(Strings.Global_Phone);
            headrow.Cells[3].AddParagraph(Strings.Global_Email);
            headrow.Cells[0].AddParagraph(Strings.Company);

            int c = -1;
            Row r = null;
            foreach (var row in trader.OrderBy(x => x.Name).Where(x => x.Live == live))
            {
                c++;
                r = table.AddRow();
                var primaryAddr = row.Address.FirstOrDefault(x => x.AddressIsPrimary);
                if (primaryAddr != null)
                {
                    var invoiceContact = primaryAddr.TraderEmployees.FirstOrDefault();
                    string email = "(empty)";
                    var users = Membership.GetAllUsers().Cast<MembershipUser>().ToDictionary(user => (Guid)user.ProviderUserKey, user => user.UserName);
                    r.VerticalAlignment = VerticalAlignment.Center;
                    if (invoiceContact == null) //jesli nie ma nikogo do adresu podpietego
                        invoiceContact = new TraderEmployee { FirstName = "nobody", LastName = "nobody", Phone = "" };

                    if (users.ContainsKey(invoiceContact.ExternalAccountData))
                    {
                        email = users[invoiceContact.ExternalAccountData];
                    }

                    r.Cells[1].AddParagraph((invoiceContact.FirstName + " " + invoiceContact.LastName) ?? "");
                    r.Cells[2].AddParagraph(invoiceContact.Phone ?? "");
                    r.Cells[3].AddParagraph(email ?? "");
                }
                r.Cells[0].AddParagraph(row.Name);
                r.Shading.Color = (c % 2 == 0)
                                      ? new Color(0xee, 0xee, 0xee)
                                      : Color.FromRgbColor(255, Colors.White);
                r.Borders.Top.Visible = false;
                r.Borders.Bottom.Visible = false;
            }
            r.Borders.Bottom.Visible = true;

            return doc;
        }

        public Document CreateDeliveryNotesDoc(string date, ChlebyContext db)
        {
            var deliveryNoteDate = DateTime.UtcNow;
            if (date != null)
            {
                deliveryNoteDate = DateTime.ParseExact(date, "ddMMyyyy", CultureInfo.InvariantCulture);
            }

            var q = from pos in db.SnapshotOrderPositions
                    where EntityFunctions.TruncateTime(pos.EndDate) == deliveryNoteDate.Date
                    && (pos.OrderedCount > 0 || pos.Adhoc)
                    group pos by pos.Address.AddressGroup
                        into byround
                        orderby byround.Key.Order
                        select new
                        {
                            Round = byround.Key,
                            Addresses = from adr in byround
                                        group adr by adr.Address into byaddr
                                        orderby byaddr.Key.RoundOrder
                                        select new
                                        {
                                            Address = byaddr.Key,
                                            Items = from it in byaddr
                                                    group it by it.Item into byitem
                                                    select byitem
                                        }

                        };
      
            var deliveryId = db.Entities.First(x => x.Name == "Delivery").Id;

            // to local
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(_business.Timezone);
            var localDate = TimeZoneInfo.ConvertTimeFromUtc(deliveryNoteDate, timeZoneInfo);

            var doc = new Document();
            doc.Info.Title = "";
            doc.Styles["Normal"].Font.Size = "8pt";
            doc.DefaultPageSetup.PageFormat = PageFormat.A4;
            doc.DefaultPageSetup.Orientation = Orientation.Portrait;
            doc.DefaultPageSetup.BottomMargin = "1cm";
            doc.DefaultPageSetup.TopMargin = "1cm";
            doc.DefaultPageSetup.LeftMargin = "1cm";
            doc.DefaultPageSetup.RightMargin = "1cm";

            var section = doc.AddSection();

            foreach (var round in q.ToList())
            {
                string roundName = (round.Round != null) ? round.Round.Name : "no round";
                //  var headname = section.AddParagraph(roundName);
                foreach (var addr in round.Addresses)
                {
                    var table = section.AddTable();
                    table.TopPadding = "0cm";
                    table.BottomPadding = "0cm";
                    table.Rows.Height = "1cm";
                    table.Rows.HeightRule = RowHeightRule.Auto;
                    table.KeepTogether = true;
                    table.Borders.Style = BorderStyle.Single;
                    table.Borders.Color = Colors.LightGray;

                    table.AddColumn("4.75cm", ParagraphAlignment.Left);
                    table.AddColumn("4.75cm", ParagraphAlignment.Left);
                    table.AddColumn("9.5cm", ParagraphAlignment.Left);

                    var row = table.AddRow();
                    row.Height = "2cm";
                    row.HeightRule = RowHeightRule.Exactly;
                    row.Format.Font.Size = "36pt";
                    row[0].MergeRight = 1;
                    row[0].AddParagraph("Delivery Note");
                    row[2].Format.Alignment = ParagraphAlignment.Right;
                    var imgpar = row[2].AddParagraph();
                    imgpar.Format.Alignment = ParagraphAlignment.Right;
                    var path = HttpContext.Current.Server.MapPath("~/img/" + _business.Logo);
                    if (File.Exists(path))
                    {
                        var imgLogo = new Image(path);
                        imgpar.Add(imgLogo);
                    }

                    row = table.AddRow();
                    row.Height = "2cm";
                    row.Format.Font.Size = "20pt";
                    row.HeightRule = RowHeightRule.Exactly;
                    row[0].AddParagraph(localDate.ToString(_business.DateFormat));
                    row[1].AddParagraph(roundName);

                    var itemsCell = row[2];
                    itemsCell.MergeDown = 2;
                    itemsCell.Format.Font.Size = "12pt";
                    itemsCell.Format.LeftIndent = "3mm";

                    row = table.AddRow();
                    row.Height = "2cm";
                    row.Format.Font.Size = "20pt";
                    row.HeightRule = RowHeightRule.Exactly;
                    row[0].AddParagraph(addr.Address.Trader.Name);
                    row[1].AddParagraph(addr.Address.Post);

                    var noteRow = table.AddRow();
                    noteRow.Height = "6.7cm";
                    noteRow.Format.Font.Size = "12pt";
                    noteRow.HeightRule = RowHeightRule.Exactly;
                    noteRow[0].MergeRight = 1;
                    if (!string.IsNullOrWhiteSpace(addr.Address.SpecialNote))
                        noteRow[0].AddParagraph(addr.Address.SpecialNote);

                    var errs = new List<Tuple<string, int, int>>();

                    foreach (var order in addr.Items)
                    {
                        if (order.Key.Id != deliveryId)
                        {

                            var ir = itemsCell.AddParagraph();
                            ir.Format.AddTabStop("1cm");
                            var ordered = order.Sum(x => x.OrderedCount);
                            var made = order.Sum(x => x.MadeCount);

                            if (ordered != made)
                            {
                                errs.Add(Tuple.Create(order.Key.Name, ordered, made));
                            }

                            ir.AddFormattedText(made.ToString());
                            ir.AddTab();
                            ir.AddFormattedText(order.Key.Name);
                        }
                    }

                    if (errs.Any(x => x.Item2 > x.Item3))
                    {
                        var errRow = noteRow[0].AddParagraph();
                        errRow.AddText("Items unavailable:");
                        foreach (var err in errs.Where(x => x.Item2 > x.Item3))
                        {
                            errRow = noteRow[0].AddParagraph();
                            errRow.AddText(err.Item1 + " ");
                            errRow.AddText(err.Item2.ToString());
                            errRow.AddText(" » ");
                            errRow.AddText(err.Item3.ToString());
                        }
                    }
                    if (errs.Any(x => x.Item2 < x.Item3))
                    {
                        var errRow = noteRow[0].AddParagraph();
                        errRow.AddText("Extra items:");
                        foreach (var err in errs.Where(x => x.Item2 < x.Item3))
                        {
                            errRow = noteRow[0].AddParagraph();
                            errRow.AddText(err.Item1 + " ");
                            errRow.AddText(err.Item2.ToString());
                            errRow.AddText(" » ");
                            errRow.AddText(err.Item3.ToString());
                        }
                    }

                    var margin = table.AddRow();
                    margin.Borders.Visible = false;
                    margin.HeightRule = RowHeightRule.Exactly;
                    margin.Height = "1.5cm";
                }
            }

            return doc;
        }

        public Document CreateAddressGroupDoc(AddressGroup round)
        {
            var doc = new Document();

            doc.Info.Title = round.Name;
            doc.Styles["Normal"].Font.Size = "8pt";

            var section = doc.AddSection();
            section.PageSetup.BottomMargin = "3cm";
            section.PageSetup.TopMargin = "2cm";
            section.Footers.Primary.AddParagraph(_business.InvoiceFooter ?? "").Format.Alignment = ParagraphAlignment.Center;
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);


            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            var headname = section.AddParagraph(Strings.Global_Round + ": " + round.Name);
            headname.Format.Font = new Font("Verdana", "16pt");
            headname.Format.SpaceAfter = "1cm";
            headname.AddTab();
            headname.Format.AddTabStop("20cm", TabAlignment.Right);

            var table = section.AddTable();
            table.KeepTogether = true;
            table.Borders.Style = BorderStyle.Single;
            table.Borders.Color = Colors.LightGray;

            table.AddColumn("4cm", ParagraphAlignment.Left);
            table.AddColumn("3cm", ParagraphAlignment.Left);
            table.AddColumn("3cm", ParagraphAlignment.Left);
            table.AddColumn("5cm", ParagraphAlignment.Left);

            var headrow = table.AddRow();
            headrow.VerticalAlignment = VerticalAlignment.Center;
            headrow.Height = "1cm";
            headrow.Cells[1].AddParagraph(Strings.Global_Postcode);
            headrow.Cells[2].AddParagraph(Strings.Global_Address);
            headrow.Cells[3].AddParagraph(Strings.Note);
            headrow.Cells[0].AddParagraph(Strings.Company);

            int c = 0;
            foreach (var row in round.Addresses.OrderBy(x => x.RoundOrder).Where(x => x.Trader.Live > 0))
            {
                c++;
                var r = table.AddRow();

                r.VerticalAlignment = VerticalAlignment.Center;

                r.Cells[1].Format.Font.Size = "14pt";
                r.Cells[1].AddParagraph(row.Post);
                r.Cells[2].AddParagraph(row.Address1 ?? "");
                r.Cells[2].AddParagraph(row.Address2 ?? "");
                // r.Cells[1].AddParagraph(row.Address3 ?? "");              
                r.Cells[3].AddParagraph(row.SpecialNote ?? "");
                r.Cells[0].AddParagraph(row.Trader.Name);
                r.Shading.Color = (c % 2 == 0)
                                      ? new Color(0xee, 0xee, 0xee)
                                      : Color.FromRgbColor(255, Colors.White);
                r.Borders.Top.Visible = false;
                r.Borders.Bottom.Visible = false;
            }
            var bottomrow = table.AddRow();
            bottomrow.Height = "0cm";
            bottomrow.Borders.Top.Visible = true;

            return doc;
        }

        public Document CreateInvoiceDoc(Invoice invoice, ChlebyContext db)
        {
            var doc = new Document();

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(_business.Timezone);
            DateTime invoiceDateLocal = TimeZoneInfo.ConvertTimeFromUtc(invoice.Date, timeZoneInfo);
            DateTime startDateLocal = TimeZoneInfo.ConvertTimeFromUtc(invoice.StartDate, timeZoneInfo);
            DateTime endDateLocal = TimeZoneInfo.ConvertTimeFromUtc(invoice.EndDate, timeZoneInfo);

            var dailyMode = invoice.StartDate == invoice.EndDate;

            doc.Info.Title = "Invoice " + invoice.InvoiceNumber;
            doc.Styles["Normal"].Font.Size = "8pt";

            var section = doc.AddSection();
            section.Footers.Primary.AddParagraph(_business.InvoiceFooter ?? "").Format.Alignment = ParagraphAlignment.Center;
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);
            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            {
                var headname = section.AddParagraph();
                headname.Format.Font = new Font("Verdana", "16pt");
                if (_business.LogoOnInvoice)
                {
                    var path = HttpContext.Current.Server.MapPath("~/img/" + _business.Logo);
                    if (File.Exists(path))
                    {
                        var imgLogo = new Image(path);
                        headname.Add(imgLogo);
                    }
                }
                headname.Format.SpaceAfter = "1cm";
                headname.AddTab();
                headname.Format.AddTabStop("16cm", TabAlignment.Right);
                headname.AddFormattedText(_business.UsingTradingProps ? _business.TradingName : _business.LegalName,
                    new Font("Verdana", "24pt"));

                //-------------------INVOICE HEADER INFO -----------------//

                var invoiceinfo = section.AddParagraph();
                invoiceinfo.Format.AddTabStop("5cm");
                invoiceinfo.Format.AddTabStop("16cm", TabAlignment.Right);
                invoiceinfo.AddFormattedText(Strings.PDFs_Invoice_to, TextFormat.Bold); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(Strings.PDFs_Delivered_to, TextFormat.Bold); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(Strings.PDFs_Date + invoiceDateLocal.ToShortDateString(), TextFormat.Bold);
                invoiceinfo.AddLineBreak();

                invoiceinfo.AddFormattedText(invoice.Trader.Name); invoiceinfo.AddTab();
                var emp = invoice.DeliveryAddress.TraderEmployees.FirstOrDefault();
                if (emp != null)
                {
                    invoiceinfo.AddFormattedText(emp.FirstName + " " + emp.LastName); 
                }
                invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(Strings.PDFs_Terms + " " + invoice.PaymentTerm, TextFormat.Bold);
                invoiceinfo.AddLineBreak();

                invoiceinfo.AddFormattedText(invoice.Address.Address1); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(invoice.DeliveryAddress.Address1); invoiceinfo.AddTab();
                if (!dailyMode)
                    invoiceinfo.AddFormattedText(string.Format("{0} - {1}", startDateLocal.ToString(_business.DateFormat), endDateLocal.ToString(_business.DateFormat)));
                else
                    invoiceinfo.AddFormattedText(string.Format(Strings.PDFs_for + " {0}", startDateLocal.ToString(_business.DateFormat)));
                invoiceinfo.AddLineBreak();

                invoiceinfo.AddFormattedText(invoice.Address.Address2 ?? ""); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(invoice.DeliveryAddress.Address2 ?? ""); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(Strings.PDFs_CreateInvoiceDoc_Number, TextFormat.Bold);
                invoiceinfo.AddFormattedText(" " + invoice.InvoiceNumber);
                invoiceinfo.AddLineBreak();

                invoiceinfo.AddFormattedText(invoice.Address.Address3 ?? ""); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(invoice.DeliveryAddress.Address3 ?? "");
                invoiceinfo.AddLineBreak();

                invoiceinfo.AddFormattedText(invoice.Address.City + " " + invoice.Address.Post); invoiceinfo.AddTab();
                invoiceinfo.AddFormattedText(invoice.DeliveryAddress.City + " " + invoice.DeliveryAddress.Post);
                invoiceinfo.AddLineBreak();

                //------------------------ ITEM TABLE --------------------//

                section.AddParagraph().Format.SpaceBefore = "0.5cm";

                var table = section.AddTable();
                table.KeepTogether = true;
                table.Borders.Style = BorderStyle.Single;
                table.Borders.Color = Colors.LightGray;

                table.AddColumn("6.5cm", ParagraphAlignment.Left);
                table.AddColumn("2cm", ParagraphAlignment.Right);
                table.AddColumn("2cm", ParagraphAlignment.Right);
                table.AddColumn("2cm", ParagraphAlignment.Right);
                table.AddColumn("2cm", ParagraphAlignment.Right);
                table.AddColumn("2cm", ParagraphAlignment.Right);

                var headrow = table.AddRow();
                headrow.VerticalAlignment = VerticalAlignment.Center;
                headrow.Height = "1cm";
                headrow.Cells[0].AddParagraph(Strings.Global_Item);
                headrow.Cells[1].AddParagraph(Strings.Global_Units);
                headrow.Cells[2].AddParagraph(Strings.Global_Price);
                headrow.Cells[3].AddParagraph(Strings.Global_Subtotal);
                headrow.Cells[4].AddParagraph(Strings.Global_Vat);
                headrow.Cells[5].AddParagraph(Strings.Global_Total);

                int c = 0;
                //DELIVERYCHUJ TODO: fix nazwy
                foreach (var row in invoice.Rows.Where(x => x.Text != "Delivery"))
                {
                    if (row.Count == 0) continue;
                    c++;
                    var r = table.AddRow();
                    var textpar = r.Cells[0].AddParagraph(row.Text);
                    if (!string.IsNullOrWhiteSpace(row.Info))
                        textpar.AddFormattedText("  (" + row.Info + ")", TextFormat.Italic);
                    r.Cells[1].AddParagraph(row.Count.ToString("0.##"));
                    r.Cells[2].AddParagraph((row.Price / row.Count).ToString("C2"));
                    r.Cells[3].AddParagraph(row.Price.ToString("C2"));
                    r.Cells[4].AddParagraph(row.Vat == 0 ? "" : row.Vat.ToString("C2"));
                    r.Cells[5].AddParagraph((row.Price + row.Vat).ToString("C2"));
                    r.Shading.Color = (c % 2 == 0)
                                          ? new Color(0xee, 0xee, 0xee)
                                          : Color.FromRgbColor(255, Colors.White);
                    r.Borders.Top.Visible = false;
                    r.Borders.Bottom.Visible = false;
                }

                var delrow = table.AddRow();
                delrow.Format.SpaceBefore = "0.0cm";
                delrow.Format.Font.Bold = true;
                delrow.Borders.Top.Visible = true;
                delrow.Cells[0].AddParagraph(Strings.PDFs_Item_SubTotal);
                delrow.Cells[3].AddParagraph(invoice.ItemsPrice.ToString("C2"));
                delrow.Cells[4].AddParagraph(invoice.Vat.ToString("C2"));
                delrow.Cells[5].AddParagraph(invoice.TotalPrice.ToString("C2"));

                delrow = table.AddRow();
                //DELIVERYCHUJ
                delrow.Cells[0].AddParagraph(Strings.Global_Delivery);
                delrow.Cells[1].AddParagraph(invoice.DeliveryCount.ToString("0.##"));
                delrow.Cells[3].AddParagraph((invoice.DeliveryPrice).ToString("C2"));
                delrow.Cells[4].AddParagraph(invoice.DeliveryVat == 0 ? "" : invoice.DeliveryVat.ToString("C2"));
                delrow.Cells[5].AddParagraph((invoice.DeliveryPrice + invoice.DeliveryVat).ToString("C2"));

                delrow = table.AddRow();
                delrow.Height = "1cm";
                delrow.VerticalAlignment = VerticalAlignment.Center;
                delrow.Format.Font.Bold = true;
                delrow.Cells[0].AddParagraph(Strings.Global_Total);
                delrow.Cells[3].AddParagraph((invoice.ItemsPrice + invoice.DeliveryPrice).ToString("C2"));
                delrow.Cells[4].AddParagraph((invoice.Vat + invoice.DeliveryVat).ToString("C2"));
                delrow.Cells[5].AddParagraph((invoice.TotalPrice + invoice.DeliveryPrice + invoice.DeliveryVat).ToString("C2"));

                //--------------------------------------DELIVERY STATEMENT ---------------------------------//
                if (!dailyMode)
                {
                    var daycounts = new Dictionary<int, Dictionary<DateTime, decimal>>();
                    DateTime? firstDate = null;
                    foreach (var row in invoice.Rows)
                    {
                        daycounts.Add(row.Id, new Dictionary<DateTime, decimal>());
                        row.DayData.ToList().ForEach(x =>
                            {
                                if (daycounts[row.Id].ContainsKey(x.Date))
                                    daycounts[row.Id][x.Date] += x.Count;
                                else
                                    daycounts[row.Id].Add(x.Date, x.Count);

                                if (firstDate.HasValue) return;

                                //compute week beginning
                                firstDate = x.Date;
                                firstDate =
                                    firstDate.Value.GetStartOfWeek(
                                        (DayOfWeek)
                                        MvcApplication.Settings[db.AppConfig.Name]["Invoice"].Get<int>("FirstDay"));
                            });
                    }
                    if (!daycounts.Any())
                    {
                        section.AddParagraph("invoice contains no data");
                        return doc;
                    }

                    var lastDate = firstDate;
                    if (daycounts.Values.Where(x => x.Values.Count > 0).Any())
                    {
                        lastDate = daycounts.Values.Max(x => x.Keys.Max());
                    }

                    if (firstDate.HasValue)
                    {
                        section.AddParagraph().Format.SpaceAfter = "0.5cm";
                        table = new Table();
                        table.KeepTogether = true;
                        table.Borders.Style = BorderStyle.Single;
                        table.Borders.Color = Colors.LightGray;
                        table.AddColumn("6.5cm", ParagraphAlignment.Left);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn(new Unit(4f / 7 * 2, UnitType.Centimeter), ParagraphAlignment.Right);
                        table.AddColumn("2cm", ParagraphAlignment.Right);

                        headrow = table.AddRow();
                        headrow.Cells[0].AddParagraph("Delivery statement").Format.Font = new Font("Verdana", "14pt");
                        headrow.Cells[0].MergeRight = 8;

                        var rr = table.AddRow();
                        for (int i = 0; i < 7; ++i)
                            rr.Cells[i + 1].AddParagraph(firstDate.Value.AddDays(i).ToString("ddd"));

                        while (firstDate <= lastDate)
                        {
                            var daysrow = table.AddRow();
                            daysrow.Height = "1cm";
                            daysrow.VerticalAlignment = VerticalAlignment.Center;
                            daysrow[0].AddParagraph(Strings.Global_Item);
                            daysrow[8].AddParagraph(Strings.Global_Total);
                            //FILLED LATER

                            if (firstDate.HasValue)
                            {
                                for (int i = 0; i < 7; ++i)
                                {
                                    daysrow[i + 1].AddParagraph(firstDate.Value.AddDays(i).ToString(_business.ShortDateFormat));
                                }

                                foreach (var row in invoice.Rows)
                                {
                                    if (row.Count == 0) continue;
                                    c++;
                                    var r = table.AddRow();
                                    var textpar = r.Cells[0].AddParagraph(row.Text);
                                    if (!string.IsNullOrWhiteSpace(row.Info))
                                        textpar.AddFormattedText("  (" + row.Info + ")", TextFormat.Italic);
                                    var weeksum = 0m;
                                    for (int i = 0; i < 7; ++i)
                                    {
                                        var count = daycounts[row.Id].
                                            Where(kvp => kvp.Key.Date == firstDate.Value.AddDays(i).Date).Select(
                                                kvp => kvp.Value).
                                            FirstOrDefault();
                                        weeksum += count;
                                        if (count != 0)
                                            r.Cells[i + 1].AddParagraph(count.ToString("0.##"));
                                    }
                                    r.Cells[8].AddParagraph(weeksum.ToString("0.##"));
                                    r.Shading.Color = (c % 2 == 0)
                                                          ? new Color(0xee, 0xee, 0xee)
                                                          : Color.FromRgbColor(255, Colors.White);
                                }

                            }
                            firstDate = firstDate.Value.AddDays(7);
                        }
                        section.Add(table);
                    }
                }

                var pi = section.AddParagraph(_business.PaymentInfo ?? "");
                pi.Format.Alignment = ParagraphAlignment.Center;
                pi.Format.SpaceBefore = "0.5cm";
                pi.Format.Font = new Font("Verdana", "12pt");
                pi.Format.Font.Bold = true;
            }
            return doc;
        }


        public Document SalesReportSnapshot(ChlebyContext db, DateTime start, DateTime end, char invoicePeriod, DayOfWeek dow)
        {
            var summary = InvoicesController.GetSnapshotSummary(db, start, end).OrderBy(x => x.Address.Trader.Name);

            return SalesReportPDF(summary, start, end, invoicePeriod, dow);
        }

        public Document SalesReportInvoice(ChlebyContext db, DateTime start, DateTime end, char invoicePeriod, DayOfWeek dow, bool? published, string order)
        {
            var summary = GetSalesReportInvoiceData(db, start, end, published, order);

            return SalesReportPDF(summary, start, end, invoicePeriod, dow);
        }

        public static List<InvoiceWeekData> GetSalesReportInvoiceData(ChlebyContext db, DateTime start, DateTime end, bool? published, string order)
        {
            var hasValue = published.HasValue;
            var invoices = db.Invoices.
                              Where(x => x.Date >= start && x.Date <= end);
            var summary = new List<InvoiceWeekData>(
                from invoice in invoices
                where (!hasValue || invoice.Published == published) && !invoice.MyClones.Any()
                orderby invoice.Trader.PrimaryName //TODO: legalname?
                group invoice by invoice.DeliveryAddress
                    into byaddr
                    select new InvoiceWeekData
                        {
                            Address = byaddr.Key,
                            DeliveryPrice = byaddr.Sum(y => y.DeliveryPrice),
                            ItemPrice = byaddr.Sum(y => y.ItemsPrice),
                            VatPrice = byaddr.Sum(y => y.Vat + y.DeliveryVat),
                        });
            if (order != null)
            {
                var orderdata = order.Split('-');
                if (orderdata.Length == 2)
                {
                    var dir = orderdata[1] == "asc" ? 1 : -1;
                    switch (orderdata[0])
                    {
                        case "9":
                            summary.Sort((x, y) => dir * string.Compare(x.Address.Trader.Name, y.Address.Trader.Name, StringComparison.InvariantCultureIgnoreCase));
                            break;
                        case "10":
                            summary.Sort((x, y) => dir * string.Compare(x.Address.Address1, y.Address.Address1, StringComparison.InvariantCultureIgnoreCase));
                            break;
                        case "11":
                            summary.Sort((x, y) => dir * x.ItemPrice.CompareTo(y.ItemPrice));
                            break;
                        case "12":
                            summary.Sort((x, y) => dir * x.DeliveryPrice.CompareTo(y.DeliveryPrice));
                            break;
                        case "13":
                            summary.Sort((x, y) => dir * x.VatPrice.CompareTo(y.VatPrice));
                            break;
                        case "14":
                            summary.Sort((x, y) => dir * (x.DeliveryPrice + x.VatPrice + x.ItemPrice).CompareTo(y.DeliveryPrice + y.VatPrice + y.ItemPrice));
                            break;
                    }
                }
            }
            else
                summary.Sort((x, y) => string.Compare(x.Address.Trader.Name, y.Address.Trader.Name, StringComparison.InvariantCultureIgnoreCase));
            return summary;
        }

        public Document SalesItemsSnapshot(ChlebyContext db, DateTime start, DateTime end, char invoicePeriod, DayOfWeek dow)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(_business.Timezone);
            var startLocal = TimeZoneInfo.ConvertTimeFromUtc(start, timeZoneInfo);
            var endLocal = TimeZoneInfo.ConvertTimeFromUtc(end, timeZoneInfo);

            var doc = new Document();
            var titleP1 = Strings.PDF_Sales_Item_Raport;
            string titleP2 = "";
            string titleP3 = "";
            switch (invoicePeriod)
            {
                case 'a':
                case 'u':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", startLocal, endLocal);
                    break;
                case 'd':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "}", startLocal);
                    break;
                case 'w':
                    titleP2 = string.Format(Strings.Global_Week + " {0}/{1:yyyy}", startLocal.GetWeek(dow), startLocal);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", startLocal, endLocal);
                    break;
                case 'm':
                    titleP2 = string.Format(Strings.Global_Month + " {0:" + _business.ShortDateFormat +"}", start);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", startLocal, endLocal);
                    break;
            }
            doc.Info.Title = titleP1 + " - " + titleP2;
            doc.Styles["Normal"].Font.Size = "8pt";

            var section = doc.AddSection();
            section.PageSetup.TopMargin = "1cm";
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);
            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            var par = section.AddParagraph();
            par.Format.SpaceAfter = "1cm";
            par.Format.AddTabStop("8cm");//, TabAlignment.Right);
            par.AddFormattedText(titleP1, new Font("Verdana", "20pt"));
            par.AddTab();
            par.AddFormattedText(titleP2, new Font("Verdana", "16pt"));
            if (titleP3 != "")
            {
                par.Format.SpaceAfter = "0cm";
                par = section.AddParagraph();
                par.Format.SpaceAfter = "1cm";
                par.Format.AddTabStop("8cm");
                par.AddTab();
                par.AddFormattedText(titleP3, new Font("Verdana", "12pt"));
            }

            var table = section.AddTable();
            table.KeepTogether = true;
            table.Borders.Style = BorderStyle.Single;
            table.Borders.Color = Colors.LightGray;

            table.AddColumn("8cm", ParagraphAlignment.Left);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);

            var headrow = table.AddRow();
            headrow.VerticalAlignment = VerticalAlignment.Center;
            headrow.Height = "1cm";
            headrow.Cells[0].AddParagraph(Strings.Global_Name);
            headrow.Cells[1].AddParagraph(Strings.Global_Qty);
            headrow.Cells[2].AddParagraph(Strings.Global_Items);
            headrow.Cells[3].AddParagraph(Strings.Global_Vat);
            headrow.Cells[4].AddParagraph(Strings.Global_Total);

            var summary = from pos in db.SnapshotOrderPositions
                          where pos.OrderedCount > 0 && pos.Item is Item
                            && EntityFunctions.TruncateTime(pos.EndDate) >= EntityFunctions.TruncateTime(start)
                            && EntityFunctions.TruncateTime(pos.EndDate) <= EntityFunctions.TruncateTime(end)
                          group pos by pos.Item
                              into byitem
                              select new
                                  {
                                      item = byitem.Key,
                                      qty = byitem.Sum(x => x.MadeCount),
                                      price = byitem.Sum(x => x.Price * x.MadeCount),
                                  };

            var netto = 0m;
            var vat = 0m;
            foreach (var it in summary)
            {
                var row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph(it.item.Name);
                row.Cells[1].AddParagraph(it.qty.ToString("0.##"));
                row.Cells[2].AddParagraph(it.price.ToString("C2"));
                row.Cells[3].AddParagraph((it.price * (it.item as Item).Vat / 100).ToString("C2"));
                row.Cells[4].AddParagraph((it.price * (1 + (it.item as Item).Vat / 100)).ToString("C2"));
                netto += it.price;
                vat += it.price * (it.item as Item).Vat / 100;
            }

            var totalrow = table.AddRow();
            totalrow.Height = "1cm";
            totalrow.VerticalAlignment = VerticalAlignment.Center;
            totalrow.Cells[0].AddParagraph(Strings.Global_Total).Format.Font.Bold = true;
            totalrow.Cells[2].AddParagraph(netto.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[3].AddParagraph(vat.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[4].AddParagraph((netto + vat).ToString("C2")).Format.Font.Bold = true;

            return doc;
        }

        public Document SalesItemsInvoice(ChlebyContext db, DateTime start, DateTime end, char invoicePeriod, DayOfWeek dow, bool? published)
        {
            var doc = new Document();
            var titleP1 = Strings.PDF_Sales_Item_Raport;
            string titleP2 = "";
            string titleP3 = "";
            switch (invoicePeriod)
            {
                case 'a':
                case 'u':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", start, end);
                    break;
                case 'd':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "}", start);
                    break;
                case 'w':
                    titleP2 = string.Format(Strings.Global_Week + " {0}/{1:yyyy}", start.GetWeek(dow), start);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {" + _business.DateFormat + ":d}", start, end);
                    break;
                case 'm':
                    titleP2 = string.Format(Strings.Global_Month + " {0:MM/yyyy}", start);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", start, end);
                    break;
            }
            doc.Info.Title = titleP1 + " - " + titleP2;
            doc.Styles["Normal"].Font.Size = "8pt";

            var section = doc.AddSection();
            section.PageSetup.TopMargin = "1cm";
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);
            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            var par = section.AddParagraph();
            par.Format.SpaceAfter = "1cm";
            par.Format.AddTabStop("8cm");//, TabAlignment.Right);
            par.AddFormattedText(titleP1, new Font("Verdana", "20pt"));
            par.AddTab();
            par.AddFormattedText(titleP2, new Font("Verdana", "16pt"));
            if (titleP3 != "")
            {
                par.Format.SpaceAfter = "0cm";
                par = section.AddParagraph();
                par.Format.SpaceAfter = "1cm";
                par.Format.AddTabStop("8cm");
                par.AddTab();
                par.AddFormattedText(titleP3, new Font("Verdana", "12pt"));
            }

            var table = section.AddTable();
            table.KeepTogether = true;
            table.Borders.Style = BorderStyle.Single;
            table.Borders.Color = Colors.LightGray;

            table.AddColumn("8cm", ParagraphAlignment.Left);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);

            var headrow = table.AddRow();
            headrow.VerticalAlignment = VerticalAlignment.Center;
            headrow.Height = "1cm";
            headrow.Cells[0].AddParagraph(Strings.Global_Name);
            headrow.Cells[1].AddParagraph(Strings.Global_Qty);
            headrow.Cells[2].AddParagraph(Strings.Global_Item);
            headrow.Cells[3].AddParagraph(Strings.Global_Vat);
            headrow.Cells[4].AddParagraph(Strings.Global_Total);

            var summary = GetItemReportInvoiceData(db, start, end, published);

            var netto = 0m;
            var vat = 0m;
            foreach (var it in summary)
            {
                var row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph(it.Item1);
                row.Cells[1].AddParagraph(it.Item2.ToString("0.##"));
                row.Cells[2].AddParagraph(it.Item3.ToString("C2"));
                row.Cells[3].AddParagraph(it.Item4.ToString("C2"));
                row.Cells[4].AddParagraph((it.Item3 + it.Item4).ToString("C2"));
                netto += it.Item3;
                vat += it.Item4;
            }

            var totalrow = table.AddRow();
            totalrow.Height = "1cm";
            totalrow.VerticalAlignment = VerticalAlignment.Center;
            totalrow.Cells[0].AddParagraph(Strings.Global_Total).Format.Font.Bold = true;
            totalrow.Cells[2].AddParagraph(netto.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[3].AddParagraph(vat.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[4].AddParagraph((netto + vat).ToString("C2")).Format.Font.Bold = true;

            return doc;
        }

        public static IEnumerable<Tuple<string, decimal, decimal, decimal>> GetItemReportInvoiceData(ChlebyContext db, DateTime start, DateTime end, bool? published)
        {
            var hasValue = published.HasValue;
            var summary = from invoice in db.Invoices
                          from pos in invoice.Rows
                          where pos.Count > 0 && (!hasValue || invoice.Published == published) && !invoice.Deleted && !invoice.MyClones.Any()
                                && EntityFunctions.TruncateTime(invoice.Date) >= EntityFunctions.TruncateTime(start) 
                                && EntityFunctions.TruncateTime(invoice.Date) <= EntityFunctions.TruncateTime(end)
                          orderby pos.Text
                          group pos by pos.Text
                              into byitem
                              select new
                                  {
                                      item = byitem.Key,
                                      qty = byitem.Sum(x => x.Count),
                                      price = byitem.Sum(x => x.Price),
                                      vat = byitem.Sum(x => x.Vat)
                                  };
            return summary.ToList().Select(x => Tuple.Create(x.item, x.qty, x.price, x.vat)).ToList();
        }

        public Document SalesReportPDF(IEnumerable<InvoiceWeekData> summary, DateTime start, DateTime end, char invoicePeriod, DayOfWeek dow)
        {
            var doc = new Document();
            var titleP1 = Strings.PDF_Sales_Customer_Report;
            string titleP2 = "";
            string titleP3 = "";
            switch (invoicePeriod)
            {
                case 'a':
                case 'u':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", start, end);
                    break;
                case 'd':
                    titleP2 = string.Format("{0:" + _business.DateFormat + "}", start);
                    break;
                case 'w':
                    titleP2 = string.Format(Strings.Global_Week + " {0}/{1:yyyy}", start.GetWeek(dow), start);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", start, end);
                    break;
                case 'm':
                    titleP2 = string.Format(Strings.Global_Month + " {0:MM/yyyy}", start);
                    titleP3 = string.Format("{0:" + _business.DateFormat + "} - {1:" + _business.DateFormat + "}", start, end);
                    break;
            }
            doc.Info.Title = titleP1 + " - " + titleP2;
            doc.Styles["Normal"].Font.Size = "8pt";

            var section = doc.AddSection();
            section.PageSetup.TopMargin = "1cm";
            var pagecount = section.Footers.Primary.AddParagraph(Strings.PDFs_Page);
            pagecount.AddPageField();
            pagecount.AddText("/");
            pagecount.AddSectionPagesField();
            pagecount.Format.Alignment = ParagraphAlignment.Right;

            var par = section.AddParagraph();
            par.Format.SpaceAfter = "1cm";
            par.Format.AddTabStop("8cm");//, TabAlignment.Right);
            par.AddFormattedText(titleP1, new Font("Verdana", "20pt"));
            par.AddTab();
            par.AddFormattedText(titleP2, new Font("Verdana", "16pt"));

            if (titleP3 != "")
            {
                par.Format.SpaceAfter = "0cm";
                par = section.AddParagraph();
                par.Format.SpaceAfter = "1cm";
                par.Format.AddTabStop("8cm");
                par.AddTab();
                par.AddFormattedText(titleP3, new Font("Verdana", "12pt"));
            }

            var table = section.AddTable();
            table.KeepTogether = true;
            table.Borders.Style = BorderStyle.Single;
            table.Borders.Color = Colors.LightGray;

            table.AddColumn("4cm", ParagraphAlignment.Left);
            table.AddColumn("4cm", ParagraphAlignment.Left);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);
            table.AddColumn("2cm", ParagraphAlignment.Right);

            var headrow = table.AddRow();
            headrow.VerticalAlignment = VerticalAlignment.Center;
            headrow.Height = "1cm";
            headrow.Cells[0].AddParagraph(Strings.Global_Customer);
            headrow.Cells[1].AddParagraph(Strings.Global_Address);
            headrow.Cells[2].AddParagraph(Strings.Global_Items);
            headrow.Cells[3].AddParagraph(Strings.Global_Delivery);
            headrow.Cells[4].AddParagraph(Strings.Global_Vat);
            headrow.Cells[5].AddParagraph(Strings.Global_Total);

            var netto = 0m;
            var deliv = 0m;
            var vat = 0m;
            foreach (var iwd in summary)
            {
                var row = table.AddRow();
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[0].AddParagraph(iwd.Address.Trader.Name);
                row.Cells[1].AddParagraph(iwd.Address.Address1);
                netto += iwd.ItemPrice;
                deliv += iwd.DeliveryPrice;
                vat += iwd.VatPrice;
                row.Cells[2].AddParagraph(iwd.ItemPrice.ToString("C2"));
                row.Cells[3].AddParagraph(iwd.DeliveryPrice.ToString("C2"));
                row.Cells[4].AddParagraph(iwd.VatPrice.ToString("C2"));
                row.Cells[5].AddParagraph((iwd.ItemPrice + iwd.DeliveryPrice + iwd.VatPrice).ToString("C2"));
            }
            var totalrow = table.AddRow();
            totalrow.Height = "1cm";
            totalrow.VerticalAlignment = VerticalAlignment.Center;
            totalrow.Cells[0].AddParagraph(Strings.Global_Total).Format.Font.Bold = true;
            totalrow.Cells[2].AddParagraph(netto.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[3].AddParagraph(deliv.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[4].AddParagraph(vat.ToString("C2")).Format.Font.Bold = true;
            totalrow.Cells[5].AddParagraph((netto + vat).ToString("C2")).Format.Font.Bold = true;


            return doc;
        }
    }

    static class MigraDocExt
    {
        public static Column AddColumn(this Table table, Unit w, ParagraphAlignment a)
        {
            var column = table.AddColumn();
            column.Width = w;
            column.Format.Alignment = a;
            return column;
        }
    }
}