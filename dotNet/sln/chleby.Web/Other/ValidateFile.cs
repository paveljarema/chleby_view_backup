﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace chleby.Web
{
    public class ValidateFileAttribute : ValidationAttribute
    {
        public int MaxContentLength { get; set; }
        public string[] AllowedFileExtensions { get; set; }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;

            if (file == null)
                return true;
            else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
            {
                ErrorMessage = "Accepted type: " + string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.ContentLength > MaxContentLength)
            {
                ErrorMessage = "Your file is too large, maximum allowed size is : " + (MaxContentLength / 1024).ToString(CultureInfo.InvariantCulture) + "MB";
                return false;
            }
            else
                return true;
        }
    }

}