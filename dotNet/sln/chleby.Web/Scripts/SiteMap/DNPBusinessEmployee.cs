﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using MvcSiteMapProvider.Extensibility;
using chleby.Web.Models;

namespace chleby.Web.SiteMap
{
    public class DNPBusinessEmployee : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection()
        {
            var db = ChlebyContext.ForCurrentApp();
            var nodes = new List<DynamicNode>();
            foreach (var bh in db.BusinessEmployees)
                nodes.Add(new DynamicNode
                {
                    Title = bh.FirstName+" "+bh.LastName,
                    RouteValues = new Dictionary<string, object>() {
                        { "id", bh.Id } //id dobre? mvc sitemap sie pieprzy
                    }
                });
            return nodes;
        }
    }
}