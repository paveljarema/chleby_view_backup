﻿function permissionsCheckIntegrity(categoryId) {

    var checkedAll = $('.permissions-input[value=write]:checked').length == $('.permissions-input[value=write]').length;
    $('.perm-simple').attr('checked', false);
    if (checkedAll) {
        $('.perm-simple[value=full]').attr('checked', true);
    } else {
        $('.perm-simple[value=custom]').attr('checked', true);
    }

    var section = $("#cat_" + categoryId);

    var total = section.find('input:radio[value=write]').size();
    var write = section.find('input:radio:checked[value=write]').size();
    var read = section.find('input:radio:checked[value=read]').size();
    var hidden = section.find('input:radio:checked[value=hidden]').size();

    if (write == total) {
        $("#full_" + categoryId).attr('checked', true);
        $("#limit_" + categoryId).attr('checked', false);
    }
    else {
        $("#full_" + categoryId).attr('checked', false);
        $("#limit_" + categoryId).attr('checked', true);
    }

    section.find(".permissions-all-modify input:radio").attr('checked', false);
    section.find(".permissions-all-modify input:radio").each(function () {

        if ($(this).data('value') == 'read') {
            if (read == total) {
                $(this).attr('checked', true);
            }
            else { $(this).attr('checked', false); }
        }

        if ($(this).data('value') == 'write') {
            if (write == total) {
                $(this).attr('checked', true);
            }
            else { $(this).attr('checked', false); }
        }

        if ($(this).data('value') == 'hidden') {
            if (hidden == total) {
                $(this).attr('checked', true);
            }
            else { $(this).attr('checked', false); }
        }

    });
}

$(document).ready(function () {

    //permissions batch edit
    $("body").on("click", ".permissions-all-modify", function () {

        var value = $(this).data('value');
        var parent = $(this).closest('table');
        parent.find('input[type=radio]').each(function () {
            if ($(this).val() == value) {
                $(this).prop("checked", "checked");
            }
            else {
                $(this).prop("checked", false);
            }
        });
        permissionsCheckIntegrity($(this).data('category'));

    });

    $("body").on("click", ".permissions-global-allow", function () {
        var categoryId = $(this).data("category");
        var section = $("#cat_" + categoryId);
        section.find('input:radio').attr('checked', null);
        section.find('input:radio[value=write]').attr('checked', true);
        permissionsCheckIntegrity(categoryId);
    });

    $("body").on("change", ".permissions-input", function () {
        var categoryId = $(this).data("category");
        permissionsCheckIntegrity(categoryId);
    });

    $("body").on("click", ".permissions-global-toggle", function () {
        var section = $('#permContent');
        var value = $(this).data('value');
        section.find('input:radio').attr('checked', null);
        section.find('input:radio[value=' + value + ']').attr('checked', true);
        if (value == "write") {
            section.find('input:checkbox').attr('checked', true);
        } else {
            section.find('input:checkbox').attr('checked', null);
        }
        
        $('#permTab').find('li a').each(function () {
            permissionsCheckIntegrity($(this).data('category'));
        });
    });

    var curTab;
    var curTabIdx;
    var permTabs;
    
    permClearTabs = function() {
        permTabs = [];
    };

    permAddTab = function(name) {
        permTabs.push(name);
    };

    $("body").on("click", "#perm-simple-next", function () {
        var selPermSimple = $('input:radio.perm-simple:checked');
        if (selPermSimple.val() == "full") {
            $('#form-submit').click();
        } else {
            $('#panel_start').hide();
            $('#panel_details').show();
            $(window).trigger('resize');
            var tabs = $('#permTab');
            var selectedTab = tabs.children('.permTab_tab.active');
            curTab = selectedTab.data('category');
            curTabIdx = permTabs.indexOf(curTab);
            console.log(curTab);
        }
    });

    $("body").on("click", ".permTab_link", function() {
        curTab = $(this).data('category');
        curTabIdx = permTabs.indexOf(curTab);
        console.log(curTab);
    });
    
    $("body").on("click", "#perm-details-prev", function () {
        if (curTabIdx == 0) {
            $('#panel_details').hide();
            $('#panel_start').show();
            $(window).trigger('resize');
        } else {
            curTabIdx--;
            curTab = permTabs[curTabIdx];
            $(".permTab_link#link_" + curTab).click();
        }
    });
    
    $("body").on("click", "#perm-details-next", function () {
        if (curTabIdx == permTabs.length - 1) {
            $('#form-submit').click();
        } else {
            curTabIdx++;
            curTab = permTabs[curTabIdx];
            $(".permTab_link#link_" + curTab).click();
        }
    });

});
