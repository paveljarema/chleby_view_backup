﻿$(document).ready(function () {		 
    $(".chleby-edit").click(function (e) {
		e.preventDefault();
		var href = $(this).attr("href");
		$.ajax({
			url: href,
			context: document.body
		}).done(function (data) {
			// alert(href + "success" + " data:" + data);
			$("#chleby-edit").html(data);
			$("#editModal").modal();
		});
	});
});
