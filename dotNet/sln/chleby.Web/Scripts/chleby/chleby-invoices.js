﻿
$(document).ready(function () {
    
    //invoices batch create super forma ajaxem
    $("body").on("click", "#invoices-batch-create-confirm", function (e) {
        e.preventDefault();
        var slug = $("#invoices-batch-create-form").serialize();
        var url = $(this).data('target');
        console.log(slug);
        $.ajax({
            type: "POST",
            url: url,
            context: document.body,
            data: slug,
        }).done(function (data) {
            $('#invoices-batch-create-modal .modal-body').html(data);
            $('#invoices-batch-create-modal').modal('show');

        });
    });
    
    //showing price edits in invoice create/edit
    $("body").on("click", ".input-price-show", function (e) {
        e.preventDefault();
        $(this).closest('tr').find(".input-invoice-price").toggle();
    });
    
    $("body").on("focusin", ".input-price-proxy", function (e) {
        e.preventDefault();
        $(this).closest('tr').find(".input-invoice-price").show();
    });
    
    $("body").on("focusout", ".input-price-proxy", function (e) {
        e.preventDefault();
        $(this).closest('tr').find(".input-invoice-price").hide();
    });
    
    //updating price edits when batch editing
    $("body").on("keyup", ".input-price-proxy", function () {
        var val = $(this).val();
        $(this).closest('tr').find(".input-invoice-price").val(val);
    });
    
    //qbexport modal range refresh
    $("#invoices-export-modal").on("click", ".qbexport-refresh", function () {
        var url = $(this).data('action');
        var button = $(this);
        var parent = button.closest('.form-inline');
        var dtstart = parent.find('.qbexport-input-dtstart').val();
        var dtend = parent.find('.qbexport-input-dtend').val();
        var numstart = parent.find('.qbexport-input-numstart').val();
        var numend = parent.find('.qbexport-input-numend').val();

        $.ajax({
            url: url,
            context: document.body,
            data: {
                dtstart: dtstart,
                dtend: dtend,
                numstart: numstart,
                numend: numend
            }
        }).done(function (data) {
            $('#invoices-export-modal .modal-body').hide().html(data).fadeIn();
        });
    });

    //filling <select> for address via ajax (to allow only addresses of trader that we're invoicing)
    $(".controls-invoice-delivery").on("change", ".input-invoice-address", function () {
        var url = $(this).data('action');
        var invoiceAddress = $(this);
        var deliveryAddress = invoiceAddress.closest('.controls-invoice-delivery').find('.input-delivery-address');
        $.ajax({
            url: url,
            context: document.body,
            data: {
                id: $(this).val()
            }
        }).done(function (data) {
            deliveryAddress.empty();
            for (var x in data) {
                deliveryAddress.append("<option value='" + x + "'>" + data[x] + "</option>");
            }
        });
    });

    //new invoice number validation
    $(".controls-invoice-number").on("change", ".input-invoice-number", function () {
        var url = $(this).data('action');
        var input = $(this);
        var parent = $(this).closest('.controls-invoice-number');
        var validation = parent.find('.validation-invoice-number');
        var invoiceId = $(this).data('invoiceid');
        $.ajax({
            url: url,
            context: document.body,
            data: {
                number: $(this).val(),
                id: invoiceId
            }
        }).done(function (data) {
            console.log(data);
            if (data != true) {
                input.addClass("input-validation-error");
                validation.text(data);
            }
            else {
                input.removeClass("input-validation-error");
                validation.empty();
            }
        });
    });

    //invoice edit - unlock number editing
    $(".controls-invoice-number").on("click", ".unlock-invoice-number", function () {
        var a = $(this);
        var parent = $(this).closest('.controls-invoice-number');
        var input = parent.find('.input-invoice-number');
        var text = parent.find('.text-invoice-number');

        text.hide();
        a.hide();
        input.show();

    });

    //addrow in invoices / synchronized itemid
    $("select[name=ItemId],select[name=NewItemId]").change(function () {
        $("select[name=ItemId],select[name=NewItemId]").val($(this).val());
    });
    

});
