﻿function orderOverrideCalculateTotal(input) {
    var minorder = parseFloat($("#chleby-orderoverride-min-delivery").val());
    var delprice = parseFloat($("#chleby-orderoverride-delivery-price").val());
    var delvat = parseFloat($("#chleby-orderoverride-delivery-vat").val());
    delvat = delvat / 100;

    var newqty = $(input).val();
    var originalqty = $(input).data('originalvalue');
    if (newqty != originalqty) {
        $(input).parent().find('.chleby-qty-reset').show();
    }
    else {
        $(input).parent().find('.chleby-qty-reset').hide();
        $(input).dirtyForms('setClean');
    }

    if (newqty < 0) {
        $(input).val(null);
        newqty = 0;
    }
    var price = $(input).data("price");
    var vat = $(input).data("vat");
    if (price === undefined) price = 0;
    if (vat === undefined) vat = 0;
    if (newqty === undefined) newqty = 0;
    var total = $(input).parent().parent().find(".chleby-orderoverride-item-total");
    var vattotal = $(input).parent().parent().find(".chleby-orderoverride-item-vat");
    var net = $(input).parent().parent().find(".chleby-orderoverride-item-net");
    vat = vat / 100;
    var newtotal = (newqty * price) + (newqty * price * vat);
    var newnet = (newqty * price);
    var newvat = (newqty * price * vat);

    if (newtotal != 0) {
        total.html(CurrencySymbol + newtotal.toFixed(2));
    } else {
        total.html(null);
    }
    if (newvat != 0) {
        vattotal.html(CurrencySymbol + newvat.toFixed(2));
    } else {
        vattotal.html(null);
    }
    if (newnet != 0) {
        net.html(CurrencySymbol + newnet.toFixed(2));
    } else {
        net.html(null);
    }

    var newTotal = 0;
    $(".chleby-orderoverride-item-qty").each(function () {
        var newqty = $(this).val();
        var price = $(this).data("price");
        var vat = $(this).data("vat");
        if (price === undefined) price = 0;
        if (vat === undefined) vat = 0;
        if (newqty === undefined) newqty = 0;
        vat = vat / 100;
        newtotal = (newqty * price) + (newqty * price * vat);
        newTotal += newtotal;
    });

    if (minorder > newTotal) {
        $("#chleby-orderoverride-delivery").attr("disabled", true);
        $("#chleby-orderoverride-delivery").attr("checked", false);
    } else {
        $("#chleby-orderoverride-delivery").attr("disabled", false);
        if ($("#chleby-orderoverride-delivery").is(":checked")) {
            newTotal = newTotal + (delprice) + (delprice * delvat);
        }
    }

    $(".chleby-orderoverride-day-total").html(newTotal.toFixed(2));
}

$(document).ready(function () {

    $(".chleby-orderoverride-favoruite-filter").toggle(
       function (e) {
           e.preventDefault();
           var table = $(this).closest("table");
           $(this).html("<i class='icon-star'></i>");

           table.find("tbody tr").each(function () {
               if ($(this).find(".chleby-not-favorite").is('*')) {
                   $(this).hide();
               }
               else {
                   $(this).show();
               }

           });

       },
       function (e) {
           e.preventDefault();
           var table = $(this).closest("table");
           $(this).html("<i class='icon-star-empty'></i>");
           table.find("tbody tr").each(function () { $(this).show(); });
       }

   );

    $(".chleby-orderoverride-filter").change(function () {
        var val = $(this).val();
        var table = $(this).closest("table");

        //var dataTable = table.dataTable();
        //fnResetAllFilters(dataTable);
        // dataTable.fnDraw();

        if (val == 'all') {
            table.find("tbody tr").show();
        }
        else if (val == 'in') {
            table.find("tbody tr").each(function () {

                var qty = $(this).find(".chleby-orderoverride-item-qty").first().val();
                if (isNaN(qty) || qty == '' || qty == null) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }

            });
        }
        else if (val == 'out') {
            table.find("tbody tr").each(function () {

                var qty = $(this).find(".chleby-orderoverride-item-qty").first().val();
                if (!isNaN(qty) && qty != '' && qty != null) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            });
        }
    });

    $(".chleby-orderoverride-item-qty, #chleby-orderoverride-delivery").change(function () {
        orderOverrideCalculateTotal(this);
    });



});
