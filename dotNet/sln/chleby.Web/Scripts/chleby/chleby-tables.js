﻿function fnResetAllFilters(oTable) {
    var oSettings = oTable.fnSettings();
    for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
        oSettings.aoPreSearchCols[iCol].sSearch = '';
    }
    oSettings.oPreviousSearch.sSearch = '';
    oTable.fnDraw();
}

function isDataTable(nTable) {
    var settings = $.fn.dataTableSettings;
    for (var i = 0, iLen = settings.length ; i < iLen ; i++) {
        if (settings[i].nTable == nTable) {
            return true;
        }
    }
    return false;
}

function bindDataTables() {

    $('.data-table').not('.initialized').each(function () {
             
        //scan for "no-sort" css class in thead, disable sorting
        //enable filters with classes filter-text or filter-select
        var dontSort = [];
        var filters = [];
        var sort = [];
        var scrollheight = "";

        if ($(this).hasClass('finance-dashboard-table')) {
            scrollheight = "225px";
        }
        $(this).find('thead th').each(function () {

            if ($(this).hasClass('filter-text')) {
                dontSort.push({ "bSortable": false });
                filters.push({ type: "text" });
            }
            else if ($(this).hasClass('filter-select')) {
                dontSort.push({ "bSortable": false });
                filters.push({ type: "select", bSmart: false, bRegex: false });
            }
            else if ($(this).hasClass('no-sort')) {
                dontSort.push({ "bSortable": false });
                filters.push(null);
            }
            else if ($(this).hasClass('currency-sort')) {
                dontSort.push({ "bSortable": true, "sSortDataType": "dom-text", "sType": "currency" });
                filters.push(null);
            }
            else {
                dontSort.push(null);
                filters.push(null);

            }
        });
        $(this).addClass('initialized');
        var oTable = $(this).dataTable({
            //"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'dataTables_controls'<'span6'i><'span1'<'pull-right'>><'span5'p>>", //old layout with filters and shit
            "sDom": "<'row-fluid'r>t>",
            "aoColumns": dontSort,
            "sScrollY": scrollheight,
            "iDisplayLength": 100,
            "bPaginate": false,
            //  "bStateSave": true, //save filters & ordering to cookies
            "aaSorting": [], //default sort column (none!)
            "oLanguage":
				{
				    "sUrl": "/Scripts/datatables/langs/" + Globalize.culture().name + ".js", //hejmus dirty fix
				},
            "fnDrawCallback": function (oSettings) {
                //ommiting duplicate data
                var tdcontent = new Array();
                var rowchanged = false;
                $(this).find("tr").each(function () {

                    rowchanged = false;
                    $(this).find("td").each(function (key, value) {

                        var tdvalue = $(this).text();

                        if (tdcontent[key] != tdvalue) {
                            //console.log(tdcontent[key] + " != " + tdvalue);
                        }

                        if (tdcontent[key] == tdvalue && !rowchanged) {
                            $(this).find(".tohide").hide();

                        } else {
                            tdcontent[key] = tdvalue;
                            $(this).find(".tohide").show();
                            rowchanged = true;
                        }
                    });
                });
            }
        });
        setTimeout(function(){
            oTable.columnFilter({ "aoColumns": filters });
        }, 100);
        
   
    });


    //------------
}

$(document).ready(function () {


    bindDataTables();


    //FILTERS RELEASE BUTTONS
    
    $("body").on("keyup", ".text_filter", function () {
        if (!$(this).hasClass('search_init')) {
            if ($(this).val() != '')
            {
                $(this).parent().find('.filter-release').show();
            }
            else {
                $(this).parent().find('.filter-release').hide();
            }
        }
        else {
        }
    });
    
    $("body").on("click", ".filter-release", function () {
        $(this).parent().find('.text_filter').val('').trigger('keyup').trigger('blur');
        $(this).hide();
    });


    //------------
    //TABLES
    $("table.checkbox-batch").each(function () {
        var checkBoxesActive = $(this).find(' input:checkbox.batch:checked');
        $(".batch-items-number-selected").html(checkBoxesActive.length);

    });

    $("table.checkbox-batch input:checkbox.batch-all").click(function () {
        var checkedStatus = this.checked;
        var parentTable = $(this).parents("table.checkbox-batch");
        var checkBoxes = parentTable.find(' input:checkbox.batch');
        var checkBoxesActive = parentTable.find(' input:checkbox.batch:checked');
        checkBoxes.each(function () {
            //console.log(this);
            this.checked = checkedStatus;
        });
      //  parentTable.
            $(".batch-items-number-selected").html(checkBoxesActive.length);
    });

    $("input:checkbox.batch-all, input:checkbox.batch").click(function () {
        var parentTable = $(this).parents("table.checkbox-batch");
        if (parentTable.find('input:checkbox.batch:checked').length) {
            parentTable.find(".batch-actions").show();
        } else {
            parentTable.find(".batch-actions").hide();
        }
        var checkBoxesActive = parentTable.find(' input:checkbox.batch:checked');
        //parentTable.
            $(".batch-items-number-selected").html(checkBoxesActive.length);
    });
    //------------

});

$.extend($.fn.dataTableExt.oStdClasses, {
    "sWrapper": "dataTables_wrapper form-inline"
});

jQuery.fn.dataTableExt.oSort['currency-pre'] = function (a) {
    var ms = a.match(/data-val="*(-?[0-9\.]+)/);
    if (ms != null && ms.length == 2) {
        return parseFloat(ms[1]);
    }
    return parseFloat(a);
};

jQuery.fn.dataTableExt.oSort['currency-asc'] = function (a, b) {
    var x = parseFloat(a);
    var y = parseFloat(b);
    return x - y;
};

jQuery.fn.dataTableExt.oSort['currency-desc'] = function (a, b) {
    var x = parseFloat(a);
    var y = parseFloat(b);
    return y - x;
};

//stare zostaja dla backcompat
jQuery.fn.dataTableExt.oSort['dataval-pre'] = jQuery.fn.dataTableExt.oSort['currency-pre'];
jQuery.fn.dataTableExt.oSort['dataval-asc'] = jQuery.fn.dataTableExt.oSort['currency-asc'];
jQuery.fn.dataTableExt.oSort['dataval-desc'] = jQuery.fn.dataTableExt.oSort['currency-desc'];