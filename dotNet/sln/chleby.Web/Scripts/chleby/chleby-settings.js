﻿function checkTradingProps() {
    var tradingdetails = $("#trading-details");
    if ($("#UsingTradingProps").attr("checked")) {
        tradingdetails.fadeIn();
    } else {
        tradingdetails.fadeOut();
    }
}

$(document).ready(function () {
    
    //company/trading details
    $("body").on("click", "#UsingTradingProps", function () {
        checkTradingProps();
    });
    
    //CompanySettings -> paste legal details
    $('.company-details-copy-legal-to-trading').click(function () {
        $('#legal-details').find('input[type=text]').each(function () {
            var inputid = $(this).attr('id');
            var newinputid = inputid.replace("Legal", "Trading");
            $('#' + newinputid).val($(this).val()).dirtyForms('setDirty');
        });
    });
    
    //CompanySettings -> check if trading empty
    $('#business-company-details-form').submit(function (e) {
        var form = $(this);
        if (form.find('#UsingTradingProps').prop("checked")) {
            if (!$('#TradingName').val() || !$('#TradingAddress1').val() || !$('#TradingCity').val() || !$('#TradingPost').val()) {
                alert('You have selected that your trading details are different than your leagal details, but have not filled these in. Please either fill them in, or deselect the box. The data cannot be left blank.');
                e.preventDefault();
                return false;
            }
        }
        return true;
    });

});
