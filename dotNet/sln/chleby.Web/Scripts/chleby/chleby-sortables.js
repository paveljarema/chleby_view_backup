﻿function sortableDummyRowsUpdate() {
    $("tbody.sortable").each(function (i, v) {
        if ($(v).children("tr").length == 1) {
            $(v).find('.sort-disabled').show();
        } else {
            $(v).find('.sort-disabled').hide();
        }
    });
}

$(document).ready(function () {
    // SORTABLES
    var fixHelper = function (e, ui) {
        ui.children().each(function () {
            $(this).width($(this).width());
        });
        return ui;
    };
    
    $("ol.sortable").sortable({
        items: "li:not(.ui-state-disabled)"
    });
    
    $(".sortable").sortable({});

    $("tbody.sortable").sortable({
        helper: fixHelper,
        connectWith: "tbody.sortable",
        dropOnEmpty: true,
        cancel: "tr.sort-disabled",
        receive: function (event, ui) {
          
            ui.item.find("input[type='hidden']").val(this.id);
            sortableDummyRowsUpdate();
           
        }
    }).disableSelection();
});
