﻿window.onbeforeunload = function (evt) {
    var message = 'You\'ve made changes on this page which aren\'t saved. If you leave you will lose these changes.';
    if (typeof evt == 'undefined') {
        evt = window.event;
    }

    var flag = false;
    if (("form").length > 1) {

        $("form.dirtyforms").each(function () {
            //if ($.DirtyForms.isDirty()) {
            //console.log(this);
            if ($(this).hasClass('dirty')) {
                flag = true;
            }
        });
    }
    if (flag) {
        if (evt) {
            evt.returnValue = message;
        }
        return message;
    }
}

function chlebySmartReload() {
    history.go(0);
}


//debouncer żeby nie zarżnąć przeglądarki przy zmianie rozmiaru okna
function debouncer(func, timeout) {
    var timeoutID, timeout = timeout || 200;
    return function () {
        var scope = this, args = arguments;
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function () {
            func.apply(scope, Array.prototype.slice.call(args));
        }, timeout);
    }
}


//reinit popovers & sortables when async load modal body
function chlebyBeforeModalShow() {
    bindDataTables();
    $('.has-popover').powerTip({ mouseOnToPopup: true, smartPlacement: true });
    $(".sortable").sortable({
        items: "li:not(.ui-state-disabled)"
    });
    rebindUpload();
}

function rebindUpload() {
    $("#UploadButton").ajaxUpload({
        action: $("#UploadButton").data('target'),
        name: "file",
        responseType: 'html',
        allowedExtensions: ['csv'],
        submit: function () {
        },
        complete: function (fileName, result) {

            $("#UploadButton").closest('.modal-body').html(result);
            rebindUpload();

        }
    });
}

//equal div height in row
function fixRowSpanHeight() {
    $('div.row:visible, div.row-fluid:visible').each(function () {
        var maxHeight = 0;
        var spans = $(this).find('[class^=span].jsresize');
        spans.each(function () {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        spans.height(maxHeight);
    });
}



//chrome date validation override
//via http://stackoverflow.com/questions/6906725/unobtrusive-validation-in-chrome-wont-validate-with-dd-mm-yyyy
jQuery.validator.methods.date = function (value, element) {
    var dateRegex = /^(0?[1-9]\/|[12]\d\/|3[01]\/){2}(19|20)\d\d$/;
    return this.optional(element) || dateRegex.test(value);
};

$(document).ready(function () {


    var unslider = $('.unslider').unslider({
        keys: true,
        fluid: false,
        dots: true,
        delay: 10000
    });

    $('.unslider-arrow').click(function (e) {
        e.preventDefault();
        var fn = this.className.split(' ')[1];
        //  Either do unslider.data('unslider').next() or .prev() depending on the className
        unslider.data('unslider')[fn]();
    });


    $('.blink').each(function () {
        var obj = $(this);

        console.log(obj.css('color'));

        setInterval(function () {
            if (obj.hasClass("blink")) {
                obj.removeClass("blink");
            } else {
                obj.addClass("blink");
            }
        }, 500);
    });

    //dirtyforms (prompt if values changed & leaving the page w/o save)
    $.DirtyForms.dialog = false;
    $.DirtyForms.ignoreClass = "no-dirtyforms";
    $('form.dirtyforms').dirtyForms();


    //cleanforms on submit to prevent false prompts
    $("form.dirtyforms").on("click", "input[type=submit], button[type=submit]", function () {
        $('form').dirtyForms('setClean');
    });

    //FIX FLOAT GRID HEIGHT
    fixRowSpanHeight();

    $("#cart-delcart").click(function (e) {
        e.preventDefault();
        // var r = confirm(msg);
        if (true) {
            var href = $(this).attr("href");
            var cartname = $("#cart-selectcart option:selected").text().replace(/^\s+|\s+$/g, ''); //remove whitespace & newlines    
            if (href) { // require a URL
                $.ajax({
                    url: href,
                    context: document.body,
                    data: {
                        "name": cartname
                    }
                }).done(function (data) {
                    $("#cart-selectcart").children().remove();
                    for (itkey in data) {
                        $('#cart-selectcart').append('<option value=\'' + data[itkey].Content + '\'>' + data[itkey].Name + '</option>');
                        //console.log(data[itkey].Content);
                        //console.log(data[itkey].Name);
                    }
                    $("#cart-selectcart").trigger("change");
                });
            }
        }
    });

    $("#cart-loadsavedcart").click(function (e) {
        e.preventDefault();

        if ($("#cart-selectcart").val() != "") {

            var savedcart = JSON.parse($("#cart-selectcart").val());
            // console.log(savedcart);
            $("#note").html(savedcart.note);

            if (savedcart.delivery) {
                $("#chleby-orderoverride-delivery").attr("checked", true);
            } else {
                $("#chleby-orderoverride-delivery").attr("checked", false);
            }

            for (itkey in savedcart.items) {
                $("input[name='items[" + itkey + "]']").val(savedcart.items[itkey]).change().addClass("dirty");
            }
        }
    });

    //hide trash icon
    $('#cart-delcart').hide();

    $('#cart-selectcart').change(
        function () {
            if ($(this).val() != '') {
                $('#cart-delcart').show();
            }
            else {
                $('#cart-delcart').hide();
            }
        }
    );

    $("a[rel^='prettyPhoto']").prettyPhoto({
        default_width: 500,
        default_height: 344,
        social_tools: ''

    });

    $(".finance-dashboard").on("change", ".finance-dashboard-select-redirect", function () {
        var targetdom = $(this).closest('.finance-dashboard-table-container');
        var href = $(this).val(); // get selected value
        if (href) { // require a URL
            $.ajax({
                url: href,
                context: document.body
            }).done(function (data) {
                targetdom.html(data);
                chlebyBeforeModalShow();
            });
        }
    });

    $(".finance-dashboard").on("click", ".finance-dashboard-remove", function () {
        $(this).closest('li').remove();
    });

    $("body").on("mouseenter", ".modal-body .has-popover", function () {
        $(this).powerTip('show');
    });

    //handwalk leech save proxy   
    if ($('#handwalk-leechproxy').length != 0) {
        //$('.nav-secondary .btn-save').hide();
    }

    $('#handwalk-leechproxy').click(function (e) {
        if ($('.nav-secondary .btn-save').length != 0) {
            e.preventDefault();
            $('.nav-secondary .btn-save').click();
        }
    });

    $('.handwalk-save').click(function (e) {
        if ($('#handwalk-form-save-stay').length != 0) {
            e.preventDefault();
            $('#handwalk-form-save-stay').click();
        }
    });

    $('.handwalk-save-prev').click(function (e) {
        if ($('#handwalk-form-save-prev').length != 0) {
            e.preventDefault();
            $('#handwalk-form-save-prev').click();
        }
    });

    //Formula1 horizontal master menusy
    //$('.nav.nav-admin>li').mouseenter(function () {
    //    $('.nav.nav-admin li .chleby-dropdown').hide();
    //    $(this).find('.chleby-dropdown').show();
    // });

    $('.nav.nav-admin>li').each(function () {
        if ($(this).find('.dropdown-menu li.active').size() > 0) {
            $(this).addClass('active');
            // $(this).find('.chleby-dropdown').show();
        }
    });

    //jsproxy for putting inputs in SecondaryActions bar (thanks, MVC layouts&sections. thanks, html.)
    $("body").on("change", ".input-jsproxy", function () {
        var targetId = $(this).data('target');
        $('#' + targetId).val($(this).val());
    });

    //rounds move addr between rounds by click
    $("body").on("click", ".rounds-move-addr", function () {
        var target = $(this).data('target');
        var tr = $(this).closest('tr');
        tr.appendTo('#' + target);
        sortableDummyRowsUpdate();
        tr.find('.rounds-addr-roundid').val(target);
    });

    //simple list remove from dom
    $("body").on("click", ".chleby-simplelist .remove", function () {
        $(this).parent().remove();
        // console.log($(this).parent());
    });

    //simplelist
    $("body").on("click", ".chleby-simplelist-add", function () {
        var btn = $(this);
        var href = $(this).closest('form').find('input[name=chleby-simplelist-new-url]').val();
        var form = $(this).closest('form');
        $.ajax({
            url: href,
            context: document.body
        }).done(function (data) {
            btn.siblings('.chleby-simplelist').append(data);
            form.removeData("validator").removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse(form);
        });
    });

    //scrollto after page reload
    if ($.cookie('chlebyScrollTo')) {
        $(document).scrollTop($.cookie('chlebyScrollTo'));
        $.removeCookie('chlebyScrollTo');
    }



    //number fields plus/minus/remove/reset btns
    $("body").on("click", ".chleby-qty-minus", function (e) {
        e.preventDefault();
        var input = $(this).parent().find("input");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (currentVal > 0) {
                input.val(currentVal - 1);
            }
        }
        else (input.val(0));
        input.dirtyForms('setDirty');
        input.change();
    });

    $("body").on("click", ".chleby-qty-plus", function (e) {
        e.preventDefault();
        var input = $(this).parent().find("input");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            input.val(currentVal + 1);
        } else {
            input.val(1);
        }
        input.dirtyForms('setDirty');
        input.change();

    });

    $("body").on("click", ".chleby-qty-remove", function (e) {
        e.preventDefault();
        var input = $(this).parent().find("input");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            input.val(null);
            input.change();
        }
    });

    $(".chleby-qty-reset").click(function (e) {
        e.preventDefault();
        var input = $(this).parent().find("input");
        input.val(input.data('originalvalue'));
        input.change();
    });

    // SELECT-RELOADS (go to href in value attribute of <option> after <select> change)
    $('.select-redirect').on('change', function () {
        var url = $(this).val(); // get selected value
        if (url) { // require a URL
            window.location = url; // redirect
        }
        return false;
    });
    $("#admin-actor").on("change", function () {
        document.getElementById('switcher').submit();
    });

    $("#price-filter").on("change", function (e) { // TODO: can switch to generic select-redirect?
        var href = $(".select2").select2('val');
        window.location = href;
    });

    // TOOLTIPS
    $('a[rel="tooltip"]').tooltip();
    $('.nav-switcher>li>a, [rel=tooltip]').tooltip();

    $('*[data-helppopover="true"]').popover({
        "html": true,
        "animation": false,
        "trigger": "manual"
    });

    var helpPopoversHidden = true;

    //przesunięcie dymków na poprawne pozycje po zmianie rozmiaru viewportu
    $(window).resize(debouncer(function (e) {
        if (!helpPopoversHidden) {
            $('*[data-helppopover="true"]').popover('hide').popover('show');
        }
    }));

    $('.has-popover').powerTip({ mouseOnToPopup: true, smartPlacement: true });
    $('.has-popover-manual').powerTip({ mouseOnToPopup: true, placement: 'sw' }); //special placement to prevent text running off the screen

    //	SELECT2 (filters!)
    var adminActorSelected = $(".admin-actor option[selected=selected]").attr('value');
    $(".admin-actor").val(adminActorSelected); //fix for back button in browser setting last remembered value
    $(".select2").select2();
    document.onscroll = function () {
        if ($('.select2').length > 0) {
            $(".select2-container-active").select2('close'); // called when the window is scrolled. 
        }
    };

    //DATEPICKER
    $("body").on("click", "input.datepicker", function () {

        $(this).datepicker({
            dateFormat: businessDateFormat,//set in _Layout
            altField: ".datepicker-autopopulate",
            firstDay: 1,
            monthNames: Globalize.culture().calendar.months.names,
            monthNamesShort: Globalize.culture().calendar.months.namesAbbr,
            dayNames: Globalize.culture().calendar.days.names,
            dayNamesShort: Globalize.culture().calendar.days.namesAbbr,
            dayNamesMin: Globalize.culture().calendar.days.namesShort,
        }).datepicker('show');
    });

    $('.toggle-help-popovers').toggle(function () {
        $('*[data-helppopover="true"]').popover('show');
        helpPopoversHidden = false;
    }, function () {
        $('*[data-helppopover="true"]').popover('hide');
        helpPopoversHidden = true;
    });

    $('body').on('click', '.dismiss-popover', function () {
        $(this).closest('.popover').hide();
    });


    $('.batch-attrs').click(function () {
        $('tbody input[type=radio]').attr('checked', false);
        $('tbody input[type=radio][value=' + $(this).val() + ']').attr('checked', true);
    });

    var attrTypes = ['hidden', 'internal', 'all'];
    $('.batch-attrs').attr('checked', false);
    for (var v in attrTypes) {
        var checkedAll = $('.attr-input[value=' + attrTypes[v] + ']:checked').length ==
            $('.attr-input[value=' + attrTypes[v] + ']').length;
        if (checkedAll) {
            $('.batch-attrs[value=' + attrTypes[v] + ']').attr('checked', true);
        }
    }
});






