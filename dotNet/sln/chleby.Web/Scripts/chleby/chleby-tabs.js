﻿$(document).ready(function () {

	//------------
	//BOOSTRAP TABS
	/*go to proper tab after rld*/
	var hash = document.location.hash;
	var prefix = "tab_";
	if (hash) {
		$('a[href=' + hash.replace(prefix, "") + ']').tab('show');
	}

	// Change hash for page-reload
	$('.order-addresses a').on('shown', function (e) {
		window.location.hash = e.target.hash.replace("#", "#" + prefix);
		fixRowSpanHeight();
	});
	//------------


	//------------
	//TABLES
	$("table.checkbox-batch input:checkbox.batch-all").click(function () {
		var checkedStatus = this.checked;
		var parentTable = $(this).parents("table.checkbox-batch");
		parentTable.find(' input:checkbox.batch').each(function () {
			console.log(this);
			this.checked = checkedStatus;
		});
	});

	$("input:checkbox.batch-all, input:checkbox.batch").click(function () {
		var parentTable = $(this).parents("table.checkbox-batch");
		if (parentTable.find('input:checkbox.batch:checked').length) {
			parentTable.find(".batch-actions").show();
		} else {
			parentTable.find(".batch-actions").hide();
		}
	});
	//------------
	
});

