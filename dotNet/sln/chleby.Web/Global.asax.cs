﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Xml.Serialization;
using AutoMapper;
using chleby.Web.Models;
using log4net;
using System.Data.Entity;

namespace chleby.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static List<CultureInfo> ExistingCultureList = new List<CultureInfo>();
        private static readonly ILog _log = LogManager.GetLogger(typeof(MvcApplication));
        private static string _cronRefreshPage;
        private readonly static WebClient _wc = new WebClient();
        public static Cron Cron = new Cron();
        public static List<AppRegister> AppRegister { get; private set; }
        public static List<MailTemplate> DefaultMailTemplates { get; private set; }

        public static void UpdateCachedDB()
        {
            using (var maindb = new MainAppContext())
            {
                AppRegister = maindb.AppRegister.AsNoTracking().ToList();
                ModuleRegister = maindb.ModuleRegister.Include(x => x.SubPerms).AsNoTracking().ToList();
                UsedModules = maindb.UsedModules.AsNoTracking().ToList();
                Help = maindb.SystemHelp.AsNoTracking().ToList();
            }

            if (_settings != null)
                foreach (var sett in _settings)
                    sett.Value.Dispose();

            _settings = new Dictionary<string, SettingsManager>();
            foreach (var app in AppRegister)
            {
                var db = new ChlebyContext(app);
                _settings.Add(app.Name, new SettingsManager(db));
            }
        }

        private static Dictionary<string, SettingsManager> _settings;

        public static Dictionary<string, SettingsManager> Settings
        {
            get { return _settings; }
        }

        public static List<ModuleRegister> ModuleRegister { get; private set; }
        public static List<UsedModule> UsedModules { get; private set; }
        public static List<MenuRoot> Menus { get; set; }
        public static List<SystemHelp> Help { get; set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();

            var ms = new MultiSnapshoter();
            Cron.Jobs.Add(new Cron.MinuteJob("snapshot", ms.Do));
            RegisterCacheEntry();

            UpdateCachedDB();

            var xs = new XmlSerializer(typeof(List<MenuRoot>));
            using (var fs = System.IO.File.OpenRead(Server.MapPath("~/menu.xml")))
                Menus = (List<MenuRoot>)xs.Deserialize(fs);

            DefaultMailTemplates = new List<MailTemplate>();
            var xsmt = new XmlSerializer(typeof(List<MailTemplate>));
            using (var fs = System.IO.File.OpenRead(System.Web.HttpContext.Current.
                            Server.MapPath("~/data/defaultMailTemplates.xml")))
            {
                var mts = (List<MailTemplate>)xsmt.Deserialize(fs);
                foreach (var mt in mts)
                {
                    mt.Id = 0;
                    DefaultMailTemplates.Add(mt);
                }
            }

            Mapper.CreateMap<BusinessEmployeeAccount, BusinessEmployee>().IgnoreAllNonExisting();
            Mapper.CreateMap<TraderEmployeeAccount, TraderEmployee>().IgnoreAllNonExisting();


            Mapper.CreateMap<Business, GeneralSettings>();
            Mapper.CreateMap<Business, CompanySettings>();
            Mapper.CreateMap<Business, InvoiceSettings>().IgnoreAllNonExisting();
            Mapper.CreateMap<GeneralSettings, Business>().IgnoreAllNonExisting();
            Mapper.CreateMap<CompanySettings, Business>().IgnoreAllNonExisting();
            Mapper.CreateMap<InvoiceSettings, Business>().IgnoreAllNonExisting();

            Mapper.CreateMap<AttributeMeta, AttrMetaVM>().IgnoreAllNonExisting().
                AfterMap((s, d) =>
                {
                    if (!s.ShowToTrader && !s.ShowInternal)
                        d.Visible = 0; //hidden
                    else if (s.ShowInternal && !s.ShowToTrader)
                        d.Visible = 1; //internal
                    else
                        d.Visible = 2; //all
                });

            Mapper.CreateMap<AttrMetaVM, AttributeMeta>().IgnoreAllNonExisting().AfterMap(
                (s, d) =>
                {
                    if (s.Visible == 0)
                    {
                        d.ShowInternal = false;
                        d.ShowToTrader = false;
                    }
                    else if (s.Visible == 1)
                    {
                        d.ShowInternal = true;
                        d.ShowToTrader = false;
                    }
                    else
                    {
                        d.ShowInternal = true;
                        d.ShowToTrader = true;
                    }
                });

            //Mapper.AssertConfigurationIsValid();

            ModelBinders.Binders.DefaultBinder = new DefaultDictionaryBinder();
            ControllerBuilder.Current.SetControllerFactory(new LangControllerFactory());

            var a = Assembly.GetExecutingAssembly();
            ExistingCultureList.Add(CultureInfo.GetCultureInfo("en"));
            foreach (var c in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                try
                {
                    if (a.GetSatelliteAssembly(c) != null)
                    {
                        ExistingCultureList.Add(c);
                        _log.InfoFormat("found {0} ({1}) resource", c.NativeName, c.EnglishName);
                    }
                }
                catch
                {
                }
            }
        }

        private bool RegisterCacheEntry()
        {
            if (null != HttpContext.Current.Cache[Defines.CronCacheKey]) return false;

            //removing crono
            //_log.Info("registering cron cache entry");
            //HttpContext.Current.Cache.Add(Defines.CronCacheKey, "Test", null,
            //    DateTime.MaxValue, TimeSpan.FromMinutes(2),
            //    CacheItemPriority.Normal,
            //    new CacheItemRemovedCallback(CronoTrigger));

            return true;
        }

        public void CronoTrigger(string key,
            object value, CacheItemRemovedReason reason)
        {
            _log.Debug("fakecron cache expired");

            Cron.ExecuteJobs();

            if (_cronRefreshPage != null)
            {
                _wc.DownloadString(_cronRefreshPage);
            }
            else
            {
                _log.Warn("couldn't wake up cron (refreshpage url not set)");
            }
        }

        protected void Application_BeginRequest()
        {
            _cronRefreshPage = new Uri(
                new Uri(Request.Url.GetLeftPart(UriPartial.Authority)),
                "/FakeCron/Refresh.aspx").ToString();

            Context.Items.Add("rendertime", DateTime.Now);

            //if (HttpContext.Current.Request.Url.ToString() == _cronRefreshPage)
            //{
            //    _log.Info("forcing cron refresh via magic url");
            //    RegisterCacheEntry();
            //    Response.StatusCode = 200;
            //    Response.ContentType = "text/plain";
            //    Response.Write("niemiecki metakapelusz namietnosci");
            //    Response.End();
            //}

            /*string lang = "pl-PL";

            CultureInfo ci = new CultureInfo(lang);
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);*/
        }
    }
}