﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Resources;
using System.Web;
using chleby.Web.Resources;

namespace chleby.Web.Models
{
    public class AuditLog
    {
        public AuditLog()
        {
            try
            {
                EndPoint = HttpContext.Current.Request.UserHostAddress;
            }
            catch { }
            Date = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public string User { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        public string Action { get; set; }

        public string EndPoint { get; private set; }

        public string Module { get; set; }

        public override string ToString()
        {
            var fmt = Strings.ResourceManager.GetString("Format_AuditLog_General_" + Action);
            if (fmt == null)
                fmt = "@@ User {0} {1} at {2} @@";
            return string.Format(fmt, User, Action, Date, ObjectId);
        }

        public int? ObjectId { get; set; }

        public int? IntValue { get; set; }
    }

}