﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class Business : ICloneable
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string LegalName { get; set; }


        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingName")]
        public string TradingName { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address1")]
        [StringLength(200)]
        public string LegalAddress1 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address2")]
        public string LegalAddress2 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address3")]
        public string LegalAddress3 { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress1")]
        [StringLength(200)]
        public string TradingAddress1 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress2")]
        public string TradingAddress2 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress3")]
        public string TradingAddress3 { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "City")]
        [StringLength(50)]
        public string LegalCity { get; set; }

        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "City")]
        public string TradingCity { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PostCode")]
        [StringLength(200)]
        public string LegalPost { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PostCode")]
        public string TradingPost { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PhoneNumber")]
        public string LegalPhone { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "PhoneNumber")]
        public string TradingPhone { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TimeZone")]
        [StringLength(200)]
        public string Timezone { get; set; }

        [Required]
        [StringLength(5)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "CurrencySymbol")]
        public string Currency { get; set; }

        [Required]
        public bool UsingTradingProps { get; set; }

        [Required]
        public int DeliveryDayValue { get; set; }

        public IEnumerable<int> DeliveryDays
        {
            get { return IntToDoWs(DeliveryDayValue); }
            set { DeliveryDayValue = DoWsToInt(value); }
        }

        [Display(ResourceType = typeof(Resources.Strings), Name = "OrderDeadline")]
        [Column(TypeName = "datetime2")]
        public DateTime CraftingDeadline { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "OrderDeadline")]
        [NotMapped]
        public DateTime CraftingDeadlineLocal { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastSnapshotDate { get; set; }

        public string DateFormat { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "PaymentInfo")]
        public string PaymentInfo { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "InvoiceFooter")]
        public string InvoiceFooter { get; set; }

        [NotMapped]
        [ValidateFile(AllowedFileExtensions = new[] { ".png", ".jpg", ".gif" }, MaxContentLength = 1024 * 512)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "LogoFile")]
        public HttpPostedFileBase LogoFile { get; set; }

        public string Logo { get; set; }

        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "MailConfirmAddress")]
        public string MailConfirmAddress { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Settings_SendOrdersBackup")]
        public bool SendOrdersBackup { get; set; }

        [Required]
        public int HandwalkStep { get; set; }

        [Required]
        [Range(0, 15)]
        public int CurrencyNegativePattern { get; set; }

        [Required]
        [Range(0, 4)]
        public int CurrencyPositivePattern { get; set; }

        public string NIP { get; set; }

        public string REGON { get; set; }

        [Required]
        public bool LogoOnInvoice { get; set; }

        [Required]
        public decimal MinDelivery { get; set; }

        public int WeightSystem { get; set; }

        public string ShortDateFormat { get; set; }

        public string DateFormatJs { get; set; }
        public string LongDateFormat { get; set; }

        public static IEnumerable<int> IntToDoWs(int value)
        {
            var ret = new List<int>();
            if ((value & 1) == 1)
                ret.Add(0);
            if ((value & 2) == 2)
                ret.Add(1);
            if ((value & 4) == 4)
                ret.Add(2);
            if ((value & 8) == 8)
                ret.Add(3);
            if ((value & 16) == 16)
                ret.Add(4);
            if ((value & 32) == 32)
                ret.Add(5);
            if ((value & 64) == 64)
                ret.Add(6);
            return ret;
        }

        public static int DoWsToInt(IEnumerable<int> value)
        {
            int ret = 0;
            foreach (var dow in value)
            {
                if (dow == 0)
                    ret |= 1;
                if (dow == 1)
                    ret |= 2;
                if (dow == 2)
                    ret |= 4;
                if (dow == 3)
                    ret |= 8;
                if (dow == 4)
                    ret |= 16;
                if (dow == 5)
                    ret |= 32;
                if (dow == 6)
                    ret |= 64;
            }
            return ret;
        }

        public override string ToString()
        {
            return Id.ToString(CultureInfo.InvariantCulture);
        }

        public virtual XeroConfig XeroConfig { get; set; }

        public int AccSoftware { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        
    }

    

    public static class BusinessExtensions
    {
        public static string ConvertWeithtUp(this decimal weitht, Business b)
        {
            switch ((WeightSystem) b.WeightSystem)
            {
                case WeightSystem.Metric: return (weitht / 10).ToString("0") + "kg";
                case WeightSystem.Imperial: return (weitht / 16).ToString("0") + "lb";
            }

            return (weitht / 10).ToString("0") + "kg";
        }
    }

    public class GeneralSettings
    {
        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TimeZone")]
        [StringLength(200)]
        public string Timezone { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "OrderDeadline")]
        [Column(TypeName = "datetime2")]               
        public DateTime CraftingDeadline { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "OrderDeadline")]
        [NotMapped]
        public DateTime CraftingDeadlineLocal { get; set; }

        [NotMapped]
        [ValidateFile(AllowedFileExtensions = new[] { ".png", ".jpg", ".gif" }, MaxContentLength = 1024 * 512)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "LogoFile")]
        public HttpPostedFileBase LogoFile { get; set; }

        public string Logo { get; set; }

        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "MailConfirmAddress")]
        public string MailConfirmAddress { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Settings_SendOrdersBackup")]
        public bool SendOrdersBackup { get; set; }

        [Required]
        public int DeliveryDayValue { get; set; }

        public IEnumerable<int> DeliveryDays
        {
            get { return Business.IntToDoWs(DeliveryDayValue); }
            set { DeliveryDayValue = Business.DoWsToInt(value); }
        }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "LogoOnInvoice")]
        public bool LogoOnInvoice { get; set; }
    }

    public class CompanySettings
    {
        [Required]
        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string LegalName { get; set; }

        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingName")]
        public string TradingName { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address1")]
        [StringLength(200)]
        public string LegalAddress1 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address2")]
        public string LegalAddress2 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address3")]
        public string LegalAddress3 { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress1")]
        [StringLength(200)]
        public string TradingAddress1 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress2")]
        public string TradingAddress2 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TradingAddress3")]
        public string TradingAddress3 { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "City")]
        [StringLength(50)]
        public string LegalCity { get; set; }

        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "City")]
        public string TradingCity { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PostCode")]
        [StringLength(200)]
        public string LegalPost { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PostCode")]
        public string TradingPost { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PhoneNumber")]
        public string LegalPhone { get; set; }

        //[Display(ResourceType = typeof(Resources.Strings), Name = "PhoneNumber")]
        [NotMapped]
        public string TradingPhone { 
            get { return LegalPhone; }
            set { LegalPhone = value; }
        }

        [Required]
        public bool UsingTradingProps { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "NIP")]
        public string NIP { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "REGON")]
        public string REGON { get; set; }
    }

    public class InvoiceSettings
    {
        [Required]
        [StringLength(5)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "CurrencySymbol")]
        public string Currency { get; set; }

        [AutoMapper.IgnoreMap]
        [Required]
        public DayOfWeek FirstDay { get; set; }

        [AutoMapper.IgnoreMap]
        [Required]
        [RegularExpression("[dwma]")]
        public string InvoicePeriod { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "PaymentInfo", Prompt = "PaymentInfo_Placeholder")]
        public string PaymentInfo { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "InvoiceFooter", Prompt = "InvoiceFooter_Placeholder")]
        public string InvoiceFooter { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Business_NegativeCurrencyPattern")]
        public int CurrencyNegativePattern { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Business_PositiveCurrencyPattern")]
        public int CurrencyPositivePattern { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "MinDelivery")]
        public decimal MinDelivery { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "AccSoftware")]
        public int AccSoftware { get; set; }
    }

    public class FinishVM
    {
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Guid Guid { get; set; }
    }
}