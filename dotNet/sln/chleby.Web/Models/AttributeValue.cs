﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AttributeValue
    {
        [Key]
        public int Id { get; set; }

        public string Value { get; set; }

        public int MetadataId { get; set; }

        [Required]
        [ForeignKey("MetadataId")]
        public virtual AttributeMeta Metadata { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] @{2}", Id, Value, Metadata);
        }
    }
}