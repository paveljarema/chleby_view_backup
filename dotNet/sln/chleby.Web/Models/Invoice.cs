﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.Mappers;
using MigraDoc.DocumentObjectModel.Tables;

namespace chleby.Web.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }

        public int Address_Id { get; set; }

        [ForeignKey("Address_Id")]
        public virtual Address Address { get; set; }

        public virtual Trader Trader { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        public bool Paid { get; set; }

        public string PaymentTerm { get; set; }
        
        public int Year { get; set; }

        public int InvoiceNumber { get; set; }

        public bool Deleted { get; set; }

        public int Revision { get; set; }

        public bool Cloned { get; set; }

        [InverseProperty("MyParent")]
        public virtual ICollection<Invoice> MyClones { get; set; }

        public virtual Invoice MyParent { get; set; }

        public bool Published { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public bool Exported { get; set; }

        public int DeliveryAddress_Id { get; set; }

        [ForeignKey("DeliveryAddress_Id")]
        public virtual Address DeliveryAddress { get; set; }

        public decimal ItemsPrice { get; set; } //
        public decimal Vat { get; set; } //
        public decimal DeliveryPrice { get; set; }
        public decimal DeliveryVat { get; set; }
        public int DeliveryCount { get; set; }
        public decimal TotalPrice { get; set; } //

        public virtual ICollection<InvoiceRow> Rows { get; set; }

        public int Week
        {
            get
            {
                return new GregorianCalendar().
                    GetWeekOfYear(this.Date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            }
        }

        public Invoice Clone()
        {
            if (Cloned)
                throw new InvalidOperationException("invoice already cloned.");
            if (Deleted)
                throw new InvalidOperationException("invoice deleted.");
            if (!Published)
                throw new InvalidOperationException("invoice unpublished.");

            var clone = new Invoice
            {
                Address_Id = Address_Id,
                Address = Address,
                DeliveryAddress_Id = DeliveryAddress_Id,
                DeliveryAddress=DeliveryAddress,
                ItemsPrice = ItemsPrice,
                Vat=Vat,
                DeliveryCount=DeliveryCount,
                DeliveryPrice=DeliveryPrice,
                DeliveryVat=DeliveryVat,
                TotalPrice=TotalPrice,
                Trader = Trader,
                Date = Date,
                Paid = Paid,
                PaymentTerm = PaymentTerm,
                Year = Year,
                InvoiceNumber = InvoiceNumber,
                Deleted = Deleted,
                Revision = Revision + 1,
                Published = false,
                StartDate = StartDate,
                EndDate = EndDate,
                MyParent = this,
            };
            this.MyClones = new Collection<Invoice> { clone };
            clone.Rows=new Collection<InvoiceRow>();
            foreach (var row in Rows)
                clone.Rows.Add(row.Clone());
            this.Cloned = true;
            return clone;
        }

        public void PublishClone()
        {
            if (Cloned || !Published)
            {
                Published = true;
                if (MyParent != null)
                    MyParent.Published = false;
            }
        }

        public List<Tuple<int, int, int>> Parents
        {
            get
            {
                var parents = new List<Tuple<int, int, int>>();
                var node = this;
                do
                {
                    parents.Add(Tuple.Create(node.InvoiceNumber, node.Revision, node.Id));
                } while ((node = node.MyParent) != null);
                return parents;
            }
        }

        public List<Tuple<int, int, int>> AllClones
        {
            get
            {
                var clones = new List<Tuple<int, int, int>>();
                var node = this;
                while ((node = node.MyClones.FirstOrDefault()) != null)
                {
                    clones.Add(Tuple.Create(node.InvoiceNumber, node.Revision, node.Id));
                }
                return clones;
            }
        }
    }

    public class InvoiceRow
    {
        [Key]
        public int Id { get; set; }

        public int? Item_Id { get; set; }

        [ForeignKey("Item_Id")]
        public virtual WorldEntity Item { get; set; }

        public string Text { get; set; }

        public decimal Count { get; set; }

        public decimal Price { get; set; }

        public decimal Vat { get; set; }

        public virtual ICollection<InvoiceRowDayData> DayData { get; set; }

        [InverseProperty("Rows")]
        public virtual Invoice Invoice { get; set; }

        [NotMapped]
        public int DmgItemId { get; set; }

        [NotMapped]
        public DmgItem DmgItem { get; set; }

        public string Info { get; set; }

        public InvoiceRow Clone()
        {
            var clone = new InvoiceRow
                {
                    Item = Item,
                    Text = Text,
                    Count = Count,
                    Price = Price,
                    Vat = Vat,
                    DmgItem = DmgItem,
                    DmgItemId = DmgItemId,
                };
            clone.DayData = new Collection<InvoiceRowDayData>();
            foreach (var dd in DayData)
                clone.DayData.Add(dd.Clone());
            return clone;
        }
    }

    public class InvoiceRowDayData
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public decimal Count { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal Vat { get; set; }

        //public virtual InvoiceRow Row { get; set; }

        public InvoiceRowDayData Clone()
        {
            return new InvoiceRowDayData
                {
                    Count = Count,
                    Date = Date,
                    Price = Price,
                    Vat = Vat
                };
        }
    }

    public class InvoiceWeekData
    {
        public Address Address { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal VatPrice { get; set; }
        public decimal DeliveryPrice { get; set; }
        public bool Published { get; set; }
        public bool Cloned { get; set; }
        public int Id { get; set; }
        public int InvoiceNumber { get; set; }
        public bool HasZeroPrice { get; set; }
        public bool HasManualChanges { get; set; }
        public bool HasComplaints { get; set; }
        public bool HasNoOrder { get; set; }
        public bool Deleted { get; set; }
        public int Revision { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateLocal { get; set; }
        public Invoice Parent { get; set; }
        public Invoice Child { get; set; }

        public string TraderName { get; set; }
        public bool Exported { get; set; }
    }

    public class InvoiceWeekSummary
    {
        public DateTime? Day { get; set; }
        public DateTime? DayLocal { get; set; }
        public int? Week { get; set; }
        public int? Month { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public decimal? Delivery { get; set; }
        public decimal Vat { get; set; }
    }

   

    public class InvoiceCreateModel
    {
        //[Remote("CheckInvoiceNumber", "Invoices")]
        //public int Number { get; set; }

        public DateTime Date { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public DateTime DateLocal { get; set; }
        public DateTime StartDateLocal { get; set; }
        public DateTime EndDateLocal { get; set; }

        public int InvoiceAddressId { get; set; }
        public int DeliveryAddressId { get; set; }

        public string PaymentTerm { get; set; }

        public List<P<string, Dictionary<DateTime, decimal>>> FieldCount { get; set; }
        public List<P<string, Dictionary<DateTime, decimal>>> FieldPrice { get; set; }
        public Dictionary<int, decimal> Vats { get; set; }
        public Dictionary<int, string> Notes { get; set; }

        public Dictionary<DateTime, int> DeliveryCount { get; set; }
        public Dictionary<DateTime, decimal> DeliveryPrice { get; set; }

        public InvoiceCreateModel()
        {
            FieldCount = new List<P<string, Dictionary<DateTime, decimal>>>();
            FieldPrice = new List<P<string, Dictionary<DateTime, decimal>>>();
            Vats = new Dictionary<int, decimal>();

            Notes = new Dictionary<int, string>();

            DeliveryCount = new Dictionary<DateTime, int>();
            DeliveryPrice = new Dictionary<DateTime, decimal>();
        }

        //create
        public int OldNumber { get; set; }

        //addrow
        public string NewItem { get; set; }
        public string NewItemId { get; set; }
        public Dictionary<DateTime, decimal> NewRow { get; set; }
    }

    public class InvoiceCreateRowModel
    {
        public string Item { get; set; }
        public Dictionary<DateTime, decimal> Counts { get; set; }
        public Dictionary<DateTime, decimal> Prices { get; set; }

        public decimal Total { get; set; }
        public decimal Vat { get; set; }
    }

    public class InvoicePrepModel
    {
        public bool DataInPast { get; set; }
        public IEnumerable<int> TakenNumbers { get; set; }

        public IEnumerable<InvoiceWeekData> Summary { get; set; }

        public int FirstNumber { get; set; }
    }

    public class P<T1, T2>
    {
        public T1 Key { get; set; }
        public T2 Value { get; set; }

        public P()
        {
        }

        public P(T1 item1, T2 item2)
        {
            Key = item1;
            Value = item2;
        }
    }
    public static class P
    {
        public static P<T1, T2> Create<T1, T2>(T1 a, T2 b)
        {
            return new P<T1, T2>(a, b);
        }
    }


    public class InvoicePrepRetModel
    {
        public int Number { get; set; }
        public bool Publish { get; set; }
    }
}

