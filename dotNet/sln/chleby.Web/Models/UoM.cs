﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class UnitOfMeasure
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    public class UoMConversion
    {
        [Key]
        public int Id { get; set; }

        public int FromId { get; set; }

        [ForeignKey("FromId")]
        [Required]
        public virtual UnitOfMeasure From { get; set; }

        public int ToId { get; set; }

        [ForeignKey("ToId")]
        [Required]
        public virtual UnitOfMeasure To { get; set; }

        [Required]
        [DefaultValue(1.0)]
        public decimal FromMultiplier { get; set; }

        [Required]
        [DefaultValue(1.0)]
        public decimal ToMultiplier { get; set; }
    }
}