﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class BusinessHoliday
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "HolidayStartD")]
        [Column(TypeName = "datetime2")] //chyba rozwaliło validacje i umiera po cichu, bo holidaye sie nie store'ują
        public DateTime StartDate { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "HolidayEndD")]
        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2:d}->{3:d}", Id, Name, StartDate, EndDate);
        }
    }
}