﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AppSnapshots
    {
        [Key]
        public int Id { get; set; }
        public int AppRegisterId { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime Deadline { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime LastSnapshotDate { get; set; }

    }

    public class AppSnapshotsConfiguration : EntityTypeConfiguration<AppSnapshots>
    {
        public AppSnapshotsConfiguration()
        {
            ToTable("AppSnapshots");
            Property(c => c.AppRegisterId).HasColumnName("AppRegisterId");
            Property(c => c.Deadline).HasColumnName("Deadline");
            Property(c => c.LastSnapshotDate).HasColumnName("LastSnapshotDate");
        }
    }
}