﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using chleby.Web.Resources;

namespace chleby.Web.Models
{
    public class MenuNode
    {
        [XmlAttribute]
        public string Id { get; set; }

        [XmlAttribute("Name")]
        public string XmlName { get; set; }

        [XmlIgnore]
        public string Name
        {
            get
            {
                return Strings.ResourceManager.GetString("menu_" + Id) ?? XmlName;
            }
        }

        [XmlAttribute]
        public string Action { get; set; }

        [XmlAttribute]
        public string Controller { get; set; }

        [XmlAttribute]
        public string Role { get; set; }


        public List<MenuNode> Nodes { get; set; }

        public MenuNode()
        {
            Nodes = new List<MenuNode>();
        }

        public MenuNode(string name, string action, string controller, string role)
            : this()
        {
            XmlName = name;
            Action = action;
            Controller = controller;
            Role = role;
        }

        public MenuNode(string name, string action, string controller, string role, List<MenuNode> nodes)
        {
            XmlName = name;
            Action = action;
            Controller = controller;
            Role = role;
            Nodes = nodes;
        }
    }

    public class MenuRoot
    {
        [XmlAttribute]
        public string Name { get; set; }

        public List<MenuNode> Nodes { get; set; }
    }
}