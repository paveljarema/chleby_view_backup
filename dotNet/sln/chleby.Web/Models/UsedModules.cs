﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class UsedModule
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public virtual AppRegister App { get; set; }

        [Required]
        public virtual ModuleRegister Module { get; set; }

        public string Alias { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} {1} -> {2}", Id, Module, App);
        }

    }
}