﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AddressHoliday
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "HolidayStartD")]
        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "HolidayEndD")]
        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public virtual Address Address { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2:d}->{3:d} @[{4}]", Id, Name, StartDate, EndDate, Address);
        }
    }
}
