﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class TraderManager
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public int TraderId { get; set; }

        [ForeignKey("TraderId")]
        public virtual Trader Trader { get; set; }
    }
}