﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class FrontPageContactModel
    {
        [Required]
        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        public string Email { get; set; }

        [Required]
        public string BusinessName { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
    }
}