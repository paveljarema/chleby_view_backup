﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class Item : WorldEntity, IComparable<Item>
    {
        public Item()
        {
            Live = true;
        }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Description")]
        public string AdditionalInfo { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Live")]
        public bool Live { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        [Range(0, 100)]
        public decimal Vat { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "BarcodeType")]
        public string BarcodeType { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "BarcodeValue")]
        public string BarcodeValue { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "Tags")]
        public string Tags { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "Weight")]
        public decimal Weight { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ProductionTime")]
        [Range(0, 365)]
        public int CraftingTime { get; set; } //days!

        public int? ItemGroupId { get; set; }

        [ForeignKey("ItemGroupId")]
        public virtual ItemGroup ItemGroup { get; set; }

        public virtual ICollection<AttributeValue> Attributes { get; set; }

        public virtual ICollection<Price> Prices { get; set; }

        public int CompareTo(Item other)
        {
            return string.Compare(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
        }

        public override string ToString()
        {
            return string.Format("#{0} ITEM [{1}] @[{2}] {3}", Id, Name, ItemGroup, Live ? "L" : "-");
        }

        public virtual ICollection<VisibleTo> Visibility { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Price")]
        public decimal MagicPrice
        {
            get { return 0m; }
            //get { return Prices == null ? 0 : Prices.OfType<ItemPrice>().FirstOrDefault().Value; }
        }

        [NotMapped]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ItemPrice")]
        public decimal ItemPriceValue
        {
            get { return Prices == null ? 0 : Prices.OfType<ItemPrice>().FirstOrDefault().Value; }
        }

        [NotMapped]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ImageFile")]
        [ValidateFile(AllowedFileExtensions = new[] { ".png", ".jpg" }, MaxContentLength = 1024 * 512)]
        public HttpPostedFileBase ImageFile { get; set; }


        public string Image { get; set; }

        public override Price GetItemPrice(object arg)
        {
            var trader = (Trader)arg;
            Price price = Prices.OfType<TraderPrice>().FirstOrDefault(x => x.TraderId == trader.Id);
            if (price == null)
            {
                price = Prices.OfType<TraderTypePrice>().FirstOrDefault(x => x.TraderTypeId == trader.TypeId);
                if (price == null)
                {
                    price = this.Prices.OfType<ItemPrice>().FirstOrDefault();
                }
            }
            return price;
        }

        public override IComparer CreateComparer(object arg)
        {
            return new ItemListComparatorUniversal((Trader)arg);
        }

        public bool CanOrder(Business b, DateTime date)
        {
            var now = DateTime.UtcNow;
            if (DateTime.UtcNow.TimeOfDay > b.CraftingDeadline.TimeOfDay)
                now = now.AddDays(1); //po deadline czyli juz jest "jutro"
            if (now.AddDays(this.CraftingTime) > date) //plus delay itemu
                return false;
            else
                return true;
        }
    }

    public class ItemListComparator : IComparer<Item>
    {
        private readonly Trader _trader;

        public ItemListComparator(Trader trader)
        {
            _trader = trader;
        }

        public int Compare(Item x, Item y)
        {
            return new ItemListComparatorUniversal(_trader).Compare(x, y);
        }
    }

    public class ItemListComparatorUniversal : IComparer
    {
        private readonly Trader _trader;

        public ItemListComparatorUniversal(Trader trader)
        {
            _trader = trader;
        }

        public int Compare(object ox, object oy)
        {
            var x = (Item)ox;
            var y = (Item)oy;
            if (_trader == null)
            {
                var ord = x.ItemGroup.Order.CompareTo(y.ItemGroup.Order);
                if (ord == 0)
                    return string.Compare(x.Name, y.Name, StringComparison.InvariantCultureIgnoreCase);
                else
                    return ord;
            }

            var vix = x.Visibility.FirstOrDefault(i => i.TraderId == _trader.Id);
            var viy = y.Visibility.FirstOrDefault(i => i.TraderId == _trader.Id);

            var fx = vix == null ? false : vix.Favourite;
            var fy = viy == null ? false : viy.Favourite;

            var cfav = fx.CompareTo(fy);
            if (cfav == 0)
            {
                var ord = x.ItemGroup.Order.CompareTo(y.ItemGroup.Order);
                if (ord == 0)
                    return string.Compare(x.Name, y.Name, StringComparison.InvariantCultureIgnoreCase);
                else
                    return ord;
            }
            else
                return -cfav;
        }
    }
}