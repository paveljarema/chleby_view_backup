﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class MailTemplate
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        [DataType(DataType.MultilineText)]
        public string Template { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public bool Send { get; set; }
    }
}