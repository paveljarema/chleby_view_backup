﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class XeroTaxMapping
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Tax")]
        public decimal Tax { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string Name { get; set; }
    }

    public class XeroConfig
    {
        [Display(ResourceType = typeof(Resources.Strings), Name = "XeroSettings_SalesAccount")]
        [Required(ErrorMessageResourceType = typeof(Resources.Strings),ErrorMessageResourceName = "XeroSettings_SalesAccount_Error")]
        public int SalesAccount { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "XeroSettings_PurchaseAccount")]
        [Required(ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "XeroSettings_PurchaseAccount_Error")]
        public int PurchaseAccount { get; set; }
    }

    public class XeroSettings
    {
        public XeroConfig Config { get; set; }
        public IEnumerable<XeroTaxMapping> Taxes { get; set; }
    }
}