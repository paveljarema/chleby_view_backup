﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class ExportItemsViewModel
    {
        public IList<ItemAttribute> Attributes { get; set; }
    }

    public class ItemAttribute
    {
        public string DisplayName { get; set; }
        public string PropertyName { get; set; }
        public bool IsCustom { get; set; }
        public bool Selected { get; set; }
    }
        
}