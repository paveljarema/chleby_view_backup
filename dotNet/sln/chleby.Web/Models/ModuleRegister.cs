﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Web;
using chleby.Web.Resources;

namespace chleby.Web.Models
{
    public class ModuleRegister : IComparable<ModuleRegister>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public bool IsCore { get; set; }

        [Required]
        public string Name { get; set; }

        public string HumanName
        {
            get
            {
                var hn = Strings.ResourceManager.GetString("module_" + Name);
                if (hn == null) return "@@module_" + Name + "@@";
                else return hn;
            }
        }

        [Required]
        public int Order { get; set; }

        [Required]
        public bool Admin { get; set; }

        public int CompareTo(ModuleRegister other)
        {
            var byOrder = this.Order.CompareTo(other.Order);
            if (byOrder == 0)
                return string.Compare(this.Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
            else return byOrder;
        }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2}", Id, Name, Admin ? "A" : "T");
        }

        public virtual ICollection<SubPerm> SubPerms { get; set; }

        public string Category { get; set; }
    }

    public class SubPerm
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ModuleRegister Parent { get; set; }

        public string HumanName
        {
            get
            {
                var hn = Strings.ResourceManager.GetString("perm_" + Name);
                if (hn == null) return "@@perm_" + Name + "@@";
                else return hn;
            }
        }
    }
}