﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class Supplier
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<SuppliedEntity> Supplies { get; set; }
    }

    public class SuppliedEntity
    {
        [Key]
        public int Id { get; set; }

        public int Entity_Id { get; set; }

        [ForeignKey("Entity_Id")]
        [Required]
        public virtual WorldEntity Entity { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public virtual UnitOfMeasure Unit { get; set; }

        public int Supplier_Id { get; set; }

        [ForeignKey("Supplier_Id")]
        public virtual Supplier Supplier { get; set; }

        public decimal Price { get; set; }
    }
}