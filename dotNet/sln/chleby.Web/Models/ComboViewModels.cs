﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class ComboViewModel<T>: ComboViewModel<T,T>
    {
    }

    public class ComboViewModel<T, R>
    {
        public IEnumerable<R> Index { get; set; }
        public T Create { get; set; }
        public T Edit { get; set; }
    }

    public class ComboViewModelQuad<T, R, S> : ComboViewModel<T, R>
    {
        public S Create2 { get; set; }
    }

    public class ComboViewModelPenta<T, R, S> : ComboViewModelQuad<T, R, S>
    {
        public S Edit2 { get; set; }
    }

    public class DualModel<T, R>
    {
        public IEnumerable<T> One { get; set; }
        public IEnumerable<R> Two { get; set; }
    }

    public class FakeGrouping<T, R> : IGrouping<T, R>
    {
        private readonly T _key;

        public FakeGrouping(T key)
        {
            _key = key;
        }

        public T Key
        {
            get { return _key; }
        }

        public IEnumerator<R> GetEnumerator()
        {
            return Enumerable.Empty<R>().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (IEnumerator)Enumerable.Empty<R>().GetEnumerator();
        }
    }
}