﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class UserPwdResetInfo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public DateTime ResetDate { get; set; }

        [Required]
        public Guid Token { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} {1:d} {2} -> {3}", Id, ResetDate, UserId, Token);
        }
    }
}