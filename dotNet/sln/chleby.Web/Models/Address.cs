﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace chleby.Web.Models
{
    public class Address
    {
        public Address()
        {
            TraderEmployees = new Collection<TraderEmployee>();
            Live = true;
        }

        public bool HasAdhoc(DateTime date)
        {
            return this.Orders.OfType<AdhocOrder>().Any(x => x.Date.Date == date.Date);
        }

        public decimal GetOrderValue(DateTime date)
        {
            var dayofweek = (int)date.DayOfWeek - 1;
            if (dayofweek < 0)
            {
                dayofweek += 7;
            }
            var standing = this.Orders.OfType<StandingOrder>().FirstOrDefault(x => x.Day == dayofweek);
            var adhoc = this.Orders.OfType<AdhocOrder>().FirstOrDefault(x => x.Date.Date == date.Date);

            var fromso = new Dictionary<Item, int>();
            var fromao = new Dictionary<Item, int>();
            var final = new Dictionary<Item, int>();

            if (standing != null)
            {
                fromso =
(from pos in standing.Items
 select new { pos.Item, pos.OrderedCount }).ToDictionary(ks => ks.Item, es => es.OrderedCount);

            }

            if (adhoc != null)
            {
                fromao =
(from pos in adhoc.Items
 select new { pos.Item, pos.OrderedCount }).ToDictionary(ks => ks.Item, es => es.OrderedCount);

            }


            foreach (var so in fromso)
            {
                if (fromao.ContainsKey(so.Key))
                {
                    final.Add(so.Key, fromao[so.Key]);
                }
                else
                {
                    final.Add(so.Key, fromso[so.Key]);
                }
            }

            foreach (var so in fromao)
            {
                if (!final.ContainsKey(so.Key))
                {
                    final.Add(so.Key, fromao[so.Key]);
                }
            }



            decimal daytotal = 0;
            foreach (var kvp in final)
            {
                decimal price = kvp.Key.GetItemPrice(this.Trader).Value;
                decimal vat = price * (kvp.Key.Vat / 100);
                daytotal = daytotal + kvp.Value * (vat + price);
            }
            return daytotal;
        }


        [Key]
        public int Id { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address1")]
        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address2")]
        public string Address2 { get; set; }

        [StringLength(200)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Address3")]
        public string Address3 { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PostCode")]
        [StringLength(200)]
        public string Post { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "DeliveryPrice")]
        [Column(TypeName = "Money")]
        public decimal DeliveryPrice { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Note")]
        public string SpecialNote { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "MapLink")]
        public string MapLink { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "LegacyCode")]
        [StringLength(50)]
        public string LegacyCode { get; set; }

        public int RoundOrder { get; set; }

        public int TraderId { get; set; }

        [ForeignKey("TraderId")]
        public virtual Trader Trader { get; set; }

        //[Required]
        //public int Invoice { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "InvoiceAddress")]
        public bool AddressIsPrimary { get; set; }

        public int? AddressGroupId { get; set; }

        [ForeignKey("AddressGroupId")]
        public virtual AddressGroup AddressGroup { get; set; }

        [Required]
        public bool Live { get; set; }

        public virtual ICollection<AddressHoliday> AddressHolidays { get; set; }

        public virtual ICollection<TraderEmployee> TraderEmployees { get; set; }

        public virtual ICollection<AddressPrice> AddressPrices { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] @[{2}]", Id, Address1, Trader.Name);
        }

        [Required]
        public int DeliveryDayValue { get; set; }

        public IEnumerable<int> DeliveryDays
        {
            get { return Business.IntToDoWs(DeliveryDayValue); }
            set { DeliveryDayValue = Business.DoWsToInt(value); }
        }
    }
}