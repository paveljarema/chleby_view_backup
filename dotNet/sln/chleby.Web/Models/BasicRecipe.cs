﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class BasicRecipe
    {
        [Key]
        public int Id { get; set; }

        public int Entity_Id { get; set; }

        [Required]
        [ForeignKey("Entity_Id")]
        public virtual WorldEntity Entity { get; set; }

        public int SuppliedEntity_Id { get; set; }

        [ForeignKey("SuppliedEntity_Id")]
        public virtual SuppliedEntity SuppliedEntity { get; set; }
    }
}