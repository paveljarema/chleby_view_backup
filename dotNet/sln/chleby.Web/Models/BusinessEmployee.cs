﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class BusinessEmployee : Employee
    {
        public int Favourite { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "IBAN")]
        public string Iban { get; set; }
    }
}