﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class PaymentTerm
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [StringLength(100)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "PaymentTerms")]
        public string Name { get; set; }

        public int Order { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}]", Id, Name);
        }

    }
}