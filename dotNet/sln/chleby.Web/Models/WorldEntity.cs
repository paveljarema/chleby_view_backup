﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace chleby.Web.Models
{
    [Table("Items")]
    public class WorldEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual int BaseUnitId { get; set; }

        [ForeignKey("BaseUnitId")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Unit")]
        public virtual UnitOfMeasure BaseUnit { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "LegacyCode")]
        [StringLength(50)]
        public string LegacyCode { get; set; }

        public string RecipeProvider { get; set; }

        public virtual RecipeProvider CreateProvider(ChlebyContext db)
        {
            Type type;
            if (RecipeProvider == null)
                type = typeof(NullRecipeProvider);
            else
                type = Type.GetType(RecipeProvider) ?? typeof(NullRecipeProvider);

            return (RecipeProvider)Activator.CreateInstance(type, new object[] {this, db});
        }

        public virtual Price GetItemPrice(object arg)
        {
            return new ItemPrice { Item = null, Value = 0 };
        }

        public virtual IComparer CreateComparer(object arg)
        {
            return new WorldEntityComparer();
        }

        public override int GetHashCode()
        {
            return Id ^ (Name == null ? 0 : Name.GetHashCode());
        }

        public override bool Equals(object obj)
        {
            if (obj is Item)
            {
                var item = (Item)obj;
                return this.Id == item.Id &&
                       this.Name == item.Name &&
                       this.LegacyCode == item.LegacyCode &&
                       this.BaseUnitId == item.BaseUnitId;
            }
            else
                return false;
        }

        public override string ToString()
        {
            return string.Format("WE-{0}", Id);
        }
    }

    public class WorldEntityComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return string.Compare(((WorldEntity)x).Name, ((WorldEntity)y).Name, 
                StringComparison.InvariantCultureIgnoreCase);
        }
    }

    public class UniversalWorldEntityComparer : IComparer<WorldEntity>
    {
        private readonly object _arg;

        public UniversalWorldEntityComparer(object arg)
        {
            _arg = arg;
        }

        public int Compare(WorldEntity x, WorldEntity y)
        {
            var typeX = x.GetType();
            var typeY = y.GetType();
            var cmpType = string.Compare(typeX.FullName, typeY.FullName, StringComparison.InvariantCultureIgnoreCase);
            if (cmpType == 0)
            {
                return x.CreateComparer(_arg).Compare(x, y);
            }
            else
            {
                return cmpType;
            }
        }
    }
}