﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class DmgItem
    {
        [Key]
        public int Id { get; set; }

        public int ItemId { get; set; }

        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime2")]
        public DateTime RaportDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Column(TypeName = "datetime2")]
        public DateTime DeliveryDate { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [NotMapped]
        public DateTime DeliveryDateLocal { get; set; }

        public decimal Quantity { get; set; }

        public int AddressId { get; set; }

        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        public bool Ack { get; set; }

        [Column(TypeName = "Money")]
        [DataType(DataType.Currency)]
        public decimal PricePerItem { get; set; }
    }

    public class ComplaintData
    {
        public ComplaintData(DateTime date, Item item, decimal count, Address address, decimal price, bool invoiced)
        {
            Date = date;
            Item = item;
            Count = count;
            Address = address;
            Price = price;
            Invoiced = invoiced;
        }

        public bool Invoiced { get; set; }
        public DateTime Date { get; set; }
        public Item Item { get; set; }
        public decimal Count { get; set; }
        public Address Address { get; set; }
        public decimal Price { get; set; }
    }
}