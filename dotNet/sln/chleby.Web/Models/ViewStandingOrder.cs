﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class ViewStandingOrder
    {
        [Key]
        public int OrderId { get; set; }
        public int AddressId { get; set; }
        public bool? Delivery { get; set; }
        public int Day { get; set; }
        public int PositionId { get; set; }
        public int OrderedCount { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int CraftingTime { get; set; }
        public string GroupName { get; set; }

        public override bool Equals(object obj)
        {
            var casted = obj as ViewStandingOrder;
            if (casted != null)
            {
                return casted.OrderId == OrderId;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return OrderId.GetHashCode();
        }
    }
}