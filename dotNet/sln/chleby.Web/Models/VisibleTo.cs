﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class VisibleTo
    {
        [Key]
        public int Id { get; set; }

        public bool HiddenByAdmin { get; set; }
        public bool HiddenByCustomer { get; set; }
        public bool Favourite { get; set; }
        public int ItemId { get; set; }
        public int TraderId { get; set; }


        [Required]
        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }

        [Required]
        [ForeignKey("TraderId")]
        public virtual Trader Trader { get; set; }

    }
}