﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AppRegister
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string DBConnectionString { get; set; }

        public int DeliveryId { get; set; }
        public int DefaultPTId { get; set; }
        public int DefaultTTId { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}]", Id, Name);
        }
    }
}