﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class TraderEmployee : Employee
    {
        public int TraderId { get; set; }

        [ForeignKey("TraderId")]
        public virtual Trader Trader { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Note")]
        public string Note { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "MainContact")]
        public bool MainContact { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format(" @[{0}]", Trader);
        }

    }
}