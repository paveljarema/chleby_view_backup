﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace chleby.Web.Models
{
    public class ChlebyContext : DbContext
    {
        public ChlebyContext(AppRegister appConfig)
            : base(appConfig.DBConnectionString)
        {
            AppConfig = appConfig;
        }

        public AppRegister AppConfig { get; private set; }

        public DbSet<BusinessEmployee> BusinessEmployees { get; set; }
        public DbSet<Business> Businesses { get; set; }
        public DbSet<DmgItem> DmgItems { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressHoliday> AddressHolidays { get; set; }
        public DbSet<AttributeMeta> AtributeMetas { get; set; }
        public DbSet<AttributeValue> AttributeValues { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<BusinessHoliday> BusinessHolidays { get; set; }
        public DbSet<ItemGroup> ItemGroups { get; set; }
        public DbSet<Trader> Traders { get; set; }
        public DbSet<TraderEmployee> TraderEmployees { get; set; }
        public DbSet<TraderType> TraderTypes { get; set; }
        public DbSet<UserPwdResetInfo> UserPwdResetInfos { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<PaymentTerm> PaymentTerms { get; set; }
        public DbSet<UoMConversion> Conversions { get; set; }
        public DbSet<UnitOfMeasure> Units { get; set; }
        public DbSet<VisibleTo> VisibleToes { get; set; }
        public DbSet<HelpBuble> HelpBubles { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderPosition> OrderPositions { get; set; }
        public DbSet<SnapshotOrderPosition> SnapshotOrderPositions { get; set; }
        public DbSet<AddressGroup> AddressGroups { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceRow> InvoiceRows { get; set; }
        //public DbSet<InvoiceAddressPart> InvoiceAddressParts { get; set; }
        public DbSet<TraderManager> TraderManagers { get; set; }
        public DbSet<WorldEntity> Entities { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<BasicRecipe> BasicRecipes { get; set; }
        public DbSet<SuppliedEntity> SuppliedEntities { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<MailTemplate> MailTemplates { get; set; }
        public DbSet<InvoiceRowDayData> InvoiceRowDayDatas { get; set; }
        public DbSet<SavedCart> SavedCarts { get; set; }

        // public DbSet<AddressTraderEmployee> AddressTraderEmployees { get; set; }

        public DbSet<ViewStandingOrder> ViewStandingOrders { get; set; }

        public DbSet<XeroTaxMapping> XeroTaxMappings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ItemGroup>().
                HasMany(x => x.Items).
                WithRequired(x => x.ItemGroup);

            

            modelBuilder.Entity<Address>().
                HasMany(addr => addr.TraderEmployees).
                WithMany(te => te.Addresses).
                Map(
                   m =>
                   {
                       m.MapLeftKey("Address_Id");
                       m.MapRightKey("TraderEmployee_Id");
                       m.ToTable("AddressTraderEmployees");
                   });

            modelBuilder.Entity<Address>().
                HasOptional(x => x.AddressGroup).
                WithMany(x => x.Addresses);

            modelBuilder.Entity<Trader>().
                HasMany(x => x.Employees).
                WithRequired(x => x.Trader);

            modelBuilder.Entity<TraderType>().
                HasMany(x => x.Trader).
                WithRequired(x => x.Type);

            modelBuilder.Entity<ItemGroup>().
                HasMany(x => x.Metas).
                WithMany(x => x.UselessField);

            modelBuilder.Entity<TraderPrice>().
                HasRequired(x => x.Trader).
                WithMany(x => x.TraderPrices).
                HasForeignKey(x => x.TraderId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<TraderTypePrice>().
                HasRequired(x => x.TraderType).
                WithMany(x => x.TraderTypePrices).
                HasForeignKey(x => x.TraderTypeId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<AddressPrice>().
                HasRequired(x => x.Address).
                WithMany(x => x.AddressPrices).
                HasForeignKey(x => x.AddressId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Price>().
                HasRequired(x => x.Item).
                WithMany(x => x.Prices).
                HasForeignKey(x => x.ItemId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<UoMConversion>().
                HasRequired(x => x.From).
                WithMany().
                HasForeignKey(x => x.FromId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<UoMConversion>().
                HasRequired(x => x.To).
                WithMany().
                HasForeignKey(x => x.ToId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Item>().
                HasRequired(x => x.BaseUnit).
                WithMany().
                HasForeignKey(x => x.BaseUnitId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<VisibleTo>().
                HasRequired(x => x.Item).
                WithMany(x => x.Visibility).
                HasForeignKey(x => x.ItemId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Address>().
                HasRequired(x => x.Trader).
                WithMany(x => x.Address).
                HasForeignKey(x => x.TraderId).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>().
                HasRequired(x => x.Address).
                WithMany(x => x.Orders);

            modelBuilder.Entity<AddressHoliday>().
                HasRequired(x => x.Address).
                WithMany(x => x.AddressHolidays);

            modelBuilder.Entity<OrderPosition>().
                HasRequired(x => x.Order).
                WithMany(x => x.Items);

            modelBuilder.Entity<SuppliedEntity>().
                HasRequired(x => x.Unit).
                WithMany().
                WillCascadeOnDelete(false);

            modelBuilder.Entity<SuppliedEntity>().
                HasRequired(x => x.Unit).
                WithMany().
                WillCascadeOnDelete(false);

            modelBuilder.Entity<BasicRecipe>().
                HasRequired(x => x.SuppliedEntity).
                WithMany().
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>().
                HasMany(x => x.Rows).
                WithOptional(x => x.Invoice).
                WillCascadeOnDelete(false);

            modelBuilder.Entity<Invoice>().
                HasRequired(x => x.DeliveryAddress).
                WithMany().WillCascadeOnDelete(false);



            base.OnModelCreating(modelBuilder);
        }

        public static ChlebyContext ForCurrentApp()
        {
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
            var routeApp = routeData.Values["app"].ToString();
            var app = MvcApplication.AppRegister.FirstOrDefault(a => a.Name == routeApp);
            return new ChlebyContext(app);
        }
    }

    public class MigrationsContextFactory : IDbContextFactory<ChlebyContext>
    {
        public ChlebyContext Create()
        {
            return new ChlebyContext(new AppRegister
            {
                DBConnectionString = "MyLittleDatabase-RelationshipIsMagic"
            }); //szczęśliwe bazy
        }
    }
}