﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace chleby.Web.Models
{
    public class ResetPasswordModelFirst
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "MailAddress")]
        public string Email { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "UserName")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "CurrentPassword")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "CharactersLong", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "NewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ConfirmNewPass")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "TheNewPass")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "Model_UsernameRequired")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Email")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Password")]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "Remember")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "CharactersLong", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ConfirmNewPass")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "TheNewPass")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class SetPasswordModel
    {
        [Required]
        public Guid Guid { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "CharactersLong", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "NewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ConfirmNewPass")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "TheNewPass")]
        public string ConfirmPassword { get; set; }

        public bool Expired { get; set; }
    }
}
