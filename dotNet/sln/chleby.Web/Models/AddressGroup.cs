﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AddressGroup : IComparable<AddressGroup>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string Name { get; set; }

        [Required]
        public int Order { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }

        public int CompareTo(AddressGroup other)
        {
            var ord = Order.CompareTo(other.Order);
            if (ord == 0)
                return string.Compare(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
            else
                return ord;
        }

        public override string ToString()
        {
            return string.Format("#{0} [{1}]", Id, Name);
        }
    }
}