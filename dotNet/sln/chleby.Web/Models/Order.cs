﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace chleby.Web.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public int Address_Id { get; set; }

        [ForeignKey("Address_Id")]
        public virtual Address Address { get; set; }

        public bool Delivery { get; set; }

        public string Note { get; set; }

        public virtual ICollection<OrderPosition> Items { get; set; }

        public virtual string GetDescription()
        {
            return "Order";
        }
    }

    public class StandingOrder : Order
    {
        [Column(TypeName = "datetime2")]
        public DateTime WeekDate { get; set; }

        public int Day { get; set; }

        public override string ToString()
        {
            return string.Format("SO-{3}-{0}-{1}-{2}", Address, Day, Delivery ? 'Y' : 'N', Id);
        }

        public override string GetDescription()
        {
            return Day.GetLongDayName(CultureInfo.InvariantCulture);
        }
    }

    public class AdhocOrder : Order
    {
        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return string.Format("AO-{2}-{0}-{1}", Address, Delivery ? 'Y' : 'N', Id);
        }

        public override string GetDescription()
        {
            return "date " + Date.ToShortDateString();
        }
    }

    public class OrderPosition
    {
        [Key]
        public int Id { get; set; }

        public int Item_Id { get; set; }

        [ForeignKey("Item_Id")]
        public virtual Item Item { get; set; }

        public int OrderedCount { get; set; }

        public bool Adhoc { get; set; }

        public int Order_Id { get; set; }

        [ForeignKey("Order_Id")]
        public virtual Order Order { get; set; }
    }

    public class SnapshotOrderPosition
    {
        [Key]
        public int Id { get; set; }

        public int Address_Id { get; set; }

        [ForeignKey("Address_Id")]
        public virtual Address Address { get; set; }

        public int Item_Id { get; set; }

        [ForeignKey("Item_Id")]
        public virtual WorldEntity Item { get; set; }

        public int OrderedCount { get; set; }

        public bool Adhoc { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public int? Order_Id { get; set; }

        [ForeignKey("Order_Id")]
        public virtual Order Order { get; set; }

        public int MadeCount { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        public decimal Vat { get; set; }

        public string Note { get; set; }

        public override string ToString()
        {
            return string.Format("SN-{0}-{1}-{2}-{3}", Id, EndDate.ToShortDateString(), Item.Id, Address.Id);
        }
    }

    public class OrderDiff
    {
        public int NewCount { get; set; }
        public int OldCount { get; set; }
        public bool AdHoc { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string Trader { get; set; }

        public string Info { get; set; }

        public DateTime Date { get; set; }

        public string Email { get; set; }

        public OrderDiff(bool adHoc, OrderPosition orderPosition, int newCount, int oldCount)
        {
            AdHoc = adHoc;
            NewCount = newCount;
            OldCount = oldCount;
            Name = orderPosition.Item.Name;
            Address = orderPosition.Order.Address.Address1;
            Trader = orderPosition.Order.Address.Trader.Name;
            Info = orderPosition.Order.GetDescription();
            Date = DateTime.Now; //quick fix TODO: fixnac timezone

            var emp = orderPosition.Order.Address.TraderEmployees.First();
            if (emp!=null)
            {
                var user = Membership.GetUser(emp.ExternalAccountData);
                Email = user == null ? "" : user.Email;
            }
        }

        public OrderDiff(bool adHoc, SnapshotOrderPosition orderPosition, int newCount, int oldCount)
        {
            AdHoc = adHoc;
            NewCount = newCount;
            OldCount = oldCount;
            Name = orderPosition.Item.Name;
            Address = orderPosition.Address.Address1;
            Trader = orderPosition.Address.Trader.Name;
            Info = "for " + orderPosition.EndDate;
            Date = DateTime.Now; //quick fix TODO: fixnac timezone

            var emp = orderPosition.Address.TraderEmployees.First();
            if (emp != null)
            {
                var user = Membership.GetUser(emp.ExternalAccountData);
                Email = user == null ? "" : user.Email;
            }
        }

        public override string ToString()
        {
            return string.Format("{7:d}: {0} {1} -> {2} for {3} ({4}) {5} {6}\n",
                                 Name, OldCount, NewCount,
                                 Address, Trader,
                                 AdHoc ? "ADHOC" : "", Info, Date);
        }
    }


    public class CartAdHoc
    {
        public DateTime Date { get; set; }
        public string Note { get; set; }
        public bool Delivery { get; set; }
        public Dictionary<string, int> Order { get; set; }
    }

    public class ProdIndexModel
    {
        public WorldEntity Entity { get; set; }
        public int OrderedCount { get; set; }
        public IEnumerable<IGrouping<Address, SnapshotOrderPosition>> ByAddr { get; set; }
        public int MadeCount { get; set; }
        public ProdIndexModel(WorldEntity entity, int orderedCount, IEnumerable<IGrouping<Address, SnapshotOrderPosition>> byAddr, int madeCount)
        {
            Entity = entity;
            OrderedCount = orderedCount;
            ByAddr = byAddr;
            MadeCount = madeCount;
        }
    }

    public class DistIndexModel
    {
        public Address Address { get; set; }
        public int OrderedCount { get; set; }
        public IEnumerable<IGrouping<WorldEntity, SnapshotOrderPosition>> ByItem { get; set; }
        public int MadeCount { get; set; }
        public DistIndexModel(Address address, int orderedCount, IEnumerable<IGrouping<WorldEntity, SnapshotOrderPosition>> byItem, int madeCount)
        {
            Address = address;
            OrderedCount = orderedCount;
            ByItem = byItem;
            MadeCount = madeCount;
        }
    }

    public enum ValidDayReason
    {
        OK,
        AddressHoliday,
        BusinessHoliday,
        NoWork
    }

    public class SavedCart
    {
        [Key]
        public int Id { get; set; }

        public Guid UserId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }
    }

}