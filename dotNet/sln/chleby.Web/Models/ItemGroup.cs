﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class ItemGroup : IComparable<ItemGroup>
    {
        public ItemGroup()
        {
            Metas = new Collection<AttributeMeta>();
            Live = true;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Live")] 
        public bool Live { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        public int Order { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        public virtual ICollection<AttributeMeta> Metas { get; set; }

        public virtual ItemGroup Parent { get; set; }

        public int CompareTo(ItemGroup other)
        {
            var ord = Order.CompareTo(other.Order);
            if (ord == 0)
                return string.Compare(Name, other.Name, StringComparison.InvariantCultureIgnoreCase);
            else
                return ord;
        }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2}", Id, Name, Live ? "L" : "-");
        }

    }
}