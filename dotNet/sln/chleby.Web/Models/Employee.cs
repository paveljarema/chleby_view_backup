﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public Guid ExternalAccountData { get; set; }

        [Required]
        [StringLength(100)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "LastName")]
        public string LastName { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "TelephoneNumber")]
        public string Phone { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "Role")]
        public string BusinessRole { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} {1} {2}", Id, FirstName, LastName);
        }
    }
}