﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using chleby.Web.Areas.AppAdmin.Models;

namespace chleby.Web.Models
{
    public class MainAppContext : DbContext
    {
        public MainAppContext() : base("DefaultConnection")
        {
            
        }

        public DbSet<ModuleRegister> ModuleRegister { get; set; }
        public DbSet<AppRegister> AppRegister { get; set; }

        public DbSet<UsedModule> UsedModules { get; set; }

        public DbSet<SystemHelp> SystemHelp { get; set; }

        public DbSet<QueuedApp> QueuedApps { get; set; }

        public DbSet<AppSnapshots> AppSnapshots { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ModuleRegister>().
                HasMany(x => x.SubPerms).
                WithRequired(x => x.Parent).
                WillCascadeOnDelete();

            modelBuilder.Configurations.Add(new AppSnapshotsConfiguration());
        }
    }
}