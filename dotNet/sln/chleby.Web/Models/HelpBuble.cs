﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace chleby.Web.Models
{
    public class HelpBuble
    {
        [Key]
        public int Id { get; set; }
        public string name { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string value { get; set; }

    }

    public class SystemHelp
    {
        [Key]
        public int Id { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Content { get; set; }
    }
}