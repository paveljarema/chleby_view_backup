﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class Trader
    {
        public Trader()
        {
            Live = 1;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Column("Name")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TraderPrimaryName")]
        public string PrimaryName { get; set; }

        [StringLength(100)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TraderTradingName")]
        public string TradingName { get; set; }

        public string Name { get { return string.IsNullOrWhiteSpace(TradingName) ? PrimaryName : TradingName; } }

        [DataType(DataType.MultilineText)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TraderTags")]
        public string Tags { get; set; } //NOTE!

        [Required]
        public int Live { get; set; }

        public virtual ICollection<Address> Address { get; set; }

        public virtual ICollection<TraderEmployee> Employees { get; set; }

        [Required]
        public int TypeId { get; set; }

        [ForeignKey("TypeId")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "TraderType")]
        public virtual TraderType Type { get; set; }

        public virtual ICollection<TraderPrice> TraderPrices { get; set; }

        [Required]
        public int PayId { get; set; }

        [ForeignKey("PayId")]
        public virtual PaymentTerm PaymentTerms { get; set; }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2}", Id, Name, Live == 1 ? "L" : Live == 2 ? "S" : "-");
        }
    }
}