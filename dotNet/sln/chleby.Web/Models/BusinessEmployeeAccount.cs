﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace chleby.Web.Models
{
    [NotMapped]
    public class BusinessEmployeeAccount : BusinessEmployee
    {
        [Required]
        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "UserName")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [MinLength(6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Password2")]
        [NotMapped]
        [MinLength(6)]
        [Compare("Password")]
        public string Password2 { get; set; }
    }

    [NotMapped]
    public class TraderEmployeeAccount : TraderEmployee
    {
        [Required]
        [RegularExpression(EmailAddressAttribute._pattern, ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "InvalidEmail")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "UserName")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [MinLength(6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [NotMapped]
        [Compare("Password")]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Password2")]
        public string Password2 { get; set; }

        public override string ToString()
        {
            return base.ToString() + string.Format(" {0}:{1}", UserName, Password);
        }
    }
    
}