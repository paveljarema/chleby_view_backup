﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class AttributeMeta
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ShowInternal")]
        public bool ShowInternal { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "ShowTraders")]
        public bool ShowToTrader { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "DataType")]
        public int DataTypeValue { get; set; }

        public int Order { get; set; }

        public AttrDataType DataType
        {
            get { return (AttrDataType)DataTypeValue; }
            set { DataTypeValue = (int)value; }
        }

        [Display(ResourceType = typeof(Resources.Strings), Name = "PossibleValues")]
        public string PossibleValues { get; set; }

        public virtual ICollection<ItemGroup> UselessField { get; set; }

        public double Execute(Item item)
        {
            var parser = new Parser();
            parser.UnknownVariable += name =>
                {
                    double ret = double.NaN;
                    var prop = TypeDescriptor.GetProperties(typeof(Item))[name];
                    if (prop != null)
                    {
                        var val = prop.GetValue(item);
                        if (val == null)
                            return ret;
                        double.TryParse(val.ToString(), out ret);
                        return ret;
                    }
                    else
                    {
                        var attr = item.Attributes.FirstOrDefault(x => x.Metadata.Name == name);
                        if (attr != null)
                        {
                            double.TryParse(attr.Value, out ret);
                            return ret;
                        }
                        else
                            return double.NaN;
                    }
                };
            var queue = parser.Parse(PossibleValues);
            return parser.Execute(queue);
        }

        public override string ToString()
        {
            return string.Format("#{0} [{1}] {2} {3}{4}", Id, Name, DataTypeValue, ShowInternal ? "I" : "-",
                                 ShowToTrader ? "T" : "-");
        }

    }

    public enum AttrDataType
    {
        [EnumDescription(typeof(Resources.Strings))]
        Int,
        [EnumDescription(typeof(Resources.Strings))]
        String,
        [EnumDescription(typeof(Resources.Strings))]
        Bool,
        [EnumDescription(typeof(Resources.Strings))]
        SelectOne,
        [EnumDescription(typeof(Resources.Strings))]
        SelectMany,
        [EnumDescription(typeof(Resources.Strings))]
        Date,
        [EnumDescription(typeof(Resources.Strings))]
        Expr
    }

    public class AttrMetaVM
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Strings), Name = "Name")]
        public string Name { get; set; }

        public AttrDataType DataType
        {
            get { return (AttrDataType)DataTypeValue; }
            set { DataTypeValue = (int)value; }
        }

        [Required]
        [Display(ResourceType = typeof(Resources.Strings), Name = "DataType")]
        public int DataTypeValue { get; set; }

        [Display(ResourceType = typeof(Resources.Strings), Name = "PossibleValues")]
        public string PossibleValues { get; set; }

        [Required]
        [Range(0, 2)]
        public int Visible { get; set; }
    }

    public class MetaValue
    {
        public AttributeMeta meta { get; set; }
        public string value { get; set; }
    }
}