﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public class TraderType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        //do uzgodnienia podczas tworzenia UI

        public virtual ICollection<Trader> Trader { get; set; }

        public virtual ICollection<TraderTypePrice> TraderTypePrices { get; set; } 

        public override string ToString()
        {
            return string.Format("#{0} {1}", Id, Name);
        }
    }
}