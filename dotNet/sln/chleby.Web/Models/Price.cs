﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace chleby.Web.Models
{
    public abstract class Price
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        [DataType(DataType.Currency)]
        [Range(0, int.MaxValue)]
        public decimal Value { get; set; }

        [Required]
        public int Priority { get; set; }

        public int ItemId { get; set; }

        [Required]
        [ForeignKey("ItemId")]
        public virtual Item Item { get; set; }

        public override string ToString()
        {
            return Value.ToString("f2");
        }
    }

    public class AddressPrice:Price
    {
        public AddressPrice()
        {
            Priority = 0;
        }

        public int AddressId { get; set; }

        [Required]
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
    }

    public class TraderPrice : Price
    {
        public TraderPrice()
        {
            Priority = 1;
        }

        public int TraderId { get; set; }

        [Required]
        [ForeignKey("TraderId")]
        public virtual Trader Trader { get; set; }
    }

    public class TraderTypePrice:Price
    {
        public TraderTypePrice()
        {
            Priority = 2;
        }

        public int TraderTypeId { get; set; }

        [Required]
        [ForeignKey("TraderTypeId")]
        public virtual TraderType TraderType { get; set; }
    }

    public class ItemPrice : Price
    {
        public ItemPrice()
        {
            Priority = 3;
        }
    }
}