﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Mounstain Stream - Page not found</title>
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width" />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../Content/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="../Content/fontawesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../Content/chleby.css" rel="stylesheet" />
    <style type="text/css">
        body {
            padding: 0px;
            margin: 0px;
        }
    </style>
</head>
<body>
    <div class="front-body-wrapper">
        <div class="page-container container front-container">
            <div class="row front-header">
                <div class="span4 front-logo">
                    <a href="/">
                        <img src="../Images/frontpage/mountainstream_logo_gray.png" /></a>
                </div>
                <div class="span8 clearfix">
                </div>

            </div>

            <div class="body container">
                <p style="line-height: 30px">
                    <span style="font-size:30pt">404</span><br /><br/>
                    Oooops!  Something's wrong :-(<br />
                    We cannot find the specified page on our server.<br />
                    If you have typed the address, please check your spelling.<br />
                    If you have been redirected here, please send us the link of the page that sent you here.<br />
                    We appreciate your patience.
                </p>
            </div>
        </div>
    </div>
    <div class="front-footer-wrapper" style="bottom: 0; height:50px; width: 100%; position:fixed">
        <div class="container front-container front-footer" style="margin-top: 14px">
            <div class="row front-footer-section-horizontal" style="text-align:center;">
                <div class="span12">
                    <ul class="front-footer-menu">
                        <li><a>&copy; Mountain Stream 2012 - 2013</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Le javascript
            ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../Scripts/modernizr-2.6.2.js"></script>

    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) { z._.push(c) }, $ = z.s =
            d.createElement(s), e = d.getElementsByTagName(s)[0]; z.set = function (o) {
                z.set.
                _.push(o)
            }; z._ = []; z.set._ = []; $.async = !0; $.setAttribute('charset', 'utf-8');
            $.src = '//v2.zopim.com/?1OM3bDbTnBWyqZB7OXejaeEJROhk9sLY'; z.t = +new Date; $.
            type = 'text/javascript'; e.parentNode.insertBefore($, e)
        })(document, 'script');
    </script>
    <!--End of Zopim Live Chat Script-->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-42268293-1', 'mountainstream.ms');
        ga('send', 'pageview');

    </script>
</body>
</html>
