﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using MvcSiteMapProvider.Extensibility;
using chleby.Web.Models;

namespace chleby.Web.SiteMap
{
    public class DNPBusinessHoliday : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection()
        {
            var db = ChlebyContext.ForCurrentApp();
            var nodes = new List<DynamicNode>();
            foreach (var bh in db.BusinessHolidays)
                nodes.Add(new DynamicNode
                {
                    Title = bh.Name,
                    RouteValues = new Dictionary<string, object>() {
                        { "id", bh.Id }
                    }
                });
            return nodes;
        }
    }
}