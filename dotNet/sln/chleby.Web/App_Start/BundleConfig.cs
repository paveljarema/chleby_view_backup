﻿using System.Web;
using System.Web.Optimization;

namespace chleby.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/css").Include(
            "~/Content/bootstrap/css/bootstrap.css",
            "~/Content/bootstrap/css/bootstrap-modal.css",

            "~/Content/bootstrap/css/bootstrap-formhelpers.css",
            "~/Content/bootstrap/css/bootstrap-formhelpers-countries.flags.css",

            "~/Content/fontawesome/css/font-awesome.css",
            "~/Content/uihelpers/DT_bootstrap.css",

            "~/Content/fullcalendar/fullcalendar.css",
            "~/Content/fullcalendar/fullcalendar-chleby.css",

            "~/Content/jqueryui/jquery-ui-1.10.0.custom.css",

            "~/Content/uihelpers/select2.css",
            "~/Content/uihelpers/prettyPhoto.css",
            "~/Content/uihelpers/jquerypowertip.css",

            "~/Content/chleby.css"));

            bundles.Add(new ScriptBundle("~/js/chleby").Include(
            "~/Scripts/chleby/chleby-simpleui.js",
            "~/Scripts/chleby/chleby-tables.js",
            "~/Scripts/chleby/chleby-tabs.js",
            "~/Scripts/chleby/chleby-sortables.js",
            "~/Scripts/chleby/chleby-invoices.js",
            "~/Scripts/chleby/chleby-orderoverride.js",
            "~/Scripts/chleby/chleby-permissions.js",
           "~/Scripts/chleby/chleby-settings.js"
            ));

            bundles.Add(new ScriptBundle("~/js/jquery").Include(
            "~/Scripts/jquery/jquery.js")); // 1.7.cośtam bo inaczej fullcalendar sie sypie ;__: // v1.7.x otherwise fullcalendar crashes

            bundles.Add(new ScriptBundle("~/js/jqueryui").Include(
            "~/Scripts/jqueryui/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/js/jqueryval").Include(
            "~/Scripts/jqueryval/jquery.unobtrusive*",
            "~/Scripts/jqueryval/jquery.validate*"));

            //bootstrap form helpers
            bundles.Add(new ScriptBundle("~/js/bootstrap").Include(
            "~/Scripts/bootstrap/bootstrap.js",
             "~/Scripts/bootstrap/bootstrap-modalmanager.js",
              "~/Scripts/bootstrap/bootstrap-modal.js",
              "~/Scripts/bootstrap/bootstrap-hover-dropdown.js"
            ));

            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
            "~/Scripts/modernizr-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/bootstrap/formhelpers").Include(
            "~/Scripts/bootstrap-formhelpers/bootstrap-formhelpers-selectbox.js",
            "~/Scripts/bootstrap-formhelpers/bootstrap-formhelpers-timepicker.js"));

            //data-tables
            bundles.Add(new ScriptBundle("~/js/datatables").Include(
            "~/Scripts/datatables/jquery-dataTables-min.js",
            "~/Scripts/datatables/jquery-dataTables-columnFilter.js",
            "~/Scripts/datatables/jquery-dataTables-boostrapPatch.js"));

            // UI HELPERS
            // select2, spinnner, colorpicker, tooltips
            bundles.Add(new ScriptBundle("~/js/uihelpers").Include(
            "~/Scripts/jquery.cookie.js",     
            "~/Scripts/uihelpers/select2.js",
            "~/Scripts/uihelpers/jquerypowertip.js",
            "~/Scripts/uihelpers/jquerycolorpicker.js",
            "~/Scripts/uihelpers/jquerystickyscroll.js",
            "~/Scripts/uihelpers/recoverscroll.js",
            "~/Scripts/jquery.ajax-upload.1.0.1.js",
            //"~/Scripts/uihelpers/jqueryCookieGuard.js",
            "~/Scripts/uihelpers/prettyPhoto.js",
            "~/Scripts/uihelpers/Chart.js",
            "~/Scripts/uihelpers/spin.js",
            "~/Scripts/uihelpers/jquery.dirtyforms.js",
            "~/Scripts/uihelpers/unslider.js"
            ));

            //fullcalendar      
            bundles.Add(new ScriptBundle("~/js/fullcalendar").Include(
            "~/Scripts/fullcalendar/fullcalendar.js",
                /*"~/Scripts/fullcalendar/fullcalendar.year.js",*/
            "~/Scripts/fullcalendar/gcal.js"));
        }
    }
}