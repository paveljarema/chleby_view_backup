﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace chleby.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new ErrorLoggerAttribute());
        }
    }

    public class ErrorLoggerAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            if (filterContext.IsChildAction)
            {
                return;
            }
            if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled)
            {
                return;
            }

            Exception exception = filterContext.Exception;
            var httpEx = new HttpException(null, exception);

            if (httpEx.GetHttpCode() != 500)
            {
                //return;
            }

            var message = string.Format(
                "Wystąpił bład HTTP: {0} ({1}) - {2} @ {3}:{4}",
                httpEx.GetHttpCode(),
                httpEx.GetHtmlErrorMessage(),
                exception.GetType().FullName,
                filterContext.RouteData.Values["controller"],
                filterContext.RouteData.Values["action"]
                );

            Guid id = Guid.NewGuid();

            using (var sw = new StreamWriter(
                filterContext.RequestContext.HttpContext.Server.MapPath("~/logs/" + id + ".log")))
            {
                sw.WriteLine("HTTP ERROR {0} {1}", httpEx.GetHttpCode(), httpEx.GetHtmlErrorMessage());
                sw.WriteLine("Date: {0}", DateTime.Now);
                sw.WriteLine("Controller: {0}", filterContext.RouteData.Values["controller"]);
                sw.WriteLine("Action: {0} ", filterContext.RouteData.Values["action"]);
                sw.WriteLine("URL: {0}", filterContext.HttpContext.Request.Url);
                sw.WriteLine("Remote host: {0} [{1}]", filterContext.HttpContext.Request.UserHostAddress, filterContext.HttpContext.Request.UserHostName);
                sw.WriteLine("User agent: {0}", filterContext.HttpContext.Request.UserAgent);
                sw.WriteLine(new string('-', 76));
                sw.WriteLine("Exception: {0}", exception);
                sw.WriteLine(new string('-', 76));
                sw.WriteLine("Headers");
                var headers = filterContext.HttpContext.Request.Headers;
                foreach (var header in headers.AllKeys)
                {
                    foreach (var value in headers.GetValues(header) ?? new[] { "" })
                        sw.WriteLine("{0}: {1}", header, value);
                }
            }
            
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 200;
            
            var routedata = filterContext.RouteData.Values;

            if (routedata["action"].ToString().ToLowerInvariant() == "index")
            {
                if (routedata["controller"].ToString().ToLowerInvariant() == "home")
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "UserError",
                        ViewData =  new ViewDataDictionary { { "ErrorID", id } }
                    };
                    return;
                }
                routedata["controller"] = "Home";
            }
            routedata["action"] = "Index";
            filterContext.Result = new RedirectToRouteResult("Default_full", routedata);
            if (filterContext.HttpContext.Session != null)
            {
                filterContext.HttpContext.Session["__FILTERERROR"] =
                    "An error occured while processing your request. Error ID = " + id;
            }
        }
    }
}