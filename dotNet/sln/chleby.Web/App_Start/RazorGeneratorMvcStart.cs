using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using RazorGenerator.Mvc;
using System.Linq;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(chleby.Web.App_Start.RazorGeneratorMvcStart), "Start")]

namespace chleby.Web.App_Start {
    public static class RazorGeneratorMvcStart {
        public static void Start() {
            var engine = new PrecompiledMvcEngine(typeof(RazorGeneratorMvcStart).Assembly) { 
                UsePhysicalViewsIfNewer = true
            };
            ViewEngines.Engines.Insert(0, engine);

            // StartPage lookups are done by WebPages. 
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);
        }
    }
}
