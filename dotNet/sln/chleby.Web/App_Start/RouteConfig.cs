﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace chleby.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "start info with lang",
                url: "{lang}/{controller}/{action}/{id}",
                defaults: new { controller = "Start", action = "Index", lang = "u", id = UrlParameter.Optional },
                constraints: new { lang = "([a-z]{2}(-[A-Z]{2,4})?)|u", controller = "Start" }
            );

            routes.MapRoute(
                name: "start info",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Start", action = "Index", lang = "u", id = UrlParameter.Optional },
                constraints: new { controller = "Start" }
            );

            routes.MapRoute(
                name: "Invoice with period",
                url: "{app}/{lang}/{tid}/{controller}/{invoiceperiod}/{action}/{id}",
                defaults: new { app = "default", controller = "Home", action = "Index", tid = -1, lang = "u", invoiceperiod = "u", id = UrlParameter.Optional },
                constraints: new { lang = "([a-z]{2}(-[A-Z]{2,4})?)|u", tid = @"-?\d+", invoiceperiod = "[dwmau]", controller = "Invoices" }
            );

            routes.MapRoute(
                name: "Default_full",
                url: "{app}/{lang}/{tid}/{controller}/{action}/{id}",
                defaults: new { app = "default", controller = "Home", action = "Index", tid = -1, lang = "u", id = UrlParameter.Optional },
                constraints: new { lang = "([a-z]{2}(-[A-Z]{2,4})?)|u", tid = @"-?\d+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{app}/{controller}/{action}/{id}",
                defaults: new { app = "default", controller = "Home", action = "Index", tid = -1, lang = "u", id = UrlParameter.Optional },
                constraints: new {}
            );

        }
    }
}