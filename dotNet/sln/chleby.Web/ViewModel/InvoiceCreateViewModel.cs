﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web.ViewModel
{
    public class InvoiceCreateViewModel
    {
        public InvoiceCreateViewModel()
        {
            Complaints = new List<DmgItemViewModel>();
            Items = new List<InvoiceRowViewModel>();
            AllItems = new List<SOItem>();
        }


        /// <summary>
        /// Id of the invoice
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// Invoice date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Invoice date local
        /// </summary>
        public DateTime DateLocal { get; set; }

        /// <summary>
        /// Create period start date
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Create period start date local
        /// </summary>
        public DateTime StartDateLocal { get; set; }

        /// <summary>
        /// Create period end date
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Create period end date local
        /// </summary>
        public DateTime EndDateLocal { get; set; }

        /// <summary>
        /// Invoice number
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Invoice address
        /// </summary>
        public int InvoiceAddressId { get; set; }

        /// <summary>
        /// Delivery address
        /// </summary>
        public int DeliveryAddressId { get; set; }

        /// <summary>
        /// Payment term
        /// </summary>
        public string PaymentTerm { get; set; }

        /// <summary>
        /// Total gross on invoice
        /// </summary>
        public decimal TotalGross { get; set; }

        /// <summary>
        /// Total vat on invoice
        /// </summary>
        public decimal Vat { get; set; }

        /// <summary>
        /// Total net on invoice
        /// </summary>
        public decimal TotalNet { get; set; }

        public IList<DmgItemViewModel> Complaints { get; set; }

        /// <summary>
        /// Products on the invoice
        /// </summary>
        public IList<InvoiceRowViewModel> Items { get; set; }

        /// <summary>
        /// Delivery row
        /// </summary>
        public DeliveryRowViewModel Delivery { get; set; }

        /// <summary>
        /// List of all items, used in add item dropdown
        /// </summary>
        public IList<SOItem> AllItems { get; set; }

    }

    public class DmgItemViewModel
    {
        /// <summary>
        /// Id of the dmg item
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Item id
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// Item id
        /// </summary>
        public int ItemName { get; set; }

        /// <summary>
        /// Date of dmg report
        /// </summary>
        public DateTime RaportDate { get; set; }

        /// <summary>
        /// Date of delivery
        /// </summary>
        public DateTime DeliveryDate { get; set; }

        /// <summary>
        /// Local date of the delivery
        /// </summary>
        public DateTime DeliveryDateLocal { get; set; }

        /// <summary>
        /// Dmg item quantity
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// Addess id
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Is ack
        /// </summary>
        public bool Ack { get; set; }

        /// <summary>
        /// Price per item
        /// </summary>
        public decimal PricePerItem { get; set; }
    }
       

    public class InvoiceRowViewModel
    {
        /// <summary>
        /// If of the item
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// Name of the item
        /// </summary>
        public int ItemName { get; set; }

        /// <summary>
        /// Product count
        /// </summary>
        public int Count { get; set; }

        public decimal MondayPrice { get; set; }
        public decimal TuesdayPrice { get; set; }
        public decimal WednesdayPrice { get; set; }
        public decimal ThursdayPrice { get; set; }
        public decimal FridayPrice { get; set; }
        public decimal SaturdayPrice { get; set; }
        public decimal SundayPrice { get; set; }

        public decimal Vat { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalGross { get; set; }

    }

    public class DeliveryRowViewModel
    {
        /// <summary>
        /// If of the item
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// Name of the item
        /// </summary>
        public int ItemName { get; set; }

        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }

        public decimal Price { get; set; }
        public decimal Vat { get; set; }
        public decimal TotalNet { get; set; }
        public decimal TotalGross { get; set; }
    }
}