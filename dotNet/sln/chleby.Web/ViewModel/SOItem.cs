﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web.ViewModel
{
    /// <summary>
    /// Item view model
    /// </summary>
    public class SOItem
    {
        /// <summary>
        /// Id of the item
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Item name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Item code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Item group
        /// </summary>
        public SOItemGroup Group { get; set; }

        /// <summary>
        /// Item hidden by admin
        /// </summary>
        public bool HiddenByAdmin { get; set; }

        /// <summary>
        /// Item hidden by customer
        /// </summary>
        public bool HiddenByCustomer { get; set; }

        /// <summary>
        /// Item favourite
        /// </summary>
        public bool Favourite { get; set; }

        /// <summary>
        /// Crafting time
        /// </summary>
        public int CraftingTime { get; set; }

        /// <summary>
        /// Item privce
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Additional info
        /// </summary>
        public string AdditionalInfo { get; set; }

        public override bool Equals(object obj)
        {
            SOItem casted = obj as SOItem;
            
            if (casted != null)
            {
                return casted.Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, Group: {3}", Id, Code, Name, Group.ToString());
        }
        
    }

    public class SOItemGroup
    {
        /// <summary>
        /// Group id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Group order
        /// </summary>
        public int Order { get; set; }

        public override bool Equals(object obj)
        {
            SOItemGroup casted = obj as SOItemGroup;
            if (casted != null)
            {
                return casted.Id == Id;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", Id, Name, Order);
        }

    }
}