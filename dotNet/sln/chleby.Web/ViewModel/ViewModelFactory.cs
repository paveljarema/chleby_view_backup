﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using chleby.Web.Models;
using MS.Utils;

namespace chleby.Web.ViewModel
{
    public class ViewModelFactory
    {
        private ChlebyContext _db;
        private Business _business;

        public ViewModelFactory(ChlebyContext db)
        {
            _db = db;
            _business = _db.Businesses.First();
        }

        /// <summary>
        /// Creates standing order view model
        /// </summary>
        /// <param name="addressId">Id of the address</param>
        /// <returns>Standing order view model</returns>
        public StandingOrderViewModel CreateStandingOrderViewModel(int traderId, int addressId)
        {
            var model = new StandingOrderViewModel();

            // holidays
            var businessHolidaysDays = new List<DateTime>();
            foreach (var holiDay in _db.BusinessHolidays.ToList())
            {
                DateTime helperdate = holiDay.StartDate;
                while (helperdate != holiDay.EndDate.AddDays(1))
                {
                    businessHolidaysDays.Add(helperdate);
                    helperdate = helperdate.AddDays(1);
                }

            }
            model.BusinessHolidaysDays = businessHolidaysDays;

            // delivery days
            model.DeliveryDaysBusiness = _business.DeliveryDays;

            // trader
            var trader = _db.Traders.First(tr => tr.Id == traderId);
            model.TraderId = traderId;
            model.Trader = trader;
            model.HasEmployees = trader.Employees.Any();

            // addresses
            model.Addresses = trader.Address.Where(x => x.Live).ToDictionary(addr => addr.Id, addr => addr.Address1);

            // selected address
            model.AddressId = addressId;
            var address = trader.Address.First(add => add.Id == addressId);
            model.CurrentAddress = address.Address1;
            model.AddressDeliveryPrice = address.DeliveryPrice;
            model.DeliveryDaysAddress = address.DeliveryDays;

            // get week dates
            var timezone = TimeZoneInfo.FindSystemTimeZoneById(_business.Timezone);

            var deadline = _business.CraftingDeadline;

            DateTime dedlineTodayUtc = DateTime.UtcNow.Date;
            dedlineTodayUtc = dedlineTodayUtc.AddHours(deadline.Hour);
            dedlineTodayUtc = dedlineTodayUtc.AddMinutes(deadline.Minute);

            IList<SODay> days = new List<SODay>();
            DateTime deadlineTodayLocal = TimeZoneInfo.ConvertTimeFromUtc(dedlineTodayUtc, timezone);
            int msDay = (int)dedlineTodayUtc.DayOfWeek - 1;
            while (msDay < 0) msDay += 7;
            SODay day = new SODay
            {
                MsDay = msDay,
                DayDate = dedlineTodayUtc,
                DayDateLocal = deadlineTodayLocal,
                DayOfWeek = dedlineTodayUtc.DayOfWeek,
                DayOfWeekLocal = deadlineTodayLocal.DayOfWeek,
                Active = !businessHolidaysDays.Contains(dedlineTodayUtc.Date) && model.DeliveryDaysAddress.Contains(msDay)
                                && model.DeliveryDaysBusiness.Contains(msDay)
            };

            days.Add(day);

            for (int i = 1; i < 7; i++)
            {
                DateTime nextDay = dedlineTodayUtc.AddDays(i);
                DateTime nextDayLocal = TimeZoneInfo.ConvertTimeFromUtc(nextDay, timezone);
                int nextMsDay = (int)nextDay.DayOfWeek - 1;
                while (nextMsDay < 0) nextMsDay += 7;
                day = new SODay
                {
                    MsDay = nextMsDay,
                    DayDate = nextDay,
                    DayDateLocal = nextDayLocal,
                    DayOfWeek = nextDay.DayOfWeek,
                    DayOfWeekLocal = nextDayLocal.DayOfWeek,
                    Active = !businessHolidaysDays.Contains(nextDay.Date) && model.DeliveryDaysAddress.Contains(msDay)
                                && model.DeliveryDaysBusiness.Contains(msDay)
                };
                days.Add(day);
            }

            model.Days = days;

            // get all items
            IList<SOItem> allItems = _db.Items.Select(itm => new SOItem
            {
                Id = itm.Id,
                Code = itm.LegacyCode,
                Name = itm.Name,
                Group = new SOItemGroup
                {
                    Id = itm.ItemGroup.Id,
                    Name = itm.ItemGroup.Name,
                    Order = itm.ItemGroup.Order
                },
                CraftingTime = itm.CraftingTime,
                AdditionalInfo = itm.AdditionalInfo
            }).ToList();

            // set prices and visibility
            foreach (Item itm in _db.Items.ToList())
            {
                SOItem vmItem = allItems.Where(vm => vm.Id == itm.Id).First();

                Price price = itm.Prices.OfType<TraderPrice>().FirstOrDefault(x => x.TraderId == traderId);
                if (price == null)
                {
                    price = itm.Prices.OfType<TraderTypePrice>().FirstOrDefault(x => x.TraderTypeId == trader.TypeId);
                    if (price == null)
                    {
                        price = itm.Prices.OfType<ItemPrice>().FirstOrDefault();
                    }
                }
                if (price != null)
                {
                    vmItem.Price = price.Value;
                }
                vmItem.HiddenByAdmin = itm.Visibility.Any(x => x.TraderId == traderId) ? itm.Visibility.FirstOrDefault(x => x.TraderId == traderId).HiddenByAdmin : false;
                vmItem.HiddenByCustomer = itm.Visibility.Any(x => x.TraderId == traderId) ? itm.Visibility.FirstOrDefault(x => x.TraderId == traderId).HiddenByCustomer : false;
                vmItem.Favourite = itm.Visibility.Any(x => x.TraderId == traderId) ? itm.Visibility.FirstOrDefault(x => x.TraderId == traderId).Favourite : false;
            }
            model.AllItems = allItems;

            // get orders

            IList<ViewStandingOrder> orders = _db.Database.SqlQuery<ViewStandingOrder>(
                string.Format(@"select 
	ord.Id as OrderId,
	ord.Address_Id as AddressId,
	ord.Delivery,
	ord.[Day],
	pos.Id as PositionId,
	pos.OrderedCount,
	itm.Id as ItemId,
	itm.Name as ItemName,
	itm.CraftingTime,
	itmGr.Name as GroupName
from 
	Orders ord
inner join OrderPositions pos on pos.Order_Id = ord.Id
inner join Items itm on itm.Id = pos.Item_Id
inner join ItemGroups itmGr on itm.ItemGroupId = itmGr.Id
where
	ord.Discriminator = 'StandingOrder'
    and ord.Address_Id = {0}",
                addressId)
                ).ToList();
            IList<SOOrderRow> rows = new List<SOOrderRow>();
            IList<SODelivery> deliveries = new List<SODelivery>();
            foreach (SODay soDay in days)
            {
                foreach (ViewStandingOrder ord in orders.Where(o => o.Day == soDay.MsDay))
                {
                    var item = allItems.First(i => i.Id == ord.ItemId);

                    // get or create row
                    SOOrderRow row = rows.FirstOrDefault(r => r.Item.Id == ord.ItemId);
                    if (row == null)
                    {
                        row = new SOOrderRow
                        {
                            Item = item,
                            Positions = new List<SOOrderCell>()
                        };
                        rows.Add(row);
                    }

                    // add cell to row
                    SOOrderCell cell = row.Positions.FirstOrDefault(p => p.PositionId == ord.PositionId);
                    if (cell == null)
                    {
                        cell = new SOOrderCell
                        {
                            OrderId = ord.OrderId,
                            PositionId = ord.PositionId,
                            Active = !businessHolidaysDays.Contains(soDay.DayDate) && model.DeliveryDaysAddress.Contains(soDay.MsDay)
                                && model.DeliveryDaysBusiness.Contains(soDay.MsDay),
                            Count = ord.OrderedCount,
                            Day = soDay,
                        };
                        row.Positions.Add(cell);
                    }
                    else
                    {
                        cell.Count = ord.OrderedCount;
                    }

                    // check delivery
                    var delivery = deliveries.FirstOrDefault(d => d.Day.MsDay == ord.Day);
                    if (delivery == null)
                    {
                        delivery = new SODelivery
                        {
                            OrderId = ord.OrderId,
                            PositionId = ord.PositionId,
                            Active = !businessHolidaysDays.Contains(soDay.DayDate) && model.DeliveryDaysAddress.Contains(soDay.MsDay)
                                && model.DeliveryDaysBusiness.Contains(soDay.MsDay),
                            Selected = ord.Delivery != null && ord.Delivery.Value,
                            Day = soDay,
                        };
                        deliveries.Add(delivery);
                    }
                    else
                    {
                        delivery.Selected = ord.Delivery != null && ord.Delivery.Value;
                    }
                }
            }

            // add missing days
            foreach (SODay soDay in days)
            {
                foreach (SOOrderRow row in rows)
                {
                    // try to find position for this day
                    SOOrderCell cell = row.Positions.FirstOrDefault(c => c.Day.MsDay == soDay.MsDay);
                    if (cell == null)
                    {
                        // if there is no position at this day, create one
                        cell = new SOOrderCell
                        {
                            PositionId = -1,
                            Active = !businessHolidaysDays.Contains(soDay.DayDate) && model.DeliveryDaysAddress.Contains(soDay.MsDay),
                            Count = 0,
                            Day = soDay,
                        };
                        row.Positions.Add(cell);
                    }
                }
            }

            // add missing days
            foreach (SODay soDay in days)
            {

                // try to find position for this day
                SODelivery delivery = deliveries.FirstOrDefault(c => c.Day.MsDay == soDay.MsDay);
                if (delivery == null)
                {
                    // if there is no position at this day, create one
                    delivery = new SODelivery
                    {
                        PositionId = -1,
                        Active = !businessHolidaysDays.Contains(soDay.DayDate) && model.DeliveryDaysAddress.Contains(soDay.MsDay),
                        Selected = false,
                        Day = soDay,
                    };
                    deliveries.Add(delivery);
                }
            }

            // mark next week orders
            if (DateTime.UtcNow > dedlineTodayUtc)
            {
                foreach (SOOrderRow row in rows)
                {

                    int nextMsDay = (msDay + row.Item.CraftingTime + 1) % 7;
                    var cells = row.Positions.Where(cell => cell.Day.MsDay <= nextMsDay && cell.Day.MsDay > msDay).ToList();
                    foreach (SOOrderCell cell in cells)
                    {
                        cell.NextWeek = true;
                    }

                }
            }

            model.OrderRows = rows;
            model.Deliveries = deliveries;

            return model;
        }
    }
}
