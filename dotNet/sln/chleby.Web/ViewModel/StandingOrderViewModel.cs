﻿using chleby.Web.Models;
using System;
using System.Collections.Generic;

namespace chleby.Web.ViewModel
{
    public class StandingOrderViewModel
    {
        /// <summary>
        /// List of orders
        /// </summary>
        public IList<SOOrderRow> OrderRows { get; set; }
        
        /// <summary>
        /// List of all items
        /// </summary>
        public IList<SOItem> AllItems { get; set; }

        /// <summary>
        /// Delivery row
        /// </summary>
        public IList<SODelivery> Deliveries { get; set; }

        /// <summary>
        /// Trader id
        /// </summary>
        public int TraderId { get; set; }

        /// <summary>
        /// Trader 
        /// </summary>
        public Trader Trader { get; set; }

        /// <summary>
        /// Selected address
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Selected address desc
        /// </summary>
        public string CurrentAddress { get; set; }

        /// <summary>
        /// All client addresses
        /// </summary>
        public IDictionary<int, string> Addresses { get; set; }

        /// <summary>
        /// Delivery days business
        /// </summary>
        public IEnumerable<int> DeliveryDaysBusiness { get; set; }

        /// <summary>
        /// Delivery days address
        /// </summary>
        public IEnumerable<int> DeliveryDaysAddress { get; set; }

        /// <summary>
        /// Business holidays
        /// </summary>
        public IList<DateTime> BusinessHolidaysDays { get; set; }

        /// <summary>
        /// Address delivery price
        /// </summary>
        public decimal AddressDeliveryPrice { get; set; }

        /// <summary>
        /// Has employess
        /// </summary>
        public bool HasEmployees { get; set; }

        /// <summary>
        /// List of SO days
        /// </summary>
        public IList<SODay> Days { get; set; }
    }

    public class SOOrderRow
    {
        public SOItem Item { get; set; }
        public IList<SOOrderCell> Positions { get; set; }
    }

    public class SOOrderCell
    {
        public SODay Day { get; set; }
        public int OrderId { get; set; }
        public int PositionId { get; set; }
        public int Count { get; set; }
        public bool Active { get; set; }
        public bool NextWeek { get; set; }
    }

    public class SODelivery
    {
        public int OrderId { get; set; }
        public int PositionId { get; set; }
        public SODay Day { get; set; }
        public bool Active { get; set; }
        public bool Selected { get; set; }
    }

   
}