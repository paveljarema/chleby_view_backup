﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace chleby.Web.ViewModel
{
    public class SODay
    {
        public DayOfWeek DayOfWeek { get; set; }
        public DayOfWeek DayOfWeekLocal { get; set; }

        public DateTime DayDate { get; set; }
        public DateTime DayDateLocal { get; set; }

        public int MsDay { get; set; }

        public bool Active { get; set; }
    }
}

