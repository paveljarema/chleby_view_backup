DROP TABLE [dbo].[InvoiceAuditLogs]
CREATE TABLE [dbo].[AuditLogs] (
    [Id] [int] NOT NULL IDENTITY,
    [User] [nvarchar](max),
    [Date] [datetime2](7) NOT NULL,
    [Action] [nvarchar](max),
    [InvoiceId] [int],
    [Discriminator] [nvarchar](128) NOT NULL,
    CONSTRAINT [PK_dbo.AuditLogs] PRIMARY KEY ([Id])
)