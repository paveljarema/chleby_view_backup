alter table [dbo].[Invoices] add [Deleted] [bit] NOT NULL default 0
alter table [dbo].[Invoices] add [Revision] [int] NOT NULL default 0

CREATE TABLE [dbo].[InvoiceAuditLogs] (
    [Id] [int] NOT NULL IDENTITY,
    [User] [nvarchar](max),
    [Date] [datetime] NOT NULL,
    [InvoiceId] [int] NOT NULL,
    [Action] [nvarchar](max),
    CONSTRAINT [PK_dbo.InvoiceAuditLogs] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_InvoiceId] ON [dbo].[InvoiceAuditLogs]([InvoiceId])
ALTER TABLE [dbo].[InvoiceAuditLogs] ADD CONSTRAINT [FK_dbo.InvoiceAuditLogs_dbo.Invoices_InvoiceId] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoices] ([Id]) ON DELETE CASCADE