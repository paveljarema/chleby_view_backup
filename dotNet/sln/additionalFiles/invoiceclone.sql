alter table [dbo].[Invoices] add [Cloned] bit not null default 0
alter table [dbo].[Invoices] add [Published] bit not null default 0
alter table [dbo].[Invoices] drop column [Week]
insert into [dbo].[Settings] values ('Default_InvoicePeriod','w')
-- w design zmienic typ InvoiceNumber na int i daty na datetime2(7)