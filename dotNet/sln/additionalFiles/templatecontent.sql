SET IDENTITY_INSERT [dbo].[MailTemplates] ON
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (1, N'backup', N'Hello,

Order backups attached

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] Order backup')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (2, N'confirmationMail', N'
Hello, {name}

Your Streamline account has been created

Login: {user}
Password: {pass}

Please visit http://{host}/{app}/

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] account confirmation')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (3, N'newAddress', N'Hello,

A new address has been added for {trader} - {addr.Address1} {addr.Post}.
Please set delivery price for it and accept it

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] New address confirmation')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (4, N'newInvoice', N'Hello, {invoice.TraderName}

You have new invoice attached for {invoice.StartDate:d} - {invoice.EndDate:d}.

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] New invoice')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (5, N'orderChange', N'Your orders have been changed.
Date: {date}
User: {user}
Changes ({changes.*}):
{changes[0[{#}. {Date}: {Name} {OldCount} -> {NewCount} for {Address} ({Trader}) {Info}
]0]}

---
This message was sent automatically, please do not reply to it.', N'your orders changed')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (6, N'prodChange', N'Your PRODUCTION have been changed.
Date: {date}
User: {user}
Changes ({changes.*}):
{changes[0[{#}. {Date}: {Name} {OldCount} -> {NewCount} for {Address} ({Trader}) {Info}
]0]}

---
This message was sent automatically, please do not reply to it.', N'your delivery changed')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (7, N'pwdFirstSetMail', N'Hello, {name}

Your account on standing-order.com has been created
Login: {user}

To start managing your standing order, please visit http://{host}{link} to set your password.

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] Invitation')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (8, N'pwdFirstSetTraderMail', N'Hello, {name}

We have set up a new system online, where you will now be able to:

�Manage your standing orders
�Order a la Cart
�View your invoices
�View our item catalogue
�Tell us when you''re closed (use this feature to zero out your standing order for a given period)
�Manage your delivery and invoicing addresses 
�Invite as many colleagues from your organisation to view or edit your data

We hope you enjoy using the new system.  

Please activate your account in the next 48 hours by clicking the following link:
Login: {user}

To start managing your standing order, please visit http://{host}{link} to set your password.

For any queries please do not hesitate to contact us on [confirmation email]
---
This message was sent automatically, please do not reply to it.', N'')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (9, N'pwdResetMail', N'User {user} requested a password reset.
To reset your password, please visit http://{host}/{app}/Account/ResetPasswordEmail/{code}.

---
This message was sent automatically, please do not reply to it.', N'[standing-order.com] password reset attempt')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (10, N'questionHelp', N'{typ} from user: {user}

Date: {date}

Message:
{tresc}


This message was sent from standing-order.com, user is waiting for your answer.', N'[standing-order.com] Message from user')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (11, N'testMail', N'TEST MAIL
TEST MAIL
TEST MAIL
TEST MAIL
TEST MAIL

---
This message was sent automatically, please do not reply to it.', N'test subject')
SET IDENTITY_INSERT [dbo].[MailTemplates] OFF