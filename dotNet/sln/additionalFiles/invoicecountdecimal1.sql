/*
   18 lipca 201310:56:16
   User: 
   Server: LAPTOP7
   Database: chlebydwa
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DmgItems
	DROP CONSTRAINT [FK_dbo.DmgItems_dbo.Items_ItemId]
GO
ALTER TABLE dbo.Items SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DmgItems
	DROP CONSTRAINT [FK_dbo.DmgItems_dbo.Addresses_AddressId]
GO
ALTER TABLE dbo.Addresses SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_DmgItems
	(
	Id int NOT NULL IDENTITY (1, 1),
	ItemId int NOT NULL,
	RaportDate datetime2(7) NOT NULL,
	DeliveryDate datetime2(7) NOT NULL,
	Quantity decimal(7, 2) NOT NULL,
	AddressId int NOT NULL,
	Note nvarchar(MAX) NULL,
	Ack bit NOT NULL,
	PricePerItem money NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_DmgItems SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_DmgItems ON
GO
IF EXISTS(SELECT * FROM dbo.DmgItems)
	 EXEC('INSERT INTO dbo.Tmp_DmgItems (Id, ItemId, RaportDate, DeliveryDate, Quantity, AddressId, Note, Ack, PricePerItem)
		SELECT Id, ItemId, RaportDate, DeliveryDate, CONVERT(decimal(7, 2), Quantity), AddressId, Note, Ack, PricePerItem FROM dbo.DmgItems WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_DmgItems OFF
GO
DROP TABLE dbo.DmgItems
GO
EXECUTE sp_rename N'dbo.Tmp_DmgItems', N'DmgItems', 'OBJECT' 
GO
ALTER TABLE dbo.DmgItems ADD CONSTRAINT
	[PK_dbo.DmgItems] PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_AddressId ON dbo.DmgItems
	(
	AddressId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ItemId ON dbo.DmgItems
	(
	ItemId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.DmgItems ADD CONSTRAINT
	[FK_dbo.DmgItems_dbo.Addresses_AddressId] FOREIGN KEY
	(
	AddressId
	) REFERENCES dbo.Addresses
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.DmgItems ADD CONSTRAINT
	[FK_dbo.DmgItems_dbo.Items_ItemId] FOREIGN KEY
	(
	ItemId
	) REFERENCES dbo.Items
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
