CREATE TABLE [dbo].[SubPerms] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](max) NOT NULL,
    [Parent_Id] [int] NOT NULL,
    CONSTRAINT [PK_dbo.SubPerms] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_Parent_Id] ON [dbo].[SubPerms]([Parent_Id])
ALTER TABLE [dbo].[SubPerms] ADD CONSTRAINT [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id] FOREIGN KEY ([Parent_Id]) REFERENCES [dbo].[ModuleRegisters] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[ModuleRegisters] ADD [Admin] [bit]