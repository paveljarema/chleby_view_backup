/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Addresses SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Invoices ADD
	ItemsPrice decimal(18, 2) NOT NULL default 0,
	Vat decimal(18, 2) NOT NULL default 0,
	DeliveryPrice decimal(18, 2) NOT NULL default 0,
	DeliveryVat decimal(18, 2) NOT NULL default 0,
	DeliveryCount int NOT NULL default 0,
	TotalPrice decimal(18, 2) NOT NULL default 0,
	DeliveryAddress_Id int NULL
GO
ALTER TABLE dbo.Invoices ADD CONSTRAINT
	FK_Invoices_Addresses_delivery FOREIGN KEY
	(
	DeliveryAddress_Id
	) REFERENCES dbo.Addresses
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Invoices
	DROP COLUMN Address1, Address2, Address3, City, Post, TraderName
GO
ALTER TABLE dbo.Invoices SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceRows ADD
	Invoice_Id int NULL
GO
ALTER TABLE dbo.InvoiceRows ADD CONSTRAINT
	FK_InvoiceRows_Invoices FOREIGN KEY
	(
	Invoice_Id
	) REFERENCES dbo.Invoices
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.InvoiceRows SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
