CREATE TABLE [dbo].[SystemHelps] (
    [Id] [int] NOT NULL IDENTITY,
    [Controller] [nvarchar](max),
    [Action] [nvarchar](max),
    [Content] [nvarchar](max),
    CONSTRAINT [PK_dbo.SystemHelps] PRIMARY KEY ([Id])
)