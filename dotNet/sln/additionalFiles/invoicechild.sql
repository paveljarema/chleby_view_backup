alter table [dbo].[Invoices] add [MyParent_Id] [int]
CREATE INDEX [IX_MyParent_Id] ON [dbo].[Invoices]([MyParent_Id])
ALTER TABLE [dbo].[Invoices] ADD CONSTRAINT [FK_dbo.Invoices_dbo.Invoices_MyParent_Id] FOREIGN KEY ([MyParent_Id]) REFERENCES [dbo].[Invoices] ([Id])