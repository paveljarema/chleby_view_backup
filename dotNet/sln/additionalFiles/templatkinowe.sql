GO
/****** Object:  Table [dbo].[MailTemplates]    Script Date: 07/22/2013 15:37:59 ******/
SET IDENTITY_INSERT [dbo].[MailTemplates] ON
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (15, N'addressDayChanged', N'address {address.Address1} delivers only on {address.DeliveryDays}', N'address delivery changed')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (17, N'itemDead', N'item {item.Name} is no longed available. Your orders will continue to work when it will be avaiable again.', N'item not available now')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (20, N'itemLive', N'item {item.Name} is available now. Your orders will continue to work now.', N'item available now')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (21, N'minDeliveryWarningOk', N'orders {poss[0[ {Item.Name} {StartDate} -> {EndDate} ]0]}', N'orders processing')
INSERT [dbo].[MailTemplates] ([Id], [Name], [Template], [Subject]) VALUES (24, N'minDeliveryWarningBad', N'orders {poss[0[ {Item.Name} {StartDate} -> {EndDate} ]0]} but didnt met minimum price to be delivered ({minPrice:C2})', N'orders processing without delivery')
SET IDENTITY_INSERT [dbo].[MailTemplates] OFF
