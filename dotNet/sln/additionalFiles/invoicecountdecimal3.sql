/*
   18 lipca 201310:54:44
   User: 
   Server: LAPTOP7
   Database: chlebydwa
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceRows
	DROP CONSTRAINT [FK_dbo.InvoiceRows_dbo.Items_Item_Id]
GO
ALTER TABLE dbo.Items SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceRows
	DROP CONSTRAINT [FK_dbo.InvoiceRows_dbo.InvoiceAddressParts_InvoiceAddressPart_Id]
GO
ALTER TABLE dbo.InvoiceAddressParts SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InvoiceRows
	(
	Id int NOT NULL IDENTITY (1, 1),
	Text nvarchar(MAX) NULL,
	Count decimal(7, 2) NOT NULL,
	Price decimal(18, 2) NOT NULL,
	Vat decimal(18, 2) NOT NULL,
	ByDays nvarchar(MAX) NULL,
	Item_Id int NULL,
	InvoiceAddressPart_Id int NULL,
	Info nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_InvoiceRows SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceRows ON
GO
IF EXISTS(SELECT * FROM dbo.InvoiceRows)
	 EXEC('INSERT INTO dbo.Tmp_InvoiceRows (Id, Text, Count, Price, Vat, ByDays, Item_Id, InvoiceAddressPart_Id, Info)
		SELECT Id, Text, CONVERT(decimal(7, 2), Count), Price, Vat, ByDays, Item_Id, InvoiceAddressPart_Id, Info FROM dbo.InvoiceRows WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceRows OFF
GO
ALTER TABLE dbo.InvoiceRowDayDatas
	DROP CONSTRAINT FK_InvoiceRowDayDatas_Invoices
GO
DROP TABLE dbo.InvoiceRows
GO
EXECUTE sp_rename N'dbo.Tmp_InvoiceRows', N'InvoiceRows', 'OBJECT' 
GO
ALTER TABLE dbo.InvoiceRows ADD CONSTRAINT
	[PK_dbo.InvoiceRows] PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_InvoiceAddressPart_Id ON dbo.InvoiceRows
	(
	InvoiceAddressPart_Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Item_Id ON dbo.InvoiceRows
	(
	Item_Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.InvoiceRows ADD CONSTRAINT
	[FK_dbo.InvoiceRows_dbo.InvoiceAddressParts_InvoiceAddressPart_Id] FOREIGN KEY
	(
	InvoiceAddressPart_Id
	) REFERENCES dbo.InvoiceAddressParts
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.InvoiceRows ADD CONSTRAINT
	[FK_dbo.InvoiceRows_dbo.Items_Item_Id] FOREIGN KEY
	(
	Item_Id
	) REFERENCES dbo.Items
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceRowDayDatas ADD CONSTRAINT
	FK_InvoiceRowDayDatas_Invoices FOREIGN KEY
	(
	InvoiceRow_Id
	) REFERENCES dbo.InvoiceRows
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.InvoiceRowDayDatas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
