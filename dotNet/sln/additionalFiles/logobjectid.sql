/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.AuditLogs.InvoiceId', N'Tmp_ObjectId_2', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.AuditLogs.Tmp_ObjectId_2', N'ObjectId', 'COLUMN' 
GO
ALTER TABLE dbo.AuditLogs DROP COLUMN Discriminator
GO
ALTER TABLE dbo.AuditLogs ADD IntValue INT
GO
ALTER TABLE dbo.AuditLogs SET (LOCK_ESCALATION = TABLE)
GO
COMMIT