INSERT INTO [dbo].[MailTemplates] VALUES ('newComplaint',
'user {user} made complaint 
{di.Quantity} {di.Note} {di.Item.Name} {di.DeliveryDate:dd/MM/yyyy}',
'new complaint')

INSERT INTO [dbo].[MailTemplates] VALUES ('delComplaint',
'user {user} deleted complaint 
{di.Quantity} {di.Note} {di.Item.Name} {di.DeliveryDate:dd/MM/yyyy}',
'del complaint')

INSERT INTO [dbo].[MailTemplates] VALUES ('setComplaint',
'user {user} changed complaint 
{old} -> {di.Quantity} {oldnote} -> {di.Note} {di.Item.Name} {di.DeliveryDate:dd/MM/yyyy}',
'set complaint')