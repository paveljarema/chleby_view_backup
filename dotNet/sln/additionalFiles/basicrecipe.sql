CREATE TABLE [dbo].[Suppliers] (
    [Id] [int] NOT NULL IDENTITY,
    [Name] [nvarchar](max) NOT NULL,
    CONSTRAINT [PK_dbo.Suppliers] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[SuppliedEntities] (
    [Id] [int] NOT NULL IDENTITY,
    [Amount] [decimal](18, 2) NOT NULL,
    [Price] [decimal](18, 2) NOT NULL,
    [Entity_Id] [int] NOT NULL,
    [Unit_Id] [int] NOT NULL,
    [Supplier_Id] [int],
    CONSTRAINT [PK_dbo.SuppliedEntities] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[BasicRecipes] (
    [Id] [int] NOT NULL IDENTITY,
    [Entity_Id] [int] NOT NULL,
    [SuppliedEntity_Id] [int],
    CONSTRAINT [PK_dbo.BasicRecipes] PRIMARY KEY ([Id])
)

CREATE INDEX [IX_Entity_Id] ON [dbo].[SuppliedEntities]([Entity_Id])
CREATE INDEX [IX_Unit_Id] ON [dbo].[SuppliedEntities]([Unit_Id])
CREATE INDEX [IX_Supplier_Id] ON [dbo].[SuppliedEntities]([Supplier_Id])
CREATE INDEX [IX_Entity_Id] ON [dbo].[BasicRecipes]([Entity_Id])
CREATE INDEX [IX_SuppliedEntity_Id] ON [dbo].[BasicRecipes]([SuppliedEntity_Id])

ALTER TABLE [dbo].[SuppliedEntities] ADD CONSTRAINT [FK_dbo.SuppliedEntities_dbo.Items_Entity_Id] FOREIGN KEY ([Entity_Id]) REFERENCES [dbo].[Items] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[SuppliedEntities] ADD CONSTRAINT [FK_dbo.SuppliedEntities_dbo.UnitOfMeasures_Unit_Id] FOREIGN KEY ([Unit_Id]) REFERENCES [dbo].[UnitOfMeasures] ([Id]) -- ON DELETE CASCADE
ALTER TABLE [dbo].[SuppliedEntities] ADD CONSTRAINT [FK_dbo.SuppliedEntities_dbo.Suppliers_Supplier_Id] FOREIGN KEY ([Supplier_Id]) REFERENCES [dbo].[Suppliers] ([Id])
ALTER TABLE [dbo].[BasicRecipes] ADD CONSTRAINT [FK_dbo.BasicRecipes_dbo.Items_Entity_Id] FOREIGN KEY ([Entity_Id]) REFERENCES [dbo].[Items] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[BasicRecipes] ADD CONSTRAINT [FK_dbo.BasicRecipes_dbo.SuppliedEntities_SuppliedEntity_Id] FOREIGN KEY ([SuppliedEntity_Id]) REFERENCES [dbo].[SuppliedEntities] ([Id])

ALTER TABLE [dbo].[Items] ADD [RecipeProvider] [nvarchar](max)