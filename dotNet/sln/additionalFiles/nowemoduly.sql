/****** Object:  ForeignKey [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id]    Script Date: 07/24/2013 12:08:08 ******/
ALTER TABLE [dbo].[SubPerms] DROP CONSTRAINT [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id]
GO
/****** Object:  Table [dbo].[SubPerms]    Script Date: 07/24/2013 12:08:08 ******/
ALTER TABLE [dbo].[SubPerms] DROP CONSTRAINT [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id]
GO
DROP TABLE [dbo].[SubPerms]
GO
/****** Object:  Table [dbo].[ModuleRegisters]    Script Date: 07/24/2013 12:08:08 ******/
DROP TABLE [dbo].[ModuleRegisters]
GO
/****** Object:  Table [dbo].[ModuleRegisters]    Script Date: 07/24/2013 12:08:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleRegisters](
	[Id] [int] IDENTITY(100,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Category] [nvarchar](max) NOT NULL,
	[Controller] [nvarchar](max) NULL,
	[Action] [nvarchar](max) NULL,
	[Order] [int] NOT NULL,
	[IsCore] [bit] NOT NULL,
	[HumanName] [nvarchar](max) NULL,
	[Admin] [bit] NULL,
 CONSTRAINT [PK_ModuleRegisters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ModuleRegisters] ON
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (2, N'Employees', N'5_users', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (7, N'Customers', N'0_traders', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (8, N'Settings', N'7_settings', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (11, N'CTypes', N'-', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (14, N'CEmployees', N'7_contacts', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (15, N'Addresses', N'6_addrs', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (17, N'Attributes', N'6_items', N'-', N'-', 2, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (18, N'Items', N'6_items', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (19, N'Prices', N'6_items', N'-', N'-', 1, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (21, N'CItems', N'4_items', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (22, N'UoM', N'-', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (23, N'UoMConv', N'-', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (25, N'CHoliday', N'5_cal', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (26, N'Holidays', N'4_cal', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (27, N'Orders', N'1_so', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (29, N'Rounds', N'2_dist', N'-', N'-', 1, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (33, N'TurboTraders', N'-', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (34, N'Invoices', N'3_finance', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (36, N'CInvoices', N'3_inv', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (37, N'SimpleProd', N'1_prod', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (101, N'SimpleDist', N'2_dist', N'-', N'-', 0, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (102, N'Home', N'-', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (103, N'Log', N'7_settings', N'-', N'-', 1, 1, N'-', 1)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (104, N'OrderOverride', N'2_cart', N'-', N'-', 0, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (105, N'DmgItem', N'3_inv', N'-', N'-', 1, 1, N'-', 0)
INSERT [dbo].[ModuleRegisters] ([Id], [Name], [Category], [Controller], [Action], [Order], [IsCore], [HumanName], [Admin]) VALUES (106, N'HelpBuble', N'7_settings', N'-', N'-', 2, 1, N'-', 1)
SET IDENTITY_INSERT [dbo].[ModuleRegisters] OFF
/****** Object:  Table [dbo].[SubPerms]    Script Date: 07/24/2013 12:08:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubPerms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Parent_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.SubPerms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Parent_Id] ON [dbo].[SubPerms] 
(
	[Parent_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SubPerms] ON
INSERT [dbo].[SubPerms] ([Id], [Name], [Parent_Id]) VALUES (5, N'traderperm', 14)
INSERT [dbo].[SubPerms] ([Id], [Name], [Parent_Id]) VALUES (6, N'userperm', 2)
SET IDENTITY_INSERT [dbo].[SubPerms] OFF
/****** Object:  ForeignKey [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id]    Script Date: 07/24/2013 12:08:08 ******/
ALTER TABLE [dbo].[SubPerms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[ModuleRegisters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubPerms] CHECK CONSTRAINT [FK_dbo.SubPerms_dbo.ModuleRegisters_Parent_Id]
GO
