/****** Object:  Table [dbo].[InvoiceRowDayDatas]    Script Date: 07/16/2013 15:35:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InvoiceRowDayDatas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime2](7) NOT NULL,
	[Count] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[InvoiceRow_Id] [int] NOT NULL,
 CONSTRAINT [PK_InvoiceRowDayData] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceRowDayDatas]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceRowDayDatas_Invoices] FOREIGN KEY([InvoiceRow_Id])
REFERENCES [dbo].[InvoiceRows] ([Id])
GO

ALTER TABLE [dbo].[InvoiceRowDayDatas] CHECK CONSTRAINT [FK_InvoiceRowDayDatas_Invoices]
GO


