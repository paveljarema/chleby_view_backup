/*
   18 lipca 201310:56:00
   User: 
   Server: LAPTOP7
   Database: chlebydwa
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InvoiceRowDayDatas
	DROP CONSTRAINT FK_InvoiceRowDayDatas_Invoices
GO
ALTER TABLE dbo.InvoiceRows SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InvoiceRowDayDatas
	(
	Id int NOT NULL IDENTITY (1, 1),
	Date datetime2(7) NOT NULL,
	Count decimal(7, 2) NOT NULL,
	Price money NOT NULL,
	Vat money NOT NULL,
	InvoiceRow_Id int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_InvoiceRowDayDatas SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceRowDayDatas ON
GO
IF EXISTS(SELECT * FROM dbo.InvoiceRowDayDatas)
	 EXEC('INSERT INTO dbo.Tmp_InvoiceRowDayDatas (Id, Date, Count, Price, Vat, InvoiceRow_Id)
		SELECT Id, Date, CONVERT(decimal(7, 2), Count), Price, Vat, InvoiceRow_Id FROM dbo.InvoiceRowDayDatas WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_InvoiceRowDayDatas OFF
GO
DROP TABLE dbo.InvoiceRowDayDatas
GO
EXECUTE sp_rename N'dbo.Tmp_InvoiceRowDayDatas', N'InvoiceRowDayDatas', 'OBJECT' 
GO
ALTER TABLE dbo.InvoiceRowDayDatas ADD CONSTRAINT
	PK_InvoiceRowDayData PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.InvoiceRowDayDatas ADD CONSTRAINT
	FK_InvoiceRowDayDatas_Invoices FOREIGN KEY
	(
	InvoiceRow_Id
	) REFERENCES dbo.InvoiceRows
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
