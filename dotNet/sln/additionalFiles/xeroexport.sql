USE [chlebydwa]
GO

/****** Object:  Table [dbo].[XeroTaxMappings]    Script Date: 07/19/2013 16:39:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[XeroTaxMappings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tax] [decimal](7, 2) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_XeroTaxMappings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


ALTER TABLE [dbo].[Businesses] ADD [XeroConfig_SalesAccount] INT NOT NULL DEFAULT 0
ALTER TABLE [dbo].[Businesses] ADD [XeroConfig_PurchaseAccount] INT NOT NULL DEFAULT 0