﻿using chleby.Web;
using chleby.Web.Models;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Castle.Facilities.TypedFactory;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using chleby.Web.Other;

namespace SnapshotRunner
{
    public class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        private static IWindsorContainer _container;


        static void Main(string[] args)
        {
            try
            {
                _container = new WindsorContainer();
                _container.AddFacility<TypedFactoryFacility>();
                _container.AddFacility<LoggingFacility>(l => l.UseLog4Net("log4net.config"));

                _container.Register(Component.For<ISnapshotLogger>().ImplementedBy<SnapshotLogger>());

                log4net.Config.XmlConfigurator.Configure();
                SnapshotLogger snapshotLogger = new SnapshotLogger();
                ILog snpLog = LogManager.GetLogger("SnapshoterLogger");
                snapshotLogger.Logger = snpLog;

                var chleby = new MainAppContext();
                var registeredApps = chleby.AppRegister.ToList();
                foreach (AppRegister app in registeredApps)
                {
                    _log.InfoFormat("Checking {0}", app.Name);
                    var db = new ChlebyContext(app);
                    Business currentClient = null;
                    AppSnapshots lastSnapshot = null;
                    try
                    {
                        currentClient = db.Businesses.First();
                        lastSnapshot = chleby.AppSnapshots.Where(s => s.AppRegisterId == app.Id).FirstOrDefault();
                        if (lastSnapshot == null)
                        {
                            lastSnapshot = new AppSnapshots
                            {
                                AppRegisterId = app.Id,
                                LastSnapshotDate = DateTime.MinValue,
                                Deadline = currentClient.CraftingDeadline
                            };
                            chleby.AppSnapshots.Add(lastSnapshot);
                        }
                    }
                    catch (Exception exc)
                    {
                        _log.Error("Error getting last snapshot for " + app.Name + " :", exc);
                        continue;
                    }


                    var utc = DateTime.UtcNow;

                    _log.InfoFormat("UTC now is: {0}.", utc.ToString());
                    //create deadline for current day
                    var deadlineDay = new DateTime(utc.Year, utc.Month, utc.Day);
                    deadlineDay = deadlineDay.AddHours(currentClient.CraftingDeadline.Hour);
                    deadlineDay = deadlineDay.AddMinutes(currentClient.CraftingDeadline.Minute);




                    _log.InfoFormat("Comparing {0} last to {1} today order deadline.", lastSnapshot.LastSnapshotDate.ToString(), deadlineDay.ToString());

                    if (lastSnapshot.LastSnapshotDate >= deadlineDay)
                    {
                        _log.InfoFormat("snapshot for {0} already done at {1}", app.Name, lastSnapshot.LastSnapshotDate);
                    }
                    else if (utc.Day == lastSnapshot.LastSnapshotDate.Day && Math.Abs((utc - deadlineDay).Hours) <= 24)
                    {
                        _log.InfoFormat("snapshot for {0} already done today {1}", app.Name, lastSnapshot.LastSnapshotDate);
                    }
                    else if (utc >= deadlineDay)
                    {
                        _log.InfoFormat("doing snapshot for {0} at {1}", app.Name, utc);
                        try
                        {
                            //get delivery VAT (PriceData_DeliveryVat)
                            Settings vat = db.Settings.Where(s => string.Compare(s.Name, "PriceData_DeliveryVat", true) == 0).FirstOrDefault();
                            if (vat == null)
                            {
                                _log.Error(string.Format("Can't find VAT for {0}", app.Name));
                                continue;
                            }
                            var parameterProvider = new TaskSnapshotParameterProvider(lastSnapshot, vat);
                            var snapshoter = new Snapshoter(db, new TaskSnapshotParameterProvider(lastSnapshot, vat));
                            snapshoter.SnapshotLogger = snapshotLogger;
                            snapshoter.Snapshot(utc);
                            lastSnapshot.LastSnapshotDate = utc;
                            currentClient.LastSnapshotDate = utc;
                            db.SaveChanges();
                            if (parameterProvider.Mode != SnapshoterMode.Test)
                            {
                                chleby.SaveChanges();
                            }
                        }
                        catch (Exception e)
                        {
                            _log.Error("error while doing snapshot", e);
                        }
                    }
                    else
                    {
                        _log.InfoFormat("It is no time for {0} snapshot. Deadline is set to {1}", app.Name, deadlineDay.ToString());
                    }
                }

                
            }
            catch (Exception exc)
            {
                if (exc is System.Reflection.ReflectionTypeLoadException)
                {
                    Exception[] loaderExc = ((System.Reflection.ReflectionTypeLoadException)exc).LoaderExceptions;
                    if (loaderExc != null)
                    {
                        foreach (Exception e in loaderExc)
                        {
                            _log.Error("Loader exception!", e);
                        }
                    }
                }

                _log.Error("Root exception!", exc);
                Exception inner = exc.InnerException;
                while (inner != null)
                {
                    _log.Error("Root inner exception!", inner);
                    inner = inner.InnerException;
                }
            }
        }
    }

}
