﻿using chleby.Web;
using chleby.Web.Models;
using chleby.Web.Other;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SnapshotRunner
{
    public class TaskSnapshotParameterProvider : ISnapshotParameterProvider
    {

        private AppSnapshots _lastSnapshot;
        private Settings _settings;

        public TaskSnapshotParameterProvider(AppSnapshots lastSnapshot, Settings settings)
        {
            _lastSnapshot = lastSnapshot;
            _settings = settings;
        }

        public DateTime LastSnapshotDate
        {
            get
            {
                return _lastSnapshot.LastSnapshotDate;
            }
            set
            {
                _lastSnapshot.LastSnapshotDate = value;
            }
        }

        public decimal DeliveryVat
        {
            get { return decimal.Parse(_settings.Value); }
        }

        public SnapshoterMode Mode
        {
            get
            {
                if (string.Compare(ConfigurationManager.AppSettings["SnapshoterMode"], "Test", true) == 0)
                {
                    return SnapshoterMode.Test;
                }
                return SnapshoterMode.Production;
            }
        }


        public DateTime CurrentDate
        {
            get { return DateTime.UtcNow; }
        }
    }
}
